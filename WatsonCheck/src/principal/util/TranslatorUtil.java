package principal.util;

import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.language_translator.v3.LanguageTranslator;
import com.ibm.watson.language_translator.v3.model.TranslateOptions;
import com.ibm.watson.language_translator.v3.model.TranslationResult;

public class TranslatorUtil {
	//
	private String idiomaOriginal;
	private String idiomaTraducido;
	private String textoOriginal;
	public TranslatorUtil() {super();}
	
	public TranslatorUtil(String idiomaOriginal, String idiomaTraducido, String textoOriginal) {
		super();
		this.idiomaOriginal = idiomaOriginal;
		this.idiomaTraducido = idiomaTraducido;
		this.textoOriginal = textoOriginal;
	}

	public String getIdiomaOriginal() {		return idiomaOriginal;}
	public void setIdiomaOriginal(String idiomaOriginal) {		this.idiomaOriginal = idiomaOriginal;	}
	
	public String getIdiomaTraducido() {		return idiomaTraducido;	}
	public void setIdiomaTraducido(String idiomaTraducido) {		this.idiomaTraducido = idiomaTraducido;	}

	public String getTextoOriginal() {		return textoOriginal;	}
	public void setTextoOriginal(String textoOriginal) {		this.textoOriginal = textoOriginal;	}
	
	public String traducir(){
		   Authenticator authenticator = new IamAuthenticator("k9u-JBSZzx-ds0CdoZKS7yrRX1juig5MQsaK98663ogm");
		    LanguageTranslator service = new LanguageTranslator("2018-05-01", authenticator);
		    StringBuffer sbOriDest= new StringBuffer(idiomaOriginal);
		    sbOriDest.append("-");
		    sbOriDest.append(idiomaTraducido);
		    
		    TranslateOptions translateOptions = new TranslateOptions.Builder()
		        .addText(textoOriginal)
		        .modelId(sbOriDest.toString())
		        .build();
		    TranslationResult translationResult = service.translate(translateOptions).execute().getResult();		
		return translationResult.toString();
	}
	
	
	
	

}
