package principal.pantalla;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PruebaIdioma2 {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PruebaIdioma2 window = new PruebaIdioma2();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PruebaIdioma2() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//TODO Jonothan pone tu magia
		frame = new JFrame();
		frame.setBounds(100, 100, 523, 340);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTraductor = new JLabel("Traductor");
		lblTraductor.setFont(new Font("Times New Roman", Font.ITALIC, 14));
		lblTraductor.setBounds(215, 0, 83, 25);
		frame.getContentPane().add(lblTraductor);
		
		textField = new JTextField();
		textField.setBounds(10, 77, 190, 213);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(308, 77, 181, 213);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(10, 46, 190, 20);
		frame.getContentPane().add(comboBox);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.setBounds(308, 46, 181, 20);
		frame.getContentPane().add(comboBox_1);
		
		JLabel lblIdiomaDeEntrada = new JLabel("Idioma de entrada:");
		lblIdiomaDeEntrada.setBounds(10, 21, 146, 14);
		frame.getContentPane().add(lblIdiomaDeEntrada);
		
		JLabel lblIdiomaDeSalida = new JLabel("Idioma de salida:");
		lblIdiomaDeSalida.setBounds(308, 21, 181, 14);
		frame.getContentPane().add(lblIdiomaDeSalida);
		
		JButton btnTraducir = new JButton("Traducir");
		btnTraducir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnTraducir.setBounds(210, 151, 89, 23);
		frame.getContentPane().add(btnTraducir);
	}
}
