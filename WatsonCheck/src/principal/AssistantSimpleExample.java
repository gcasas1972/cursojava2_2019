package principal;
/*
* Ejemplo 1: configura el derivador de servicio, env�a el mensaje inicial y
* recibe respuesta.
*/

import java.util.logging.LogManager;

import com.ibm.cloud.sdk.core.security.Authenticator;
import com.ibm.cloud.sdk.core.security.IamAuthenticator;
import com.ibm.watson.assistant.v2.Assistant;
import com.ibm.watson.assistant.v2.model.CreateSessionOptions;
import com.ibm.watson.assistant.v2.model.DeleteSessionOptions;
import com.ibm.watson.assistant.v2.model.MessageOptions;
import com.ibm.watson.assistant.v2.model.MessageResponse;
import com.ibm.watson.assistant.v2.model.SessionResponse;


//import com.ibm.cloud.sdk.core.service.security.IamOptions;

public class AssistantSimpleExample {
 public static void main(String[] args) {

   // Suprimir mensajes de registro en stdout.
   LogManager.getLogManager().reset();

   // Configurar servicio de asistente.
   //IamOptions iamOptions = new IamOptions.Builder().apiKey("{apikey}").build();
   
   //Authenticator authenticator = new IamAuthenticator("LJLrGdwt6-VgBuBsNLu1jrV1s6HOk-LUueB_ZyUghfQM");//Alina
   Authenticator authenticator = new IamAuthenticator("bvRZvMMIbKc9WDFLXLcTlDUMZLp3RIx-oB_0-MbQ_uDG"); //Aylen Labarque
   Assistant service = new Assistant("2019-02-28", authenticator);
   String assistantId = "960d7c4b-92e9-4ebb-9eeb-94ea104a2b6f"; // es el workSpace
   
   //skill id
   //String assistantId = "a11c78bf-0ece-4556-b684-2906fc5d8c19"; // sustituir por ID de asistente
   
   //iam_serviceid_crn
  // String assistantId = "crn:v1:bluemix:public:iam-identity::a/24ef74ca39d24d56a8040a7878596723::serviceid:ServiceId-579c58da-b769-46c8-82e6-9db997849be8"; // sustituir por ID de asistente
   
   
   // Crear sesi�n.
   CreateSessionOptions createSessionOptions = new CreateSessionOptions.Builder(assistantId).build();
   SessionResponse session = service.createSession(createSessionOptions).execute().getResult();
   String sessionId = session.getSessionId();

   // Iniciar conversaci�n con mensaje vac�o.
   MessageOptions messageOptions = new MessageOptions.Builder(assistantId,
                                                       sessionId).build();
   MessageResponse response = service.message(messageOptions).execute().getResult();

   // Imprimir la salida del di�logo, si la hay. Se supone que solo hay una respuesta de texto.
   System.out.println(response.getOutput().getGeneric().get(0).messageToHumanAgent());

   // Hemos terminado, as� que suprimimos la sesi�n.
   DeleteSessionOptions deleteSessionOptions = new DeleteSessionOptions.Builder(assistantId, sessionId).build();
   service.deleteSession(deleteSessionOptions).execute();
 }
}