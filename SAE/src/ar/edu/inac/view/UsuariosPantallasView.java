package ar.edu.inac.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JLabel;
import java.awt.Label;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class UsuariosPantallasView {

	private JFrame frame;
	private JTextField textField;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UsuariosPantallasView window = new UsuariosPantallasView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UsuariosPantallasView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textField = new JTextField();
		textField.setBounds(20, 96, 217, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblUsuariosperfiles = new JLabel("USUARIOS-PERFILES");
		lblUsuariosperfiles.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lblUsuariosperfiles.setBounds(119, 11, 190, 50);
		frame.getContentPane().add(lblUsuariosperfiles);
		
		JLabel lblIngresarUsuario = new JLabel("ingresar usuario:");
		lblIngresarUsuario.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblIngresarUsuario.setBounds(10, 53, 200, 50);
		frame.getContentPane().add(lblIngresarUsuario);
		
		JButton btnBuscar = new JButton("BUSCAR");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnBuscar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnBuscar.setBounds(277, 126, 89, 23);
		frame.getContentPane().add(btnBuscar);
		
		JButton btnAgregar = new JButton("AGREGAR");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAgregar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnAgregar.setBounds(277, 94, 109, 21);
		frame.getContentPane().add(btnAgregar);
		
		JButton btnModificar = new JButton("MODIFICAR");
		btnModificar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnModificar.setBounds(277, 199, 121, 20);
		frame.getContentPane().add(btnModificar);
		
		JButton btnBorrar = new JButton("BORRAR");
		btnBorrar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnBorrar.setBounds(277, 160, 109, 23);
		frame.getContentPane().add(btnBorrar);
	}
}
