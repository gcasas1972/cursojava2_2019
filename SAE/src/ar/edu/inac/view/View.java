package ar.edu.inac.view;

import java.util.List;

public interface View {
	public void llenarGrilla(List lista);
	public void refresh();
	public void limpiarCampos();

}
