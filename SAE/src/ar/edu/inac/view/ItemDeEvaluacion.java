package ar.edu.inac.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;

public class ItemDeEvaluacion {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	//tutorial grilla https://www.youtube.com/watch?v=GaICabukib0
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ItemDeEvaluacion window = new ItemDeEvaluacion();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ItemDeEvaluacion() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblItemDeEvaluacin = new JLabel("Item de Evaluaci\u00F3n");
		lblItemDeEvaluacin.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblItemDeEvaluacin.setBounds(122, 26, 137, 17);
		frame.getContentPane().add(lblItemDeEvaluacin);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblCodigo.setBounds(32, 66, 46, 14);
		frame.getContentPane().add(lblCodigo);
		
		JLabel lblTexto = new JLabel("Texto");
		lblTexto.setFont(new Font("Tahoma", Font.PLAIN, 12));
		lblTexto.setBounds(32, 93, 46, 14);
		frame.getContentPane().add(lblTexto);
		
		textField = new JTextField();
		textField.setBounds(73, 64, 143, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(73, 91, 143, 55);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnBorrar.setBounds(32, 181, 89, 23);
		frame.getContentPane().add(btnBorrar);
	}
}
