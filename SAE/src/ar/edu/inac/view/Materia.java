package ar.edu.inac.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Materia {

	private JFrame frame;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Materia window = new Materia();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Materia() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 495, 370);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblMateria = new JLabel("Materia");
		lblMateria.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblMateria.setBounds(191, 0, 112, 43);
		frame.getContentPane().add(lblMateria);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(61, 57, 46, 14);
		frame.getContentPane().add(lblCodigo);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(61, 88, 46, 14);
		frame.getContentPane().add(lblNombre);
		
		JLabel lblTemario = new JLabel("Temario");
		lblTemario.setBounds(61, 113, 71, 26);
		frame.getContentPane().add(lblTemario);
		
		JLabel lblModulo = new JLabel("Modulo");
		lblModulo.setBounds(61, 150, 46, 14);
		frame.getContentPane().add(lblModulo);
		
		textField = new JTextField();
		textField.setBounds(117, 54, 132, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(117, 85, 132, 20);
		frame.getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		textField_2 = new JTextField();
		textField_2.setBounds(117, 116, 132, 20);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setBounds(117, 147, 132, 20);
		frame.getContentPane().add(comboBox);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(270, 48, 89, 35);
		frame.getContentPane().add(btnAgregar);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(270, 94, 89, 35);
		frame.getContentPane().add(btnBorrar);
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.setBounds(369, 48, 89, 35);
		frame.getContentPane().add(btnModificar);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(369, 94, 89, 35);
		frame.getContentPane().add(btnBuscar);
		
		JButton btnVerDetalle = new JButton("Ver Detalle");
		btnVerDetalle.setBounds(270, 140, 188, 37);
		frame.getContentPane().add(btnVerDetalle);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(117, 206, 341, 114);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
				{null, null, null},
			},
			new String[] {
				"Codigo", "Nombre", "Temario"
			}
		));
		scrollPane.setViewportView(table);
	}
}
