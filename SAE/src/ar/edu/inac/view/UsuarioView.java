package ar.edu.inac.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import javax.swing.BoxLayout;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.ListSelectionModel;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Toolkit;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPanel;
import java.awt.Font;
import java.awt.Color;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import org.eclipse.wb.swing.FocusTraversalOnArray;

import com.mysql.jdbc.Statement;

import ar.edu.inac.controller.Controller;
import ar.edu.inac.controller.CursoController;
import ar.edu.inac.controller.UsuarioController;
import ar.edu.inac.dao.CursoDao;
import ar.edu.inac.dao.DAO;
import ar.edu.inac.dao.UsuariosDAO;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Usuario;
import ar.edu.inac.modelo.exception.ModeloException;

import java.awt.Component;
import java.sql.SQLException;
import java.util.List;
import javax.swing.JProgressBar;

public class UsuarioView implements View{

	private JFrame frmCreadorDeUsuarios;
	private JTable tabla_usuarios_per;
	private JTextField txt_nom;
	private JLabel lblPerfil;
	private JTextField txt_pas;
	DefaultTableModel dtm = new DefaultTableModel();
	private JLabel lblPassword;
	private JLabel lblCodigo;
	private JTextField txt_cod;
	private JButton btnBuscar;
	private JButton btnModificar;
	private JButton btnEliminar;
	private JButton limpiar_Campos;

	// controller
	Controller usuControler = new UsuarioController();
	// dao
	DAO usuDao  = new UsuariosDAO();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UsuarioView window = new UsuarioView();
					window.frmCreadorDeUsuarios.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public UsuarioView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCreadorDeUsuarios = new JFrame();
		frmCreadorDeUsuarios.getContentPane().setBackground(Color.WHITE);
		frmCreadorDeUsuarios.setIconImage(Toolkit.getDefaultToolkit().getImage(UsuarioView.class.getResource("/imagenes/icono-utn.png")));
		frmCreadorDeUsuarios.setTitle("Generador de usuarios");
		frmCreadorDeUsuarios.setBounds(100, 100, 426, 372);
		frmCreadorDeUsuarios.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	
		JButton btn_buscar_per = new JButton("Asociar perfil");
		btn_buscar_per.setBounds(43, 119, 140, 25);
		btn_buscar_per.setBackground(Color.WHITE);
		btn_buscar_per.setFont(new Font("Segoe UI Semibold", Font.BOLD, 12));
		btn_buscar_per.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(43,155,324,87);
		frmCreadorDeUsuarios.getContentPane().add(scrollPane);
		refresh();
		tabla_usuarios_per = new JTable();
		tabla_usuarios_per.setBackground(Color.LIGHT_GRAY);
		tabla_usuarios_per.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		tabla_usuarios_per.setColumnSelectionAllowed(true);
		tabla_usuarios_per.setCellSelectionEnabled(true);
		tabla_usuarios_per.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tabla_usuarios_per.setRowSelectionAllowed(true);
		tabla_usuarios_per.setModel(dtm);
		
		final ListSelectionModel selectModel = tabla_usuarios_per.getSelectionModel();
		scrollPane.setViewportView(tabla_usuarios_per);
		
			selectModel.addListSelectionListener( new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				
				if(!selectModel.isSelectionEmpty()){
					int irow = selectModel.getMinSelectionIndex();
					txt_cod.setText((String)tabla_usuarios_per.getValueAt(irow, 0));
					txt_nom.setText((String)tabla_usuarios_per.getValueAt(irow, 1));
					txt_pas.setText((String)tabla_usuarios_per.getValueAt(irow, 2));
					//JOptionPane.showMessageDialog(null, "fila seleccionada=" + irow);
				}
			}
			});
		
		JButton btnCrearUsuario = new JButton("Crear usuario");
		btnCrearUsuario.setBounds(230, 254, 116, 25);
		btnCrearUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
					Usuario usu = new Usuario( //txt_cod.getText().isEmpty()?0:Integer.parseInt(txt_cod.getText())	,
							txt_nom.getText(), txt_pas.getText() );
					try {
						usuControler.agregarController(usu, usuDao);
						refresh();
						limpiarCampos();
					} catch (ClassNotFoundException e1) {
						JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
						e1.printStackTrace();
					} catch (SQLException e1) {
						JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
						e1.printStackTrace();
					} catch (ModeloException e2) {
						StringBuffer sbError = new StringBuffer(e2.getMessage() );
						sbError.append("Para solucionar el problema \n\n");
						sbError.append(e2.getSolucion());
						JOptionPane.showMessageDialog(null, sbError);
						e2.printStackTrace();
					}
					
				
			}
		});
		btnCrearUsuario.setBackground(Color.WHITE);
		btnCrearUsuario.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		
		txt_nom = new JTextField();
		txt_nom.setBounds(114, 51, 232, 23);
		txt_nom.setColumns(10);
		
		lblPerfil = new JLabel("Nombre :");
		lblPerfil.setBounds(43, 51, 67, 23);
		lblPerfil.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		
		txt_pas = new JTextField();
		txt_pas.setBounds(114, 85, 232, 23);
		txt_pas.setColumns(10);
		
		lblPassword = new JLabel("Password :");
		lblPassword.setBounds(43, 85, 67, 23);
		lblPassword.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		
		lblCodigo = new JLabel("Codigo :");
		lblCodigo.setBounds(43, 20, 67, 23);
		lblCodigo.setFont(new Font("Segoe UI Semibold", Font.PLAIN, 12));
		
		txt_cod = new JTextField();
		txt_cod.setEditable(false);
		txt_cod.setBounds(114, 22, 106, 23);
		txt_cod.setColumns(10);
		
		btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int codigo=0;
				if (txt_cod.getText()!=null && !txt_cod.getText().isEmpty())
					codigo=Integer.parseInt(txt_cod.getText());
				
				Usuario usu;
					//if ( txt_cod.getText()!=null && !txt_cod.getText().isEmpty());
					usu = new Usuario(codigo, txt_nom.getText());
				try {
					llenarGrilla(usuControler.leer(usu, usuDao));
					//refresh();
					
				} catch (Exception e2) {
					e2.printStackTrace();
				}
				
			}
		});
		btnBuscar.setBounds(238, 20, 108, 25);
		btnBuscar.setFont(new Font("Segoe UI Semibold", Font.BOLD, 12));
		btnBuscar.setBackground(Color.WHITE);
		
		btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usu;
				usu = new Usuario (Integer.parseInt(txt_cod.getText()), txt_nom.getText(), txt_pas.getText());
				
				try {
					usuControler.modificarController(usu, usuDao);
					refresh();
					
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
					e1.printStackTrace();
				} catch (ModeloException e2) {
					StringBuffer sbError = new StringBuffer(e2.getMessage() );
					sbError.append("Para solucionar el problema \n\n");
					sbError.append(e2.getSolucion());
					JOptionPane.showMessageDialog(null, sbError);
					e2.printStackTrace();
				}
				
			}
		});
		btnModificar.setBounds(43, 254, 140, 25);
		btnModificar.setFont(new Font("Segoe UI Semibold", Font.BOLD, 12));
		btnModificar.setBackground(Color.WHITE);
		
		btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Usuario usu;
				int codigo = 0;
				if (txt_cod.getText()!=null && !txt_cod.getText().isEmpty())
					codigo=Integer.parseInt(txt_cod.getText());
				usu = new Usuario (codigo);
				
				try {
					usuControler.eliminarController(usu, usuDao);
					refresh();
					limpiarCampos();
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
					e1.printStackTrace();
				} catch (ModeloException e2) {
					StringBuffer sbError = new StringBuffer(e2.getMessage() );
					sbError.append("Para solucionar el problema \n\n");
					sbError.append(e2.getSolucion());
					JOptionPane.showMessageDialog(null, sbError);
					e2.printStackTrace();
				}
			}
		});
		btnEliminar.setBounds(43, 285, 140, 25);
		btnEliminar.setFont(new Font("Segoe UI Semibold", Font.BOLD, 12));
		btnEliminar.setBackground(Color.WHITE);
		frmCreadorDeUsuarios.getContentPane().setLayout(null);
		frmCreadorDeUsuarios.getContentPane().add(scrollPane);
		frmCreadorDeUsuarios.getContentPane().add(btn_buscar_per);
		frmCreadorDeUsuarios.getContentPane().add(btnModificar);
		frmCreadorDeUsuarios.getContentPane().add(btnCrearUsuario);
		frmCreadorDeUsuarios.getContentPane().add(btnEliminar);
		frmCreadorDeUsuarios.getContentPane().add(lblPassword);
		frmCreadorDeUsuarios.getContentPane().add(txt_pas);
		frmCreadorDeUsuarios.getContentPane().add(lblPerfil);
		frmCreadorDeUsuarios.getContentPane().add(txt_nom);
		frmCreadorDeUsuarios.getContentPane().add(lblCodigo);
		frmCreadorDeUsuarios.getContentPane().add(txt_cod);
		frmCreadorDeUsuarios.getContentPane().add(btnBuscar);
		
		limpiar_Campos = new JButton("Limpiar campos");
		limpiar_Campos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarCampos();
			}
		});
		limpiar_Campos.setFont(new Font("Segoe UI Semibold", Font.BOLD, 12));
		limpiar_Campos.setBackground(Color.WHITE);
		limpiar_Campos.setBounds(227, 119, 140, 25);
		frmCreadorDeUsuarios.getContentPane().add(limpiar_Campos);
		frmCreadorDeUsuarios.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{txt_cod, txt_nom, txt_pas, btn_buscar_per, btnCrearUsuario, btnModificar, btnBuscar, btnEliminar}));
	}

	@Override
	public void llenarGrilla(List lista) {
	
		List<Usuario> usuarios = lista;
	      String strUsuarios[][];
	        strUsuarios = new String[lista.size()][3];
	        int i = 0;
			for (Usuario usuario : usuarios) {
				 strUsuarios[i][0] = String.valueOf(usuario.getCodigo());
				 strUsuarios[i][1] = String.valueOf(usuario.getNombre());
				 strUsuarios[i][2] = String.valueOf(usuario.getPassword());
	        	i++;
				
			}
			
	        dtm.setDataVector(strUsuarios, new String[] {"Codigo", "Nombre", "Password"	});		
	       
		// TODO Auto-generated method stub
		
	}

	@Override
	public void refresh() {
		// TODO Auto-generated method stub
		UsuariosDAO UsuDao = new UsuariosDAO();
		try {
			llenarGrilla(UsuDao.leer(null));
		} catch (ClassNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	
	}
	
	

	@Override
	

	public void limpiarCampos() {
		// TODO Auto-generated method stub
		txt_cod.setText("");
		txt_pas.setText("");
		txt_nom.setText("");
		txt_cod.requestFocus();
		refresh();
	}
	
	
}


