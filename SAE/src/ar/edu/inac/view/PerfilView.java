package ar.edu.inac.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.JButton;

import ar.edu.inac.controller.Controller;
import ar.edu.inac.controller.CursoController;
import ar.edu.inac.controller.PerfilController;
import ar.edu.inac.dao.CursoDao;
import ar.edu.inac.dao.DAO;
import ar.edu.inac.dao.PerfilDAO;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Perfil;
import ar.edu.inac.modelo.exception.ModeloException;


import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JTextField;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import java.awt.Toolkit;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.sql.SQLException;
import java.util.List;

public class PerfilView implements View {

	JFrame frmPerfiles;
	private JTable tbLista;
	DefaultTableModel dtm = new DefaultTableModel();
	private JTextField txtCodigo;
	private JTextField txtPerfil;
	
	//Controller
	Controller perControler = new PerfilController();
	//Dao
	DAO perDao = new PerfilDAO();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PerfilView window = new PerfilView();
					window.frmPerfiles.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PerfilView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmPerfiles = new JFrame();
		frmPerfiles.setIconImage(Toolkit.getDefaultToolkit().getImage(PerfilView.class.getResource("/imagenes/icono-utn.png")));
		frmPerfiles.getContentPane().setBackground(Color.WHITE);
		frmPerfiles.setTitle("Perfiles");
		frmPerfiles.setBounds(100, 100, 727, 585);
		frmPerfiles.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		JLabel lblCodigo = new JLabel("C\u00F3digo");
		lblCodigo.setBounds(64, 91, 128, 50);
		lblCodigo.setForeground(new Color(0, 0, 0));
		lblCodigo.setHorizontalAlignment(SwingConstants.LEFT);
		lblCodigo.setFont(new Font("Tahoma", Font.BOLD, 30));
		txtCodigo = new JTextField();
		txtCodigo.setBounds(210, 99, 139, 35);
		txtCodigo.setEditable(false);
		txtCodigo.setColumns(10);
		
//		JLabel lblPerfiles = DefaultComponentFactory.getInstance().createTitle("Perfiles");
//		lblPerfiles.setForeground(new Color(0, 0, 0));
//		lblPerfiles.setFont(new Font("Tahoma", Font.BOLD, 40));
//		lblPerfiles.setHorizontalAlignment(SwingConstants.CENTER);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(76, 344, 570, 174);
		refresh();
		tbLista = new JTable();
		tbLista.setColumnSelectionAllowed(true);
		tbLista.setCellSelectionEnabled(true);
		tbLista.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
		tbLista.setRowSelectionAllowed(true);
		tbLista.setModel(dtm);
		final ListSelectionModel selectModel = tbLista.getSelectionModel();
		scrollPane.setViewportView(tbLista);
		
		JLabel lblPerfil = new JLabel("Perfil");
		lblPerfil.setBounds(64, 151, 128, 50);
		lblPerfil.setForeground(new Color(0, 0, 0));
		lblPerfil.setHorizontalAlignment(SwingConstants.LEFT);
		lblPerfil.setFont(new Font("Tahoma", Font.BOLD, 30));
		txtPerfil = new JTextField();
		txtPerfil.setBounds(210, 167, 139, 35);
		txtPerfil.setColumns(10);
		
		JButton btnPantallas = new JButton("Ver Pantallas");
		btnPantallas.setBounds(363, 248, 283, 69);
		btnPantallas.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnPantallas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
          selectModel.addListSelectionListener( new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				
				if(!selectModel.isSelectionEmpty()){
					int irow = selectModel.getMinSelectionIndex();
				txtCodigo.setText((String)tbLista.getValueAt(irow, 0));
				txtPerfil.setText((String)tbLista.getValueAt(irow, 1));
				}
			}
		});
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(395, 0, 127, 69);
		btnAgregar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Debo crear un Perfil
				//Luego debo llamar al controller
				//Debo llamar al metodo refresh
			 Perfil Per = new Perfil(txtCodigo.getText().isEmpty()?0:Integer.parseInt(txtCodigo.getText()), txtPerfil.getText());
			 try {
					perControler.agregarController(Per, perDao);
					refresh();
					limpiarCampos();
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null,"Se produjo un error relacionado con las librer�as " + e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null,"Se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
					e1.printStackTrace();
				} catch (ModeloException e2) {
					StringBuffer sbError = new StringBuffer(e2.getMessage());
					sbError.append("Para solucionar el problema \n\n");
					sbError.append(e2.getSolucion());
					JOptionPane.showMessageDialog(null, sbError);
					e2.printStackTrace();
				}
			}
		});
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(532, 90, 127, 69);
		btnBorrar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Perfil Per;
 			if(	txtCodigo.getText()!=null && !txtCodigo.getText().isEmpty()){
						Per= new Perfil(Integer.parseInt(txtCodigo.getText()), txtPerfil.getText());						
						try {
							perControler.eliminarController(Per, perDao);
							refresh();
							limpiarCampos();
						} catch (ClassNotFoundException e1) {
							JOptionPane.showMessageDialog(null,"Se produjo un error relacionado con las librer�as " + e1.getMessage());
							e1.printStackTrace();
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(null,"Se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
							e1.printStackTrace();
						} catch (ModeloException e2) {
							StringBuffer sbError = new StringBuffer(e2.getMessage() );
							sbError.append("Para solucionar el problema \n\n");
							sbError.append(e2.getSolucion());
							JOptionPane.showMessageDialog(null, sbError);
							e2.printStackTrace();
						}
						
				}
				else
					JOptionPane.showMessageDialog(null,"Debe seleccionar una fila para poder modificar");			}
		});
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(532, 0, 127, 69);
		btnBuscar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Debo crear un Perfil
				//Luego debo llamar al controller
				Perfil Per;
				int codigo = 0;
				if(txtCodigo.getText()!=null && !txtCodigo.getText().isEmpty())
					codigo = Integer.parseInt(txtCodigo.getText());
				if(txtPerfil.getText()!=null && !txtPerfil.getText().isEmpty()){
						Per = new Perfil(codigo, txtPerfil.getText());						
						try {
						    perControler.leer(Per, perDao);
						//	refresh();
							llenarGrilla(perControler.leer(Per, perDao));
							limpiarCampos();
						} catch (ClassNotFoundException e1) {
							JOptionPane.showMessageDialog(null,"Se produjo un error relacionado con las librer�as " + e1.getMessage());
							e1.printStackTrace();
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(null,"Se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
							e1.printStackTrace();
						}
				}
				else
					JOptionPane.showMessageDialog(null,"La descripci�n debe tener valores");
			}
		});
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.setBounds(395, 90, 127, 69);
		btnModificar.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Debo crear un Perfil
				//Luego debo llamar al controller
				//Debo llamar al metodo refresh
				Perfil Per;
				if(	txtCodigo.getText()!=null && !txtCodigo.getText().isEmpty()){
						Per= new Perfil(Integer.parseInt(txtCodigo.getText()),txtPerfil.getText());						
						try {
							perControler.modificarController(Per, perDao);
							refresh();
							limpiarCampos();
						} catch (ClassNotFoundException e1) {
							JOptionPane.showMessageDialog(null,"Se produjo un error relacionado con las librerias " + e1.getMessage());
							e1.printStackTrace();
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(null,"Se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
							e1.printStackTrace();
						} catch (ModeloException e2) {
							StringBuffer sbError = new StringBuffer(e2.getMessage() );
							sbError.append("Para solucionar el problema \n\n");
							sbError.append(e2.getSolucion());
							JOptionPane.showMessageDialog(null, sbError);
							e2.printStackTrace();
						}
				}
			else
				JOptionPane.showMessageDialog(null,"Debe seleccionar una fila para poder modificar");
			}
		});
		
		JLabel lblFondo = new JLabel("");
		lblFondo.setBounds(0, 0, 711, 547);
		lblFondo.setIcon(new ImageIcon(PerfilView.class.getResource("/imagenes/Fondo Perfil.jpg")));
		frmPerfiles.getContentPane().setLayout(null);
		frmPerfiles.getContentPane().add(btnAgregar);
		frmPerfiles.getContentPane().add(btnBuscar);
		frmPerfiles.getContentPane().add(lblPerfil);
		frmPerfiles.getContentPane().add(btnModificar);
		frmPerfiles.getContentPane().add(btnBorrar);
		frmPerfiles.getContentPane().add(btnPantallas);
		frmPerfiles.getContentPane().add(scrollPane);
		frmPerfiles.getContentPane().add(txtPerfil);
		frmPerfiles.getContentPane().add(lblCodigo);
		frmPerfiles.getContentPane().add(txtCodigo);
		frmPerfiles.getContentPane().add(lblFondo);
	}

	@Override
	public void llenarGrilla(List lista) {
		List<Perfil> perfiles = lista;

	       String strPerfiles[][];
	        strPerfiles = new String[lista.size()][2];//La cantidad de columnas
	        int i = 0;
			for (Perfil Per : perfiles) {//Obtiene los perfiles y los guarda en Per
	        	strPerfiles[i][0] = String.valueOf(Per.getCodigo());
	        	strPerfiles[i][1] = Per.getDescripcion();
	        	i++;	
			}
			
	        dtm.setDataVector(strPerfiles, new String[] {"Codigo","Descripcion"});		
	}

	@Override
	public void refresh() {
		// Por ahora para modificar
		PerfilDAO PerDAO = new PerfilDAO();
		try {
			llenarGrilla(PerDAO.leer(null));
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public void limpiarCampos() {
		txtCodigo.setText("");
		txtPerfil.setText("");
	}
}
