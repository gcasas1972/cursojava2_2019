/*
 * Creado por Diego Torres el dia 02/05/2019
 * 
 * */

package ar.edu.inac.view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;

import java.awt.Font;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JScrollPane;
import javax.swing.JCheckBox;

import ar.edu.inac.controller.Controller;
import ar.edu.inac.controller.ComponenteController;
import ar.edu.inac.dao.ComponenteDAO;
import ar.edu.inac.dao.DAO;
import ar.edu.inac.modelo.Componente;
import ar.edu.inac.modelo.exception.ModeloException;

import ar.edu.inac.modelo.validator.ComponenteValidator;
import ar.edu.inac.modelo.validator.Validator;

public class ComponenteView implements View {

	// componentes graficos
	private JFrame frame;
	private JPanel contentPane;
	private JTextField txtCodigo;
	private JTextField txtDescripcion;
	private JTable table;
	DefaultTableModel dtm = new DefaultTableModel();

	// Controller
	Controller compControler = new ComponenteController();
	// dao
	DAO compDao = new ComponenteDAO();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ComponenteView window = new ComponenteView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public ComponenteView() {
		frame = new JFrame();
		frame.setTitle("COMPONENTE ");
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 593, 409);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));		
		frame.setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblCodigo = new JLabel("Codigo:");
		lblCodigo.setBounds(31, 85, 70, 15);
		contentPane.add(lblCodigo);

		JLabel lblDescripcion = new JLabel("Descripcion:");
		lblDescripcion.setBounds(31, 112, 87, 15);
		contentPane.add(lblDescripcion);

		txtCodigo = new JTextField();
		txtCodigo.setEditable(false);
		txtCodigo.setBounds(129, 83, 144, 19);
		contentPane.add(txtCodigo);
		txtCodigo.setColumns(10);

		txtDescripcion = new JTextField();
		txtDescripcion.setBounds(129, 110, 144, 19);
		contentPane.add(txtDescripcion);
		txtDescripcion.setColumns(10);

		JLabel lblComponentes = new JLabel("Componentes");
		lblComponentes.setFont(new Font("Dialog", Font.BOLD, 20));
		lblComponentes.setBounds(215, 12, 209, 29);
		contentPane.add(lblComponentes);

		JButton btnNewButton_3 = new JButton("New button");
		btnNewButton_3.setBounds(307, 154, 260, 49);
		contentPane.add(btnNewButton_3);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(31, 215, 537, 152);
		contentPane.add(scrollPane);
		frame.getContentPane().add(scrollPane);
		refresh();
		table = new JTable();
		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setRowSelectionAllowed(true);
		table.setModel(dtm);
		final ListSelectionModel selectModel = table.getSelectionModel();
		scrollPane.setViewportView(table);

		JCheckBox chckbxVisible = new JCheckBox("Visible");
		chckbxVisible.setBounds(31, 147, 92, 23);
		contentPane.add(chckbxVisible);

		JCheckBox chckbxEditable = new JCheckBox("Editable");
		chckbxEditable.setBounds(121, 147, 110, 23);
		contentPane.add(chckbxEditable);

		// Boton Limpiar

		JButton btnLimpiar = new JButton("Limpiar");

		// *********************************************************
		// se establecen todas las relaciones con los controller y los daos.

		compControler = new ComponenteController();
		compDao = new ComponenteDAO();

		// se establecen todas las atenciones a los eventos

		selectModel.addListSelectionListener(new ListSelectionListener() {

			@Override
			public void valueChanged(ListSelectionEvent e) {

				if (!selectModel.isSelectionEmpty()) {
					int irow = selectModel.getMinSelectionIndex();
					txtCodigo.setText((String) table.getValueAt(irow, 0));
					txtDescripcion.setText((String) table.getValueAt(irow, 1));
					// JOptionPane.showMessageDialog(null, "fila seleccionada="
					// + irow);
				}
			}
		});
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarCampos();
			}
		});
		btnLimpiar.setBounds(78, 178, 117, 25);
		contentPane.add(btnLimpiar);

		// boton Agregar

		// 3- atencion de los eventos

		JButton btnAgregar = new JButton("Agregar");

		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 1- Debo crear un objeto de tipo curso
				// 2- Luego debo llamar al controller
				// 3- Debo llamar al metodo refresh
				Componente comp = new Componente(
						txtCodigo.getText().isEmpty() ? 0 : Integer
								.parseInt(txtCodigo.getText()), txtDescripcion
								.getText());
				try {
					compControler.agregarController(comp, compDao);
					refresh();
					limpiarCampos();
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null,
							"se produjo un error relacionado con las librerias "
									+ e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null,
							"se produjo un error desconocido comuniquese con alquien que sepa "
									+ e1.getMessage());
					e1.printStackTrace();
				} catch (ModeloException e2) {
					StringBuffer sbError = new StringBuffer(e2.getMessage());
					sbError.append("Para solucionar el problema \n\n");
					sbError.append(e2.getSolucion());
					JOptionPane.showMessageDialog(null, sbError);
					e2.printStackTrace();
				}
			}
		});
		btnAgregar.setBounds(307, 80, 117, 25);
		contentPane.add(btnAgregar);

		// Boton Modificar.

		JButton btnModificar = new JButton("Modificar");

		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 1- Debo crear un objeto de tipo Componente
				// 2- Luego debo llamar al controller
				// 3- Debo llamar al metodo refresh
				Componente comp;
				
				comp = new Componente(Integer.parseInt(txtCodigo.getText()),
						txtDescripcion.getText());
				try {
					compControler.modificarController(comp, compDao);
					refresh();
					limpiarCampos();
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null,
							"se produjo un error relacionado con las librerias "
									+ e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null,
							"se produjo un error desconocido comuniquese con alquien que sepa "
									+ e1.getMessage());
					e1.printStackTrace();
				} catch (ModeloException e2) {
					StringBuffer sbError = new StringBuffer(e2.getMessage());
					sbError.append("Para solucionar el problema \n\n");
					sbError.append(e2.getSolucion());
					JOptionPane.showMessageDialog(null, sbError);
					e2.printStackTrace();
				}
			}			
		});
		btnModificar.setBounds(307, 117, 117, 25);
		contentPane.add(btnModificar);

		// Boton Borrar

		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 1- Debo crear un objeto de tipo curso
				// 2- Luego debo llamar al controller
				// 3- Debo llamar al metodo refresh
				Componente comp;
				
				comp = new Componente(Integer.parseInt(txtCodigo.getText()),
						txtDescripcion.getText());
				try {
					compControler.eliminarController(comp, compDao);
					refresh();
					limpiarCampos();
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null,
							"se produjo un error relacionado con las librerias "
									+ e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null,
							"se produjo un error desconocido comuniquese con alquien que sepa "
									+ e1.getMessage());
					e1.printStackTrace();
				} catch (ModeloException e2) {
					StringBuffer sbError = new StringBuffer(e2.getMessage());
					sbError.append("Para solucionar el problema \n\n");
					sbError.append(e2.getSolucion());
					JOptionPane.showMessageDialog(null, sbError);
					e2.printStackTrace();
				}				
			}
		});
		btnBorrar.setBounds(451, 117, 117, 25);
		contentPane.add(btnBorrar);

		// boton Buscar

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 1- Debo crear un objeto de tipo curso
				// 2- Luego debo llamar al controller
				// 3- Debo llamar al metodo refresh
				Componente comp;
				if (txtDescripcion.getText() != null
						&& !txtDescripcion.getText().isEmpty()) {
					comp = new Componente(Integer.parseInt(txtDescripcion
							.getText()));
					try {
						compControler.leer(comp, compDao);
						refresh();
						limpiarCampos();
					} catch (ClassNotFoundException e1) {
						JOptionPane.showMessageDialog(null,
								"se produjo un error relacionado con las librerias "
										+ e1.getMessage());
						e1.printStackTrace();
					} catch (SQLException e1) {
						JOptionPane.showMessageDialog(null,
								"se produjo un error desconocido comuniquese con alquien que sepa "
										+ e1.getMessage());
						e1.printStackTrace();
					}
				} else
					JOptionPane.showMessageDialog(null,
							"El a�o o la descripcion deben tener valores");
			}
		});
		btnBuscar.setBounds(451, 80, 117, 25);
		contentPane.add(btnBuscar);

	}

	
	@Override
	public void llenarGrilla(List lista) {
		List<Componente> componentes = lista;

		String strComponentes[][];
		strComponentes = new String[lista.size()][2];
		int i = 0;
		for (Componente componente : componentes) {
			strComponentes[i][0] = String.valueOf(componente.getCodigo());
			strComponentes[i][1] = componente.getDescripcion();
			i++;

		}

		dtm.setDataVector(strComponentes,
				new String[] { "Codigo", "Descripion" });

	}

	@Override
	public void refresh() {
		// 2- asignacion de valores pre-definidos
		try {
			llenarGrilla(compDao.leer(null));
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	@Override
	public void limpiarCampos() {
		txtCodigo.setText("");
		txtDescripcion.setText("");

	}
}
