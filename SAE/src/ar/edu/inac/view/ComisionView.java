package ar.edu.inac.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;

import java.awt.Toolkit;

public class ComisionView {

	private JFrame frame;
	private JTextField textFieldDivision;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ComisionView window = new ComisionView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ComisionView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(ComisionView.class.getResource("/com/sun/java/swing/plaf/windows/icons/Computer.gif")));
		frame.setBounds(100, 100, 450, 550);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		//Paso 1 creo los encabezados de las columnas
		String[] encabezadosTabla = {"Codigo", "Curso", "Turno","Division"};
		
		//Paso 2
		DefaultTableModel TablaModelo = new DefaultTableModel(encabezadosTabla, 10);
		
		JLabel lblComisin = new JLabel("Comisi\u00F3n");
		lblComisin.setHorizontalAlignment(SwingConstants.CENTER);
		lblComisin.setFont(new Font("Tahoma", Font.BOLD, 18));
		lblComisin.setBounds(10, 11, 414, 30);
		frame.getContentPane().add(lblComisin);
		
		JLabel lblCdigo = new JLabel("C\u00F3digo:");
		lblCdigo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCdigo.setHorizontalAlignment(SwingConstants.LEFT);
		lblCdigo.setBounds(41, 52, 64, 22);
		frame.getContentPane().add(lblCdigo);
		
		JLabel lblCurso = new JLabel("Curso:");
		lblCurso.setHorizontalAlignment(SwingConstants.LEFT);
		lblCurso.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblCurso.setBounds(41, 85, 64, 22);
		frame.getContentPane().add(lblCurso);
		
		JLabel lblTurno = new JLabel("Turno:");
		lblTurno.setHorizontalAlignment(SwingConstants.LEFT);
		lblTurno.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblTurno.setBounds(41, 118, 64, 22);
		frame.getContentPane().add(lblTurno);
		
		JLabel lblDivisin = new JLabel("Divisi\u00F3n:");
		lblDivisin.setHorizontalAlignment(SwingConstants.LEFT);
		lblDivisin.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblDivisin.setBounds(41, 151, 64, 22);
		frame.getContentPane().add(lblDivisin);
		
		textFieldDivision = new JTextField();
		textFieldDivision.setToolTipText("Ingrese la divisi\u00F3n");
		textFieldDivision.setBounds(115, 154, 240, 20);
		frame.getContentPane().add(textFieldDivision);
		textFieldDivision.setColumns(10);
		
		JComboBox comboBoxTurno = new JComboBox();
		comboBoxTurno.setToolTipText("Listado de turnos");
		comboBoxTurno.setBounds(115, 121, 240, 20);
		frame.getContentPane().add(comboBoxTurno);
		
		JComboBox comboBoxCurso = new JComboBox();
		comboBoxCurso.setToolTipText("Listado de cursos");
		comboBoxCurso.setBounds(115, 88, 240, 20);
		frame.getContentPane().add(comboBoxCurso);
		
		JLabel lblIntCodigo = new JLabel("CODIGO");
		lblIntCodigo.setHorizontalAlignment(SwingConstants.LEFT);
		lblIntCodigo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblIntCodigo.setBounds(115, 53, 240, 20);
		frame.getContentPane().add(lblIntCodigo);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.setToolTipText("Haga click para agregar una comisi\u00F3n");
		btnAgregar.setIcon(new ImageIcon(ComisionView.class.getResource("/com/sun/java/swing/plaf/windows/icons/File.gif")));
		btnAgregar.setBounds(30, 199, 159, 30);
		frame.getContentPane().add(btnAgregar);
		
		JButton btnModificiar = new JButton("Modificar");
		btnModificiar.setToolTipText("Haga click para modificar una comisi\u00F3n");
		btnModificiar.setBounds(30, 240, 159, 30);
		frame.getContentPane().add(btnModificiar);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setToolTipText("Haga click para buscar una comisi\u00F3n");
		btnBuscar.setBounds(238, 240, 159, 30);
		frame.getContentPane().add(btnBuscar);
		
		JButton btnEliminar = new JButton("Eliminar");
		btnEliminar.setToolTipText("Haga click para eliminar una comisi\u00F3n");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnEliminar.setIcon(new ImageIcon(ComisionView.class.getResource("/com/sun/java/swing/plaf/windows/icons/image-failed.png")));
		btnEliminar.setSelectedIcon(new ImageIcon(ComisionView.class.getResource("/com/sun/java/swing/plaf/windows/icons/Error.gif")));
		btnEliminar.setBounds(238, 199, 159, 30);
		frame.getContentPane().add(btnEliminar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(30, 294, 367, 206);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{"", null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
				{null, null, null, null},
			},
			new String[] {
				"codigo", "curso", "turno", "division"
			}
		));
		scrollPane.setViewportView(table);
	}
}
