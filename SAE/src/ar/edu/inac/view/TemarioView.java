package ar.edu.inac.view;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ar.edu.inac.controller.Controller;
import ar.edu.inac.controller.CursoController;
import ar.edu.inac.controller.TemarioController;
import ar.edu.inac.dao.CursoDao;
import ar.edu.inac.dao.DAO;
import ar.edu.inac.dao.TemarioDAO;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Temario;
import ar.edu.inac.modelo.exception.ModeloException;

public class TemarioView implements View {
	//componentes graficos
	private JFrame frame;
	private JTextField txtCodigo;
	private JTable table;
	DefaultTableModel dtm = new DefaultTableModel();
	private JTextField txtDescripcion;
	
	// controller
	Controller temControler = new TemarioController();
	// dao
	DAO temDAO  = new TemarioDAO();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TemarioView window = new TemarioView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public TemarioView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//1- Establecer todos los componentes gráficos
		
		frame = new JFrame();
		frame.setBounds(100, 100, 455, 355);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTemarios = new JLabel("Temarios");
		lblTemarios.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblTemarios.setBounds(154, 11, 130, 44);
		frame.getContentPane().add(lblTemarios);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(26, 67, 46, 14);
		frame.getContentPane().add(lblCodigo);

		txtCodigo = new JTextField();
		txtCodigo.setEditable(false);
		txtCodigo.setBounds(92, 63, 111, 22);
		frame.getContentPane().add(txtCodigo);
		txtCodigo.setColumns(10);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(213, 63, 89, 37);
		frame.getContentPane().add(btnAgregar);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(312, 110, 89, 37);
		frame.getContentPane().add(btnBorrar);
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.setBounds(213, 110, 89, 37);
		frame.getContentPane().add(btnModificar);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(311, 63, 89, 37);
		frame.getContentPane().add(btnBuscar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(77, 206, 324, 99);
		frame.getContentPane().add(scrollPane);
		refresh();
		table = new JTable();
		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
		table.setRowSelectionAllowed(true);
		table.setModel(dtm);
		final ListSelectionModel selectModel = table.getSelectionModel();
		scrollPane.setViewportView(table);
		
		JButton btnVerDetalles = new JButton("Ver Materias");
		btnVerDetalles.setBounds(213, 158, 188, 37);
		frame.getContentPane().add(btnVerDetalles);
		
		JLabel lblDescripcionTitulo = new JLabel("Descripcion");
		lblDescripcionTitulo.setBounds(26, 129, 56, 14);
		frame.getContentPane().add(lblDescripcionTitulo);
		
		txtDescripcion = new JTextField();
		txtDescripcion.setColumns(10);
		txtDescripcion.setBounds(92, 125, 111, 22);
		frame.getContentPane().add(txtDescripcion);
		
		JButton btnLimpiar = new JButton("Limpiar");
		//*********************************************************
		//2- se etablecen todas las relaciones con los controller y los daos
		temControler = new TemarioController();
		temDAO = new TemarioDAO();
		
		// se establecen todas las atenciones a los eventos
		selectModel.addListSelectionListener( new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				
				if(!selectModel.isSelectionEmpty()){
					int irow = selectModel.getMinSelectionIndex();
					txtCodigo.setText((String)table.getValueAt(irow, 0));
					txtDescripcion.setText((String)table.getValueAt(irow, 1));
					//JOptionPane.showMessageDialog(null, "fila seleccionada=" + irow);
				}
			}
		});
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarCampos();
			}
		});
		btnLimpiar.setBounds(78, 165, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		
		
		//3- atencion  de los eventos
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- Debo crear un objeto de tipo temario
				//2- Luego debo llamar al controller
				//3- Debo llamar al metodo refresh
				Temario tem;
				if(		txtDescripcion.getText()!=null 	&& !txtDescripcion.getText().isEmpty()){
						tem = new Temario(txtDescripcion.getText());						
						try {
							temControler.agregarController(tem, temDAO);
							refresh();
							limpiarCampos();
						} catch (ClassNotFoundException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
							e1.printStackTrace();
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
							e1.printStackTrace();
						} catch (ModeloException e2) {
							StringBuffer sbError = new StringBuffer(e2.getMessage() );
							sbError.append("Para solucionar el problema \n\n");
							sbError.append(e2.getSolucion());
							JOptionPane.showMessageDialog(null, sbError);
							e2.printStackTrace();
						}
				}
				else
					JOptionPane.showMessageDialog(null,"La descripción debe tener un valor");
			}
		});
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- Debo crear un objeto de tipo temario
				//2- Luego debo llamar al controller
				//3- Debo llamar al metodo refresh
				Temario tem;
				if(	txtCodigo.getText()!=null && !txtCodigo.getText().isEmpty()){
						tem = new Temario(Integer.parseInt(txtCodigo.getText()), txtDescripcion.getText());						
						try {
							temControler.modificarController(tem, temDAO);
							refresh();
							limpiarCampos();
						} catch (ClassNotFoundException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
							e1.printStackTrace();
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
							e1.printStackTrace();
						} catch (ModeloException e2) {
							StringBuffer sbError = new StringBuffer(e2.getMessage() );
							sbError.append("Para solucionar el problema \n\n");
							sbError.append(e2.getSolucion());
							JOptionPane.showMessageDialog(null, sbError);
							e2.printStackTrace();
							
						}
				}
				else
					JOptionPane.showMessageDialog(null,"Debe seleccionar una fila para poder modificar");
			}
		});
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- Debo crear un objeto de tipo tem
				//2- Luego debo llamar al controller
				//3- Debo llamar al metodo refresh
				Temario tem;
				if(	txtCodigo.getText()!=null && !txtCodigo.getText().isEmpty()){
						tem = new Temario(Integer.parseInt(txtCodigo.getText()), txtDescripcion.getText());						
						try {
							temControler.eliminarController(tem, temDAO);
							refresh();
							limpiarCampos();
						} catch (ClassNotFoundException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
							e1.printStackTrace();
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
							e1.printStackTrace();
						} catch (ModeloException e2) {
							StringBuffer sbError = new StringBuffer(e2.getMessage() );
							sbError.append("Para solucionar el problema \n\n");
							sbError.append(e2.getSolucion());
							JOptionPane.showMessageDialog(null, sbError);
							e2.printStackTrace();
						}
				}
				else
					JOptionPane.showMessageDialog(null,"Debe seleccionar una fila para poder modificar");
			}
		});
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- Debo crear un objeto de tipo curso
				//2- Luego debo llamar al controller
				//3- Debo llamar al metodo refresh
				Temario tem;
				if(		txtDescripcion.getText()!=null 	&& !txtDescripcion.getText().isEmpty()){
						tem = new Temario(txtDescripcion.getText());						
						try {						
							llenarGrilla(temControler.leer(tem, temDAO));
							limpiarCampos();
						} catch (ClassNotFoundException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
							e1.printStackTrace();
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
							e1.printStackTrace();
						}
				}
				else
					JOptionPane.showMessageDialog(null,"La descripción debe tener un valor");
			}
		});
		
	}

	@Override
	public void llenarGrilla(List lista) {
		List<Temario> temarios = lista;

	       String strTemarios[][];
	        strTemarios = new String[lista.size()][2];
	        int i = 0;
			for (Temario temario : temarios) {
	        	strTemarios[i][0] = String.valueOf(temario.getCodigo());
	        	strTemarios[i][1] = temario.getDescripcion();
	        	i++;
				
			}
			
	        dtm.setDataVector(strTemarios, new String[] {"Codigo", "Descripion"	});		
		
	}

	@Override
	public void refresh() {
		//2- asignacion de valores pre-definidos
		try {
			llenarGrilla(temDAO.leer(null) );
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}

	@Override
	public void limpiarCampos() {
		txtCodigo.setText("");
		txtDescripcion.setText("");
	}
	
}
