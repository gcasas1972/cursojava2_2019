package ar.edu.inac.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;

import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.plaf.basic.BasicTreeUI.SelectionModelPropertyChangeHandler;
import javax.swing.table.DefaultTableModel;

import ar.edu.inac.controller.Controller;
import ar.edu.inac.controller.CursoController;
import ar.edu.inac.dao.CursoDao;
import ar.edu.inac.dao.DAO;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Pantalla;
import ar.edu.inac.modelo.exception.ModeloException;
import ar.edu.inac.modelo.validator.CursoAnioValidator;
import ar.edu.inac.modelo.validator.CursoValidator;
import ar.edu.inac.modelo.validator.Validator;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.sql.SQLException;
import java.util.List;

public class CursoView implements View {
	//componentes graficos
	JFrame frame;
	private JTextField txtCodigo;
	private JTable table;
	DefaultTableModel dtm = new DefaultTableModel();
	private JTextField txtDescripcion;
	private JTextField txtAnio;
	
	// controller
	Controller curControler = new CursoController();
	// dao
	DAO curDao  = new CursoDao();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					CursoView window = new CursoView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public CursoView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//1- Establecer todos los componentes gr�ficos
		
		frame = new JFrame();
		frame.setAlwaysOnTop(true);
		frame.setBounds(100, 100, 455, 355);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCursos = new JLabel("Cursos");
		lblCursos.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblCursos.setBounds(154, 11, 98, 44);
		frame.getContentPane().add(lblCursos);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setBounds(26, 67, 46, 14);
		frame.getContentPane().add(lblCodigo);
		
		JLabel lblAnio = new JLabel("A\u00F1o");
		lblAnio.setBounds(26, 102, 46, 14);
		frame.getContentPane().add(lblAnio);
		
		txtCodigo = new JTextField();
		txtCodigo.setEditable(false);
		txtCodigo.setBounds(92, 63, 111, 22);
		frame.getContentPane().add(txtCodigo);
		txtCodigo.setColumns(10);
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(213, 63, 89, 37);
		frame.getContentPane().add(btnAgregar);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(312, 110, 89, 37);
		frame.getContentPane().add(btnBorrar);
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.setBounds(213, 110, 89, 37);
		frame.getContentPane().add(btnModificar);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBounds(311, 63, 89, 37);
		frame.getContentPane().add(btnBuscar);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(77, 206, 324, 99);
		frame.getContentPane().add(scrollPane);
		//cargar todos los cursos en la grilla
		refresh();
		table = new JTable();
		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
		table.setRowSelectionAllowed(true);
		table.setModel(dtm);
		//TODO todos pueden copiar de aca el evanto
		final ListSelectionModel selectModel = table.getSelectionModel();
		scrollPane.setViewportView(table);
		
		JButton btnVerDetalles = new JButton("Ver Materias");
		btnVerDetalles.setBounds(213, 158, 188, 37);
		frame.getContentPane().add(btnVerDetalles);
		
		JLabel lblDescripcionTitulo = new JLabel("Descripcion");
		lblDescripcionTitulo.setBounds(26, 129, 56, 14);
		frame.getContentPane().add(lblDescripcionTitulo);
		
		txtDescripcion = new JTextField();
		txtDescripcion.setColumns(10);
		txtDescripcion.setBounds(92, 125, 111, 22);
		frame.getContentPane().add(txtDescripcion);
		
		txtAnio = new JTextField();
		txtAnio.setBounds(91, 99, 112, 20);
		frame.getContentPane().add(txtAnio);
		txtAnio.setColumns(10);
		
		JButton btnLimpiar = new JButton("Limpiar");
		//*********************************************************
		//2- se etablecen todas las relaciones con los controller y los daos
		curControler = new CursoController();
		curDao = new CursoDao();
		
		// se establecen todas las atenciones a los eventos
		//TODO patricio tenes que copiar de aca y cambiar los txt que correspondan
		selectModel.addListSelectionListener( new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				
				if(!selectModel.isSelectionEmpty()){
					int irow = selectModel.getMinSelectionIndex();
					txtCodigo.setText((String)table.getValueAt(irow, 0));
					txtAnio.setText((String)table.getValueAt(irow, 1));
					txtDescripcion.setText((String)table.getValueAt(irow, 2));
					//JOptionPane.showMessageDialog(null, "fila seleccionada=" + irow);
				}
			}
		});
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarCampos();
			}
		});
		btnLimpiar.setBounds(78, 165, 89, 23);
		frame.getContentPane().add(btnLimpiar);
		
		
		//3- atencion  de los eventos
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- Debo crear un objeto de tipo curso
				//2- Luego debo llamar al controller
				//3- Debo llamar al metodo refresh
				Curso cur= new Curso(	txtCodigo.getText().isEmpty()?0:Integer.parseInt(txtCodigo.getText())	,
										txtAnio.getText().isEmpty()?0:Integer.parseInt(txtAnio.getText())		, 
										txtDescripcion.getText())												;
				
				try {
					curControler.agregarController(cur, curDao);
					refresh();
					limpiarCampos();
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
					e1.printStackTrace();
				} catch (ModeloException e2) {
					StringBuffer sbError = new StringBuffer(e2.getMessage() );
					sbError.append("Para solucionar el problema \n\n");
					sbError.append(e2.getSolucion());
					JOptionPane.showMessageDialog(null, sbError);
					e2.printStackTrace();
				}
			}
		});
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- Debo crear un objeto de tipo curso
				//2- Luego debo llamar al controller
				//3- Debo llamar al metodo refresh
				Curso cur;
//				if(	txtCodigo.getText()!=null && !txtCodigo.getText().isEmpty()){
						cur= new Curso(Integer.parseInt(txtCodigo.getText()),Integer.parseInt(txtAnio.getText()), txtDescripcion.getText());						
						try {
							curControler.modificarController(cur, curDao);
							refresh();
							limpiarCampos();
						} catch (ClassNotFoundException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
							e1.printStackTrace();
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
							e1.printStackTrace();
						} catch (ModeloException e2) {
							StringBuffer sbError = new StringBuffer(e2.getMessage() );
							sbError.append("Para solucionar el problema \n\n");
							sbError.append(e2.getSolucion());
							JOptionPane.showMessageDialog(null, sbError);
							e2.printStackTrace();
						}
				}
//				else
//					JOptionPane.showMessageDialog(null,"Debe seleccionar una fila para poder modificar");
//			}
		});
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- Debo crear un objeto de tipo curso
				//2- Luego debo llamar al controller
				//3- Debo llamar al metodo refresh
				Curso cur;
//				if(	txtCodigo.getText()!=null && !txtCodigo.getText().isEmpty()){
						cur= new Curso(Integer.parseInt(txtCodigo.getText()),Integer.parseInt(txtAnio.getText()), txtDescripcion.getText());						
						try {
							curControler.eliminarController(cur, curDao);
							refresh();
							limpiarCampos();
						} catch (ClassNotFoundException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
							e1.printStackTrace();
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
							e1.printStackTrace();
						} catch (ModeloException e2) {
							StringBuffer sbError = new StringBuffer(e2.getMessage() );
							sbError.append("Para solucionar el problema \n\n");
							sbError.append(e2.getSolucion());
							JOptionPane.showMessageDialog(null, sbError);
							e2.printStackTrace();
						}
						
				}
//				else
//					JOptionPane.showMessageDialog(null,"Debe seleccionar una fila para poder modificar");
//			}
		});
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- Debo crear un objeto de tipo curso
				//2- Luego debo llamar al controller
				//3- Debo llamar al metodo refresh
				Curso cur;
				int codigo = 0;
				if(txtAnio.getText()!=null 		&& !txtAnio.getText().isEmpty()){
						codigo = Integer.parseInt(txtAnio.getText());
					}
				cur= new Curso(codigo, txtDescripcion.getText());						
				try {
					llenarGrilla(curControler.leer(cur, curDao));
				
// if(	txtAnio.getText()!=null 		&& !txtAnio.getText().isEmpty() 	||
//	txtDescripcion.getText()!=null 	&& !txtDescripcion.getText().isEmpty()){
//		cur= new Curso(Integer.parseInt(txtAnio.getText()), txtDescripcion.getText());						
//		try {
//			curControler.leer(cur, curDao);
//							refresh();
							limpiarCampos();
						} catch (ClassNotFoundException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
							e1.printStackTrace();
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
							e1.printStackTrace();
						}
				}});
			
//				else
//				
//					JOptionPane.showMessageDialog(null,"El a�o o la descripci�n deben tener valores");
		
	}
	
	@Override
	public void llenarGrilla(List lista) {
		List<Curso> cursos = lista;

	       String strCursos[][];
	        strCursos = new String[lista.size()][3];
	        int i = 0;
			for (Curso curso : cursos) {
	        	strCursos[i][0] = String.valueOf(curso.getCodigo());
	        	strCursos[i][1] = String.valueOf(curso.getAnio());
	        	strCursos[i][2] = curso.getDescripcion();
	        	i++;
				
			}
			
	        dtm.setDataVector(strCursos, new String[] {"Codigo", "a�o", "Descripion"	});		
		
	}

	@Override
	public void refresh() {
		//2- asignacion de valores pre-definidos
		try {
			llenarGrilla(curDao.leer(null) );
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}

	@Override
	public void limpiarCampos() {
		txtCodigo.setText("");
		txtAnio.setText("");
		txtDescripcion.setText("");
	}
	
}
