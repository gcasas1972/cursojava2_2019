package ar.edu.inac.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.event.MenuKeyListener;
import javax.swing.event.MenuKeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.KeyStroke;
import java.awt.event.InputEvent;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import javax.swing.ImageIcon;
import java.awt.Toolkit;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

public class PantallaPrincipalView {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaPrincipalView window = new PantallaPrincipalView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PantallaPrincipalView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(PantallaPrincipalView.class.getResource("/imagenes/logo-ciata-login.jpg")));
		frame.setBounds(100, 100, 632, 681);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setExtendedState(frame.MAXIMIZED_BOTH);
		
		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar(menuBar);
		
		JMenu mnAbmc = new JMenu("ABMC");
		menuBar.add(mnAbmc);
		
		JMenuItem mntmCursos = new JMenuItem("Cursos");
		mntmCursos.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, InputEvent.ALT_MASK));
		mntmCursos.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				System.out.println("Menu Key pressed");
				CursoView curView = new CursoView();
				curView.frame.setVisible(true);
			}
		});
		mnAbmc.add(mntmCursos);
		
		JMenuItem mntmUsuarios = new JMenuItem("Usuarios");
		mnAbmc.add(mntmUsuarios);
		
		JMenuItem mntmModulos = new JMenuItem("Modulos");
		mnAbmc.add(mntmModulos);
		
		JMenuItem mntmPantallas = new JMenuItem("Pantallas");
		mnAbmc.add(mntmPantallas);
		
		JMenuItem mntmPerfiles = new JMenuItem("Perfiles");
		mntmPerfiles.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.ALT_MASK));
		mntmPerfiles.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				PerfilView perView = new PerfilView();
				perView.frmPerfiles.setVisible(true);
			}
		});
		mnAbmc.add(mntmPerfiles);
		
		JMenuItem mntmPreguntas = new JMenuItem("Preguntas");
		mnAbmc.add(mntmPreguntas);
		
		JMenu mnEvaluaciones = new JMenu("Evaluaciones");
		menuBar.add(mnEvaluaciones);
		
		JMenu mnReportes = new JMenu("Reportes");
		menuBar.add(mnReportes);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(PantallaPrincipalView.class.getResource("/imagenes/Fondo Perfil nuevo.jpg")));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(lblNewLabel)
					.addContainerGap(570, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addComponent(lblNewLabel)
					.addContainerGap(607, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
