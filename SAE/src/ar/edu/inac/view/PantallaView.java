package ar.edu.inac.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ar.edu.inac.controller.Controller;
import ar.edu.inac.controller.PantallaController;
import ar.edu.inac.dao.DAO;
import ar.edu.inac.dao.PantallaDAO;
import ar.edu.inac.modelo.Pantalla;
import ar.edu.inac.modelo.exception.ModeloException;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Toolkit;

public class PantallaView implements View {

	//componentes graficos
	private JFrame frame;
	private JTextField txtCod;
	private JTable table;
	DefaultTableModel dtm = new DefaultTableModel();
	private JTextField txtDescripcion;
	private JLabel lblDescrip;
	
	// controller
	Controller panControler = new PantallaController();
	// dao
	DAO panDao  = new PantallaDAO();
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaView window = new PantallaView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PantallaView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//1- Establecer todos los componentes gr�ficos
		
		
		frame = new JFrame();
		frame.setResizable(false);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(PantallaView.class.getResource("/imagenes/escudo_inac_transparente.png")));
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		lblCodigo.setBounds(10, 63, 50, 14);
		frame.getContentPane().add(lblCodigo);
		
		/*
		JButton btnLimpiar = new JButton("Limpiar Campos");
		btnLimpiar.setBounds(21, 228, 107, 23);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().add(btnLimpiar);
		*/
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.setBackground(new Color(240, 248, 255));
		btnAgregar.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		btnAgregar.setBounds(231, 59, 89, 23);
		frame.getContentPane().add(btnAgregar);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setBackground(new Color(240, 248, 255));
		btnBuscar.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		btnBuscar.setBounds(330, 59, 89, 23);
		frame.getContentPane().add(btnBuscar);
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.setFont(new Font("Goudy Old Style", Font.PLAIN, 14));
		btnModificar.setBounds(231, 86, 89, 23);
		frame.getContentPane().add(btnModificar);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setBackground(new Color(240, 248, 255));
		btnBorrar.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		btnBorrar.setBounds(330, 86, 89, 23);
		frame.getContentPane().add(btnBorrar);

		
		JButton btnVerComponentes = new JButton("Ver Componentes");
		btnVerComponentes.setBackground(new Color(240, 248, 255));
		btnVerComponentes.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		btnVerComponentes.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnVerComponentes.setBounds(272, 228, 147, 23);
		frame.getContentPane().add(btnVerComponentes);
		
		JLabel lblPantallas = new JLabel("Pantallas");
		lblPantallas.setFont(new Font("Goudy Old Style", Font.BOLD, 35));
		lblPantallas.setBounds(24, 11, 158, 41);
		frame.getContentPane().add(lblPantallas);
		
		JScrollPane scrollPane1 = new JScrollPane();
		scrollPane1.setBounds(45, 125, 340, 94);
		frame.getContentPane().add(scrollPane1);
		refresh();
		table = new JTable();
		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION );
		table.setRowSelectionAllowed(true);
		table.setModel(dtm);
		final ListSelectionModel selectModel = table.getSelectionModel();
		scrollPane1.setViewportView(table);
		
		txtDescripcion = new JTextField();
		txtDescripcion.setBackground(new Color(240, 248, 255));
		txtDescripcion.setColumns(10);
		txtDescripcion.setBounds(90, 86, 119, 23);
		frame.getContentPane().add(txtDescripcion);
		
		txtCod = new JTextField();
		txtCod.setBackground(new Color(240, 248, 255));
		txtCod.setEditable(false);
		txtCod.setColumns(10);
		txtCod.setBounds(90, 59, 119, 22);
		frame.getContentPane().add(txtCod);
		
		JLabel lblDescrip;
		lblDescrip = new JLabel("Descripcion");
		lblDescrip.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		lblDescrip.setBounds(10, 90, 82, 14);
		frame.getContentPane().add(lblDescrip);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.setBackground(new Color(240, 248, 255));
		btnLimpiar.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		
		//2- se establecen todas las relaciones con los controller y los daos
		panControler = new PantallaController();
		panDao = new PantallaDAO();
		
		// se establecen todas las atenciones a los eventos
		selectModel.addListSelectionListener( new ListSelectionListener() {
			
			@Override
			public void valueChanged(ListSelectionEvent e) {
				
				if(!selectModel.isSelectionEmpty()){
					int irow = selectModel.getMinSelectionIndex();
					txtCod.setText((String)table.getValueAt(irow, 0));
					txtDescripcion.setText((String)table.getValueAt(irow, 1));
					//JOptionPane.showMessageDialog(null, "fila seleccionada=" + irow);
				}
			}
		});
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarCampos();
			}
		});
		btnLimpiar.setBounds(32, 228, 107, 23);
		frame.getContentPane().add(btnLimpiar);
		
		JLabel label = new JLabel("");
		label.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		label.setIcon(new ImageIcon(PantallaView.class.getResource("/imagenes/6cc4c3fb30afa73a038340aefa850b9d.jpg")));
		label.setBounds(0, 0, 434, 261);
		frame.getContentPane().add(label);
		
	
		//3- atencion  de los eventos
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- Debo crear un objeto de tipo pantalla
				//2- Luego debo llamar al controller
				//3- Debo llamar al metodo refresh
				Pantalla pan= new Pantalla(	txtCod.getText().isEmpty()?0:Integer.parseInt(txtCod.getText())	, 
										txtDescripcion.getText())												;
				
				try {
					panControler.agregarController(pan, panDao);
					refresh();
					limpiarCampos();
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
					e1.printStackTrace();
				} catch (ModeloException e2) {
					StringBuffer sbError = new StringBuffer(e2.getMessage() );
					sbError.append("Para solucionar el problema \n\n");
					sbError.append(e2.getSolucion());
					JOptionPane.showMessageDialog(null, sbError);
					e2.printStackTrace();
				}
			}
		});
		
		
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- Debo crear un objeto de tipo pantalla
				//2- Luego debo llamar al controller
				//3- Debo llamar al metodo refresh
				Pantalla pan;
				
//				if(	txtCod.getText()!=null && !txtCod.getText().isEmpty()){
				pan= new Pantalla(Integer.parseInt(txtCod.getText()), txtDescripcion.getText());						
				try {
					panControler.modificarController(pan, panDao);
					refresh();
					limpiarCampos();
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
					e1.printStackTrace();
				} catch (ModeloException e2) {
					StringBuffer sbError = new StringBuffer(e2.getMessage() );
					sbError.append("Para solucionar el problema \n\n");
					sbError.append(e2.getSolucion());
					JOptionPane.showMessageDialog(null, sbError);
					e2.printStackTrace();
				}
		}
//		else
//			JOptionPane.showMessageDialog(null,"Debe seleccionar una fila para poder modificar");
//	}
			});
			btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- Debo crear un objeto de tipo pantalla
				//2- Luego debo llamar al controller
				//3- Debo llamar al metodo refresh
				
				Pantalla pan;
				
	//		if(	txtCod.getText()!=null && !txtCod.getText().isEmpty()){
				pan= new Pantalla(Integer.parseInt(txtCod.getText()), txtDescripcion.getText());						
				try {
					panControler.eliminarController(pan, panDao);
					refresh();
					limpiarCampos();
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
					e1.printStackTrace();
				} catch (ModeloException e2) {
					StringBuffer sbError = new StringBuffer(e2.getMessage() );
					sbError.append("Para solucionar el problema \n\n");
					sbError.append(e2.getSolucion());
					JOptionPane.showMessageDialog(null, sbError);
					e2.printStackTrace();
				}
				
		}
			
//			else
	//		JOptionPane.showMessageDialog(null,"Debe seleccionar una fila para poder modificar");
			});
			
			btnBuscar.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
		//1- Debo crear un objeto de tipo pantalla
		//2- Luego debo llamar al controller
		//3- Debo llamar al metodo refresh
					Pantalla pan;
//			if(	txtCod.getText()!=null 		&& !txtCod.getText().isEmpty() 	||
//			txtDescripcion.getText()!=null 	&& !txtDescripcion.getText().isEmpty()){
				int codigo = 0;
				if(	txtCod.getText()!=null 		&& !txtCod.getText().isEmpty()){
						codigo = Integer.parseInt(txtCod.getText());
					}
				pan= new Pantalla(codigo, txtDescripcion.getText());						
				try {
					llenarGrilla(panControler.leer(pan, panDao));
					
					refresh();
					limpiarCampos();
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
					e1.printStackTrace();
				}
		}
//		else
//			JOptionPane.showMessageDialog(null,"la descripci�n deben tener valores");
//				}
		});

		}

			@Override
			public void llenarGrilla(List lista) {
			List<Pantalla> pantallas = lista;

			String strPantallas[][];
			strPantallas = new String[lista.size()][2];
			int i = 0;
			for (Pantalla pantalla : pantallas) {
    	strPantallas[i][0] = String.valueOf(pantalla.getCodigo());
    	strPantallas[i][1] = pantalla.getDescripcion();
    	i++;
		
			}
	
			dtm.setDataVector(strPantallas, new String[] {"Codigo", "Descripion"});		

		}

			@Override
			public void refresh() {
				//2- asignacion de valores pre-definidos
				try {
					llenarGrilla(panDao.leer(null) );
				} catch (ClassNotFoundException e) {
	
					e.printStackTrace();
				} catch (SQLException e) {
	
					e.printStackTrace();
				}

			}

			@Override
			public void limpiarCampos() {
				txtCod.setText("");
				txtDescripcion.setText("");
			}
		}
