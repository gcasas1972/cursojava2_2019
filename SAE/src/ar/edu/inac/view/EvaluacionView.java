package ar.edu.inac.view;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.BorderLayout;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;
import java.sql.SQLException;
import java.util.List;

import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import javax.swing.JComboBox;

import ar.edu.inac.dao.ComisionDAO;
import ar.edu.inac.dao.CursoDao;
import ar.edu.inac.dao.MateriaDAO;
import ar.edu.inac.modelo.Comision;
import ar.edu.inac.modelo.Curso;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class EvaluacionView {

	private JFrame frame;
	private JTextField textFecha;
	private JTextField textEstado;
	private JTextField textCodigo;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					EvaluacionView window = new EvaluacionView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public EvaluacionView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 667, 492);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblTitulo = new JLabel("Evaluaci\u00F3n");
		lblTitulo.setFont(new Font("Times New Roman", Font.PLAIN, 20));
		lblTitulo.setBounds(181, 27, 95, 29);
		frame.getContentPane().add(lblTitulo);
		
		JLabel lblCodigo = new JLabel("Codigo");
		lblCodigo.setBackground(Color.WHITE);
		lblCodigo.setForeground(Color.BLACK);
		lblCodigo.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblCodigo.setBounds(31, 76, 42, 29);
		frame.getContentPane().add(lblCodigo);
		
		JLabel lblFechaDeRealizacin = new JLabel("Fecha de realizaci\u00F3n");
		lblFechaDeRealizacin.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblFechaDeRealizacin.setBounds(31, 127, 111, 20);
		frame.getContentPane().add(lblFechaDeRealizacin);
		
		JLabel lblEstado = new JLabel("Estado");
		lblEstado.setFont(new Font("Tahoma", Font.PLAIN, 11));
		lblEstado.setBackground(Color.WHITE);
		lblEstado.setBounds(31, 104, 42, 20);
		frame.getContentPane().add(lblEstado);
		
		textFecha = new JTextField();
		textFecha.setBounds(155, 127, 121, 20);
		frame.getContentPane().add(textFecha);
		textFecha.setColumns(10);
		
		textEstado = new JTextField();
		textEstado.setEnabled(false);
		textEstado.setBounds(155, 104, 121, 20);
		frame.getContentPane().add(textEstado);
		textEstado.setColumns(10);
		
		JButton BuscarBt = new JButton("Buscar");
		BuscarBt.setBounds(501, 99, 89, 23);
		frame.getContentPane().add(BuscarBt);
		
		textCodigo = new JTextField();
		textCodigo.setEditable(false);
		textCodigo.setBounds(155, 80, 121, 20);
		frame.getContentPane().add(textCodigo);
		textCodigo.setColumns(10);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 292, 631, 151);
		frame.getContentPane().add(scrollPane);
		//TODO Esteban se deben agregar los combos seleccionar Materia, seleccionar moduulo y seleccionar alumno
		table = new JTable();
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null, null, "", "", null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
				{null, null, null, null, null, null},
			},
			new String[] {
				"Codigo", "Estado", "Fecha", "Curso", "Materia", "Alumno"
			}
		));
		scrollPane.setViewportView(table);
		
		JButton btnGenerar = new JButton("Generar");
		btnGenerar.setBounds(501, 133, 89, 20);
		frame.getContentPane().add(btnGenerar);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(501, 163, 89, 20);
		frame.getContentPane().add(btnBorrar);
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.setBounds(501, 194, 89, 20);
		frame.getContentPane().add(btnModificar);
		
		final JComboBox cmbCurso = new JComboBox();
		cmbCurso.setBounds(155, 158, 209, 20);
		frame.getContentPane().add(cmbCurso);
		
		DefaultComboBoxModel cmdDataCurso = new DefaultComboBoxModel();
		cmbCurso.setModel(cmdDataCurso);
		
		JComboBox cmbMateria = new JComboBox();
		cmbMateria.setBounds(155, 220, 209, 20);
		frame.getContentPane().add(cmbMateria);
		
		JComboBox cmbAlumnos = new JComboBox();
		cmbAlumnos.setBounds(155, 251, 209, 20);
		frame.getContentPane().add(cmbAlumnos);
		
		JLabel lblNewLabel = new JLabel("Cursos");
		lblNewLabel.setBounds(31, 161, 46, 14);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblMaterias = new JLabel("Materias");
		lblMaterias.setBounds(31, 223, 63, 14);
		frame.getContentPane().add(lblMaterias);
		
		JLabel lblAlumno = new JLabel("Alumnos");
		lblAlumno.setBounds(31, 254, 46, 14);
		frame.getContentPane().add(lblAlumno);
		
		JLabel lblComisiones = new JLabel("Comisiones");
		lblComisiones.setBounds(31, 186, 95, 14);
		frame.getContentPane().add(lblComisiones);
		
		JComboBox cmbComisiones = new JComboBox();
		cmbComisiones.setBounds(155, 189, 207, 20);
		final DefaultComboBoxModel cmdDataComision = new DefaultComboBoxModel();
		cmbComisiones.setModel(cmdDataComision);

		frame.getContentPane().add(cmbComisiones);
		
		//llenado de combox
		
		CursoDao curDao = new CursoDao();
		try {
			//leer un curso			
			List<Curso> cursos =curDao.leer(null);
			cmdDataCurso.addElement("Seleccione curso");			
			for (Curso curso : cursos) 
				cmdDataCurso.addElement(String.valueOf(curso.getAnio()) + "-" + curso.getDescripcion());
		} catch (ClassNotFoundException e) {
			JOptionPane.showMessageDialog(null, "se produjo un error de librerias " + e.getMessage() );
			e.printStackTrace();
		} catch (SQLException e) {
			JOptionPane.showMessageDialog(null, "se produjo un error de sql  " + e.getMessage() );
			e.printStackTrace();
		}
		//atencion de eventos change 
		cmbCurso.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				CursoDao curDao = new CursoDao();
				ComisionDAO comDao = new ComisionDAO();
				List<Comision> comisiones = null;
				String strTexto = cmbCurso.getSelectedItem().toString();
				
				String cursoPartes[] = strTexto.split("-");
				Curso curso = new Curso(Integer.parseInt(cursoPartes[0]), cursoPartes[1]);
				
				try {
					curso = (Curso)(curDao.leer(curso).iterator().next());
					comisiones = comDao.leer(new Comision(0, curso, null, null, null, null, null));
					//llenar las comisiones 
					cmdDataComision.addElement("Seleccione comision");			
					for (Comision comision : comisiones) 
						cmdDataComision.addElement(String.valueOf(comision.getCodigo()) + "-" + comision.getTurno() + comision.getDivision());
					
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null, "se produjo un error de librerias " + e1.getMessage() );
					e1.printStackTrace();
				} catch (SQLException e2) {
					JOptionPane.showMessageDialog(null, "se produjo un error sql " + e2.getMessage() );
					e2.printStackTrace();
				}
				JOptionPane.showMessageDialog(null, "curso= " + curso );
				
			}
		});
		
//		MateriaDAO MateriaDao = new MateriaDAO();
//		try {
//			//leer un curso
//			List<Materia> materia =MateriaDao.leer(null);
//			for (Materia materias : materia) {
//				modelito.addElement(String.valueOf(materia.getClass()));
//				
//			}
//		} catch (ClassNotFoundException e) {
//			assertTrue(false);
//			e.printStackTrace();
//		} catch (SQLException e) {
//			assertTrue(false);
//			e.printStackTrace();
//		}
		
	}
}