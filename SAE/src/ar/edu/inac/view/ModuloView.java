package ar.edu.inac.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import java.awt.Color;
import java.awt.Font;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.JButton;
import javax.swing.JScrollBar;
import javax.swing.JSlider;
import javax.swing.JScrollPane;

import ar.edu.inac.controller.Controller;
import ar.edu.inac.controller.ModuloController;
import ar.edu.inac.dao.CursoDao;
import ar.edu.inac.dao.DAO;
import ar.edu.inac.dao.MateriaDAO;
import ar.edu.inac.dao.ModuloDao;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Modulo;
import ar.edu.inac.modelo.exception.ModeloException;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;

public class ModuloView implements View {

	private JFrame frame;
	private JTextField txtDescripcion;
	private JTextField txtCodigo;
	private JTable table;
	DefaultTableModel dtm = new DefaultTableModel();
	private String curArray[];
	private String modArray[];

	// controller
	Controller modControler = new ModuloController();
	// dao
	DAO modDao = new ModuloDao();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ModuloView window = new ModuloView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ModuloView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setForeground(Color.WHITE);
		frame.getContentPane().setBackground(Color.DARK_GRAY);
		frame.setBounds(100, 100, 603, 668);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JLabel lblModulo = new JLabel("MODULO");
		lblModulo.setFont(new Font("Verdana", Font.BOLD, 30));
		lblModulo.setForeground(Color.RED);
		lblModulo.setBounds(201, 14, 148, 38);
		frame.getContentPane().add(lblModulo);

		JLabel lblCodigo = new JLabel("Codigo:");
		lblCodigo.setForeground(Color.WHITE);
		lblCodigo.setBounds(10, 80, 54, 15);
		frame.getContentPane().add(lblCodigo);

		JLabel lblDescripcion = new JLabel("Descripcion:");
		lblDescripcion.setForeground(Color.WHITE);
		lblDescripcion.setBounds(10, 114, 94, 14);
		frame.getContentPane().add(lblDescripcion);

		txtDescripcion = new JTextField();
		txtDescripcion.setColumns(10);
		txtDescripcion.setBounds(98, 110, 153, 23);
		frame.getContentPane().add(txtDescripcion);

		JButton btnVerItemDeTemario = new JButton("Ver Item De Temario");
		btnVerItemDeTemario.setBounds(280, 149, 188, 41);
		frame.getContentPane().add(btnVerItemDeTemario);			

		txtCodigo = new JTextField();
		txtCodigo.setBounds(98, 75, 153, 24);
		frame.getContentPane().add(txtCodigo);
		txtCodigo.setColumns(10);
		
		//boton limpiar!!!
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarCampos();
			}
		});
		btnLimpiar.setBounds(125, 157, 117, 25);
		frame.getContentPane().add(btnLimpiar);
		
		// boton borrar!!!
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- Debo crear un objeto de tipo curso
				//2- Luego debo llamar al controller
				//3- Debo llamar al metodo refresh
				Modulo mod;
//				if(	txtCodigo.getText()!=null && !txtCodigo.getText().isEmpty()){
						mod= new Modulo(Integer.parseInt(txtCodigo.getText()), txtDescripcion.getText());						
						try {
							modControler.eliminarController(mod, modDao);
							refresh();
							limpiarCampos();
						} catch (ClassNotFoundException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
							e1.printStackTrace();
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
							e1.printStackTrace();
						} catch (ModeloException e2) {
							StringBuffer sbError = new StringBuffer(e2.getMessage() );
							sbError.append("Para solucionar el problema \n\n");
							sbError.append(e2.getSolucion());
							JOptionPane.showMessageDialog(null, sbError);
							e2.printStackTrace();
						}
						
				}
//				else
//					JOptionPane.showMessageDialog(null,"Debe seleccionar una fila para poder modificar");
//			}
		});
		btnBorrar.setBounds(379, 110, 89, 25);
		frame.getContentPane().add(btnBorrar);
		
		//boton Modificar!!!
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//1- Debo crear un objeto de tipo curso
				//2- Luego debo llamar al controller
				//3- Debo llamar al metodo refresh
				Modulo mod;
//				if(	txtCodigo.getText()!=null && !txtCodigo.getText().isEmpty()){
						mod= new Modulo(Integer.parseInt(txtCodigo.getText()), txtDescripcion.getText());						
						try {
							modControler.modificarController(mod, modDao);
							refresh();
							limpiarCampos();
						} catch (ClassNotFoundException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error relacionado con las librerias " + e1.getMessage());
							e1.printStackTrace();
						} catch (SQLException e1) {
							JOptionPane.showMessageDialog(null,"se produjo un error desconocido comuniquese con alquien que sepa " + e1.getMessage());
							e1.printStackTrace();
						} catch (ModeloException e2) {
							StringBuffer sbError = new StringBuffer(e2.getMessage() );
							sbError.append("Para solucionar el problema \n\n");
							sbError.append(e2.getSolucion());
							JOptionPane.showMessageDialog(null, sbError);
							e2.printStackTrace();
						}
				}
//				else
//					JOptionPane.showMessageDialog(null,"Debe seleccionar una fila para poder modificar");
//			}
		});
		btnModificar.setBounds(280, 110, 99, 25);
		frame.getContentPane().add(btnModificar);

		// boton buscar!!!
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 1- Debo crear un objeto de tipo curso
				// 2- Luego debo llamar al controller
				// 3- Debo llamar al metodo refresh
				Modulo mod;
				if (txtCodigo.getText() != null
						&& !txtCodigo.getText().isEmpty()
						|| txtDescripcion.getText() != null
						&& !txtDescripcion.getText().isEmpty()) {
					mod = new Modulo(Integer.parseInt(txtCodigo.getText()),
							txtDescripcion.getText());
					try {
//						modControler.leer(mod, modDao);
						llenarGrilla(modControler.leer(mod, modDao));
//						refresh();
						limpiarCampos();
					} catch (ClassNotFoundException e1) {
						JOptionPane.showMessageDialog(null,
								"se produjo un error relacionado con las librerias "
										+ e1.getMessage());
						e1.printStackTrace();
					} catch (SQLException e1) {
						JOptionPane.showMessageDialog(null,
								"se produjo un error desconocido comuniquese con alquien que sepa "
										+ e1.getMessage());
						e1.printStackTrace();
					}
				}
			}

		});
		btnBuscar.setBounds(379, 64, 91, 25);
		frame.getContentPane().add(btnBuscar);

		// Boton Agregar!!
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 1- Debo crear un objeto de tipo Modulo
				// 2- Luego debo llamar al controller
				// 3- Debo llamar al metodo refresh
				Modulo mod = new Modulo(txtCodigo.getText().isEmpty() ? 0
						: Integer.parseInt(txtCodigo.getText()), txtDescripcion
						.getText());

				try {
					modControler.agregarController(mod, modDao);
					refresh();
					limpiarCampos();
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null,
							"se produjo un error relacionado con las librerias "
									+ e1.getMessage());
					e1.printStackTrace();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null,
							"se produjo un error desconocido comuniquese con alquien que sepa "
									+ e1.getMessage());
					e1.printStackTrace();
				} catch (ModeloException e2) {
					StringBuffer sbError = new StringBuffer(e2.getMessage());
					sbError.append("Para solucionar el problema \n\n");
					sbError.append(e2.getSolucion());
					JOptionPane.showMessageDialog(null, sbError);
					e2.printStackTrace();
				}
			}
		});
		btnAgregar.setBounds(280, 64, 99, 25);
		frame.getContentPane().add(btnAgregar);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(36, 357, 527, 250);
		frame.getContentPane().add(scrollPane);
		refresh();
		table = new JTable();
		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setRowSelectionAllowed(true);
		// table.setModel(new DefaultTableModel(
		// new Object[][] {
		// {null, null},
		// {null, null},
		// {null, null},
		// },
		// new String[] {
		// "Codigo", "Descripcion"
		// }
		// ));
		table.setModel(dtm);
		table.getColumnModel().getColumn(1).setPreferredWidth(90);
		final ListSelectionModel selectModel = table.getSelectionModel();
		scrollPane.setViewportView(table);
		
		JLabel lblAnio = new JLabel("Año:");
		lblAnio.setForeground(Color.WHITE);
		lblAnio.setBounds(31, 228, 54, 15);
		frame.getContentPane().add(lblAnio);
		
		JLabel lblCursos = new JLabel("Cursos:");
		lblCursos.setForeground(Color.WHITE);
		lblCursos.setBounds(322, 228, 54, 15);
		frame.getContentPane().add(lblCursos);
		
		JLabel lblMateria = new JLabel("Materia:");
		lblMateria.setForeground(Color.WHITE);
		lblMateria.setBounds(31, 262, 60, 15);
		frame.getContentPane().add(lblMateria);
		
		JLabel lblModulo2 = new JLabel("Modulo:");
		lblModulo2.setForeground(Color.WHITE);
		lblModulo2.setBounds(322, 262, 57, 15);
		frame.getContentPane().add(lblModulo2);		
		
		
		final JComboBox cmbAnio = new JComboBox();
		cmbAnio.setModel(new DefaultComboBoxModel(new String[] {"Seleccionar Año:", "1", "2", "3", "4", "5", "6", "7"}));
		
		final JComboBox cmbCursos = new JComboBox();
		cmbCursos.setModel(new DefaultComboBoxModel(new String[] {"Seleccionar Cursos:"}));
		cmbCursos.setBounds(389, 223, 174, 24);
		frame.getContentPane().add(cmbCursos);
		
		//defino una array cursos
		 
		cmbAnio.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				//1- obtener el año
				//2- obtener todos los cursos
				//3- cargar combo de cursos
				
				int anio = Integer.parseInt((String) (cmbAnio.getSelectedItem()));
				
				Curso curso = new Curso(anio, null);
				CursoDao curDao = new CursoDao();
				List<Curso> curList;
				try {
					curList = curDao.leer(curso);
					curArray = new String[curList.size()];
					int i = 0;
					for (Curso cursoItem : curList) {
						curArray[i] = cursoItem.getCodigo() + 
														"-" +
										cursoItem.getAnio() + 
														"-" +
							      cursoItem.getDescripcion();
						i++;
					}
					cmbCursos.setModel(new DefaultComboBoxModel(curArray));
					
				} catch (ClassNotFoundException e1) {	
					JOptionPane.showMessageDialog(null, "No se encontro la clase");
					e1.printStackTrace();
				} catch (SQLException e) {					
					JOptionPane.showMessageDialog(null, "Se produjo errar de sql");
					e.printStackTrace();
				}
			}
		});
		
		cmbAnio.setBounds(125, 223, 174, 24);
		frame.getContentPane().add(cmbAnio);
		
		final JComboBox cmbModulo = new JComboBox();
		cmbModulo.setBounds(389, 262, 174, 24);
		frame.getContentPane().add(cmbModulo);
		cmbModulo.setModel(new DefaultComboBoxModel(new String[] {"Seleccionar Modulos:"}));
		
		//comprobar combobox !!!!! nose si da bien!!!!
		
		JComboBox cmbMateria = new JComboBox();
		cmbMateria.setModel(new DefaultComboBoxModel(new String[] {"Seleccionar Materias:"}));
		cmbMateria.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				//1- obtener todas los modulos
				//2- cargar combo de materia
				
				Modulo modulo = new Modulo();
				ModuloDao modDao = new ModuloDao();
				
				List<Modulo> modList;
				try {
					modList = modDao.leer(modulo);
					modArray = new String[modList.size()];
					int i = 0;
					for (Modulo moduloItem : modList) {
						modArray[i] = moduloItem.getCodigo() + 
														 "-" +
								  moduloItem.getDescripcion();
						i++;
					}
					cmbModulo.setModel(new DefaultComboBoxModel(modArray));
					
				} catch (ClassNotFoundException e1) {	
					JOptionPane.showMessageDialog(null, "No se encontro la clase");
					e1.printStackTrace();
				} catch (SQLException e) {					
					JOptionPane.showMessageDialog(null, "Se produjo errar de sql");
					e.printStackTrace();
				}
				
			}
		});
		cmbMateria.setBounds(125, 262, 174, 24);
		frame.getContentPane().add(cmbMateria);
		
				
		
		
		
		
	}

	@Override
	public void llenarGrilla(List lista) {
		List<Modulo> modulos = lista;// lista dinamica

		String strModulos[][];
		strModulos = new String[lista.size()][2];// la cantidad de columnas.
		int i = 0;
		for (Modulo modulo : modulos) {
			strModulos[i][0] = String.valueOf(modulo.getCodigo());
			strModulos[i][1] = modulo.getDescripcion();
			i++;
		}
		dtm.setDataVector(strModulos, new String[] { "Codigo", "Descripcion" });

	}

	@Override
	public void refresh() {
		// por ahora para modificar.
		ModuloDao moduloDao = new ModuloDao();
		try {
			llenarGrilla(moduloDao.leer(null));
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public void limpiarCampos() {

	}
}
