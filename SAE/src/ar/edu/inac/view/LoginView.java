package ar.edu.inac.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import java.awt.Component;
import java.awt.Toolkit;
import javax.swing.JButton;

import com.sun.corba.se.pept.transport.Connection;

import ar.edu.inac.modelo.Usuario;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginView {

	private JFrame frmLogin;
	private JTextField txt_usu;
	private JPasswordField txt_pas;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginView window = new LoginView();
					window.frmLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmLogin = new JFrame();
		frmLogin.setResizable(false);
		frmLogin.setTitle("LOGIN ");
		frmLogin.setIconImage(Toolkit.getDefaultToolkit().getImage(LoginView.class.getResource("/imagenes/icono-utn.png")));
		frmLogin.getContentPane().setBackground(Color.WHITE);
		frmLogin.setBounds(100, 100, 536, 328);
		frmLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLogin.getContentPane().setLayout(null);
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setBounds(325, 136, 65, 42);
		lblNewLabel_3.setIcon(new ImageIcon(LoginView.class.getResource("/imagenes/acceso-icon-login.png")));
		frmLogin.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(146, 195, 220, 93);
		frmLogin.getContentPane().add(lblNewLabel);
		lblNewLabel.setIcon(new ImageIcon(LoginView.class.getResource("/imagenes/logo-login.jpg")));
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setBounds(156, 77, 34, 36);
		lblNewLabel_2.setIcon(new ImageIcon(LoginView.class.getResource("/imagenes/password-icon-login.png")));
		frmLogin.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(153, 40, 26, 26);
		frmLogin.getContentPane().add(lblNewLabel_1);
		lblNewLabel_1.setIcon(new ImageIcon(LoginView.class.getResource("/imagenes/usuario-icon-login.png")));
		
		txt_usu = new JTextField();
		txt_usu.setFont(new Font("Segoe UI Semibold", Font.BOLD, 13));
		txt_usu.setBounds(189, 40, 145, 26);
		txt_usu.setToolTipText("Ingrese su usuario");
		frmLogin.getContentPane().add(txt_usu);
		txt_usu.setColumns(10);
		
		txt_pas = new JPasswordField();
		txt_pas.setBounds(189, 87, 145, 26);
		txt_pas.setToolTipText("Ingrese su contrase\u00F1a");
		frmLogin.getContentPane().add(txt_pas);
		
		JButton btn_ing = new JButton("INGRESAR");
		btn_ing.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
//TODO Gabrielito revisar el error			
//				Usuario usu = new Usuario (txt_usu.getText(),
//										   txt_pas.getPassword());
				
			}
		});
		btn_ing.setBounds(189, 136, 119, 42);
		btn_ing.setBackground(UIManager.getColor("Button.disabledShadow"));
		btn_ing.setFont(new Font("Segoe UI Light", Font.PLAIN, 15));
		frmLogin.getContentPane().add(btn_ing);
		
		
			
		
	}
}
