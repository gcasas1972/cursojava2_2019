package ar.edu.inac.view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;

public class Recordatorio {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Recordatorio window = new Recordatorio();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Recordatorio() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblPorFavorNo = new JLabel("Por favor no olvidarse");
		lblPorFavorNo.setForeground(Color.RED);
		lblPorFavorNo.setFont(new Font("Arial", Font.PLAIN, 40));
		lblPorFavorNo.setBounds(34, 23, 390, 68);
		frame.getContentPane().add(lblPorFavorNo);
		
		JLabel lblDeArreglarLa = new JLabel("de arreglar la tabla ");
		lblDeArreglarLa.setForeground(Color.RED);
		lblDeArreglarLa.setFont(new Font("Arial", Font.PLAIN, 40));
		lblDeArreglarLa.setBounds(34, 81, 390, 68);
		frame.getContentPane().add(lblDeArreglarLa);
		
		JLabel lblDeEvaluaciones = new JLabel("de Evaluaciones");
		lblDeEvaluaciones.setForeground(Color.RED);
		lblDeEvaluaciones.setFont(new Font("Arial", Font.PLAIN, 40));
		lblDeEvaluaciones.setBounds(34, 140, 390, 68);
		frame.getContentPane().add(lblDeEvaluaciones);
	}
}
