package ar.edu.inac.view;

import java.awt.Color;
import java.awt.Dialog.ModalExclusionType;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Label;
import java.awt.TextField;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ar.edu.inac.dao.CursoDao;
import ar.edu.inac.dao.PreguntaDAO;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Pregunta;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.SwingConstants;

public class PreguntasView implements View{

	private JFrame frame;
	DefaultTableModel dtm = new DefaultTableModel();
	private JTable tblRespuestas;
	private JTable table;
	private String curArray[] ;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PreguntasView window = new PreguntasView();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public PreguntasView() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.setModalExclusionType(ModalExclusionType.APPLICATION_EXCLUDE);
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(PreguntasView.class.getResource("/imagenes/logo-ciata-login.jpg")));
		frame.getContentPane().setBackground(Color.WHITE);
		frame.setBounds(100, 100, 564, 684);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblTituloCdigo = new JLabel("C\u00F3digo");
		lblTituloCdigo.setBounds(26, 49, 60, 18);
		lblTituloCdigo.setForeground(Color.BLACK);
		lblTituloCdigo.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		

		JButton btnNewButton = new JButton("Buscar");
		btnNewButton.setBounds(157, 659, 100, 25);
		btnNewButton.setFont(new Font("Segoe UI Light", Font.PLAIN, 15));
		
		JButton btnModificar = new JButton("Modificar");
		btnModificar.setBounds(301, 659, 100, 25);
		btnModificar.setFont(new Font("Segoe UI Light", Font.PLAIN, 15));
		
		JButton btnNewButton_1 = new JButton("Borrar");
		btnNewButton_1.setBounds(431, 659, 100, 25);
		btnNewButton_1.setFont(new Font("Segoe UI Light", Font.PLAIN, 15));
		
		final JComboBox cmbAnio = new JComboBox();
		cmbAnio.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		cmbAnio.setModel(new DefaultComboBoxModel(
					new String[] {	"Seleccionar a\u00F1o"	, "1", "2", 
														"3"	, "4", "5", 
														"6", "7"}));
		
		final JComboBox cmbCursos = new JComboBox();
		cmbCursos.setBackground(new Color(240, 248, 255));
		cmbCursos.setBounds(98, 110, 159, 22);
		frame.getContentPane().add(cmbCursos);

		
		cmbAnio.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				//1- obtener el a�o	--ok
				//2- obtener todos los cursos
				//3- cargar el combo de cursos 
				int anio=Integer.parseInt((String)(cmbAnio.getSelectedItem()));
				Curso curso= new Curso(anio, null);
				CursoDao curDao = new CursoDao();
				List<Curso> curList;
				try {
					curList = curDao.leer(curso);
					curArray = new String[curList.size()];
					int i=0;
					for (Curso cursoItem : curList) {
						curArray[i]= cursoItem.getCodigo() + "-" + 
											cursoItem.getAnio() + 
															"-" + 
									cursoItem.getDescripcion();
						i++;
					}
					cmbCursos.setModel(new DefaultComboBoxModel(curArray ));
										
				} catch (ClassNotFoundException e1) {
					JOptionPane.showMessageDialog(null, "no se encontro la clase");
					e1.printStackTrace();
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(null, "se produjo un error de sql");
					e1.printStackTrace();
				}
				
				//JOptionPane.showMessageDialog(null, "a�o=" + anio);
			}
		});
		
		cmbAnio.setBounds(98, 77, 159, 22);
		
		
		JLabel lblMateria = new JLabel("Materia");
		lblMateria.setBounds(301, 80, 46, 14);
		lblMateria.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		
		JComboBox cmbMateria = new JComboBox();
		cmbMateria.setBackground(new Color(240, 248, 255));
		cmbMateria.setBounds(372, 110, 159, 22);
		
		JLabel lblMdulo = new JLabel("M\u00F3dulo");
		lblMdulo.setBounds(301, 113, 61, 14);
		lblMdulo.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		
		JComboBox cmbModulo = new JComboBox();
		cmbModulo.setBackground(new Color(240, 248, 255));
		cmbModulo.setBounds(372, 77, 159, 22);
		
		JLabel lblPregunta = new JLabel("Pregunta");
		lblPregunta.setBounds(26, 134, 60, 18);
		lblPregunta.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		
		JLabel lblExplicacin = new JLabel("Explicaci\u00F3n");
		lblExplicacin.setBounds(301, 138, 100, 14);
		lblExplicacin.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		
		JButton btnAgregarGrfico = new JButton("Agregar gr\u00E1fico");
		btnAgregarGrfico.setBounds(26, 462, 150, 23);
		btnAgregarGrfico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnAgregarGrfico.setFont(new Font("Goudy Old Style", Font.PLAIN, 20));
		
		JButton btnEliminarGrfico = new JButton("Eliminar gr\u00E1fico");
		btnEliminarGrfico.setBounds(351, 462, 180, 23);
		btnEliminarGrfico.setFont(new Font("Goudy Old Style", Font.PLAIN, 20));
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//debo crear una pregunta
				//Pregunta preg = new Pregunta(pResp, pTexto, pPathG, pExp, pMat, pMod, pEval)
			}
		});
		btnAgregar.setBounds(26, 659, 100, 25);
		btnAgregar.setFont(new Font("Segoe UI Light", Font.PLAIN, 15));
		
		frame.getContentPane().setLayout(null);
		frame.getContentPane().add(btnNewButton_1);
		frame.getContentPane().add(cmbAnio);
		frame.getContentPane().add(btnNewButton);
		frame.getContentPane().add(lblMateria);
		frame.getContentPane().add(cmbMateria);
		frame.getContentPane().add(lblMdulo);
		frame.getContentPane().add(cmbModulo);
		frame.getContentPane().add(btnAgregar);
		frame.getContentPane().add(lblPregunta);
		frame.getContentPane().add(lblExplicacin);
		frame.getContentPane().add(btnEliminarGrfico);
		frame.getContentPane().add(btnAgregarGrfico);
		frame.getContentPane().add(lblTituloCdigo);
		frame.getContentPane().add(btnModificar);
		//llena lagrilla
		refresh();
		
		JLabel lblAo_1 = new JLabel("A\u00F1o");
		lblAo_1.setForeground(Color.BLACK);
		lblAo_1.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		lblAo_1.setBounds(26, 78, 46, 18);
		frame.getContentPane().add(lblAo_1);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(26, 519, 230, 113);
		frame.getContentPane().add(scrollPane_1);
		
		tblRespuestas = new JTable();
		scrollPane_1.setViewportView(tblRespuestas);
		tblRespuestas.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null},
				{null, null},
				{null, null},
				{null, null},
				{null, null},
			},
			new String[] {
				"Respuestas", "Correctas"
			}
		) {
			Class[] columnTypes = new Class[] {
				String.class, Boolean.class
			};
			public Class getColumnClass(int columnIndex) {
				return columnTypes[columnIndex];
			}
		});
		
		final JTextArea txtPregunta = new JTextArea();
		txtPregunta.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		txtPregunta.setBackground(new Color(240, 248, 255));
		txtPregunta.setText("aqui va la pregunta");
		txtPregunta.setBounds(26, 163, 230, 88);
		frame.getContentPane().add(txtPregunta);
		
		final JTextArea txtRespuesta = new JTextArea();
		txtRespuesta.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		txtRespuesta.setBackground(new Color(240, 248, 255));
		txtRespuesta.setText("aqui va la respuesta");
		txtRespuesta.setBounds(301, 163, 230, 88);
		frame.getContentPane().add(txtRespuesta);
		
		final JLabel lblCodigo = new JLabel("");
		lblCodigo.setBounds(98, 50, 159, 22);
		frame.getContentPane().add(lblCodigo);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(301, 519, 230, 113);
		frame.getContentPane().add(scrollPane);
		
		table = new JTable();
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setColumnSelectionAllowed(true);
		table.setCellSelectionEnabled(true);
		table.setModel(dtm);
		//TODO Patricio aca  copiarla del cursoView
		final ListSelectionModel selectModel = table.getSelectionModel();
		scrollPane.setViewportView(table);
		
//		table.getColumnModel().getColumn(0).setPreferredWidth(42);
//		table.getColumnModel().getColumn(1).setPreferredWidth(145);
		scrollPane.setViewportView(table);
		
		selectModel.addListSelectionListener( new ListSelectionListener() {
		public void valueChanged(ListSelectionEvent e) {
			
			if(!selectModel.isSelectionEmpty()){
				int irow = selectModel.getMinSelectionIndex();
				lblCodigo.setText((String)table.getValueAt(irow, 0));
				txtPregunta.setText((String)table.getValueAt(irow, 1));
				txtRespuesta.setText((String)table.getValueAt(irow, 2));
				
			}
		}
		});
		
		JScrollPane scrollPane_2 = new JScrollPane();
		scrollPane_2.setBounds(24, 264, 507, 187);
		frame.getContentPane().add(scrollPane_2);
		
		JLabel lblGrafico = new JLabel("");
		scrollPane_2.setColumnHeaderView(lblGrafico);
		lblGrafico.setIcon(null);
		
		JLabel label = new JLabel("");
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(new Color(240, 248, 255));
		scrollPane_2.setViewportView(label);
		
		JLabel lblCurso = new JLabel("Curso");
		lblCurso.setForeground(Color.BLACK);
		lblCurso.setFont(new Font("Goudy Old Style", Font.PLAIN, 15));
		lblCurso.setBounds(26, 111, 46, 18);
		frame.getContentPane().add(lblCurso);
		
		JLabel lblPreguntas = new JLabel("Preguntas");
		lblPreguntas.setFont(new Font("Goudy Old Style", Font.PLAIN, 35));
		lblPreguntas.setForeground(new Color(0, 0, 0));
		lblPreguntas.setBounds(214, 0, 238, 55);
		frame.getContentPane().add(lblPreguntas);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(PreguntasView.class.getResource("/imagenes/6cc4c3fb30afa73a038340aefa850b9d.jpg")));
		lblNewLabel.setBounds(0, -65, 548, 713);
		frame.getContentPane().add(lblNewLabel);
		

		
	}

	@Override
	public void llenarGrilla(List lista) {
	List<Pregunta> preguntas = lista;
	//TODO Patricio agregar la explicacion
	 String strPreguntas[][];
     strPreguntas = new String[lista.size()][3];
     int i = 0;
		for (Pregunta pregunta : preguntas) {
     	strPreguntas[i][0] = String.valueOf(pregunta.getCodigo());
     	strPreguntas[i][1] = String.valueOf(pregunta.getTexto());
     	strPreguntas[i][2] = String.valueOf(pregunta.getExplicacion());
     	i++;
     
			
		}
		  dtm.setDataVector(strPreguntas, new String[] {"C�digo", "texto", "Explicaci�n"});		
	}
	
	

	@Override
	public void refresh() {
	PreguntaDAO preguntaDao = new PreguntaDAO();
	try {
		llenarGrilla(preguntaDao.leer(null));
	} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		
	}

	@Override
	public void limpiarCampos() {
		// TODO Auto-generated method stub
		
	}
}
