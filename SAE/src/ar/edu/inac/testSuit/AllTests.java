package ar.edu.inac.testSuit;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
import ar.edu.inac.dao.test.ComisionDAOTest;
import ar.edu.inac.dao.test.ComponenteDAOTest;
import ar.edu.inac.dao.test.CursoDaoTest;
import ar.edu.inac.dao.test.EvalaluacionDaoTest;
import ar.edu.inac.dao.test.ItemDeTemarioDAOTest;
import ar.edu.inac.dao.test.ModuloDAOTest;
import ar.edu.inac.dao.test.PantallaDAOTest;
import ar.edu.inac.dao.test.PartidoDAOTest;
import ar.edu.inac.dao.test.PerfilDAOTest;
import ar.edu.inac.dao.test.PreguntaDAOTest;
import ar.edu.inac.dao.test.ProfesorDAOTest;
import ar.edu.inac.dao.test.ProvinciaDAOTest;
import ar.edu.inac.dao.test.TemarioDAOTest;
import ar.edu.inac.dao.test.UsuariosDAOTest;
import ar.edu.inac.dao.util.test.ConnectionManagerTest;
import ar.edu.inac.dao.util.test.FechaUtilTest;
import ar.edu.inac.modelo.validator.test.ValidatorTest;

@RunWith(Suite.class)
@SuiteClasses( {ComisionDAOTest.class,
				CursoDaoTest.class,
				EvalaluacionDaoTest.class ,
				ModuloDAOTest.class,
				PartidoDAOTest.class,
				PreguntaDAOTest.class,
				ProfesorDAOTest.class,
				ProvinciaDAOTest.class,
				TemarioDAOTest.class,
				PantallaDAOTest.class,//Clase Agustin
				PerfilDAOTest.class,//Clase Nazareno
				ComponenteDAOTest.class,//Clase Diego
				UsuariosDAOTest.class,//Clase Matias
				ConnectionManagerTest.class,
				FechaUtilTest.class,
				ItemDeTemarioDAOTest.class, //gcasas
				ValidatorTest.class,
				UsuariosDAOTest.class //Vanina vivaaa				
				}  
		)
public class AllTests {

}


