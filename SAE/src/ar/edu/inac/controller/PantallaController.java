package ar.edu.inac.controller;

import java.sql.SQLException;
import java.util.List;

import ar.edu.inac.dao.DAO;
import ar.edu.inac.modelo.Pantalla;
import ar.edu.inac.modelo.Model;
import ar.edu.inac.modelo.exception.ModeloException;
import ar.edu.inac.modelo.validator.PantallaAgregarValidator;
import ar.edu.inac.modelo.validator.PantallaEliminarValidator;
import ar.edu.inac.modelo.validator.PantallaModificarValidator;
import ar.edu.inac.modelo.validator.PantallaValidator;
import ar.edu.inac.modelo.validator.Validator;

public class PantallaController implements Controller {


	@Override
	public void agregarController(Model model, DAO dao) throws ClassNotFoundException, SQLException, ModeloException {
		
		
		if(PantallaAgregarValidator.getInstance((Pantalla)model).validarTodo())
			dao.agregar(model);
		else 
			throw new ModeloException(PantallaValidator.getErrorAcumulado(), "Debe completar los campos segun lo sugerido");

	}

	@Override
	public void eliminarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException, ModeloException {
		if(PantallaEliminarValidator.getInstance((Pantalla)model).validarTodo())
			dao.eliminar(model);
		else
		throw new ModeloException(PantallaValidator.getErrorAcumulado(), "Debe completar los campos segun lo sugerido");
	}

	@Override
	public void modificarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException, ModeloException {
		if(PantallaModificarValidator.getInstance((Pantalla)model).validarTodo())
			dao.modificar(model);
		else
			throw new ModeloException(PantallaValidator.getErrorAcumulado(), "Debe completar los campos segun lo sugerido");
			
		
	}

	@Override
	public List<Model> leer(Model model, DAO dao)
			throws ClassNotFoundException, SQLException {
		
		return dao.leer(model);
	}


}
