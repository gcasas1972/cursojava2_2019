package ar.edu.inac.controller;

import java.sql.SQLException;
import java.util.List;

import ar.edu.inac.dao.DAO;
import ar.edu.inac.modelo.Model;
import ar.edu.inac.modelo.exception.ModeloException;

public interface Controller {
	public void agregarController(Model model, DAO dao) throws ClassNotFoundException, SQLException, ModeloException;
	public void eliminarController(Model model, DAO dao) throws ClassNotFoundException, SQLException, ModeloException;
	public void modificarController(Model model, DAO dao) throws ClassNotFoundException, SQLException, ModeloException;
	public List<Model> leer(Model model, DAO dao) throws ClassNotFoundException, SQLException;
}
