package ar.edu.inac.controller;

import java.sql.SQLException;
import java.util.List;

import ar.edu.inac.dao.DAO;
import ar.edu.inac.modelo.Componente;
import ar.edu.inac.modelo.Model;
import ar.edu.inac.modelo.exception.ModeloException;
import ar.edu.inac.modelo.validator.ComponenteAgregarValidator;
import ar.edu.inac.modelo.validator.ComponenteEliminarValidator;
import ar.edu.inac.modelo.validator.ComponenteModificarValidator;
import ar.edu.inac.modelo.validator.ComponenteValidator;
import ar.edu.inac.modelo.validator.Validator;

public class ComponenteController implements Controller {

	

	@Override
	public void agregarController(Model model, DAO dao) throws ClassNotFoundException, SQLException, ModeloException {
		
		
		if(ComponenteAgregarValidator.getInstance((Componente)model).validarTodo())
			dao.agregar(model);
		else 
			throw new ModeloException(ComponenteValidator.getErrorAcumulado(), "Debe completar los campos segun lo sugerido");

	}

	@Override
	public void eliminarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException, ModeloException {
		if(ComponenteEliminarValidator.getInstance((Componente)model).validarTodo())
			dao.eliminar(model);
		throw new ModeloException(ComponenteValidator.getErrorAcumulado(), "Debe completar los campos segun lo sugerido");
	}

	@Override
	public void modificarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException, ModeloException {
		if(ComponenteModificarValidator.getInstance((Componente)model).validarTodo())
			dao.modificar(model);
		else
			throw new ModeloException(ComponenteValidator.getErrorAcumulado(), "Debe completar los campos segun lo sugerido");
			
		
	}

	@Override
	public List<Model> leer(Model model, DAO dao)
			throws ClassNotFoundException, SQLException {
		
		return dao.leer(model);
	}


}
