package ar.edu.inac.controller;

import java.sql.SQLException;
import java.util.List;

import ar.edu.inac.dao.DAO;
import ar.edu.inac.modelo.Model;
import ar.edu.inac.modelo.Perfil;
import ar.edu.inac.modelo.exception.ModeloException;
import ar.edu.inac.modelo.validator.PerfilAgregarValidator;
import ar.edu.inac.modelo.validator.PerfilEliminarValidator;
import ar.edu.inac.modelo.validator.PerfilModificarValidator;
import ar.edu.inac.modelo.validator.PerfilValidator;
import ar.edu.inac.modelo.validator.Validator;

public class PerfilController implements Controller {

	

	@Override
	public void agregarController(Model model, DAO dao) throws ClassNotFoundException, SQLException, ModeloException {
		
		
		if(PerfilAgregarValidator.getInstance((Perfil)model).validarTodo())
			dao.agregar(model);
		else 
			throw new ModeloException(PerfilValidator.getErrorAcumulado(), "Debe completar los campos seg�n lo sugerido");

	}

	@Override
	public void eliminarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException, ModeloException {
		if(PerfilEliminarValidator.getInstance((Perfil)model).validarTodo())
			dao.eliminar(model);
		else
		throw new ModeloException(PerfilValidator.getErrorAcumulado(), "Debe completar los campos seg�n lo sugerido");
	}

	@Override
	public void modificarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException, ModeloException {
		if(PerfilModificarValidator.getInstance((Perfil)model).validarTodo())
			dao.modificar(model);
		else
			throw new ModeloException(PerfilValidator.getErrorAcumulado(), "Debe completar los campos seg�n lo sugerido");
			
		
	}

	@Override
	public List<Model> leer(Model model, DAO dao)
			throws ClassNotFoundException, SQLException {
		
		return dao.leer(model);
	}


}
