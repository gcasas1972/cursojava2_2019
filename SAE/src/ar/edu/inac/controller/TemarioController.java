package ar.edu.inac.controller;

import java.sql.SQLException;
import java.util.List;
import ar.edu.inac.dao.DAO;
import ar.edu.inac.modelo.Temario;
import ar.edu.inac.modelo.Model;

public class TemarioController implements Controller{

	@Override
	public void agregarController(Model model, DAO dao) throws ClassNotFoundException, SQLException {
		
		Temario tem = (Temario)model;
		// se debne realizar las validaciones
		dao.agregar(tem);

	}

	@Override
	public void eliminarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException {
		//se deben agregar las validaciones
		dao.eliminar(model);
		
	}

	@Override
	public void modificarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException {
		dao.modificar(model);
		
	}

	@Override
	public List<Model> leer(Model model, DAO dao)
			throws ClassNotFoundException, SQLException {
		
		return dao.leer(model);
	}
}

