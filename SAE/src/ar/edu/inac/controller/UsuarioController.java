package ar.edu.inac.controller;

import java.sql.SQLException;
import java.util.List;

import ar.edu.inac.dao.DAO;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Usuario;

import ar.edu.inac.modelo.Model;
import ar.edu.inac.modelo.exception.ModeloException;
import ar.edu.inac.modelo.validator.CursoAgregarValidator;
import ar.edu.inac.modelo.validator.CursoEliminarValidator;
import ar.edu.inac.modelo.validator.CursoModificarValidator;
import ar.edu.inac.modelo.validator.CursoValidator;
import ar.edu.inac.modelo.validator.UsuarioAgregarValidator;
import ar.edu.inac.modelo.validator.UsuarioEliminarValidator;
import ar.edu.inac.modelo.validator.UsuarioModificarValidator;
import ar.edu.inac.modelo.validator.UsuarioValidator;
import ar.edu.inac.modelo.validator.Validator;

public class UsuarioController implements Controller {

	

	@Override
	public void agregarController(Model model, DAO dao) throws ClassNotFoundException, SQLException, ModeloException {
		
		
		if(UsuarioAgregarValidator.getInstance((Usuario)model).validarTodo())
			dao.agregar(model);
		else 
			throw new ModeloException(UsuarioValidator.getErrorAcumulado(), "PROBLEMA CON EL AGREGAR");

	}

	@Override
	public void eliminarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException, ModeloException {
		if(UsuarioEliminarValidator.getInstance((Usuario)model).validarTodo())
			dao.eliminar(model); 
		else
			
		throw new ModeloException(UsuarioValidator.getErrorAcumulado(), "Debe completar los campos segun lo sugerido");
	}

	@Override
	public void modificarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException, ModeloException {
		if(UsuarioModificarValidator.getInstance((Usuario)model).validarTodo())
			dao.modificar(model);
		else
			throw new ModeloException(UsuarioValidator.getErrorAcumulado(), "Debe completar los campos segun lo sugerido");
			
		
	}

	@Override
	public List<Model> leer(Model model, DAO dao)
			throws ClassNotFoundException, SQLException {
		
		return dao.leer(model);
	}


}
