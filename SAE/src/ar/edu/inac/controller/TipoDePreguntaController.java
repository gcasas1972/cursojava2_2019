package ar.edu.inac.controller;

import java.sql.SQLException;
import java.util.List;

import ar.edu.inac.dao.DAO;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Model;
import ar.edu.inac.modelo.exception.ModeloException;
import ar.edu.inac.modelo.validator.CursoAgregarValidator;
import ar.edu.inac.modelo.validator.CursoEliminarValidator;
import ar.edu.inac.modelo.validator.CursoModificarValidator;
import ar.edu.inac.modelo.validator.CursoValidator;
import ar.edu.inac.modelo.validator.Validator;

public class TipoDePreguntaController implements Controller {

	

	@Override
	public void agregarController(Model model, DAO dao) throws ClassNotFoundException, SQLException, ModeloException {
		
		
////		if(CursoAgregarValidator.getInstance((Curso)model).validarTodo())
			dao.agregar(model);
////		else 
////			throw new ModeloException(CursoValidator.getErrorAcumulado(), "Debe completar los campos seg�nn lo sugerido");
//
	}

	@Override
	public void eliminarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException, ModeloException {
////		if(CursoEliminarValidator.getInstance((Curso)model).validarTodo())
				dao.eliminar(model);
//		else
//		throw new ModeloException(CursoValidator.getErrorAcumulado(), "Debe completar los campos seg�nn lo sugerido");
	}

	@Override
	public void modificarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException, ModeloException {
//		if(CursoModificarValidator.getInstance((Curso)model).validarTodo())
			dao.modificar(model);
//		else
//			throw new ModeloException(CursoValidator.getErrorAcumulado(), "Debe completar los campos seg�nn lo sugerido");
			
		
	}

	@Override
	public List<Model> leer(Model model, DAO dao)
			throws ClassNotFoundException, SQLException {
		
		return dao.leer(model);
	}


}
