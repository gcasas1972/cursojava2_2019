package ar.edu.inac.controller;

import java.sql.SQLException;
import java.util.List;

import ar.edu.inac.dao.DAO;
import ar.edu.inac.modelo.Model;
import ar.edu.inac.modelo.Modulo;
import ar.edu.inac.modelo.exception.ModeloException;
import ar.edu.inac.modelo.validator.ModuloAgregarValidator;
import ar.edu.inac.modelo.validator.ModuloEliminarValidator;
import ar.edu.inac.modelo.validator.ModuloModificarValidator;
import ar.edu.inac.modelo.validator.ModuloValidator;
import ar.edu.inac.modelo.validator.Validator;

public class ModuloController implements Controller {

	

	@Override
	public void agregarController(Model model, DAO dao) throws ClassNotFoundException, SQLException, ModeloException {
		
		
		if(ModuloAgregarValidator.getInstance((Modulo)model).validarTodo())
			dao.agregar(model);
		else 
			throw new ModeloException(ModuloValidator.getErrorAcumulado(), "Debe completar los campos seg�nn lo sugerido");

	}

	@Override
	public void eliminarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException, ModeloException {
		if(ModuloEliminarValidator.getInstance((Modulo)model).validarTodo())
			dao.eliminar(model);
		throw new ModeloException(ModuloValidator.getErrorAcumulado(), "Debe completar los campos seg�nn lo sugerido");
	}

	@Override
	public void modificarController(Model model, DAO dao)
			throws ClassNotFoundException, SQLException, ModeloException {
		if(ModuloModificarValidator.getInstance((Modulo)model).validarTodo())
			dao.modificar(model);
		else
			throw new ModeloException(ModuloValidator.getErrorAcumulado(), "Debe completar los campos seg�nn lo sugerido");
			
		
	}

	@Override
	public List<Model> leer(Model model, DAO dao)
			throws ClassNotFoundException, SQLException {
		
		return dao.leer(model);
	}


}
