package ar.edu.inac.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.PantallaDAO;
import ar.edu.inac.dao.ProvinciaDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Pantalla;
import ar.edu.inac.modelo.Provincia;


public class PantallaDAOTest { //conecxion base de datos
		static Connection con;
		Statement stat;
		Pantalla pan = null;	
		
	@BeforeClass	
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
        Statement consulta= con.createStatement();

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( PantallaDAOTest.class.getResource( "PantallasCrear.sql" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma		
           }
        }
        cm.desconectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {		
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
        Statement consulta= con.createStatement(); //generea consulta

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( PantallaDAOTest.class.getResource( "PantallasEliminar.sql" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma
           }
        }
        
        cm.desconectar();
	}


	@Before
	public void setUp() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();

		 Statement consulta= con.createStatement();
	}

	@After
	public void tearDown() throws Exception {
		con.close();
	}
	
	@Test
	public void testAgregar() {
		PantallaDAO panDao = new PantallaDAO();
		try {
			//agregue una pantalla
			panDao.agregar(new Pantalla("nueva descripcion_test" , "nuevo nombre_test"));
			//hago una consulta
			ResultSet rs=stat.executeQuery("select pan_id, pan_descripcion" +
					" from pantallas" + 
					" where pan_descripcion='nueva descripcion_test'");
			rs.next();
			assertEquals(rs.getString("pan_descripcion"), "nueva descripcion_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testEliminar() {
		PantallaDAO panDao = new PantallaDAO();
		try {
					
			//hago una consulta buscando la pantalla a eliminar
			StringBuffer sql = new StringBuffer("select pan_id, pan_descripcion");
			sql.append(" from pantallas");
			sql.append(" where pan_descripcion='pantalla1_test'");
			
			ResultSet rs=stat.executeQuery(sql.toString());
			rs.next();			
			Pantalla pan = new Pantalla(rs.getInt("pan_id"), rs.getString("pan_descripcion"));			
			panDao.eliminar(pan);

			ResultSet rs2=stat.executeQuery(sql.toString());
			//next deberia darle false, o sea no hay un proximo
			assertFalse(rs2.next());
			rs.close();
			rs2.close();			
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testModificar() {
		PantallaDAO panDao = new PantallaDAO();
		try {
					
			//hago una consulta buscando la pantalla a eliminar
			StringBuffer sql = new StringBuffer("select pan_id,pan_nombre, pan_descripcion");
									 sql.append(" from pantallas"); 
									 sql.append(" where pan_descripcion=");
									 sql.append("'pantalla2_test'");
			ResultSet rs=stat.executeQuery(sql.toString());
			rs.next();			
			Pantalla pan = new Pantalla(rs.getInt("pan_id"), 
										rs.getString("pan_Nombre"),
										rs.getString("pan_descripcion"));
			pan.setDescripcion("pantalla2amodificado_aa_test");
			panDao.modificar(pan);

			StringBuffer sql2 = new StringBuffer("select pan_id,pan_nombre, pan_descripcion");
			 sql2.append(" from pantallas"); 
			 sql2.append(" where pan_descripcion=");
			 sql2.append("'pantalla2amodificado_aa_test'");

			
			ResultSet rs2=stat.executeQuery(sql2.toString());
			rs2.next();
			//next deberia darle false, o sea no hay un proximo
			assertEquals(rs2.getString("pan_descripcion"), "pantalla2amodificado_aa_test");
			rs.close();
			rs2.close();			
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	
	@Test
	public void testLeer_porDescripcion() {
		PantallaDAO panDao = new PantallaDAO();
		try {
			//leer una pantalla 
			List pantallas =panDao.leer(new Pantalla("pantalla3_test"));
			//hago una consulta
			Pantalla pan = (Pantalla)pantallas.get(0);
			assertEquals(pan.getDescripcion(),"pantalla3_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testLeer_porCodigo() {
		PantallaDAO panDao = new PantallaDAO();
		try {
			//hago una consulta buscando la pantalla a eliminar
			StringBuffer sql = new 
			StringBuffer("select pan_id, pan_descripcion, pan_nombre");
			sql.append(" from pantallas");
			sql.append(" where pan_descripcion='pantalla4_test'");
			ResultSet rs=stat.executeQuery(sql.toString());
		    rs.next();	
		    
			Pantalla pan = new Pantalla(rs.getInt("pan_id"));	

			//leer una pantalla 
			List pantallas =panDao.leer(new Pantalla("pantalla4_test"));
			//hago una consulta
			Pantalla panLeida = (Pantalla)pantallas.get(0);
			assertEquals(panLeida.getDescripcion(), "pantalla4_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testLeer_porNombre() {
		PantallaDAO panDao = new PantallaDAO();
		try {
			//leer una pantalla 
			List pantallas =panDao.leer(new Pantalla(null,"nombre5_test"));
			//hago una consulta
			Pantalla pan = (Pantalla)pantallas.get(0);
			assertEquals(pan.getNombre(),"nombre5_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		//TODO Jonathan y Vanina hacerlo con nombre5 y por nombre
		
	}
}