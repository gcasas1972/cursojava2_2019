-- curso
insert into cursos(cur_anio, cur_descripcion) values (10, 'curso 1_test')
-- temario 
insert into temarios(tem_descripcion) values ('temario 1_test')
-- materia
insert into materias(cur_id, tem_id, mat_nombre) values ((select max(cur_id) from cursos),(select max(tem_id) from temarios), 'materia 1_test')
-- profesor
insert into profesores (prof_nombre, prof_apellido) values ('nombre_test', 'apellido_test')
-- comision
insert into comisiones(com_division, com_turno, cur_id, mat_id, prof_id) values('comision_test', 'ma�ana',(select max(cur_id) from cursos),(select max(mat_id) from materias), (select max(prof_id) from profesores))
-- usuario
insert into usuarios(usu_descripcion, usu_password) values ('usuario_test', '123')
-- alumnos
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id,usu_id) values ('Apellido_test','nombre_test',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'), (select max(usu_id) from usuarios))
-- evaluacion
insert into evaluaciones(mat_id, alu_id, eval_estado, eval_fecharealizacion) values ((select max(mat_id) from materias), (select max(alu_id) from alumnos), 1, '1972-11-17')
insert into evaluaciones(mat_id, alu_id, eval_estado, eval_fecharealizacion) values ((select max(mat_id) from materias), (select max(alu_id) from alumnos), 1, '1972-11-18')
insert into evaluaciones(mat_id, alu_id, eval_estado, eval_fecharealizacion) values ((select max(mat_id) from materias), (select max(alu_id) from alumnos), 1, '1972-11-19')
insert into evaluaciones(mat_id, alu_id, eval_estado, eval_fecharealizacion) values ((select max(mat_id) from materias), (select max(alu_id) from alumnos), 1, '1972-11-20')
