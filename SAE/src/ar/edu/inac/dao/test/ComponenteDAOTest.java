package ar.edu.inac.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.ComponenteDAO;
import ar.edu.inac.dao.PantallaDAO;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Componente;
import ar.edu.inac.modelo.Pantalla;


/**
 * @author Diego Fecha: 28/03/2019 Esta clase corresponde a la clase Componente
 *         de DAOtest para el acceso a la base de datos.
 */

public class ComponenteDAOTest {
	static Connection con;
	Statement stat;
	Componente comp = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		// 1- este es el primer metodo en ejecutarse y
		// normalmente esta relacionado con el lote de pruebas.

		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();

		Statement consulta = con.createStatement();

		String sql = "";
		BufferedReader bf = new BufferedReader(new InputStreamReader(
				ComponenteDAOTest.class.getResource("ComponentesCrear.sql")
						.openStream()));
		while ((sql = bf.readLine()) != null) {
			if (sql.trim().length() != 0 && !sql.startsWith("--")) {
				consulta.executeUpdate(sql); // aca arma el lote de pruebas.
			}
		}
		cm.desconectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		// este es el ultimo metodo en ejecutarse.
		// normalmente relacionado con la eliminacion del lote de prueba.

		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();

		Statement consulta = con.createStatement();

		String sql = "";
		
		BufferedReader bf = new BufferedReader(new InputStreamReader(
				ComponenteDAOTest.class.getResource("ComponentesEliminar.sql")
						.openStream()));
		while ((sql = bf.readLine()) != null) {
			if (sql.trim().length() != 0 && !sql.startsWith("--")) {
				consulta.executeUpdate(sql); // aca arma
			}
		}

		cm.desconectar();
	}

	@Before
	public void setUp() throws Exception {
		// 2-se ejecuta antes de cada testeo.
		// ejecuta la linea de codigo.

		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();

	}

	@After
	public void tearDown() throws Exception {
		// 3- Despues de cada testeo.
		// cierra la conexion.

		con.close();
	}

	@Test
	public void testAgregar() {
		ComponenteDAO compDao = new ComponenteDAO();
		PantallaDAO panDAO = new PantallaDAO();
		try {
			// hago una consulta
			ResultSet rs = stat.executeQuery(	"select pan_id, pan_descripcion"
												+ " from pantallas"
												+ " where pan_descripcion = 'pantalla3_test'");
			rs.next();
			
			Pantalla pan = new Pantalla(rs.getInt("pan_id"), rs.getString("pan_descripcion"));
			Componente comp = new Componente(pan, "nuevo componente_test");
			
			
			// agregue un componente
			compDao.agregar(comp);
			
			// hago una consulta
			ResultSet rs2 = stat.executeQuery(	"select pan_id, comp_id, comp_descripcion"
												+ " from componentes"
												+ " where comp_descripcion = 'nuevo componente_test'");
			rs2.next();
			assertEquals(rs2.getString(	"comp_descripcion"),
										"nuevo componente_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

	@Test
	public void testEliminar() {
		ComponenteDAO compDao = new ComponenteDAO();
		try {
			// hago una consulta buscando el componente a eliminar
			ResultSet rs = stat.executeQuery("select comp_id, comp_descripcion"
							+ " from componentes"
							+ " where comp_descripcion = 'sarasa_test'");
			rs.next();
			
			Componente comp = new Componente(rs.getInt("comp_id"), rs.getString("comp_descripcion"));
			
			compDao.eliminar(comp);

			ResultSet rs2 = stat.executeQuery("select comp_id, comp_descripcion"
							+ " from componentes"
							+ " where comp_descripcion = 'sarasa_test'");
			// next deberia darle false, o sea no hay un proximo
			assertFalse(rs2.next());
			rs.close();
			rs2.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

	
	@Test
	public void testModificar_Descripcion() {
		ComponenteDAO compDao = new ComponenteDAO();
		try {

			// hago una consulta buscando el componente a eliminar
			ResultSet rs = stat.executeQuery("select comp_id, comp_descripcion"
					+ " from componentes"
					+ " where comp_descripcion = 'sisupiera_test'");
			rs.next();
			Componente comp = new Componente(rs.getInt("comp_id"),
											rs.getString("comp_descripcion"));
											comp.setDescripcion("sisupiera modificado_test");
											
			compDao.modificar(comp);

			ResultSet rs2 = stat.executeQuery("select comp_id, comp_descripcion"
							+ " from componentes"
							+ " where comp_descripcion = 'sisupiera modificado_test'");
			rs2.next();
			// next deberia darle false, o sea no hay un proximo
			assertEquals(rs2.getString("comp_descripcion"),
						"sisupiera modificado_test");
			rs.close();
			rs2.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

	@Test
	public void testLeer_porDescripcion() {
		ComponenteDAO compDao = new ComponenteDAO();
		try {
			// leer un componente
			List componentes = compDao.leer(new Componente("123_test"));
			// hago una consulta
			Componente comp = (Componente) componentes.get(0);
			assertEquals(comp.getDescripcion(), "123_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

	@Test
	public void testLeer_porCodigo() {
		ComponenteDAO compDao = new ComponenteDAO();
		try {
			// hago una consulta buscando el componente a eliminar
			ResultSet rs = stat.executeQuery("select comp_id, comp_descripcion"
											+ " from componentes"
											+ " where comp_descripcion = '123_test'");
			rs.next();
			Componente comp = new Componente(rs.getInt("comp_id"));

			// leer un componente
			List componentes = compDao.leer(comp);
			// hago una consulta
			Componente compLeida = (Componente) componentes.get(0);
			assertEquals(compLeida.getDescripcion(), "123_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

}
