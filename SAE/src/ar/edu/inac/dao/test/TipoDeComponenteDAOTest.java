package ar.edu.inac.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.TipoDeComponenteDAO;
import ar.edu.inac.dao.TipoDePreguntaDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.TipoDeComponente;

public class TipoDeComponenteDAOTest {
	static Connection con;
	Statement stat;
	TipoDeComponente tipoComp=null;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		        
	    Statement consulta= con.createStatement();

	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( TipoDeComponenteDAOTest.class.getResource( "tipoDecomponenteCrear.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql ); // aca arma
	       }
	    }
	    cm.desconectar();
	}


	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
	    Statement consulta= con.createStatement();

	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( TipoDeComponenteDAOTest.class.getResource( "TipoDeComponenteEliminar.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql ); // aca arma
	       }
	    }
	    
	    cm.desconectar();
	}

	@Before
	public void setUp() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();

	}

	@After
	public void tearDown() throws Exception {
		con.close();
	}

	@Test
	public void testAgregar() {
		TipoDeComponenteDAO tipDao = new TipoDeComponenteDAO();
		try {
			tipDao.agregar(new TipoDeComponente(0, "tipoDeCom nuevo_test"));
			
			StringBuffer sql = new StringBuffer("select tcomp_id, tcomp_descripcion");
			sql.append(" from tiposdecomponentes");
			sql.append(" where tcomp_descripcion=");
			sql.append("'tipoDeCom nuevo_test'");	
			
			ResultSet rs=stat.executeQuery(sql.toString());
			rs.next();
			assertEquals(rs.getString("tcomp_descripcion"), "tipoDeCom nuevo_test");
			
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		
	}

	@Test
	public void testEliminar() {
		fail("Not yet implemented");
	}

	@Test
	public void testModificar() {
		fail("Not yet implemented");
	}

	@Test
	public void testLeer() {
		fail("Not yet implemented");
	}

}
