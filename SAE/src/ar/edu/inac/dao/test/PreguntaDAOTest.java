package ar.edu.inac.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.PreguntaDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Alumno;
import ar.edu.inac.modelo.Comision;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Evaluacion;
import ar.edu.inac.modelo.Materia;
import ar.edu.inac.modelo.Modulo;
import ar.edu.inac.modelo.Partido;
import ar.edu.inac.modelo.Pregunta;
import ar.edu.inac.modelo.Profesor;
import ar.edu.inac.modelo.Provincia;
import ar.edu.inac.modelo.Respuesta;
import ar.edu.inac.modelo.Temario;
import ar.edu.inac.modelo.TipoDePregunta;

public class PreguntaDAOTest {
	static Connection con;
	Statement stat;
	Pregunta preg = null;
	Partido partidoTest = null;
	Provincia provinciaTest = null;
	Alumno alumnoTest = null;
	Profesor profesorTest = null;
	Comision comisionTest = null;
	Modulo moduloTest = null;
	Materia materiaTest = null;
	Curso cursoTest = null;
	Temario temarioTest = null;
	Evaluacion evaluacionTest = null;
	TipoDePregunta tipoPreguntaTest = null;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		Statement consulta = con.createStatement();
		String sql = "";
		BufferedReader bf = new BufferedReader(new InputStreamReader(ProfesorDAOTest.class.getResource("PreguntasCrear.sql").openStream()));
		while((sql=bf.readLine())!=null){
			if(sql.trim().length()!=0 && !sql.startsWith("--")){
				consulta.executeUpdate(sql);
			}
		}
		cm.desconectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
        Statement consulta= con.createStatement();
        String sql = "";
        BufferedReader bf = new BufferedReader(new InputStreamReader(ProfesorDAOTest.class.getResource("PreguntasEliminar.sql").openStream()));
        while((sql = bf.readLine()) != null){
        	if(sql.trim().length()!=0 && !sql.startsWith("--")){
        		consulta.executeUpdate(sql);
        	}
        }
        cm.desconectar();
	}

	@Before
	public void setUp() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();
		StringBuffer sql = new StringBuffer("select ");
		sql.append("e.eval_id, e.mat_id, e.alu_id, e.eval_estado, e.eval_fecharealizacion, ");
		sql.append("a.alu_id, a.part_id, a.prov_id, a.com_id, a.alu_nombre, a.alu_apellido, a.alu_direccion, a.alu_telefono, a.alu_dni, a.alu_email, ");
		sql.append("a.part_id, a.prov_id, ");
		sql.append("com.com_id, com.cur_id, com.mat_id, com.prof_id, com.tur_id, com.div_id, ");
		sql.append("p.prof_id, p.prof_nombre, p.prof_apellido, p.prof_direccion, p.prof_telefono, p.prof_dni, p.prof_email, p.prof_iosfa, ");
		sql.append("part.part_id, part.prov_id, part.part_nombre, ");
		sql.append("prov.prov_id, prov.prov_nombre, ");
		sql.append("modu.mod_id, modu.mat_id, modu.mod_descripcion, ");
		sql.append("mat.mat_id, mat.cur_id, mat.mat_nombre, mat.tem_id, ");
		sql.append("c.cur_id, c.cur_anio, c.cur_descripcion, ");
		sql.append("t.tem_id, t.tem_descripcion, ");
		sql.append("pr.preg_id, pr.preg_pathgrafico, pr.preg_explicacion, pr.preg_texto, ");
		sql.append("re.res_id,"); //gcasas se agrego el res_id que faltaba
		sql.append("re.res_texto, re.RES_ISCORRECTA, ");
		sql.append("tpre.tpreg_id  , tpre.tpreg_descripcion ");
		
		sql.append("from evaluaciones e, alumnos a, comisiones com, profesores p, partidos part, provincias prov, modulos modu, materias mat, cursos c, temarios t, preguntas pr, respuestas re, tipopregunta tpre ");
		sql.append("where e.alu_id=a.alu_id and ");	//Evaluacion y Alumno				ALUMNO y EVALUACION
		sql.append("e.mat_id=mat.mat_id and ");		//Evaluacion y Materia				MATERIA
		sql.append("a.com_id=com.com_id and ");		//Alumno y Comision					COMISION
		sql.append("com.prof_id=p.prof_id and ");	//Comision y Profesor				PROFESOR
		sql.append("a.part_id=part.part_id and ");	//Alumno y Partido				    PARTIDO
		sql.append("a.prov_id=prov.prov_id and ");	//Alumno y Provincia				PROVINCIA
		sql.append("mat.mat_id=modu.mat_id and ");	//Materia y Modulo					MODULO
		sql.append("mat.cur_id=c.cur_id and ");		//Materia y Curso					CURSO
		sql.append("mat.tem_id=t.tem_id and ");		//Materia y Temario					TEMARIO
		sql.append("pr.eval_id= e.eval_id and ");	//Pregunta y Evaluacion				PREGUNTA	
		sql.append("re.preg_id= pr.preg_id");		//Respuesta y Pregunta				RESPUESTA
		//TODO Patricio agregar tipo de pregunta 
		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();
		//obtengo todos los objetos que son unicos
		temarioTest = new Temario(rs.getInt("tem_id"),rs.getString("tem_descripcion"));
		cursoTest = new Curso(rs.getInt("cur_id"),rs.getInt("cur_anio"),rs.getString("cur_descripcion"));
		//TODO Vanina la materia debe tener un curso
		
		materiaTest = new Materia(rs.getInt("mat_id"),rs.getString("mat_nombre"),temarioTest);
		materiaTest.setCurso(cursoTest);
		//TODO Jonathan el modulo debe tener materia
		//TODO  Claudio la comision debe tener curso, materia, profesor, turno y division
		moduloTest = new Modulo(rs.getInt("mod_id"),rs.getString("mod_descripcion"));
		moduloTest.setMateria(materiaTest);
		provinciaTest = new Provincia(rs.getInt("prov_id"),rs.getString("prov_nombre"));
		//provincias y partidos se llenaron con null
		partidoTest = new Partido(rs.getInt("part_id"),rs.getString("part_nombre"),provinciaTest);
		profesorTest = new Profesor(rs.getInt("prof_id"),rs.getString("prof_nombre"),rs.getString("prof_apellido"),rs.getString("prof_direccion"),null,null,rs.getString("prof_telefono"),rs.getString("prof_dni"),rs.getString("prof_email"),rs.getString("prof_iosfa"),null);
		comisionTest = new Comision(rs.getInt("com_id"));
		alumnoTest = new Alumno(rs.getString("alu_nombre"),rs.getString("alu_apellido"),rs.getString("alu_direccion"),rs.getString("alu_telefono"),rs.getString("alu_dni"),rs.getString("alu_email"),comisionTest,partidoTest,provinciaTest);

		evaluacionTest = new Evaluacion(rs.getInt("eval_id"),materiaTest, alumnoTest, rs.getInt("eval_estado"),0, rs.getDate("eval_fecharealizacion"),null, null);
		tipoPreguntaTest = new TipoDePregunta(rs.getInt("tpreg_id"), rs.getString("tpreg_descripcion"));
		
		
		//va a haber un do while de preguntas
		 while (rs.next()){
			Pregunta preg = new Pregunta(rs.getInt("preg_id"),
					null, //aca van las respeustas, ver de agregar un List
					rs.getString("preg_texto"), 
					rs.getString("preg_pathgrafico"), rs.getString("preg_explicacion"),
					materiaTest, moduloTest, evaluacionTest);
			//va a haber un while de respuestas,buscar la condicion de corte 
			//TODO Gabriel verificar la estructura de la respueta
			//la primer respuesta
				  Respuesta res = new Respuesta(rs.getInt("res_id")					,
						  						rs.getString("res_texto")			,
						  						rs.getBoolean("re.RES_ISCORRECTA"))	;
				  preg.addRespuestas(res);
			while (rs.next() && preg.getCodigo() == rs.getInt("preg_id") ){
				res = new Respuesta(rs.getInt("res_id"),
  						rs.getString("res_texto")			,
  						rs.getBoolean("re.RES_ISCORRECTA"))	;
				preg.addRespuestas(res);				
			}
		}
		rs.close();
	}

	@After
	public void tearDown() throws Exception {
		con.close();
		preg = null;
		tipoPreguntaTest = null;
		evaluacionTest = null;
		partidoTest = null;
		provinciaTest = null;
		alumnoTest = null;
		profesorTest = null;
		comisionTest = null;
		moduloTest = null;
		materiaTest = null;
		cursoTest = null;
		temarioTest = null;
	}

	@Test
	public void testAgregar() {
		PreguntaDAO pregDao = new PreguntaDAO();
		try {
			pregDao.agregar(new Pregunta(0, null,"Pregunta2_test","Path2_test","Explicacion2_test",materiaTest,moduloTest,evaluacionTest));
			ResultSet rs = stat.executeQuery("select preg_texto, preg_pathgrafico from preguntas where preg_texto='Pregunta2_test'");
			rs.next();
			assertEquals(rs.getString("preg_texto"),"Pregunta2_test");
			rs.close();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testEliminar() {
		PreguntaDAO pregDao = new PreguntaDAO();
		try {
			ResultSet rs=stat.executeQuery("select preg_texto, preg_explicacion, preg_pathgrafico, mat_id, mod_id, eval_id, preg_id from preguntas where preg_texto='Texto_test'");
			rs.next();
			Pregunta preg = new Pregunta(rs.getInt("preg_id"),null,rs.getString("preg_texto"),rs.getString("preg_pathgrafico"),rs.getString("preg_explicacion"),materiaTest,moduloTest,evaluacionTest);
			pregDao.eliminar(preg);
			ResultSet rs2=stat.executeQuery("select preg_texto, preg_explicacion, preg_pathgrafico, mat_id, mod_id, eval_id, preg_id from preguntas where preg_texto='Texto_test'");
			assertFalse(rs2.next());
			rs.close();
			rs2.close();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testModificar() {
		PreguntaDAO pregDao = new PreguntaDAO();
		try {
			ResultSet rs = stat.executeQuery("select preg_texto, preg_explicacion, preg_pathgrafico, mat_id, mod_id, eval_id, preg_id from preguntas where preg_texto='Texto2_test'");
			rs.next();
			Pregunta preg = new Pregunta(rs.getInt("preg_id"),null,rs.getString("preg_texto"),rs.getString("preg_pathgrafico"),rs.getString("preg_explicacion"),materiaTest,moduloTest,evaluacionTest);
			preg.setTexto("TextoModificado_test");
			pregDao.modificar(preg);
			ResultSet rs2 = stat.executeQuery("select preg_texto, preg_explicacion, preg_pathgrafico, mat_id, mod_id, eval_id, preg_id from preguntas where preg_texto='TextoModificado_test'");
			rs2.next();
			assertEquals(rs2.getString("preg_texto"),"TextoModificado_test");
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}

	@Test
	public void testLeer_porTexto() {
		PreguntaDAO pregDao = new PreguntaDAO();
		try {
			List preguntas = pregDao.leer(new Pregunta(0, null,"Texto3_test",null,null,materiaTest,moduloTest,evaluacionTest));
			Pregunta preg = (Pregunta)preguntas.get(0);
			assertEquals(preg.getTexto(),"Texto3_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testLeer_porCodigo() {
		PreguntaDAO pregDao = new PreguntaDAO();
		try {
			ResultSet rs = stat.executeQuery("select preg_texto, preg_id from preguntas where preg_texto='Texto3_test'");
			rs.next();
			Pregunta preg = new Pregunta(rs.getInt("preg_id"),null,null,null,null,materiaTest,moduloTest,evaluacionTest);
			List preguntas=pregDao.leer(preg);
			Pregunta pregLeida = (Pregunta)preguntas.get(0);
			assertEquals(pregLeida.getPathGrafico(),"Path3_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
}
