package ar.edu.inac.dao.test;


import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.ProfesorDao;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Partido;
import ar.edu.inac.modelo.Profesor;
import ar.edu.inac.modelo.Provincia;

public class ProfesorDAOTest {
	static Connection con;
	Statement stat;
	Profesor prof = null;
	//objetos adicionales necesarios para el testeo
	Partido partidoTest 	= null;
	Provincia provinciaTest = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		Statement consulta = con.createStatement();
		String sql = "";
		BufferedReader bf = new BufferedReader(new InputStreamReader(ProfesorDAOTest.class.getResource("ProfesoresCrear.sql").openStream()));
		while((sql=bf.readLine())!=null){
			if(sql.trim().length()!=0 && !sql.startsWith("--")){
				consulta.executeUpdate(sql);
			}
		}
		cm.desconectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
        Statement consulta= con.createStatement();
        String sql = "";
        BufferedReader bf = new BufferedReader(new InputStreamReader(ProfesorDAOTest.class.getResource("ProfesoresEliminar.sql").openStream()));
        while((sql = bf.readLine()) != null){
        	if(sql.trim().length()!=0 && !sql.startsWith("--")){
        		consulta.executeUpdate(sql);
        	}
        }
        cm.desconectar();
	}

	@Before
	public void setUp() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();
		//debo obtener el partido y la provincia para ser utilizado en los testeos
		StringBuffer sql = new StringBuffer("select prov.prov_id, prov.prov_nombre, part.part_id, part.part_nombre, part.prov_id ");
		sql.append("from provincias prov, partidos part ");
		sql.append("where part.prov_id=prov.prov_id ");
		sql.append("and prov.prov_nombre='Provincia_test'");
		
		ResultSet rs= stat.executeQuery(sql.toString());
		rs.next();
		provinciaTest = new Provincia(rs.getInt("prov_id")		, 
								rs.getString("prov_nombre"));
		partidoTest = new Partido(rs.getInt("part_id"), rs.getString("part_nombre"), provinciaTest);
		
		
		
	}

	@After
	public void tearDown() throws Exception {
		provinciaTest 	= null;
		partidoTest		= null;
		con.close();
	}
	
	@Test
	public void testAgregar() {
		ProfesorDao profDao = new ProfesorDao();
		try {
			//Agrego un profesor
			Profesor profNuevo = new Profesor(0, "Gabriel Nuevo_test", "Casas Nuevo_test", "nueva dir", partidoTest, provinciaTest, "123456",
												"22965_test", "gcasas1972_nuevo@gmail_test", "123456_test", null);
			profDao.agregar(profNuevo);
			//Hago una consulta
			StringBuffer sb = new StringBuffer("select prof.prof_id, prof.part_id, part.part_id, part.part_nombre, prof.prov_id, ");
			sb.append("prov.prov_id, prov.prov_nombre, prof.prof_nombre, prof.prof_apellido, prof.prof_direccion, ");
			sb.append("prof.prof_telefono, prof.prof_dni, prof.prof_email, prof.prof_iosfa ");
			sb.append("from  profesores prof, partidos part, provincias prov ");
			sb.append("where prof.part_id = part.part_id and ");
			sb.append("prof.prov_id = prov.prov_id and ");
			sb.append("prof.prof_nombre = 'Gabriel Nuevo_test'");
			ResultSet rs = stat.executeQuery(sb.toString());
			rs.next();
			assertEquals(rs.getString("prof_nombre"), "Gabriel Nuevo_test"	);
			assertEquals(rs.getInt("part_id"), partidoTest.getCodigo()	);
			assertEquals(rs.getInt("prov_id"), provinciaTest.getCodigo());
			//TODO Karen, tenes que seguir con el esto de los campos de profedor
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testEliminar() {
		ProfesorDao profDao = new ProfesorDao();
		try {
			
			ResultSet rs=stat.executeQuery(	"select prof_id, prof_nombre, prof_apellido " +
											"from profesores " +
											"where prof_nombre='Gabriel4_test'");
			rs.next();
			Profesor prof = new Profesor(	rs.getInt("prof_id"),
											rs.getString("prof_nombre"),
											rs.getString("prof_apellido"), 
											"Direccion", 
											partidoTest, 
											provinciaTest, 
											"123456", 
											"123456", 
											"email", 
											"123456/00", 
											null);
			profDao.eliminar(prof);
			ResultSet rs2=stat.executeQuery("select prof_id, prof_nombre, prof_apellido" +
											" from profesores" + 
											" where prof_nombre='Gabriel_test4'");
			assertFalse(rs2.next());
			rs2.close();
			rs.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testModificar() {
		ProfesorDao ProfDao = new ProfesorDao();
		try {
			ResultSet rs=stat.executeQuery(	"select prof_id, prof_nombre, prof_apellido " +
											"from profesores where prof_nombre='Gabriel3_test'");
			rs.next();
			Profesor prof = new Profesor(	rs.getInt("prof_id"),
											rs.getString("prof_nombre"),
											rs.getString("prof_apellido"),
											"Direccion",
											partidoTest,
											provinciaTest,
											"123",
											"123",
											"123",
											"123",
											null);
			prof.setNombre("Lautaro_test");
			ProfDao.modificar(prof);
			ResultSet rs2=stat.executeQuery(	"select prof_id, prof_nombre, prof_apellido " +
												"from profesores " +
												"where prof_nombre='Lautaro_test'");
			rs2.next();
			assertEquals(rs2.getString("prof_nombre"), "Lautaro_test");
			rs.close();
			rs2.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testLeer_porNombre() {
		ProfesorDao profDao = new ProfesorDao();
		try {
			List profesores = profDao.leer(new Profesor("Gabriel_test","Casas_test"));
			Profesor prof = (Profesor)profesores.get(0);
			assertEquals(prof.getNombre(), "Gabriel_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testLeer_porApellido() {
		ProfesorDao profDao = new ProfesorDao();
		try {
			ResultSet rs=stat.executeQuery(	"select prof_id, prof_apellido, prof_nombre " +
											"from profesores " +
											"where prof_apellido='Casas2_test'");
			rs.next();
			Profesor prof = new Profesor(	rs.getInt("prof_id"),
											rs.getString("prof_nombre"),
											rs.getString("prof_apellido"),
											null,
											partidoTest,
											provinciaTest,
											null,
											null,
											null,
											null,
											null);
			List profesores = profDao.leer(prof);
			Profesor profLeido = (Profesor)profesores.get(0);
			assertEquals(profLeido.getApellido(), "Casas2_test");
			rs.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
}
