package ar.edu.inac.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.ItemDeTemarioDAO;
import ar.edu.inac.dao.ProvinciaDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.ItemDeTemario;
import ar.edu.inac.modelo.Provincia;
import ar.edu.inac.modelo.Temario;


public class ItemDeTemarioDAOTest {
		static Connection con;
		Statement stat;
		ItemDeTemario item = null;
	@BeforeClass	
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
        Statement consulta= con.createStatement();

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( ItemDeTemarioDAOTest.class.getResource( "ItemDeTemarioCrear.sql" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma
           }
        }
        cm.desconectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {		
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
        Statement consulta= con.createStatement();

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( ItemDeTemarioDAOTest.class.getResource( "ItemDeTemarioEliminar.sql" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma
           }
        }
        
        cm.desconectar();
	}


	@Before
	public void setUp() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();

	}

	@After
	public void tearDown() throws Exception {
		con.close();
	}
	
	@Test
	public void testAgregar() {
		ItemDeTemarioDAO itemDao = new ItemDeTemarioDAO();
		try {
			//agregue un item de temario
			//debo obtener  de la base datos el temario
			ResultSet rs2=stat.executeQuery("select tem_id,tem_descripcion " +
					" from temarios"+
					" where tem_descripcion='temario1_test'");
			rs2.next();
			Temario tem=new Temario(rs2.getInt("tem_id"), rs2.getString("tem_descripcion"));
			
			ItemDeTemario item=new ItemDeTemario("nuevo itemdetemario_test", tem);
			itemDao.agregar(item);
			//hago una consulta
			ResultSet rs=stat.executeQuery("select item_id, item_descripcion" +
					" from ItemsDeTemario" + 
					" where item_descripcion='nuevo itemdetemario_test'");
			rs.next();
			assertEquals(rs.getString("item_descripcion"), "nuevo itemdetemario_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testEliminar() {
		ItemDeTemarioDAO itemDao = new ItemDeTemarioDAO();
		try {
					
			//hago una consulta buscando el item de temario a eliminar
			ResultSet rs=stat.executeQuery("select item_id, item_descripcion" +
					" from ItemsDeTemario" + 
					" where item_descripcion='itemsDeTemario1_test'");
			rs.next();			
			ItemDeTemario item = new ItemDeTemario(rs.getInt("item_id"), rs.getString("item_descripcion"));			
			itemDao.eliminar(item);
			ResultSet rs2=stat.executeQuery("select item_id, item_descripcion" +
					" from ItemsDeTemario" + 
					" where item_descripcion='itemsDeTemario1_test'");
			//next deberia darle false, o sea no hay un proximo
			assertFalse(rs2.next());
			rs.close();
			rs2.close();			
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testModificar() {
		ItemDeTemarioDAO itemDao = new ItemDeTemarioDAO();
		try {
					
			//hago una consulta buscando el item de temario a eliminar
			ResultSet rs=stat.executeQuery("select item_id, item_descripcion" +
					" from itemsDeTemario" + 
					" where item_descripcion='itemsDeTemario2_test'");
			rs.next();			
			ItemDeTemario item = new ItemDeTemario(rs.getInt("item_id"), rs.getString("item_descripcion"));	
			item.setDescripcion("itemsDeTemario2 modificado_test");
			itemDao.modificar(item);

			ResultSet rs2=stat.executeQuery("select item_id, item_descripcion" +
					" from itemsDeTemario" + 
					" where item_descripcion='itemsDeTemario2 modificado_test'");
			rs2.next();
			//next deberia darle false, o sea no hay un proximo
			assertEquals(rs2.getString("item_descripcion"), "itemsDeTemario2 modificado_test");
			rs.close();
			rs2.close();			
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testLeer_porDescripcion() {
		ItemDeTemarioDAO itemDao = new ItemDeTemarioDAO();
		try {
			//leer un item de temario 
			List itemDeTemario =itemDao.leer(new ItemDeTemario("itemsDeTemario3_test"));
			//hago una consulta
			ItemDeTemario item = (ItemDeTemario)itemDeTemario.get(0);
			assertEquals(item.getDescripcion(), "itemsDeTemario3_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	@Test
	public void testLeer_porCodigo() {
		ItemDeTemarioDAO itemDao = new ItemDeTemarioDAO();
		try {
			//hago una consulta buscando el item de temario a eliminar
			ResultSet rs=stat.executeQuery("select item_id, item_descripcion" +
					" from itemsDeTemario" + 
					" where item_descripcion='itemsDeTemario4_test'");
			rs.next();			
			ItemDeTemario item = new ItemDeTemario(rs.getInt("item_id"));	

			//leer un item de temario 
			List itemDeTemario =itemDao.leer(item);
			//hago una consulta
			ItemDeTemario itemLeida = (ItemDeTemario)itemDeTemario.get(0);
			assertEquals(itemLeida.getDescripcion(), "itemsDeTemario4_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

}
