package ar.edu.inac.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.ProvinciaDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Provincia;


public class ProvinciaDAOTest {
		static Connection con;
		Statement stat;
		Provincia prov = null;
	@BeforeClass	
	public static void setUpBeforeClass() throws Exception {
		//1-este es el primer metodo en ejecutarse y 
		//normalmente esta relacionado con el lote de pruebas.
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
        Statement consulta= con.createStatement();

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( ProvinciaDAOTest.class.getResource( "ProvinciasCrear.sql" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma
           }
        }
        cm.desconectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		//este es el ultimo metodo en ejecutarse
		//noremalmente relacionado con la eliminacion del lote de pruebas
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
        Statement consulta= con.createStatement();

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( ProvinciaDAOTest.class.getResource( "ProvinciasEliminar.sql" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma
           }
        }
        
        cm.desconectar();
	}


	@Before
	public void setUp() throws Exception {
		//2-se ejecuta antes de cada testeo
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();

	}

	@After
	public void tearDown() throws Exception {
		//3-Despues de cada testeo
		con.close();
	}
	
	@Test
	public void testAgregar() {
		ProvinciaDAO provDao = new ProvinciaDAO();
		try {
			//agregque una provincia
			provDao.agregar(new Provincia("nueva provincia_test"));
			//hago una consulta
			ResultSet rs=stat.executeQuery("select prov_id, prov_nombre" +
					" from provincias" + 
					" where prov_nombre='nueva provincia_test'");
			rs.next();
			assertEquals(rs.getString("prov_nombre"), "nueva provincia_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testEliminar() {
		ProvinciaDAO provDao = new ProvinciaDAO();
		try {
					
			//hago una consulta buscando la provincia a eliminar
			ResultSet rs=stat.executeQuery("select prov_id, prov_nombre" +
					" from provincias" + 
					" where prov_nombre='Buenos Aires_test'");
			rs.next();			
			Provincia prov = new Provincia(rs.getInt("prov_id"), rs.getString("prov_nombre"));			
			provDao.eliminar(prov);

			ResultSet rs2=stat.executeQuery("select prov_id, prov_nombre" +
					" from provincias" + 
					" where prov_nombre='Buenos Aires_test'");
			//next deberia darle false, o sea no hay un proximo
			assertFalse(rs2.next());
			rs.close();
			rs2.close();			
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testModificar() {
		ProvinciaDAO provDao = new ProvinciaDAO();
		try {
					
			//hago una consulta buscando la provincia a eliminar
			ResultSet rs=stat.executeQuery("select prov_id, prov_nombre" +
					" from provincias" + 
					" where prov_nombre='Cordoba_test'");
			rs.next();			
			Provincia prov = new Provincia(rs.getInt("prov_id"), rs.getString("prov_nombre"));	
			prov.setDescripcion("Cordoba modificado_test");
			provDao.modificar(prov);

			ResultSet rs2=stat.executeQuery("select prov_id, prov_nombre" +
					" from provincias" + 
					" where prov_nombre='Cordoba modificado_test'");
			rs2.next();
			//next deberia darle false, o sea no hay un proximo
			assertEquals(rs2.getString("prov_nombre"), "Cordoba modificado_test");
			rs.close();
			rs2.close();			
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testLeer_porNommbre() {
		ProvinciaDAO provDao = new ProvinciaDAO();
		try {
			//leer una provincia 
			List provincias =provDao.leer(new Provincia("Entre Rios_test"));
			//hago una consulta
			Provincia prov = (Provincia)provincias.get(0);
			assertEquals(prov.getDescripcion(), "Entre Rios_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	@Test
	public void testLeer_porCodigo() {
		ProvinciaDAO provDao = new ProvinciaDAO();
		try {
			//hago una consulta buscando la provincia a eliminar
			ResultSet rs=stat.executeQuery("select prov_id, prov_nombre" +
					" from provincias" + 
					" where prov_nombre='Mendoza_test'");
			rs.next();			
			Provincia prov = new Provincia(rs.getInt("prov_id"));	

			//leer una provincia 
			List provincias =provDao.leer(prov);
			//hago una consulta
			Provincia provLeida = (Provincia)provincias.get(0);
			assertEquals(provLeida.getDescripcion(), "Mendoza_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

}
