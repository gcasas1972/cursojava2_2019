package ar.edu.inac.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.CursoDao;
import ar.edu.inac.dao.EvaluacionDao;
import ar.edu.inac.dao.ProvinciaDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.dao.util.FechaUtil;
import ar.edu.inac.modelo.Alumno;
import ar.edu.inac.modelo.Comision;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Evaluacion;
import ar.edu.inac.modelo.Materia;
import ar.edu.inac.modelo.Provincia;
import ar.edu.inac.modelo.Temario;

public class EvalaluacionDaoTest {
	static Connection con;
	Statement stat;
	
	
@BeforeClass	
public static void setUpBeforeClass() throws Exception {
	ConnectionManager cm = new ConnectionManager();
	cm.conectar();
	con = cm.getConexion();
	
    Statement consulta= con.createStatement();

    String sql = "";
    BufferedReader bf = new BufferedReader( new InputStreamReader( ProvinciaDAOTest.class.getResource( "EvaluacionesCrear.sql" ).openStream() ) );
    while ( (sql = bf.readLine()) != null ) {
       if ( sql.trim().length() != 0 &&
            !sql.startsWith( "--" ) ) {              
          consulta.executeUpdate( sql ); // aca arma
       }
    }
    cm.desconectar();
}

@AfterClass
public static void tearDownAfterClass() throws Exception {		
	ConnectionManager cm = new ConnectionManager();
	cm.conectar();
	con = cm.getConexion();
	
    Statement consulta= con.createStatement();

    String sql = "";
    BufferedReader bf = new BufferedReader( new InputStreamReader( ProvinciaDAOTest.class.getResource( "EvaluacionesEliminar.sql" ).openStream() ) );
    while ( (sql = bf.readLine()) != null ) {
       if ( sql.trim().length() != 0 &&
            !sql.startsWith( "--" ) ) {              
          consulta.executeUpdate( sql ); // aca arma
       }
    }
    
    cm.desconectar();
}


@Before
public void setUp() throws Exception {
	ConnectionManager cm = new ConnectionManager();
	cm.conectar();
	con = cm.getConexion();
	stat = con.createStatement();
	

}

@After
public void tearDown() throws Exception {
	con.close();
}

@Test
public void testAgregar() {
	EvaluacionDao evalDao = new EvaluacionDao();
	try {
		//agregque una evaluacion
		
		evalDao.agregar(new Evaluacion(getMateria(), getAlumno(), 1, FechaUtil.asDate(1996,9, 19), null));
		//hago una consulta
		StringBuffer sql = new StringBuffer("select eval_id, mat_id, alu_id, eval_estado, eval_fecharealizacion");
		sql.append(" from evaluaciones");
		sql.append(" where eval_fecharealizacion='1996-09-19'");
		
		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();
		assertEquals(FechaUtil.asString(rs.getDate("eval_fecharealizacion"),"yyyy-MM-dd"), "1996-09-19");
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

@Test
public void testEliminar() {
	EvaluacionDao evalDao = new EvaluacionDao();
	try {
				
		//hago una consulta buscando la evaluacion eliminar
		evalDao.eliminar(getEvaluacion("1972-11-17"));
		
		assertNull(getEvaluacion("1970-01-01"));
		
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

@Test
public void testModificarEstado() {
	EvaluacionDao evalDao = new EvaluacionDao();
	try {
				
		//hago una consulta buscando el curso paraa modificar
		
		Evaluacion eval = getEvaluacion("1972-11-18");
		eval.setEstado(2);
		evalDao.modificar(eval);

		assertEquals(getEvaluacion("1972-11-18").getEstado(), 2);
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

@Test
public void testLeer_porFechaRealizacion() {
	EvaluacionDao evalDao = new EvaluacionDao();
	try {
		//leer un curso
		Evaluacion eval = new  Evaluacion();
		eval.setFechaDeRealizacion(FechaUtil.asDate(1972, 11, 19));
		
		List evaluaciones =evalDao.leer(eval);
		//hago una consulta
		Evaluacion evalLeida = (Evaluacion)evaluaciones.get(0);
		assertEquals(FechaUtil.asString(evalLeida.getFechaDeRealizacion(),"yyyy-MM-dd"),
					 "1972-11-19");
		
		
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (SQLException e) {
		assertTrue(true);
		e.printStackTrace();
	}
	
}
@Test
public void testLeer_porCodigo() {
	EvaluacionDao evalDao = new EvaluacionDao();
	try {
		//leer un curso
		Evaluacion eval = getEvaluacion("1972-11-20");
		
		Evaluacion evalAleer = new Evaluacion();
		evalAleer.setCodigo(eval.getCodigo());
		
		
		List evaluaciones =evalDao.leer(evalAleer);
		//hago una consulta
		Evaluacion evalLeida = (Evaluacion)evaluaciones.get(0);
		assertEquals(evalLeida.getCodigo(), evalAleer.getCodigo());
		
		
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (SQLException e) {
		assertTrue(true);
		e.printStackTrace();
	}
	
}
	

@SuppressWarnings("unused")
private Alumno getAlumno() throws SQLException{
	ResultSet rs=stat.executeQuery("select alu_id, alu_nombre, alu_apellido from alumnos where alu_nombre = 'nombre_test'");
	rs.next();
	Alumno alumno = new Alumno(rs.getInt("alu_id"),rs.getString("alu_nombre"),rs.getString("alu_apellido"));
	
	rs.close();
	return alumno;
}
private Materia getMateria() throws SQLException{
	StringBuffer sql = new StringBuffer("select mat.mat_id, mat.cur_id,cur.cur_anio, cur.cur_descripcion, mat.tem_id, tem.tem_descripcion, mat.mat_nombre");
	sql.append(" from materias mat, cursos cur, temarios tem");
	sql.append(" where mat.cur_id = cur.cur_id and mat.tem_id = tem.tem_id");
	sql.append(" and mat.mat_nombre='materia 1_test'");
	
	ResultSet rs=stat.executeQuery(sql.toString());
	rs.next();
	Curso curso = new Curso(rs.getInt("cur_id"), rs.getInt("cur_anio"), "cur_descripcion");
	
	Materia materia = new Materia(rs.getInt("mat_id"),rs.getString("mat_nombre"),new Temario(rs.getInt("tem_id"), rs.getString("tem_descripcion")));
	materia.setCurso(curso);
	
	rs.close();
	return materia;
	
}
private Evaluacion getEvaluacion(String pFecha) throws SQLException{
	StringBuffer sql = new StringBuffer("SELECT eval.eval_id, eval.mat_id, mat.mat_nombre, eval.alu_id, alu.alu_nombre, alu.alu_apellido, eval.eval_estado, eval.eval_fecharealizacion");
	sql.append(" FROM evaluaciones eval, materias mat, alumnos alu");
	sql.append(" where eval.mat_id = mat.mat_id and eval.alu_id = alu.alu_id and eval.eval_fecharealizacion ='");
	sql.append(pFecha);
	sql.append("'");

	ResultSet rs=stat.executeQuery(sql.toString());
	Evaluacion evaluacion=null;
//	if(rs.next())
		//TODO Gabrielito hay que ver este error
//		evaluacion = new Evaluacion(rs.getInt("eval_id"), 
//										new Materia(rs.getInt("mat_id"), rs.getString("mat_nombre"), null), 
//										new Alumno(rs.getInt("alu_id"), rs.getString("alu_nombre"), rs.getString("alu_apellido")),
//										rs.getInt("eval_estado"), rs.getDate("eval_fecharealizacion"),null);
	

	return evaluacion;
}

}

