package ar.edu.inac.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.CursoDao;
import ar.edu.inac.dao.TurnoDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Turno;

public class TurnoDAOTest {

	static Connection con;
	Statement stat;
	Turno turno = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();

		Statement consulta = con.createStatement();

		String sql = "";
		BufferedReader bf = new BufferedReader(new InputStreamReader(
				ProvinciaDAOTest.class.getResource("TurnosCrear.sql")
						.openStream()));
		while ((sql = bf.readLine()) != null) {
			if (sql.trim().length() != 0 && !sql.startsWith("--")) {
				consulta.executeUpdate(sql); 
			}
		}
		cm.desconectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();

		Statement consulta = con.createStatement();

		String sql = "";
		BufferedReader bf = new BufferedReader(new InputStreamReader(
				ProvinciaDAOTest.class.getResource("TurnosEliminar.sql")
						.openStream()));
		while ((sql = bf.readLine()) != null) {
			if (sql.trim().length() != 0 && !sql.startsWith("--")) {
				consulta.executeUpdate(sql); 
			}
		}

		cm.desconectar();
	}

	@Before
	public void setUp() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();

	}

	@After
	public void tearDown() throws Exception {
		con.close();
	}

	@Test
	public void testAgregar() {
		TurnoDAO turDao = new TurnoDAO();
		try {
			// agreque una provincia
			turDao.agregar(new Turno(10, "Nuevo Decimo_test"));
			// hago una consulta
			StringBuffer sql = new StringBuffer(
					"select tur_id, tur_descripcion");
			sql.append(" from turnos");
			sql.append(" where tur_descripcion=");
			sql.append("'Nuevo Decimo_test'");

			ResultSet rs = stat.executeQuery(sql.toString());
			rs.next();
			assertEquals(rs.getString("tur_descripcion"), "Nuevo Decimo_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

	@Test
	public void testEliminar() {
		TurnoDAO turDao = new TurnoDAO();
		try {

			// hago una consulta buscando el turno eliminar
			StringBuffer sql = new StringBuffer(
					"select tur_id, tur_descripcion");
			sql.append(" from turnos");
			sql.append(" where tur_descripcion=");
			sql.append("'turno ma�ana1 _test'");

			ResultSet rs = stat.executeQuery(sql.toString());
			rs.next();
			Turno tur = new Turno(rs.getInt("tur_id"), rs.getString("tur_descripcion"));
			turDao.eliminar(tur);

			ResultSet rs2 = stat.executeQuery(sql.toString());
			// next deberia darle false, o sea no hay un proximo
			assertFalse(rs2.next());
			rs.close();
			rs2.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

	@Test
	public void testModificar() {
		TurnoDAO turDao = new TurnoDAO();
		try {

			// hago una consulta buscando el curso paraa modificar

			StringBuffer sql = new StringBuffer(
					"select tur_id, tur_descripcion");
			sql.append(" from turnos");
			sql.append(" where tur_descripcion=");
			sql.append("'turno ma�ana2 _test'");

			ResultSet rs = stat.executeQuery(sql.toString());
			rs.next();
			Turno tur = new Turno(rs.getInt("tur_id"), rs.getString("tur_descripcion"));
			tur.setDescripcion("Segundo a�o modificado_test");
			turDao.modificar(tur);

			StringBuffer sql2 = new StringBuffer(
					"select tur_id, tur_descripcion");
			sql2.append(" from turnos");
			sql2.append(" where tur_descripcion=");
			sql2.append("'Segundo a�o modificado_test'");

			ResultSet rs2 = stat.executeQuery(sql2.toString());
			rs2.next();
			// next deberia darle false, o sea no hay un proximo
			assertEquals(rs2.getString("tur_descripcion"),
					"Segundo a�o modificado_test");
			rs.close();
			rs2.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

	@Test
	public void testLeer_porDescripcion() {
		TurnoDAO turDao = new TurnoDAO();
		try {
			// leer un turno
			List Turnos = turDao.leer(new Turno(0, "Tercer a�o_test"));
			// hago una consulta
			Turno tur = (Turno) Turnos.get(0);
			assertEquals(tur.getDescripcion(), "Tercer a�o_test");

		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

	@Test
	public void testLeer_porCodigo() {
		TurnoDAO turDao = new TurnoDAO();
		try {

			// hago una consulta buscando el turno para leer

			StringBuffer sql = new StringBuffer(
					"select tur_id, tur_descripcion");
			sql.append(" from turnos");
			sql.append(" where tur_descripcion=");
			sql.append("'Cuarto a�o_test'");

			ResultSet rs = stat.executeQuery(sql.toString());
			rs.next();
			Turno tur = new Turno(rs.getInt("tur_id"),rs.getString("tur_descripcion"));

			// leer un turno
			List turnos = turDao.leer(tur);
			// hago una consulta
			Turno turLeido = (Turno) turnos.get(0);
			assertEquals(turLeido.getDescripcion(), "Cuarto a�o_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}
}