package ar.edu.inac.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.CursoDao;
import ar.edu.inac.dao.ProvinciaDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Provincia;

public class CursoDaoTest {
	static Connection con;
	Statement stat;
	Curso curso = null;
@BeforeClass	
public static void setUpBeforeClass() throws Exception {
	ConnectionManager cm = new ConnectionManager();
	cm.conectar();
	con = cm.getConexion();
	
    Statement consulta= con.createStatement();

    String sql = "";
    BufferedReader bf = new BufferedReader( new InputStreamReader( ProvinciaDAOTest.class.getResource( "CursosCrear.sql" ).openStream() ) );
    while ( (sql = bf.readLine()) != null ) {
       if ( sql.trim().length() != 0 &&
            !sql.startsWith( "--" ) ) {              
          consulta.executeUpdate( sql ); // aca arma
       }
    }
    cm.desconectar();
}

@AfterClass
public static void tearDownAfterClass() throws Exception {		
	ConnectionManager cm = new ConnectionManager();
	cm.conectar();
	con = cm.getConexion();
	
    Statement consulta= con.createStatement();

    String sql = "";
    BufferedReader bf = new BufferedReader( new InputStreamReader( ProvinciaDAOTest.class.getResource( "CursosEliminar.sql" ).openStream() ) );
    while ( (sql = bf.readLine()) != null ) {
       if ( sql.trim().length() != 0 &&
            !sql.startsWith( "--" ) ) {              
          consulta.executeUpdate( sql ); // aca arma
       }
    }
    
    cm.desconectar();
}


@Before
public void setUp() throws Exception {
	ConnectionManager cm = new ConnectionManager();
	cm.conectar();
	con = cm.getConexion();
	stat = con.createStatement();

}

@After
public void tearDown() throws Exception {
	con.close();
}

@Test
public void testAgregar() {
	CursoDao curDao = new CursoDao();
	try {
		//agregue una provincia
		curDao.agregar(new Curso(10, "Nuevo Decimo_test"));
		//hago una consulta
		StringBuffer sql = new StringBuffer("select cur_id, cur_anio, cur_descripcion");
		sql.append(" from cursos");
		sql.append(" where cur_descripcion=");
		sql.append("'Nuevo Decimo_test'");
		
		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();
		assertEquals(rs.getString("cur_descripcion"), "Nuevo Decimo_test");
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

@Test
public void testEliminar() {
	CursoDao curDao = new CursoDao();
	try {
				
		//hago una consulta buscando el curso eliminar
		StringBuffer sql = new StringBuffer("select cur_id, cur_anio, cur_descripcion");
		sql.append(" from cursos");
		sql.append(" where cur_descripcion=");
		sql.append("'Primer a�o_test'");
		
		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();			
		Curso cur = new Curso(rs.getInt("cur_id"),rs.getInt("cur_anio"), rs.getString("cur_descripcion"));			
		curDao.eliminar(cur);

		ResultSet rs2=stat.executeQuery(sql.toString());
		//next deberia darle false, o sea no hay un proximo
		assertFalse(rs2.next());
		rs.close();
		rs2.close();			
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

@Test
public void testModificar() {
	CursoDao curDao = new CursoDao();
	try {
				
		//hago una consulta buscando el curso paraa modificar
		
		StringBuffer sql = new StringBuffer("select cur_id, cur_anio, cur_descripcion");
		sql.append(" from cursos");
		sql.append(" where cur_descripcion=");
		sql.append("'Segundo a�o_test'");
		
		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();			
		Curso cur = new Curso(rs.getInt("cur_id"),rs.getInt("cur_anio"),  rs.getString("cur_descripcion"));	
		cur.setDescripcion("Segundo a�o modificado_test");
		curDao.modificar(cur);

		StringBuffer sql2 = new StringBuffer("select cur_id, cur_anio, cur_descripcion");
		sql2.append(" from cursos");
		sql2.append(" where cur_descripcion=");
		sql2.append("'Segundo a�o modificado_test'");
		
	
		ResultSet rs2=stat.executeQuery(sql2.toString());
		rs2.next();
		//next deberia darle false, o sea no hay un proximo
		assertEquals(rs2.getString("cur_descripcion"), "Segundo a�o modificado_test");
		rs.close();
		rs2.close();			
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

@Test
public void testLeer_porDescripcion() {
	CursoDao curDao = new CursoDao();
	try {
		//leer un curso
		List cursos =curDao.leer(new Curso(0,"Terecer a�o_test"));
		//hago una consulta
		Curso cur = (Curso)cursos.get(0);
		assertEquals(cur.getDescripcion(), "Terecer a�o_test");
		//assertEquals(cur.getAnio(), 3);
		
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}
@Test
public void testLeer_porCodigo() {
	CursoDao curDao = new CursoDao();
	try {

		//hago una consulta buscando el curso para leer
		
		StringBuffer sql = new StringBuffer("select cur_id, cur_anio, cur_descripcion");
		sql.append(" from cursos");
		sql.append(" where cur_descripcion=");
		sql.append("'Cuarto a�o_test'");

		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();			
		Curso cur = new Curso(rs.getInt("cur_id"),0, null);	

		//leer un curso
		List cursos =curDao.leer(cur);
		//hago una consulta
		Curso curLeido = (Curso)cursos.get(0);
		assertEquals(curLeido.getDescripcion(), "Cuarto a�o_test");
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

}
