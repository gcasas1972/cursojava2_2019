package ar.edu.inac.dao.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.DivisionDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Division;

public class DivisionDAOTest {
	static Connection con;
	Statement stat;
	Division division = null;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();

		Statement consulta = con.createStatement();

		String sql = "";
		BufferedReader bf = new BufferedReader(new InputStreamReader(
				TipoDeComponenteDAOTest.class.getResource("DivisionCrear.sql")
						.openStream()));
		while ((sql = bf.readLine()) != null) {
			if (sql.trim().length() != 0 && !sql.startsWith("--")) {
				consulta.executeUpdate(sql); // aca arma
			}
		}
		cm.desconectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();

		Statement consulta = con.createStatement();

		String sql = "";
		BufferedReader bf = new BufferedReader(new InputStreamReader(
				TipoDeComponenteDAOTest.class.getResource(
						"DivisionEliminar.sql").openStream()));
		while ((sql = bf.readLine()) != null) {
			if (sql.trim().length() != 0 && !sql.startsWith("--")) {
				consulta.executeUpdate(sql); // aca arma
			}
		}

		cm.desconectar();
	}

	@Before
	public void setUp() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();

	}

	@After
	public void tearDown() throws Exception {
		con.close();
	}

	@Test
	public void testAgregar() {
		DivisionDAO divDao = new DivisionDAO();
		try {
			// agregque una
			divDao.agregar(new Division(10, "Nuevo Decimo_test"));
			// hago una consulta
			StringBuffer sql = new StringBuffer(
					"select div_id, div_descripcion");
			sql.append(" from division");
			sql.append(" where div_descripcion=");
			sql.append("'Nuevo Decimo_test'");

			ResultSet rs = stat.executeQuery(sql.toString());
			rs.next();
			assertEquals(rs.getString("div_descripcion"), "Nuevo Decimo_test");

		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

	@Test
	public void testEliminar() {
		DivisionDAO divDAO = new DivisionDAO();
		try {

			// hago una consulta buscando el curso eliminar
			StringBuffer sql = new StringBuffer(
					"select div_id, div_descripcion");
			sql.append(" from division");
			sql.append(" where div_descripcion=");
			sql.append("'1_a_test'");

			ResultSet rs = stat.executeQuery(sql.toString());
			rs.next();
			Division div = new Division(rs.getInt("div_id"),
					rs.getString("div_descripcion"));
			divDAO.eliminar(div);

			ResultSet rs2 = stat.executeQuery(sql.toString());
			// next deberia darle false, o sea no hay un proximo
			assertFalse(rs2.next());
			rs.close();
			rs2.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

	@Test
	public void testModificar() {
		DivisionDAO divDao = new DivisionDAO();
		try {

			// hago una consulta buscando el curso paraa modificar

			StringBuffer sql = new StringBuffer(
					"select div_id, div_descripcion");
			sql.append(" from division");
			sql.append(" where div_descripcion=");
			sql.append("'1_b_test'");

			ResultSet rs = stat.executeQuery(sql.toString());
			rs.next();
			Division div = new Division(rs.getInt("div_id"),
					rs.getString("div_descripcion"));
			div.setDescripcion("Segundo a�o modificado_test");
			divDao.modificar(div);

			StringBuffer sql2 = new StringBuffer(
					"select div_id, div_descripcion");
			sql2.append(" from division");
			sql2.append(" where div_descripcion=");
			sql2.append("'Segundo a�o modificado_test'");

			ResultSet rs2 = stat.executeQuery(sql2.toString());
			rs2.next();
			// next deberia darle false, o sea no hay un proximo
			assertEquals(rs2.getString("div_descripcion"),
					"Segundo a�o modificado_test");
			rs.close();
			rs2.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

	@Test
	public void testLeer_porDescripcion() {
		DivisionDAO divDao = new DivisionDAO();
		try {
			// leer un curso
			List divisiones = divDao.leer(new Division(0, "2_b_test"));
			// hago una consulta
			Division div = (Division) divisiones.get(0);
			assertEquals(div.getDescripcion(), "2_b_test");
			// assertEquals(cur.getAnio(), 3);

		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

	@Test
	public void testLeer_porCodigo() {
		DivisionDAO divDAO = new DivisionDAO();
		try {

			// hago una consulta buscando el curso para leer

			StringBuffer sql = new StringBuffer(
					"select div_id, div_descripcion");
			sql.append(" from division");
			sql.append(" where div_descripcion=");
			sql.append("'2_a_test'");

			ResultSet rs = stat.executeQuery(sql.toString());
			rs.next();
			Division Div = new Division(rs.getInt("div_id"), null);

			// leer un curso
			List Division = divDAO.leer(Div);
			// hago una consulta
			Division divLeido = (Division) Division.get(0);
			assertEquals(divLeido.getDescripcion(), "2_a_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}

	}

}
