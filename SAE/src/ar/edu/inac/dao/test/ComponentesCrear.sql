
insert into pantallas(pan_descripcion, pan_nombre) values ('pantalla1_test','nom1_test')
insert into pantallas(pan_descripcion, pan_nombre) values ('pantalla2_test','nom1_test')
insert into componentes(pan_id,comp_descripcion) values( (select max(pan_id) from pantallas),'sarasa_test')
insert into componentes(pan_id,comp_descripcion) values( (select max(pan_id) from pantallas),'sisupiera_test')
insert into componentes(pan_id,comp_descripcion) values( (select max(pan_id) from pantallas),'123_test')

--insertar en visible y editable no deja poner _test porque solo toma un caracter.--

insert into pantallas (pan_descripcion) values ('pantalla3_test')
insert into componentes (pan_id, comp_descripcion, comp_visible, comp_editable) values ((select max(pan_id)from pantallas),'probando123_test',1,0)
