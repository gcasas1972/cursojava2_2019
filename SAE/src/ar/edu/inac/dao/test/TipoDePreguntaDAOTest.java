package ar.edu.inac.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.CursoDao;
import ar.edu.inac.dao.TipoDePreguntaDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.TipoDePregunta;

public class TipoDePreguntaDAOTest {
	static Connection con;
	Statement stat;
	TipoDePregunta tipocomp= null;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
	    Statement consulta= con.createStatement();

	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader( TipoDePreguntaDAOTest.class.getResource( "TipoDePreguntaCrear.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	    	if ( sql.trim().length() != 0 &&
	                !sql.startsWith( "--" ) ) {    
	       consulta.executeUpdate( sql ); // aca arma
	    }
	    }
	    cm.desconectar();
	}
		
	

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
	    Statement consulta= con.createStatement();

	    String sql = "";
	    BufferedReader bf = new BufferedReader( new InputStreamReader(TipoDePreguntaDAOTest.class.getResource( "TipoDePreguntaEliminar.sql" ).openStream() ) );
	    while ( (sql = bf.readLine()) != null ) {
	       if ( sql.trim().length() != 0 &&
	            !sql.startsWith( "--" ) ) {              
	          consulta.executeUpdate( sql ); // aca arma
	       }
	    }
	    
	    cm.desconectar();
	}
	

	@Before
	public void setUp() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();
	}

	@After
	public void tearDown() throws Exception {
		con.close();
	}

	@Test
	public void testAgregar() {
		TipoDePreguntaDAO tipDao = new TipoDePreguntaDAO ();
		try {
			//agregque una provincia
			tipDao.agregar(new TipoDePregunta(10, "Nuevo Materia_test"));
			//hago una consulta
			StringBuffer sql = new StringBuffer("select TPREG_ID,TPREG_DESCRIPCION");
			sql.append(" from tipopregunta");
			sql.append(" where TPREG_DESCRIPCION=");
			sql.append("'Nuevo Materia_test'");
			
			ResultSet rs=stat.executeQuery(sql.toString());
			rs.next();
			assertEquals(rs.getString("TPREG_DESCRIPCION"), "Nuevo Materia_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	

	@Test
	public void testEliminar() {
		TipoDePreguntaDAO tipDao = new TipoDePreguntaDAO ();
	try {StringBuffer sql = new StringBuffer("select TPREG_ID, TPREG_DESCRIPCION");
	sql.append(" from tipopregunta");
	sql.append(" where TPREG_DESCRIPCION=");
	sql.append("'teorica_test'");
	
	ResultSet rs=stat.executeQuery(sql.toString());
	rs.next();			
	TipoDePregunta tcomp= new TipoDePregunta(rs.getInt("TPREG_ID"),rs.getString("TPREG_DESCRIPCION"));			
	tipDao.eliminar(tcomp);

	ResultSet rs2=stat.executeQuery(sql.toString());
	//next deberia darle false, o sea no hay un proximo
	assertFalse(rs2.next());
	rs.close();
	rs2.close();			
} catch (SQLException e) {
	assertTrue(false);
	e.printStackTrace();
} catch (ClassNotFoundException e) {
	assertTrue(false);
	e.printStackTrace();
}

}

		
	

	@Test
	public void testModificar() {
		TipoDePreguntaDAO tipDao = new TipoDePreguntaDAO ();
		
		try {
			
			//hago una consulta buscando el curso paraa modificar
			
			StringBuffer sql = new StringBuffer("select TPREG_ID, TPREG_DESCRIPCION");
			sql.append(" from tipopregunta");
			sql.append(" where TPREG_DESCRIPCION=");
			sql.append("'practica_test'");
			
			ResultSet rs=stat.executeQuery(sql.toString());
			rs.next();			
			TipoDePregunta tip = new TipoDePregunta(rs.getInt("TPREG_ID"),rs.getString("TPREG_DESCRIPCION"));	
			tip.setDescripcion("practica modificado_test");
			tipDao.modificar(tip);

			StringBuffer sql2 = new StringBuffer("select TPREG_ID,TPREG_DESCRIPCION");
			sql2.append(" from tipopregunta");
			sql2.append(" where TPREG_DESCRIPCION=");
			sql2.append("'practica modificado_test'");
			
		
			ResultSet rs2=stat.executeQuery(sql2.toString());
			rs2.next();
			//next deberia darle false, o sea no hay un proximo
			assertEquals(rs2.getString("TPREG_DESCRIPCION"), "practica modificado_test");
			rs.close();
			rs2.close();			
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
		
			
		
		
	

	@Test
	public void testLeer_porDescripcion() {
		TipoDePreguntaDAO tipDAO = new TipoDePreguntaDAO();
		try {
			//leer un tipopguntas
			List tipopreguntas =tipDAO.leer(new TipoDePregunta(0, "teoricoPractica_test"));
			//hago una consulta
			TipoDePregunta tip = (TipoDePregunta)tipopreguntas.get(0);
			assertEquals(tip.getDescripcion(), "teoricoPractica_test");
			
			
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

@Test
public void testLeer_porCodigo() {
	TipoDePreguntaDAO tipDao = new TipoDePreguntaDAO();
	try {

		//hago una consulta buscando el tipodepregunta para leer
		

		StringBuffer sql = new StringBuffer(" select TPREG_ID, TPREG_DESCRIPCION");

		sql.append(" from tipopregunta");
		sql.append(" where TPREG_DESCRIPCION=");
		sql.append("'oral_test'");


		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();			
		TipoDePregunta tip = new TipoDePregunta(rs.getInt("TPREG_ID"),null);	
		
		//leer tipopreguntas
		List tipopreguntas =tipDao.leer(tip);

		

		//hago una consulta
		TipoDePregunta tipLeido = (TipoDePregunta)tipopreguntas.get(0);
		assertEquals(tipLeido.getDescripcion(), "oral_test");
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

}

