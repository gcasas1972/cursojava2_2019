package ar.edu.inac.dao.test;


import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.ComisionDAO;
import ar.edu.inac.dao.ProvinciaDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Comision;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Materia;
import ar.edu.inac.modelo.Profesor;
import ar.edu.inac.modelo.Temario;

public class ComisionDAOTest {
	static Connection con;
	Statement stat;
	Comision com = null;
	Profesor profTest = null;
	Materia  matTest = null;
	
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		Statement consulta = con.createStatement();
		String sql = "";
		BufferedReader bf = new BufferedReader(new InputStreamReader(ComisionDAOTest.class.getResource("ComisionesCrear.sql").openStream()));
		while((sql=bf.readLine())!=null){
			if(sql.trim().length()!=0 && !sql.startsWith("--")){
				consulta.executeUpdate(sql);
			}
		}
		cm.desconectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
        Statement consulta= con.createStatement();
        String sql = "";
        BufferedReader bf = new BufferedReader(new InputStreamReader(ComisionDAOTest.class.getResource("ComisionesEliminar.sql").openStream()));
        while((sql = bf.readLine()) != null){
        	if(sql.trim().length()!=0 && !sql.startsWith("--")){
        		consulta.executeUpdate(sql);
        	}
        }
        cm.desconectar();
	}

	@Before
	public void setUp() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();
		//proftest
		ResultSet rs= stat.executeQuery(	"select prof_id, prof_nombre , prof_apellido " +
											"from profesores " +
											"where prof_id = (select max(prof_id) " +
											"from profesores)");
		rs.next();
		profTest = new Profesor(rs.getInt("prof_id")		, 
								rs.getString("prof_nombre")	, 
								rs.getString("prof_apellido"));
		//materia y curso
		
		rs = stat.executeQuery( "select mat.mat_id, mat.mat_nombre, mat.cur_id, cur.cur_anio, cur.cur_descripcion, mat.tem_id,tem.Tem_descripcion" +
								" from materias mat, cursos cur, temarios tem"	+
								" where mat.cur_id= cur.cur_id"					+
								" and mat.tem_id = tem.tem_id"					+
								" and mat.mat_nombre = 'materia 1_test'");
		rs.next();
		matTest = new Materia(rs.getInt("mat.mat_id"),rs.getString("mat.mat_nombre"),new Curso(rs.getInt("mat.cur_id"),rs.getInt("cur.cur_anio"), rs.getString("cur.cur_descripcion")),
				new Temario(rs.getInt("mat.tem_id"), rs.getString("tem.Tem_descripcion")));
		
	}

	@After
	public void tearDown() throws Exception {
		con.close();
	}
	
	@Test
	public void testAgregar() {
		ComisionDAO comDao = new ComisionDAO();
		try {
			//Agrego una comision
			
			comDao.agregar(new Comision(0, matTest.getCurso(), matTest, "ma�ana", "nueva division_test", null, profTest));
			//Hago una consulta
			ResultSet rs = stat.executeQuery("select com_id, com_division from comisiones"
										+	 " where com_division='nueva division_test'");
			rs.next();
			assertEquals(rs.getString("com_division"), "nueva division_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testEliminar() {
		ComisionDAO comDao = new ComisionDAO();
		try {
			ResultSet rs=stat.executeQuery(	"select com_id, com_division" +
											" from comisiones" + 
											" where com_division='comision_test'");
			rs.next();
			
			//Comision com = new Comision(rs.getString("com_division"));
			Comision com = new Comision(rs.getInt("com_id"));
			comDao.eliminar(com);
			ResultSet rs2=stat.executeQuery("select com_id, com_division" +
											" from comisiones" + 
											" where com_division='comision_test'");
			assertFalse(rs2.next());
			rs2.close();
			rs.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testModificar() {
		ComisionDAO comDao = new ComisionDAO();
		try {
			StringBuffer sql = new StringBuffer("select com.com_id, com.com_turno, com.com_division, com.prof_id, prof.prof_nombre, prof.prof_apellido,");
			sql.append("com.cur_id, cur.cur_anio, cur.cur_descripcion,");
			sql.append("com.mat_id, mat.mat_nombre");
			sql.append(" from comisiones com, profesores prof, cursos cur, materias mat");
			sql.append(" where com.prof_id = prof.prof_id");
			sql.append(" and com.cur_id= cur.cur_id");
			sql.append(" and com.mat_id = mat.mat_id");
			sql.append(" and   com.com_division='comision2_test'");
			ResultSet rs=stat.executeQuery(	sql.toString());
			rs.next();
			
			Comision com = new Comision(rs.getInt("com.com_id"),
										new Curso(rs.getInt("com.cur_id"),rs.getInt("cur.cur_anio"),rs.getString("cur.cur_descripcion")),
										new Materia(rs.getInt("com.mat_id"), rs.getString("mat.mat_nombre"), null),
										rs.getString("com.com_turno"), rs.getString("com.com_division"),
										null, new Profesor(rs.getInt("com.prof_id"), rs.getString("prof_nombre"), rs.getString("prof.prof_apellido")));
			
			
			com.setDivision("comision2 modificado_test");
			comDao.modificar(com);
			ResultSet rs2=stat.executeQuery("select com_id, com_division" +
											" from comisiones" + 
											" where com_id=" + com.getCodigo());
			rs2.next();
			assertEquals(rs2.getString("com_division"), "comision2 modificado_test");
			rs.close();
			rs2.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testLeer_porDivision() {
		ComisionDAO comDao = new ComisionDAO();
		try {
			List comisiones = comDao.leer(new Comision("comision3_test"));
			Comision com = (Comision)comisiones.get(0);
			assertEquals(com.getDivision(), "comision3_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testLeer_porCodigo() {
		ComisionDAO comDao = new ComisionDAO();
		try {
			ResultSet rs=stat.executeQuery(	"select com_id, com_division" +
											" from comisiones" + 
											" where com_division='comision4_test'");
			rs.next();
			Comision com = new Comision(rs.getInt("com_id"));
			List comisiones = comDao.leer(com);
			Comision comLeida = (Comision)comisiones.get(0);
			assertEquals(comLeida.getDivision(), "comision4_test");
			rs.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
}
