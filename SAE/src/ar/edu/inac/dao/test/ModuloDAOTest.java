package ar.edu.inac.dao.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ar.edu.inac.dao.ModuloDao;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Materia;
import ar.edu.inac.modelo.Modulo;
import ar.edu.inac.modelo.Temario;



public class ModuloDAOTest {
		static Connection con;
		Statement stat;
		Modulo mod = null;
		
	    @BeforeClass
	
	    public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
        Statement consulta= con.createStatement();

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( ModuloDAOTest.class.getResource( "ModulosCrear.sql" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma
           }
        }
        cm.desconectar();
	}

	
	    
	    @AfterClass
	    public static void tearDownAfterClass() throws Exception {		
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
        Statement consulta= con.createStatement();

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( ModuloDAOTest.class.getResource( "ModulosEliminar.sql" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma
           }
        }
        
        cm.desconectar();
	}


	   @Before
	   
	    public void setUp() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();

	}

	    @After
	    
	    public void tearDown() throws Exception {
		con.close();
	}
	
	    @Test
	    
	    public void testAgregar() {
	    	
		ModuloDao modDao = new ModuloDao();
		try {
			//agregque un Modulo
			modDao.agregar(new Modulo(0,"Nuevo Modulo_test", getMateria(),null));
			//hago una consulta
			StringBuffer sql = new StringBuffer("select mod_id, mod_descripcion");
			sql.append(" from modulos");
			sql.append(" where mod_descripcion=");
			sql.append("'Nuevo Modulo_test'");
			
			ResultSet rs=stat.executeQuery(sql.toString());
			rs.next();
			assertEquals(rs.getString("mod_descripcion"), "Nuevo Modulo_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	    @Test
	    
	    public void testEliminar() {
	    ModuloDao modDao = new ModuloDao();
		try {
			//hago una consulta buscando el modulo eliminar
			StringBuffer sql = new StringBuffer("SELECT modu.mod_id, modu.mat_id, mat.mat_nombre,  modu.mod_descripcion");
			sql.append(" FROM modulos modu, materias mat");
			sql.append(" where modu.mat_id=mat.mat_id and");
			sql.append(" modu.mod_descripcion='modulos 1_test'");
			
			ResultSet rs=stat.executeQuery(sql.toString());
			rs.next();			
			Materia mat = new Materia(rs.getInt("modu.mat_id"), rs.getString("mat.mat_nombre"), null);
			Modulo mod = new Modulo(rs.getInt("modu.mod_id"), rs.getString("modu.mod_descripcion"), mat,null);		
			modDao.eliminar(mod);

			ResultSet rs2=stat.executeQuery(sql.toString());
			//next deberia darle false, o sea no hay un proximo
			assertFalse(rs2.next());
			rs.close();
			rs2.close();			
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();;
		}
		
	}

	@Test
	public void testModificarDescripcion() {
		ModuloDao modDao = new ModuloDao();
		try {
					
			//hago una consulta buscando el modulo para modificar
			
			StringBuffer sql = new StringBuffer("SELECT modu.mod_id, modu.mat_id, mat.mat_nombre,  modu.mod_descripcion");
			sql.append(" FROM modulos modu, materias mat");
			sql.append(" where modu.mat_id=mat.mat_id and");
			sql.append(" modu.mod_descripcion='modulos 2_test'");
			
			ResultSet rs=stat.executeQuery(sql.toString());
			rs.next();			
			Materia mat = new Materia(rs.getInt("modu.mat_id"), rs.getString("mat.mat_nombre"), null);
			Modulo mod = new Modulo(rs.getInt("modu.mod_id"), rs.getString("modu.mod_descripcion"), mat,null);		
		
			mod.setDescripcion("Modulo2 modificado_test");
			modDao.modificar(mod);

			StringBuffer sql2 = new StringBuffer("SELECT modu.mod_id, modu.mat_id, mat.mat_nombre,  modu.mod_descripcion");
			sql2.append(" FROM modulos modu, materias mat");
			sql2.append(" where modu.mat_id=mat.mat_id and");
			sql2.append(" modu.mod_descripcion='Modulo2 modificado_test'");			
		
			ResultSet rs2=stat.executeQuery(sql2.toString());
			rs2.next();
			assertEquals(rs2.getString("mod_descripcion"), "Modulo2 modificado_test");
			rs.close();
			rs2.close();			
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testLeer_porDescripcion() {
		ModuloDao modDao = new ModuloDao();
		try {
			//leer un modulo
			List modulo = modDao.leer(new Modulo(0,"modulos 3_test",null,null));
			//hago una consulta
			Modulo mod = (Modulo)modulo.get(0);
			assertEquals(mod.getDescripcion(), "modulos 3_test");

			
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	@Test
	public void testLeer_porCodigo() {
		ModuloDao modDao = new ModuloDao();
		try {
			//hago una consulta buscando el modulo para leer
			
			StringBuffer sql = new StringBuffer("select mod_id, mod_descripcion");
			sql.append(" from modulos");
			sql.append(" where mod_descripcion=");
			sql.append("'modulos 4_test'");

			ResultSet rs=stat.executeQuery(sql.toString());
			rs.next();			
			Modulo mod = new Modulo(rs.getInt("mod_id"),null,null,null);	

			//leer un modulo
			List modulo =modDao.leer(mod);
			//hago una consulta
			Modulo modLeido = (Modulo)modulo.get(0);
			assertEquals(modLeido.getDescripcion(), "modulos 4_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	private Materia getMateria() throws SQLException{
		StringBuffer sql = new StringBuffer("select mat.mat_id, mat.cur_id,cur.cur_anio, cur.cur_descripcion, mat.tem_id, tem.tem_descripcion, mat.mat_nombre");
		sql.append(" from materias mat, cursos cur, temarios tem");
		sql.append(" where mat.cur_id = cur.cur_id and mat.tem_id = tem.tem_id");
		sql.append(" and mat.mat_nombre='materia 1_test'");
		
		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();
		Curso curso = new Curso(rs.getInt("mat.cur_id"), rs.getInt("cur.cur_anio"),rs.getString("cur.cur_descripcion"));
		
		Materia materia = new Materia(rs.getInt("mat.mat_id"),rs.getString("mat.mat_nombre"),new Temario(rs.getInt("mat.tem_id"), rs.getString("tem.tem_descripcion")));
		materia.setCurso(curso);
		
		rs.close();
		return materia;
		
	}

}
