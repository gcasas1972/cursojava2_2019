package ar.edu.inac.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.UsuariosDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Usuario;


public class UsuariosDAOTest {
		static Connection con;
		Statement stat;
		Usuario usu = null;
	@BeforeClass	
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
        Statement consulta= con.createStatement();

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( UsuariosDAOTest.class.getResource( "UsuariosCrear.sql" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma
           }
        }
        cm.desconectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {		
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
        Statement consulta= con.createStatement();

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( UsuariosDAOTest.class.getResource( "UsuariosEliminar.sql" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma
           }
        }
        
        cm.desconectar();
	}

	@Before
	public void setUp() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();

	}

	@After
	public void tearDown() throws Exception {
		con.close();
	}
	
	@Test
	public void testAgregar() {
		UsuariosDAO usuDao = new UsuariosDAO();
		try {
			//agregque una provincia
			usuDao.agregar(new Usuario(0,"nuevo usuarios_test", "123"));
			//hago una consulta
			ResultSet rs=stat.executeQuery("select usu_id, usu_nombre, usu_password" +
					" from usuarios" + 
					 " where usu_nombre='nuevo usuarios_test'");
			rs.next();
			assertEquals(rs.getString("usu_nombre"), "nuevo usuarios_test");
			assertEquals(rs.getString("usu_password"), "123");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testEliminar() {
UsuariosDAO usuDao = new UsuariosDAO();
		try {
					
			//hago una consulta buscando la provincia a eliminar
			ResultSet rs=stat.executeQuery("select usu_id, usu_password, usu_nombre" +
					" from usuarios" + 
					" where usu_nombre='usuarios1_test'");
			rs.next();			
			Usuario usu = new Usuario(rs.getInt("usu_id"), rs.getString("usu_nombre"), rs.getString("usu_password"));			
			usuDao.eliminar(usu);

			ResultSet rs2=stat.executeQuery("select usu_id, usu_password, usu_nombre" +
					" from usuarios" + 
					" where usu_nombre='usuarios1_test'");
			//next deberia darle false, o sea no hay un proximo
			assertFalse(rs2.next());
			rs.close();
			rs2.close();			
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testModificarNombre() {
		UsuariosDAO usuDao = new UsuariosDAO();
		try {
					
			//hago una consulta buscando la provincia a eliminar
			StringBuffer sql = new 
						StringBuffer("select usu_id, usu_password, usu_nombre ");
			sql.append(" from usuarios");
			sql.append(" where usu_nombre='usuarios2_test'");
			
			ResultSet rs=stat.executeQuery(sql.toString());
			rs.next();			
			Usuario usu = new Usuario(rs.getInt("usu_id"), 
									  rs.getString("usu_nombre"),
									  rs.getString("usu_password"));	
			usu.setNombre("usuarios2 modificado_test");
			usuDao.modificar(usu);

			StringBuffer sql2 = new 
			StringBuffer("select usu_id, usu_password, usu_nombre ");
				sql2.append(" from usuarios");
				sql2.append(" where usu_nombre='usuarios2 modificado_test'");

			
			ResultSet rs2=stat.executeQuery(sql2.toString());
			rs2.next();
			//next deberia darle false, o sea no hay un proximo
			assertEquals(rs2.getString("usu_nombre"), "usuarios2 modificado_test");
			rs.close();
			rs2.close();			
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	
	@Test
	public void testLeer_porNombre() {
		UsuariosDAO usuDao = new UsuariosDAO();
		try {
			//leer una provincia 
			List usuarios =usuDao.leer(new Usuario("usuarios3_test"));
			//hago una consulta
			Usuario usu = (Usuario)usuarios.get(0);
			assertEquals(usu.getNombre(), "usuarios3_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	@Test
	public void testLeer_porCodigo() {
		UsuariosDAO usuDao = new UsuariosDAO();
		try {
			//TODO VANINA please hacelo con un stringBuffer
			
			StringBuffer sql = new 
			StringBuffer("select usu_id, usu_password, usu_nombre ");
			sql.append(" from usuarios");
			sql.append(" where usu_nombre='usuarios4_test'");
			ResultSet rs=stat.executeQuery(sql.toString());
			rs.next();	
			
			Usuario usu = new Usuario(rs.getInt("usu_id"));	

			//leer una provincia 
			List Usuarios = usuDao.leer(usu);
			//hago una consulta
			Usuario usuLeida = (Usuario)Usuarios.get(0);
			assertEquals(usuLeida.getNombre(), "usuarios4_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

}
