package ar.edu.inac.dao.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.TemarioDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Temario;

public class TemarioDAOTest {
	static Connection con;
	Statement stat;
	Temario temario = null;	
@BeforeClass	
public static void setUpBeforeClass() throws Exception {
	ConnectionManager cm = new ConnectionManager();
	cm.conectar();
	con = cm.getConexion();
	
    Statement consulta= con.createStatement();

    String sql = "";
    BufferedReader bf = new BufferedReader( new InputStreamReader( TemarioDAOTest.class.getResource( "TemariosCrear.sql" ).openStream() ) );
    while ( (sql = bf.readLine()) != null ) {
       if ( sql.trim().length() != 0 &&
            !sql.startsWith( "--" ) ) {              
          consulta.executeUpdate( sql ); // aca arma
       }
    }
    cm.desconectar();
}

@AfterClass
public static void tearDownAfterClass() throws Exception {		
	ConnectionManager cm = new ConnectionManager();
	cm.conectar();
	con = cm.getConexion();
	
    Statement consulta= con.createStatement();

    String sql = "";
    BufferedReader bf = new BufferedReader( new InputStreamReader( TemarioDAOTest.class.getResource( "TemariosEliminar.sql" ).openStream() ) );
    while ( (sql = bf.readLine()) != null ) {
       if ( sql.trim().length() != 0 &&
            !sql.startsWith( "--" ) ) {              
          consulta.executeUpdate( sql ); // aca arma
       }
    }
    
    cm.desconectar();
}


@Before
public void setUp() throws Exception {
	ConnectionManager cm = new ConnectionManager();
	cm.conectar();
	con = cm.getConexion();
	stat = con.createStatement();

}

@After
public void tearDown() throws Exception {
	con.close();
}

@Test
public void testAgregar() {
	TemarioDAO temDAO = new TemarioDAO();
	try {
		//agregque una Temario
		temDAO.agregar(new Temario(10, "Nuevo Temario_test"));
		//hago una consulta
		StringBuffer sql = new StringBuffer("select tem_id, tem_descripcion");
		sql.append(" from temarios");
		sql.append(" where tem_descripcion=");
		sql.append("'Nuevo Temario_test'");
		
		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();
		assertEquals(rs.getString("tem_descripcion"), "Nuevo Temario_test");
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

@Test
public void testEliminar() {
	TemarioDAO temDAO = new TemarioDAO();
	try {
				
		//hago una consulta buscando el temario eliminar
		StringBuffer sql = new StringBuffer("select tem_id, tem_descripcion");
		sql.append(" from temarios");
		sql.append(" where tem_descripcion=");
		sql.append("'Temario1_test'");
		
		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();			
		Temario tem = new Temario(rs.getInt("tem_id"), rs.getString("tem_descripcion"));		
		temDAO.eliminar(tem);

		ResultSet rs2=stat.executeQuery(sql.toString());
		//next deberia darle false, o sea no hay un proximo
		assertFalse(rs2.next());
		rs.close();
		rs2.close();			
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

@Test
public void testModificar() {
	TemarioDAO temDAO = new TemarioDAO();
	try {
				
		//hago una consulta buscando el temario para modificar
		
		StringBuffer sql = new StringBuffer("select tem_id, tem_descripcion");
		sql.append(" from temarios");
		sql.append(" where tem_descripcion=");
		sql.append("'Temario2_test'");
		
		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();			
		Temario tem = new Temario(rs.getInt("tem_id"), rs.getString("tem_descripcion"));	
		tem.setDescripcion("Temario2 modificado_test");
		temDAO.modificar(tem);

		StringBuffer sql2 = new StringBuffer("select tem_id, tem_descripcion");
		sql2.append(" from temarios");
		sql2.append(" where tem_descripcion=");
		sql2.append("'Temario2 modificado_test'");
		
	
		ResultSet rs2=stat.executeQuery(sql2.toString());
		rs2.next();
		assertEquals(rs2.getString("tem_descripcion"), "Temario2 modificado_test");
		rs.close();
		rs2.close();			
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

@Test
public void testLeer_porDescripcion() {
	TemarioDAO temDAO = new TemarioDAO();
	try {
		//leer un temario
		List temarios = temDAO.leer(new Temario(0,"Temario3_test"));
		//hago una consulta
		Temario tem = (Temario)temarios.get(0);
		assertEquals(tem.getDescripcion(), "Temario3_test");

		
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}
@Test
public void testLeer_porCodigo() {
	TemarioDAO temDAO = new TemarioDAO();
	try {

		//hago una consulta buscando el temario para leer
		
		StringBuffer sql = new StringBuffer("select tem_id, tem_descripcion");
		sql.append(" from temarios");
		sql.append(" where tem_descripcion=");
		sql.append("'Temario4_test'");

		ResultSet rs=stat.executeQuery(sql.toString());
		rs.next();			
		Temario tem = new Temario(rs.getInt("tem_id"), null);	

		//leer un temario
		List temarios =temDAO.leer(tem);
		//hago una consulta
		Temario temLeido = (Temario)temarios.get(0);
		assertEquals(temLeido.getDescripcion(), "Temario4_test");
	} catch (ClassNotFoundException e) {
		assertTrue(false);
		e.printStackTrace();
	} catch (SQLException e) {
		assertTrue(false);
		e.printStackTrace();
	}
	
}

}
