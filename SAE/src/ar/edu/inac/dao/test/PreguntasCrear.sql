--1 temario
insert into temarios(tem_descripcion) values('Temario_test')
--2 cursos
insert into cursos(cur_anio,cur_descripcion) values(2018,'Curso_test')
--3 materias
insert into materias(cur_id, mat_nombre, tem_id) values((select max(cur_id) from cursos), 'Materia_test', (select max(tem_id) from temarios))
--4 modulos
insert into modulos(mat_id, mod_descripcion) values((select max(mat_id) from materias), 'Modulo_test')
--5 profesor
insert into profesores (prof_nombre, prof_apellido) values ('nombre_test', 'apellido_test')
--6 agregar un turno ----------------
insert into turnos (tur_descripcion) values ('tur_test')
--7 agregar una division-------------
insert into division(div_descripcion) values ('div_test')
--8 comision
insert into comisiones(cur_id, mat_id, prof_id, tur_id, div_id) values((select max(cur_id) from cursos),(select max(mat_id) from materias), (select max(prof_id) from profesores), (select max(tur_id) from turnos), (select max(div_id) from division))
--9 alumnos
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id) values ('nomAlu1_test','apeAlu1_test',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'))
--10 Falta agregar el tipo de pregunta 
insert into tipopregunta (tpreg_descripcion) values ('tpreg_test')
-- 11 evaluaciones
insert into evaluaciones(mat_id, alu_id, eval_estado, eval_fecharealizacion, EVAL_FECHAELABORACION, eval_nota) values((select max(mat_id) from materias),(select max(alu_id) from alumnos), 1, '2018-12-06', '2018-12-06', 10)
-- 12 pregunta para eliminar -----
-- (1) preguntas
insert into preguntas(eval_id, mod_id, mat_id, preg_pathgrafico, preg_explicacion, preg_texto, TPREG_ID) values((select max(eval_id) from evaluaciones), (select max(mod_id) from modulos), (select max(mat_id) from materias), 'Path_test','Explicacion_test','Texto_test', (select max(tpreg_id) from tipopregunta))
-- 13 respuestas para eliminar 
insert into respuestas (preg_id,res_texto,RES_ISCORRECTA) values ((select max(preg_id) from preguntas),'texto1_test',1)
insert into respuestas (preg_id,res_texto,RES_ISCORRECTA) values ((select max(preg_id) from preguntas),'texto2_test',0)
insert into respuestas (preg_id,res_texto,RES_ISCORRECTA) values ((select max(preg_id) from preguntas),'texto3_test',0)
-- pregunta para modificar pregunta
-- (2) preguntas
insert into preguntas(eval_id, mod_id, mat_id, preg_pathgrafico, preg_explicacion, preg_texto, TPREG_ID) values((select max(eval_id) from evaluaciones), (select max(mod_id) from modulos), (select max(mat_id) from materias), 'Path2_test','Explicacion2_test','Texto2_test', (select max(tpreg_id) from tipopregunta))
---- respuesta crear
insert into respuestas (preg_id,res_texto,RES_ISCORRECTA) values ((select max(preg_id) from preguntas),'texto4_test',1)
insert into respuestas (preg_id,res_texto,RES_ISCORRECTA) values ((select max(preg_id) from preguntas),'texto5_test',1)
insert into respuestas (preg_id,res_texto,RES_ISCORRECTA) values ((select max(preg_id) from preguntas),'texto6_test',1)
-- pregunta para leer  1
-- (3) preguntas
insert into preguntas(eval_id, mod_id, mat_id, preg_pathgrafico, preg_explicacion, preg_texto, TPREG_ID) values((select max(eval_id) from evaluaciones), (select max(mod_id) from modulos), (select max(mat_id) from materias), 'Path3_test','Explicacion3_test','Texto3_test', (select max(tpreg_id) from tipopregunta))
-- respuestas leer 
insert into respuestas (preg_id,res_texto,RES_ISCORRECTA) values ((select max(preg_id) from preguntas),'texto7_test',1)
insert into respuestas (preg_id,res_texto,RES_ISCORRECTA) values ((select max(preg_id) from preguntas),'texto8_test',1)
insert into respuestas (preg_id,res_texto,RES_ISCORRECTA) values ((select max(preg_id) from preguntas),'texto9_test',1)
-- pregunta para leer 2
-- (4) preguntas
insert into preguntas(eval_id, mod_id, mat_id, preg_pathgrafico, preg_explicacion, preg_texto, TPREG_ID) values((select max(eval_id) from evaluaciones), (select max(mod_id) from modulos), (select max(mat_id) from materias), 'Path4_test','Explicacion4_test','Texto4_test', (select max(tpreg_id) from tipopregunta))
insert into respuestas (preg_id,res_texto,RES_ISCORRECTA) values ((select max(preg_id) from preguntas),'texto10_test',1)
insert into respuestas (preg_id,res_texto,RES_ISCORRECTA) values ((select max(preg_id) from preguntas),'texto11_test',1)
insert into respuestas (preg_id,res_texto,RES_ISCORRECTA) values ((select max(preg_id) from preguntas),'texto12_test',1)
