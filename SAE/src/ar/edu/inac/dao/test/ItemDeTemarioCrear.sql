--ItemDeTemarioCrearbelen
insert into Temarios(TEM_DESCRIPCION) values('temario1_test')
-- el primero es para eliminar
insert into ItemsDeTemario(TEM_ID,ITEM_DESCRIPCION) values((select max(tem_id) from temarios),'itemsDeTemario1_test')
-- lo uso para modificar
insert into ItemsDeTemario(TEM_ID,ITEM_DESCRIPCION) values((select max(tem_id) from temarios),'itemsDeTemario2_test')
-- los uso para leer
insert into ItemsDeTemario(TEM_ID,ITEM_DESCRIPCION) values((select max(tem_id) from temarios),'itemsDeTemario3_test')
insert into ItemsDeTemario(TEM_ID,ITEM_DESCRIPCION) values((select max(tem_id) from temarios),'itemsDeTemario4_test')

