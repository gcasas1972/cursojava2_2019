-- curso
insert into cursos(cur_anio, cur_descripcion) values (10, 'curso 1_test')
-- temario 
insert into temarios(tem_descripcion) values ('temario 1_test')
-- materia
insert into materias(cur_id, tem_id, mat_nombre) values ((select max(cur_id) from cursos),(select max(tem_id) from temarios), 'materia 1_test')
-- profesor
insert into profesores (prof_nombre, prof_apellido) values ('nombre_test', 'apellido_test')
-- comision
insert into comisiones(com_division, com_turno, cur_id, mat_id, prof_id) values('comision_test', 'ma�ana',(select max(cur_id) from cursos),(select max(mat_id) from materias), (select max(prof_id) from profesores))
-- usuario
insert into usuarios(usu_descripcion, usu_password) values ('usuario_test', '123')
-- alumnos
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id,usu_id) values ('nomAlu1_test','apeAlu1',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'), (select max(usu_id) from usuarios))
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id,usu_id) values ('nomAlu2_test','apeAlu2',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'), (select max(usu_id) from usuarios))
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id,usu_id) values ('nomAlu3_test','apeAlu3',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'), (select max(usu_id) from usuarios))
-- comision
insert into comisiones(com_division, com_turno, cur_id, mat_id, prof_id) values('comision2_test', 'tarde',(select max(cur_id) from cursos),(select max(mat_id) from materias), (select max(prof_id) from profesores))
-- alumnos
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id,usu_id) values ('nomAlu4_test','apeAlu5',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'), (select max(usu_id) from usuarios))
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id,usu_id) values ('nomAlu5_test','apeAlu5',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'), (select max(usu_id) from usuarios))
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id,usu_id) values ('nomAlu6_test','apeAlu6',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'), (select max(usu_id) from usuarios))
-- comision
insert into comisiones(com_division, com_turno, cur_id, mat_id, prof_id) values('comision3_test', 'noche',(select max(cur_id) from cursos),(select max(mat_id) from materias), (select max(prof_id) from profesores))
-- alumnos
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id,usu_id) values ('nomAlu7_test','apeAlu7',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'), (select max(usu_id) from usuarios))
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id,usu_id) values ('nomAlu8_test','apeAlu8',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'), (select max(usu_id) from usuarios))
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id,usu_id) values ('nomAlu9_test','apeAlu9',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'), (select max(usu_id) from usuarios))
-- comision
insert into comisiones(com_division, com_turno, cur_id, mat_id, prof_id) values('comision4_test', 'ma�ana',(select max(cur_id) from cursos),(select max(mat_id) from materias), (select max(prof_id) from profesores))
-- alumnos
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id,usu_id) values ('nomAlu10_test','apeAlu10',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'), (select max(usu_id) from usuarios))
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id,usu_id) values ('nomAlu11_test','apeAlu11',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'), (select max(usu_id) from usuarios))
insert into alumnos (alu_apellido,alu_nombre,com_id,part_id, prov_id,usu_id) values ('nomAlu12_test','apeAlu12',(select max(com_id) from comisiones),(select part_id from partidos where part_nombre like '%Mor�n%'),(select prov_id from provincias where prov_nombre like '%Buenos Aires%'), (select max(usu_id) from usuarios))
