-- 13 respuestas
delete from respuestas where res_texto like '%_test'
-- 12 preguntas
delete from preguntas where preg_pathgrafico like '%_test'
-- 11 evaluaciones
delete from evaluaciones where eval_fecharealizacion='2018-12-06'
-- 10  tipo de pregunta
delete from tipopregunta where tpreg_descripcion like '%_test'
-- 9 alumnos
delete from alumnos where alu_apellido like '%_test'
-- 8 comisiones
delete from comisiones where div_id = (select max(div_id) from division)
-- 7 division
delete from division where div_descripcion like '%_test'
-- 6 turnos
delete from turnos where tur_descripcion like '%_test'
--5 profesor
delete from profesores where prof_nombre like '%_test'
-- 4 modulos
delete from modulos where mod_descripcion like '%_test'
-- 3 materias
delete from materias where mat_nombre like '%_test'
-- 2 curos
delete from cursos where cur_descripcion  like '%_test'
-- 1 
delete from temarios where tem_descripcion like '%_test'





