package ar.edu.inac.dao.test;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.PerfilDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Perfil;


public class PerfilDAOTest {
		static Connection con;
		Statement stat;
		Perfil per = null;
	@BeforeClass	
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
        Statement consulta= con.createStatement();

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( PerfilDAOTest.class.getResource( "PerfilesCrear.sql" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma
           }
        }
        cm.desconectar();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {		
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
        Statement consulta= con.createStatement();

        String sql = "";
        BufferedReader bf = new BufferedReader( new InputStreamReader( PerfilDAOTest.class.getResource( "PerfilesEliminar.sql" ).openStream() ) );
        while ( (sql = bf.readLine()) != null ) {
           if ( sql.trim().length() != 0 &&
                !sql.startsWith( "--" ) ) {              
              consulta.executeUpdate( sql ); // aca arma
           }
        }
        
        cm.desconectar();
	}


	@Before
	public void setUp() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();

	}

	@After
	public void tearDown() throws Exception {
		con.close();
	}
	
	@Test
	public void testAgregar() {
		PerfilDAO perDao = new PerfilDAO();
		try {
			//agregque un Perfil
			perDao.agregar(new Perfil("nuevo perfil_test"));
			//hago una consulta
			ResultSet rs=stat.executeQuery("select per_id, per_descripcion" +
					" from perfiles" + 
					" where per_descripcion='nuevo perfil_test'");
			rs.next();
			assertEquals(rs.getString("per_descripcion"), "nuevo perfil_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testEliminar() {
		PerfilDAO perDao = new PerfilDAO();
		try {
					
			//hago una consulta buscando un Perfil a eliminar
			ResultSet rs=stat.executeQuery("select per_id, per_descripcion" +
					" from perfiles" + 
					" where per_descripcion='perfil1_test'");
			rs.next();			
			Perfil per = new Perfil(rs.getInt("per_id"), rs.getString("per_descripcion"));			
			perDao.eliminar(per);

			ResultSet rs2=stat.executeQuery("select per_id, per_descripcion" +
					" from perfiles" + 
					" where per_descripcion='perfil1_test'");
			//next deberia darle false, o sea no hay un proximo
			assertFalse(rs2.next());
			rs.close();
			rs2.close();			
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testModificar() {
		PerfilDAO perDao = new PerfilDAO();
		try {
					
			//hago una consulta buscando el Perfil a eliminar
			ResultSet rs=stat.executeQuery("select per_id, per_descripcion" +
					" from perfiles" + 
					" where per_descripcion='perfil2_test'");
			rs.next();			
			Perfil per = new Perfil(rs.getInt("per_id"), rs.getString("per_descripcion"));	
			per.setDescripcion("Perfil2_Modificado_test");
			perDao.modificar(per);

			ResultSet rs2=stat.executeQuery("select per_id, per_descripcion" +
					" from perfiles" + 
					" where per_descripcion='Perfil2_Modificado_test'");
			rs2.next();
			//next deberia darle false, o sea no hay un proximo
			assertEquals(rs2.getString("per_descripcion"), "Perfil2_Modificado_test");
			rs.close();
			rs2.close();			
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

	@Test
	public void testLeer_porDescripcion() {
		PerfilDAO perDao = new PerfilDAO();
		try {
			//leer un Perfil
			List perfiles = perDao.leer(new Perfil("perfil3_test"));
			//hago una consulta
			Perfil per = (Perfil)perfiles.get(0);
			assertEquals(per.getDescripcion(), "perfil3_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}
	@Test
	public void testLeer_porCodigo() {
		PerfilDAO perDao = new PerfilDAO();
		try {
			//hago una consulta buscando el Perfil a eliminar
			ResultSet rs=stat.executeQuery("select per_id, per_descripcion" +
					" from perfiles" + 
					" where per_descripcion='perfil4_test'");
			rs.next();			
			Perfil per = new Perfil(rs.getInt("per_id"));	

			//leer un Perfil 
			List perfiles = perDao.leer(per);
			//hago una consulta
			Perfil perLeida = (Perfil)perfiles.get(0);
			assertEquals(perLeida.getDescripcion(), "perfil4_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
		
	}

}
