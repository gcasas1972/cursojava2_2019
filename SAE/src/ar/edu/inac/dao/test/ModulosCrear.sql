-- curso
insert into cursos(cur_anio, cur_descripcion) values (10, 'curso 1_test')
-- temario 
insert into temarios(tem_descripcion) values ('temario 1_test')
-- materia
insert into materias(cur_id, tem_id, mat_nombre) values ((select max(cur_id) from cursos),(select max(tem_id) from temarios), 'materia 1_test')
-- modulos
insert into modulos(mat_id, mod_descripcion) values ((select max(mat_id) from materias),'modulos 1_test')
insert into modulos(mat_id, mod_descripcion) values ((select max(mat_id) from materias),'modulos 2_test')
insert into modulos(mat_id, mod_descripcion) values ((select max(mat_id) from materias),'modulos 3_test')
insert into modulos(mat_id, mod_descripcion) values ((select max(mat_id) from materias),'modulos 4_test')