package ar.edu.inac.dao.test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ar.edu.inac.dao.PartidoDAO;
import ar.edu.inac.dao.ProvinciaDAO;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Partido;
import ar.edu.inac.modelo.Provincia;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.util.List;


public class PartidoDAOTest {
		static Connection con;
		Statement stat;
		Partido part = null;
		Provincia provTest=null;
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		
		Statement consulta= con.createStatement();
		
		String sql = "";
		BufferedReader bf = new BufferedReader(new InputStreamReader(PartidoDAOTest.class.getResource("ProvinciasCrear.sql").openStream()));
		while	( (sql = bf.readLine()) != null ) {
			if 	( sql.trim().length() != 0 &&
	                !sql.startsWith( "--" ) ) {              
	              consulta.executeUpdate( sql ); // aca arma
	    	}
	    }
		cm.desconectar();
	}
	
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
        Statement consulta= con.createStatement();
        String sql = "";
        BufferedReader bf = new BufferedReader(new InputStreamReader(PartidoDAOTest.class.getResource("ProvinciasEliminar.sql").openStream()));
        while((sql = bf.readLine()) != null){
        	if(sql.trim().length()!=0 && !sql.startsWith("--")){
        		consulta.executeUpdate(sql);
        	}
        }
        cm.desconectar();
	}
	@Before
	public void setUp() throws Exception {
		ConnectionManager cm = new ConnectionManager();
		cm.conectar();
		con = cm.getConexion();
		stat = con.createStatement();
		StringBuffer sql = new StringBuffer("select prov_id, prov_nombre");
		sql.append(" from provincias");
		sql.append(" where prov_id = (select max(prov_id) from provincias)");
		ResultSet rs= stat.executeQuery(sql.toString());
		rs.next();
		provTest = new Provincia(rs.getInt("prov_id"), rs.getString("prov_nombre"));
		
	}

	@After
	public void tearDown() throws Exception {
		con.close();
		stat=null;
		provTest=null;
	}
	
	@Test
	public void testAgregar() {
		PartidoDAO partDao = new PartidoDAO();
		try{
			//Agregue un partido
			//
			partDao.agregar(new Partido("nuevo partido_test",provTest));
			//Hago una consulta
			ResultSet rs=stat.executeQuery(	"select part_id, part_nombre"+
											" from partidos where part_nombre"+
											" = 'nuevo partido_test'");
			rs.next();
			assertEquals(rs.getString("part_nombre"), "nuevo partido_test");
		}catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testEliminar() {
		PartidoDAO partDao = new PartidoDAO();
		try {
			ResultSet rs=stat.executeQuery(	"select part_id, part_nombre, prov_id"+
											" from partidos" + 
											" where part_nombre='Rio tercero_test'");
			rs.next();
			Partido part = new Partido(rs.getInt("part_id"),rs.getString("part_nombre"),new Provincia(rs.getInt("prov_id")));
			partDao.eliminar(part);
			ResultSet rs2=stat.executeQuery("select part_id, part_nombre, prov_id"+
											" from partidos" + 
											" where part_nombre='Rio tercero_test'");
			assertFalse(rs2.next());
			rs2.close();
			rs.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testModificar() {
		PartidoDAO partDao = new PartidoDAO();
		try {
			ResultSet rs=stat.executeQuery(	"select part_id, part_nombre, prov_id"+
					" from partidos" + 
					" where part_nombre='Cosquin_test'");
			rs.next();
			Partido part = new Partido(rs.getInt("part_id"),rs.getString("part_nombre"),provTest);
			part.setDescripcion("Cosquin modificado_test");
			partDao.modificar(part);
			ResultSet rs2=stat.executeQuery("select part_id, part_nombre, prov_id"+
											" from partidos" + 
											" where part_nombre='Cosquin modificado_test'");
			rs2.next();
			assertEquals(rs2.getString("part_nombre"), "Cosquin modificado_test");
			rs2.close();
			rs.close();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testLeer_porNommbre() {
		PartidoDAO partDao = new PartidoDAO();
		try {
			List partidos =partDao.leer(new Partido("Carlos Paz_test"));
			Partido part = (Partido)partidos.get(0);
			assertEquals(part.getDescripcion(), "Carlos Paz_test");
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
	
	@Test
	public void testLeer_porCodigo() {
		PartidoDAO partDao = new PartidoDAO();
		try {
			ResultSet rs=stat.executeQuery(		"select part_id, part_nombre, prov_id"+
												" from partidos" + 
												" where part_nombre='Parana_test'");
			rs.next();
			Partido part = new Partido(	rs.getInt("part_id"),
										rs.getString("part_nombre"),
										provTest);
			List partidos = partDao.leer(new Partido(rs.getInt("part_id"),null, null));
			Partido partLeido = (Partido)partidos.get(0);
			assertEquals(partLeido.getDescripcion(), "Parana_test");
		} catch (SQLException e) {
			assertTrue(false);
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			assertTrue(false);
			e.printStackTrace();
		}
	}
}
