-- lote de pruebas de tipos de componenes
-- eliminar
insert  into tiposdecomponentes(TCOMP_DESCRIPCION) values ('label_test');
-- modificar
insert  into tiposdecomponentes(TCOMP_DESCRIPCION) values ('combobox_test');
-- leer por codigo
insert  into tiposdecomponentes(TCOMP_DESCRIPCION) values ('textField_test');
-- leer por descripcion
insert  into tiposdecomponentes(TCOMP_DESCRIPCION) values ('listBox_test');
