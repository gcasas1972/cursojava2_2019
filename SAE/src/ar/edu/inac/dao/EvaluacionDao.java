package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.dao.util.FechaUtil;
import ar.edu.inac.modelo.Alumno;
import ar.edu.inac.modelo.Comision;
import ar.edu.inac.modelo.Evaluacion;
import ar.edu.inac.modelo.Materia;


public class EvaluacionDao implements DAO {

	@Override
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar=conexion.createStatement();
        Evaluacion eval=(Evaluacion)obj;
        agregar.executeUpdate("insert into evaluaciones(mat_id, " +
        												"alu_id, " +
        												"eval_estado, " +
        												"eval_fecharealizacion) " +
        												"values(" +
        													eval.getMateria().getCodigo()									+", " +
        													eval.getAlumno().getCodigo()									+ ", " +
        													eval.getEstado()												+ ", '" +
        													FechaUtil.asString(eval.getFechaDeRealizacion(), "yyyy-MM-dd")	+"')");
		ConnectionManager.desconectar();	
	}

	@Override
	public void eliminar(Object obj)throws ClassNotFoundException, SQLException  {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar =conexion.createStatement();
        Evaluacion eval=(Evaluacion)obj;

        StringBuffer sql = new StringBuffer("delete from  evaluaciones where eval_id=");
        sql.append(eval.getCodigo());
        eliminar.executeUpdate(sql.toString());
        ConnectionManager.desconectar();
	}
	@Override
	public void modificar(Object obj)throws ClassNotFoundException, SQLException  {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar =conexion.createStatement();
        Evaluacion eval=(Evaluacion)obj;
		StringBuffer sql = new StringBuffer("update evaluaciones set mat_id=");
		sql.append(eval.getMateria().getCodigo());
		sql.append(",alu_id=");
		sql.append(eval.getAlumno().getCodigo());
		sql.append(",eval_estado=");
		sql.append(eval.getEstado());
		sql.append(",eval_fecharealizacion='");
		sql.append(FechaUtil.asString(eval.getFechaDeRealizacion(), "yyyy-MM-dd") );
		sql.append("' where eval_id =");
		sql.append(eval.getCodigo());
		
		modificar.executeUpdate(sql.toString())	;
		ConnectionManager.desconectar();

	}

	@Override
	public List leer(Object obj)throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement leer = conexion.createStatement();
		Evaluacion eval = (Evaluacion)obj;
		StringBuffer sql = new StringBuffer("select  eval.eval_id ,eval.mat_id  , mat.mat_nombre,eval.alu_id  , alu.alu_nombre, alu.alu_apellido,eval.eval_estado, eval.eval_fecharealizacion");
		sql.append(" from evaluaciones eval, materias mat, alumnos alu ");
		sql.append(" where eval.mat_id = mat.mat_id and eval.alu_id = alu.alu_id");
		
		if (eval!=null && !eval.isVacio()){
			if (eval.getCodigo()>0){
				sql.append(" and eval.eval_id=");
				sql.append(eval.getCodigo());				
			}else if (eval.getMateria()!=null){
				sql.append(" and eval.mat_id =");
				sql.append(eval.getMateria().getCodigo());						
			}else if (eval.getAlumno()!=null){
				sql.append(" and eval.alu_id=");
				sql.append(eval.getAlumno().getCodigo());
			}else if (eval.getEstado()>0){
				sql.append(" and eval.eval_estado=");
				sql.append(eval.getEstado());
			}else if (eval.getFechaDeRealizacion()!=null){
				sql.append(" and eval.eval_fecharealizacion= '");
				sql.append(FechaUtil.asString(eval.getFechaDeRealizacion(), "yyyy-MM-dd"));
				sql.append("'");
			}
		}
		sql.append(" order by mat.mat_nombre,alu.alu_apellido, alu.alu_nombre");
		ResultSet rs = leer.executeQuery(sql.toString());
		List provincias = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return provincias;
	}
		
		private List convertRsToObje(ResultSet rs) throws SQLException {
			List<Evaluacion> evaluacion = new ArrayList<Evaluacion>();
			Alumno alu ;
			Materia mat;
			while (rs.next()){
				//alumno
				alu = new Alumno(rs.getInt("alu_id"),rs.getString("alu_nombre"),rs.getString("alu_apellido"));
				//materia
				mat = new Materia(rs.getInt("mat_id"), rs.getString("mat_nombre"), null);
				//TOTO Gabrielito ver el error 
//				evaluacion.add(new Evaluacion(rs.getInt("eval_id"),
//											  mat,
//											  alu,
//											  rs.getInt("eval_estado"),
//											  rs.getDate("eval_fecharealizacion"),
//											  null));			
			}
			return evaluacion;
		}
	
}
