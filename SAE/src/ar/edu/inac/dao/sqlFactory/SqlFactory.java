package ar.edu.inac.dao.sqlFactory;

import java.util.ArrayList;
import java.util.List;

public abstract class SqlFactory {
	private static List<SqlFactory> sqlFactories;
	//
	public SqlFactory(){
		sqlFactories = new ArrayList<SqlFactory>();
		sqlFactories.add(new SqlPreguntaFactory());
		
	}
	public static SqlFactory getInstance(Object obj){
		SqlFactory sqlFactResult=null;
		for (SqlFactory sqlF : sqlFactories) {
			if(sqlF.validar(obj))
				 sqlFactResult=sqlF;				
		}
		return sqlFactResult;
	}
	public abstract String getSql();
	public abstract boolean validar(Object obj);
}
