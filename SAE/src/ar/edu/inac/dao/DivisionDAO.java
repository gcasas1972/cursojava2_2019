package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Division;

public class DivisionDAO implements DAO {

	@Override
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();

		Division div = (Division) obj;

		StringBuffer sql = new StringBuffer(
				"insert into division (div_descripcion)");

		sql.append(" values('");
		sql.append(div.getDescripcion());
		sql.append("')");

		agregar.execute(sql.toString());

		agregar.close();
		ConnectionManager.desconectar();

	}

	@Override
	public void eliminar(Object obj) throws ClassNotFoundException,
			SQLException {

		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar = conexion.createStatement();
		Division div = (Division) obj;

		StringBuffer sql = new StringBuffer("delete from division where");
		sql.append(" DIV_ID=");
		sql.append(div.getCodigo());

		eliminar.execute(sql.toString());

		eliminar.close();
		ConnectionManager.desconectar();

	}

	@Override
	public void modificar(Object obj) throws ClassNotFoundException,
			SQLException {

		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar = conexion.createStatement();

		Division div = (Division) obj;

		StringBuffer sql = new StringBuffer(
				"update division set div_descripcion='");
		sql.append(div.getDescripcion());
		sql.append("' where DIV_ID=");
		sql.append(div.getCodigo());
		modificar.execute(sql.toString());

		modificar.close();
		ConnectionManager.desconectar();

	}

	@Override
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement leer = conexion.createStatement();

		Division div = (Division) obj;

		StringBuffer sql = new StringBuffer(
				"select DIV_ID, DIV_DESCRIPCION from division ");

		if (div != null && !div.isVacio()) {
			if (div.getCodigo() > 0) {
				sql.append("where div_id=");
				sql.append(div.getCodigo());
			} else if (div.getDescripcion() != null
					|| !div.getDescripcion().isEmpty()) {
				sql.append("where DIV_DESCRIPCION LIKE '%");
				sql.append(div.getDescripcion());
				sql.append("%'");

			}
		}

		sql.append(" order by div_id, div_descripcion");
		ResultSet rs = leer.executeQuery(sql.toString());
		List division = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return division;
	}

	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<Division> division = new ArrayList<Division>();
		while (rs.next()) {
			division.add(new Division(rs.getInt("div_id"), rs
					.getString("div_descripcion")));
		}
		return division;
	}

}
