package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Pantalla;


/**
 * @author Agustin
 * Fecha: 28/03/2019
 * Esta clase corresponde a la clase Pantalla de DAO para el acceso a la base de datos.
 */
public class PantallaDAO implements DAO{
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Pantalla pan = (Pantalla)obj;
		
		StringBuffer sql = new StringBuffer("insert into pantallas (pan_descripcion, pan_nombre) values ('");
		sql.append(pan.getDescripcion());
		sql.append("','");
		sql.append(pan.getNombre());
		sql.append("')");

		
		agregar.executeUpdate(sql.toString())	;
		ConnectionManager.desconectar();
	}
	public void eliminar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar = conexion.createStatement();
		
		Pantalla pan = (Pantalla)obj;
		eliminar.executeUpdate("delete from pantallas where pan_id =" +
								pan.getCodigo());
		ConnectionManager.desconectar();
		
	}
	public void modificar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Pantalla pan = (Pantalla)obj;
		agregar.executeUpdate("update pantallas set " +
				"pan_descripcion='" + pan.getDescripcion() + "' "+
				" where pan_id =" 	+ pan.getCodigo() );
		ConnectionManager.desconectar();

		
	}
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Pantalla pan = (Pantalla)obj;
		
		StringBuffer sb = new StringBuffer("select pan_id, pan_descripcion, " );
		sb.append("pan_nombre from pantallas");

		if (pan!=null && !pan.isVacio()){
			if (pan.getCodigo()>0){
				sb.append(" where pan_id=");
				sb.append(pan.getCodigo());
			}if (pan.getNombre()!=null && !pan.getNombre().isEmpty()){
				sb.append(" where pan_nombre like '%");
				sb.append(pan.getNombre());
				sb.append("%'");
			}else if (pan.getDescripcion()!=null && !pan.getDescripcion().isEmpty()){
				sb.append(" where pan_descripcion like '%");
				sb.append(pan.getDescripcion());
				sb.append("%'");
			//TODO JONATHAN VANINA FINALIZADO	
				
			}
		}
		ResultSet rs = agregar.executeQuery(sb.toString());
		List pantallas = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return pantallas;
	}
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<Pantalla> pantallas = new ArrayList<Pantalla>();
		while (rs.next()){
			pantallas.add(new Pantalla(rs.getInt("pan_id"), 
										rs.getString("pan_nombre"),
										rs.getString("pan_descripcion")));			
	}
		return pantallas;
}
}
