package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.TipoDeComponente;
import ar.edu.inac.modelo.TipoDePregunta;

public class TipoDeComponenteDAO implements DAO {

	@Override
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		
		TipoDeComponente tcomp = (TipoDeComponente) obj;
		
		StringBuffer sql = new 
				StringBuffer("insert into tiposdecomponentes(TCOMP_DESCRIPCION)");
		sql.append("values('");
		sql.append(tcomp.getDescripcion());
		sql.append("')");
		
		agregar.execute(sql.toString());
		
	    agregar.close();
	    ConnectionManager.desconectar();
		
		
		
		

	}

	@Override
	public void eliminar(Object obj) throws ClassNotFoundException,
			SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement  eliminar = conexion.createStatement();
		
		TipoDeComponente tcomp = (TipoDeComponente) obj;
		
		StringBuffer sql = new StringBuffer("delete from tiposdecomponentes where ");
		sql.append("TCOMP_ID=");
		sql.append(tcomp.getCodigo());
		
		eliminar.execute(sql.toString());
		
		eliminar.close();
		ConnectionManager.desconectar();

	}

	@Override
	public void modificar(Object obj) throws ClassNotFoundException,
			SQLException {
		
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar = conexion.createStatement();

		TipoDeComponente tcomp = (TipoDeComponente) obj;
		
		StringBuffer sql = 
			 new StringBuffer("update tiposdecomponentes set TCOMP_DESCRIPCION='");
		sql.append(tcomp.getDescripcion());
		sql.append("' where TCOMP_ID=");
		sql.append(tcomp.getCodigo());
		modificar.execute(sql.toString());
		
		modificar.close();
		ConnectionManager.desconectar();
		
	}

	@Override
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement leer = conexion.createStatement();
		
		TipoDeComponente tcomp = (TipoDeComponente) obj;
		
		StringBuffer sql = 
			new StringBuffer("select TCOMP_ID, TCOMP_DESCRIPCION from tiposdecomponentes");
		if(tcomp!=null && !tcomp.isVacio()){
			if(tcomp.getCodigo()>0){
				sql.append("where tcomp_id=");
				sql.append(tcomp.getCodigo());
			}else if(tcomp.getDescripcion()!=null || !tcomp.getDescripcion().isEmpty()){
				sql.append("where TCOMP_DESCRIPCION like %'");
				sql.append(tcomp.getDescripcion());
				sql.append("%'");				
			}
			
		
	}
		ResultSet rs = leer.executeQuery(sql.toString());
		return convertRsToObje(rs);

	}
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<TipoDeComponente> tiposDecomponentes = new ArrayList<TipoDeComponente>();
		while (rs.next()){
			tiposDecomponentes.add(new TipoDeComponente(rs.getInt("TCOMP_ID"), rs.getString("TCOMP_DESCRIPCION")));			
		}
		return tiposDecomponentes;
	}
}
