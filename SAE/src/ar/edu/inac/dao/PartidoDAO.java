package ar.edu.inac.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Partido;
import ar.edu.inac.modelo.Provincia;

/**
 * @author Lautaro
 * Fecha: 19/10/2018
 * Esta clase corresponde a la clase Partido de DAO para el acceso a la base de datos.
 * mod by gcasas se agregaron las implementeacines de los metodos eliminar, modificar y leer
 */
public class PartidoDAO implements DAO{
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Partido par = (Partido)obj;
		agregar.executeUpdate("insert into partidos(prov_id, part_nombre) values(" +
								par.getProvincia().getCodigo()+", "+
								"'" + par.getDescripcion() + "')");
		ConnectionManager.desconectar();
	}
	
	public void eliminar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Partido par = (Partido)obj;
		agregar.executeUpdate("delete from partidos where part_id ="+
								par.getCodigo());
		ConnectionManager.desconectar();	
	}
	
	public void modificar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Partido par = (Partido)obj;
		agregar.executeUpdate("update partidos set " +
				"part_nombre='" + par.getDescripcion() + "' "+
				" where part_id =" 	+ par.getCodigo() );
		ConnectionManager.desconectar();
	}
	
	public List leer(Object obj) throws ClassNotFoundException, SQLException {{
			ConnectionManager.conectar();
			Connection conexion = ConnectionManager.getConexion();
			Statement agregar = conexion.createStatement();
			Partido part = (Partido)obj;
			
			StringBuffer sb = new StringBuffer("select par.part_id, par.part_nombre, prov.prov_id, prov.prov_nombre");
			sb.append(" from partidos par, provincias prov");
			sb.append(" where par.prov_id = prov.prov_id");
			
			if (part!=null && !part.isVacio()){
				if (part.getCodigo()>0){
					sb.append(" and par.part_id=");
					sb.append(part.getCodigo());
				}else if (part.getDescripcion()!=null && !part.getDescripcion().isEmpty()){
					sb.append(" and par.part_nombre='");
					sb.append(part.getDescripcion());
					sb.append("'");
				}
			}
			ResultSet rs = agregar.executeQuery(sb.toString());
			List partidos = convertRsToObje(rs);
			ConnectionManager.desconectar();
			return partidos;	
		}
	}
	
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<Partido> partidos = new ArrayList<Partido>();
		while (rs.next()){
			partidos.add(new Partido(rs.getInt("par.part_id"), rs.getString("par.part_nombre"),rs.getInt("prov.prov_id"),rs.getString("prov.prov_nombre")));}
		return partidos;
	}
}