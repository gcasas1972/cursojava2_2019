package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Provincia;

public class CursoDao implements DAO {

	@Override
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		
		//tengo que castaear a curso
		Curso cur = (Curso)obj;
		
		StringBuffer sql = new StringBuffer("insert into cursos (cur_anio, cur_descripcion) values (");
		sql.append(cur.getAnio());
		sql.append(",'");
		sql.append(cur.getDescripcion());
		sql.append("')");
		
		agregar.executeUpdate(sql.toString())	;
		
	}
	@Override
	public void eliminar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar = conexion.createStatement();
		
		//tengo que castaear a curso
		Curso cur = (Curso)obj;
		
		StringBuffer sql = new StringBuffer("delete from cursos where cur_id=");
		sql.append(cur.getCodigo());
		
		eliminar.executeUpdate(sql.toString())	;

	}

	@Override
	public void modificar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar = conexion.createStatement();
		
		//tengo que castaear a curso
		Curso cur = (Curso)obj;
		
		StringBuffer sql = new StringBuffer("update cursos set cur_anio=");
		sql.append(cur.getAnio());
		sql.append(",cur_descripcion='");
		sql.append(cur.getDescripcion());
		sql.append("' where cur_id =");
		sql.append(cur.getCodigo());
		
		modificar.executeUpdate(sql.toString())	;
	}

	@Override
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement leer = conexion.createStatement();
		Curso cur = (Curso)obj;
		
		StringBuffer sql = new StringBuffer("select cur_id, cur_anio, cur_descripcion from cursos");
		if (cur!=null && !cur.isVacio()){
			if (cur.getCodigo()>0){
				sql.append(" where cur_id=");
				sql.append(cur.getCodigo());
			}else if (cur.getAnio()>0){
				sql.append(" where cur_anio =");
				sql.append(cur.getAnio());						
			}else if (cur.getDescripcion()!=null && !cur.getDescripcion().isEmpty()){
				sql.append(" where cur_descripcion like '%");
				sql.append(cur.getDescripcion());
				sql.append("%'");				
			}
		}
		sql.append(" order by cur_anio, cur_descripcion");
		ResultSet rs = leer.executeQuery(sql.toString());
		List cursos = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return cursos;
	}
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<Curso> cursos = new ArrayList<Curso>();
		while (rs.next()){
			cursos.add(new Curso(rs.getInt("cur_id"),rs.getInt("cur_anio"), rs.getString("cur_descripcion")));			
		}
		return cursos;
	}
	

}
