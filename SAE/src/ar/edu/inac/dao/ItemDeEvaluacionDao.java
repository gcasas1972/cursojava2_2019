package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.ItemDeEvaluacion;

public class ItemDeEvaluacionDao implements DAO {

	@Override
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
	ConnectionManager.conectar();
	Connection conexion = ConnectionManager.getConexion();
	Statement agregar=conexion.createStatement();
    ItemDeEvaluacion val=(ItemDeEvaluacion)obj;
	agregar.executeUpdate("insert eva-id, itemeva_texto, itemeva_id" +
			"itemeva_texto='" + val.getTexto() + "' "+
			"itemeva_id =" 	+ val.getCodigo());
	ConnectionManager.desconectar();	
}

	@Override
    public void eliminar(Object obj)throws ClassNotFoundException, SQLException  {
	ConnectionManager.conectar();
	Connection conexion = ConnectionManager.getConexion();
	Statement agregar=conexion.createStatement();
	ItemDeEvaluacion val=(ItemDeEvaluacion)obj;
	agregar.executeUpdate("insert eva_id, itemeva_texto, itemeva_id" +
			"itemeva_texto='" + val.getCodigo() + "' "+
			"itemeva_id =" 	+ val.getCodigo());	
	ConnectionManager.desconectar();
}


	@Override
public void modificar(Object obj)throws ClassNotFoundException, SQLException  {
	ConnectionManager.conectar();
	Connection conexion = ConnectionManager.getConexion();
	Statement agregar=conexion.createStatement();
	ItemDeEvaluacion val=(ItemDeEvaluacion)obj;
	agregar.executeUpdate("insert eva_id, itemeva_texto, item_id" +
			"itemeva_texto='" + val.getCodigo() + "' "+
			"itemeva_id =" 	+ val.getCodigo());
	ConnectionManager.desconectar();

}

	@Override
public List leer(Object obj)throws ClassNotFoundException, SQLException {
	ConnectionManager.conectar();
	Connection conexion = ConnectionManager.getConexion();
	Statement agregar = conexion.createStatement();
	ItemDeEvaluacion val=(ItemDeEvaluacion)obj;
	agregar.executeUpdate("select eva_id, itemeva_texto,item_id" +
			"item_texto='" + val.getCodigo() + "' "+
			"item_eva =" 	+ val.getCodigo());
	ConnectionManager.desconectar();
	
	StringBuffer sb = new StringBuffer("select cod_id, texto set ");
	if (val!=null && !val.isVacio()){
		if (val.getCodigo()>0){
			sb.append(" where cod_id==");
			sb.append(val.getCodigo());
		}else if (val.getCodigo()!=0){
			sb.append(" where texto='");
			sb.append(val.getTexto());		
		}
		}
	ResultSet rs = agregar.executeQuery(sb.toString());
	List ItemDeEvaluacion = convertRsToObje(rs);
	ConnectionManager.desconectar();
	return ItemDeEvaluacion;
}
	
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<ItemDeEvaluacion> ItemDeEvaluacion = new ArrayList<ItemDeEvaluacion>();
//		while (rs.next()){
//			ItemDeEvaluacion.add(new ItemDeEvaluacion(rs.getInt("eva_id"),
//					                                  rs.getString("itemeva_texto"),
//					                                  rs.getInt("itemeva_id")));			
//	}
		return ItemDeEvaluacion;
	}
}
