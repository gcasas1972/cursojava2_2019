package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Alumno;
import ar.edu.inac.modelo.Comision;
import ar.edu.inac.modelo.Partido;
import ar.edu.inac.modelo.Provincia;

/**
 * @author Lautaro
 * Fecha: 30/11/2018
 * Esta clase corresponde a la interaccion entre la base de datos y Java.
 */

public class AlumnoDAO implements DAO{
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement stm = conexion.createStatement();
		Alumno alu = (Alumno)obj;
		stm.executeUpdate(	"insert into alumnos (alu_email, " +
												"alu_dni, " +
												"alu_telefono, " +
												"alu_direccion, " +
												"alu_apellido, " +
												"alu_nombre, " +
												"com_id, " +
												"prov_id, " +
												"part_id)" +
							"values('"+	alu.getEmail()								+		"', '"		+
										alu.getDni()								+		"', '"		+
										alu.getTelefono()							+		"', '"		+
										alu.getDireccion()							+		"', '"		+
										alu.getApellido()							+		"', '" 		+
										alu.getNombre()								+		"', "		+
										alu.getComision().getCodigo()				+		", "		+
										alu.getPartido().getProvincia().getCodigo()	+		", "		+
										alu.getPartido().getCodigo()				+		")")		;
		ConnectionManager.desconectar();
	}
	
	public void eliminar(Object obj) throws ClassNotFoundException,
			SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement stm = conexion.createStatement();
		Alumno alu = (Alumno)obj;
		stm.executeUpdate("delete from alumnos where alu_id="+alu.getCodigo());
		ConnectionManager.desconectar();
	}

	public void modificar(Object obj) throws ClassNotFoundException,
			SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement stm = conexion.createStatement();
		Alumno alu = (Alumno)obj;
		stm.executeUpdate("update alumnos set " +
							"alu_email='"		+alu.getEmail()									+"', " +
							"alu_dni='"			+alu.getDni()									+"', " +
							"alu_telefono='"	+alu.getTelefono()								+"', " +
							"alu_direccion='"	+alu.getDireccion()								+"', " +
							"alu_apellido='"	+alu.getApellido()								+"', " +
							"alu_nombre='"		+alu.getNombre()								+"', " +
							"com_id="			+alu.getComision().getCodigo()					+", " +
							"prov_id="			+alu.getPartido().getProvincia().getCodigo()	+", " +
							"part_id="			+alu.getPartido().getCodigo());
		ConnectionManager.desconectar();
	}

	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement stm = conexion.createStatement();
		Alumno alu = (Alumno)obj;
		StringBuffer sb = new StringBuffer(	"select a.alu_id, a.alu_email, a.alu_dni, a.alu_telefono, a.alu_direccion, a.alu_apellido, " +
											"a.alu_nombre, a.com_id, a.prov_id, a.part_id, a.alu_id, " +
											"prov.prov_id, prov.prov_nombre, " +
											"part.part_id, part.prov_id, part.part_nombre " +
											"from alumnos a, provincias prov, partidos part " +
											"where a.part_id = part.part_id and a.prov_id = prov.prov_id");
		if(alu!=null && !alu.isVacio()){
			if(alu.getCodigo()>0){
				sb.append("and alu_id="+alu.getCodigo());
			}else if((alu.getApellido()!=null || alu.getNombre()!=null)&&(!alu.getApellido().isEmpty()||!alu.getNombre().isEmpty())){
				sb.append("and alu_apellido='"+alu.getApellido()+"' and alu_nombre='"+alu.getNombre()+"'");
			}
		}
		ResultSet rs = stm.executeQuery(sb.toString());
		List alumnos = convertRsToObj(rs);
		ConnectionManager.desconectar();
		return alumnos;
	}
	
	private List convertRsToObj(ResultSet rs) throws SQLException {
		List <Alumno> alumnos= new ArrayList<Alumno>();
		while(rs.next()){
			alumnos.add(new Alumno(	rs.getString("alu_nombre"),
									rs.getString("alu_apellido"),
									rs.getString("alu_direccion"),
									rs.getString("alu_telefono"),
									rs.getString("alu_dni"),
									rs.getString("alu_email"),
									//TODO íTerminar!
									new Comision(),
									new Partido(rs.getInt("part_id"),rs.getString("part_nombre"),
											new Provincia(rs.getInt("prov_id"),rs.getString("prov_nombre"))),
									new Provincia(rs.getInt("prov_id"),rs.getString("prov_nombre"))));
		}
		return alumnos;
	}
}
