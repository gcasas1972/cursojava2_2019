package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Turno;


public class TurnoDAO implements DAO {
    
	@Override
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		
		
		Turno tur = (Turno)obj;
		
		StringBuffer sql = new StringBuffer("insert into turnos (tur_descripcion) values ('");
		
		sql.append(tur.getDescripcion());
		sql.append("')");
		
		agregar.executeUpdate(sql.toString())	;
	
	}
	
	@Override
	public void eliminar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar = conexion.createStatement();
		
	
		Turno tur = (Turno)obj;
		
		StringBuffer sql = new StringBuffer("delete from turnos where tur_id=");
		sql.append(tur.getCodigo());
		
		eliminar.executeUpdate(sql.toString())	;
		
	}

	@Override
	public void modificar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar = conexion.createStatement();
		
	    Turno tur = (Turno)obj;
		
		StringBuffer sql = new StringBuffer("update turnos set");
		
		sql.append(" tur_descripcion='");
		sql.append(tur.getDescripcion());
		sql.append("' where tur_id =");
		sql.append(tur.getCodigo());
		
		modificar.executeUpdate(sql.toString())	;
	
	}

	@Override
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement leer = conexion.createStatement();
		
		Turno tur = (Turno)obj;
		
		StringBuffer sql = new StringBuffer("select tur_id, tur_Codigo, tur_descripcion from turnos");
		if (tur!=null && !tur.isVacio()){
			if (tur.getCodigo()>0){
				sql.append(" where tur_id=");
				sql.append(tur.getCodigo());
			}else if (tur.getDescripcion()!=null && !tur.getDescripcion().isEmpty()){
				sql.append(" where tur_descripcion like '%");
				sql.append(tur.getDescripcion());
				sql.append("%'");				
			}
		}
		sql.append(" order by tur_Codigo, tur_descripcion");
		ResultSet rs = leer.executeQuery(sql.toString());
		List turnos = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return turnos;
	}
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<Turno> turnos = new ArrayList<Turno>();
		while (rs.next()){
			turnos.add(new Turno(rs.getInt("tur_id"),  rs.getString("tur_descripcion")));			
		}
		return turnos;
	}
		
		
	

	}


