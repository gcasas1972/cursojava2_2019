package ar.edu.inac.dao		;

import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Temario;
import java.sql.Connection	;
import java.sql.ResultSet	;
import java.sql.SQLException;
import java.sql.Statement	;
import java.util.ArrayList	;
import java.util.List		;
import ar.edu.inac.dao.util.ConnectionManager	;


/**
 * @author Iv�n
 * Fecha: 5/10/2018
 * Esta clase corresponde a la clase Temario de DAO para el acceso a la base de datos.
 */
public class TemarioDAO implements DAO{
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar()							;
		Connection conexion = ConnectionManager.getConexion()	;
		Statement agregar 	= conexion.createStatement()		;
		Temario tem 		= (Temario)obj						;
		agregar.executeUpdate(	"insert into temarios (tem_descripcion) values(" +
								"'" 										+
								tem.getDescripcion() 						+ "')")	;
		ConnectionManager.desconectar();
	}
	public void eliminar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar()							;
		Connection conexion = ConnectionManager.getConexion()	;
		Statement agregar 	= conexion.createStatement()		;
		Temario tem 		= (Temario)obj						;
		agregar.executeUpdate("delete from temarios where tem_id =" +
								tem.getCodigo());
		ConnectionManager.desconectar()			;
		
	}
	public void modificar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion()	;
		Statement agregar 	= conexion.createStatement()		;
		Temario tem 		= (Temario)obj						;
		agregar.executeUpdate(	"update temarios set " 	+
								"tem_descripcion='"		+ tem.getDescripcion() 	+ "' "+
								" where tem_id =" 		+ tem.getCodigo());
		ConnectionManager.desconectar();

		
	}
	
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Temario tem = (Temario)obj;
		
		StringBuffer sb = new StringBuffer("select tem_id, tem_descripcion from temarios");
		if (tem!=null && !tem.isVacio()){
			if (tem.getCodigo()>0){
				sb.append(" where tem_id=");
				sb.append(tem.getCodigo());						
			}else if (tem.getDescripcion()!=null && !tem.getDescripcion().isEmpty()){
				sb.append(" where tem_descripcion like '");
				sb.append(tem.getDescripcion().trim());
				sb.append("%'");				
			}
		}
		sb.append(" order by tem_descripcion");
		ResultSet rs = agregar.executeQuery(sb.toString());
		List temarios = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return temarios;
	}
	

	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<Temario> temarios = new ArrayList<Temario>();
		while (rs.next()){
			temarios.add(new Temario(rs.getInt("tem_id"), rs.getString("tem_descripcion")));			
	}
		return temarios;
}
}
