package ar.edu.inac.dao.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ArchivoUtil {
	public static void copiarArchivo(String pOrigen, String pDestino){
        File origen = new File(pOrigen);
        File destino = new File(pDestino);

        try {
                InputStream in = new FileInputStream(origen);
                OutputStream out = new FileOutputStream(destino);

                byte[] buf = new byte[1024];
                int len;

                while ((len = in.read(buf)) > 0) {
                        out.write(buf, 0, len);
                }

                in.close();
                out.close();
        } catch (IOException ioe){
                ioe.printStackTrace();
        }

	}

}
