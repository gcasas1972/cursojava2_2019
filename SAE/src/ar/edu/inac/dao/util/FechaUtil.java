package ar.edu.inac.dao.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class FechaUtil {
	public static Date asDate(int pAnio, int pMes, int pDia){
		Calendar cal = Calendar.getInstance();
		int mes =0;
		switch (pMes) {
		case 1:
			mes = Calendar.JANUARY;
			break;
		case 2:
			mes = Calendar.FEBRUARY;
			break;
		case 3:
			mes = Calendar.MARCH;
			break;
		case 4:
			mes = Calendar.APRIL;
			break;
		case 5:
			mes = Calendar.MAY;
			break;
		case 6:
			mes = Calendar.JUNE;
			break;
		case 7:
			mes = Calendar.JULY;
			break;
		case 8:
			mes = Calendar.AUGUST;
			break;
		case 9:
			mes = Calendar.SEPTEMBER;
			break;
		case 10:
			mes = Calendar.OCTOBER;
			break;
		case 11:
			mes = Calendar.NOVEMBER;
			break;
		case 12:
			mes = Calendar.DECEMBER;
			break;
		default:
			break;
		}
		cal.set(pAnio, mes, pDia);
		return cal.getTime();
	}
	public static String asString(Date pfecha, String pFormat){
		return new SimpleDateFormat(pFormat).format(pfecha);
	}
}
