package ar.edu.inac.dao.util.test;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.inac.dao.util.FechaUtil;

public class FechaUtilTest {
	Date fechaNac ;
	String strNacimiento = new String ("1972-11-17");
	SimpleDateFormat sdf ;
	@Before
	public void setUp() throws Exception {
		sdf = new SimpleDateFormat("yyyy/MM/dd");
		Calendar cal = Calendar.getInstance();
		cal.set(1972,Calendar.NOVEMBER,17);
		fechaNac = cal.getTime();
	}

	@After
	public void tearDown() throws Exception {
		sdf = null;
		fechaNac = null;
	}

	@Test
	public void testAsDate() {
		Date fecha= FechaUtil.asDate(1972, 11, 17);
		assertEquals("1972/11/17", sdf.format(fecha));
	}

	@Test
	public void testAsString() {
		;
		assertEquals("17/11/1972", FechaUtil.asString(fechaNac, "dd/MM/yyyy"));
		
	}

}
