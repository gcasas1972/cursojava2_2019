package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Perfil;


/**
 * @author Nazareno
 * Fecha: 28/03/2019
 * Esta clase corresponde a la clase Perfil de DAO para el acceso a la base de datos.
 */
public class PerfilDAO implements DAO{
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Perfil per = (Perfil)obj;
		
		StringBuffer sql = new StringBuffer("insert into perfiles(per_descripcion) values (");
		sql.append(per.getCodigo());
		sql.append(",'");
		sql.append(per.getDescripcion());
		sql.append("')");
		
		
		agregar.executeUpdate("insert into perfiles (per_descripcion) values(" +
								"'" +
								per.getDescripcion() + "')");
		agregar.close();
		ConnectionManager.desconectar();
	}
	public void eliminar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar = conexion.createStatement();
		Perfil per = (Perfil)obj;
		
		StringBuffer sql = new StringBuffer("delete from perfiles where per_id=");
		sql.append("per_id");
		sql.append(per.getCodigo());
		eliminar.execute(sql.toString());
		
		eliminar.close();
		
		ConnectionManager.desconectar();
		
	}
	public void modificar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar = conexion.createStatement();
		Perfil per = (Perfil)obj;
		
		StringBuffer sql = new StringBuffer("update perfiles set per_descripcion='");
		sql.append(per.getCodigo());
		sql.append(",per_descripcion='");
		sql.append(per.getDescripcion());
		sql.append("' where per_id =");
		sql.append(per.getCodigo());
		modificar.execute(sql.toString());
		
		modificar.close();
		
		ConnectionManager.desconectar();
}
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Perfil per = (Perfil)obj;
		
		StringBuffer sql = new StringBuffer("select per_id, per_descripcion from perfiles");
		if (per!=null && !per.isVacio()){
			if (per.getCodigo()>0){
				sql.append(" where per_id=");
				sql.append(per.getCodigo());
			}else if (per.getDescripcion()!=null && !per.getDescripcion().isEmpty()){
				sql.append(" where per_descripcion like '%");
				sql.append(per.getDescripcion());
				sql.append("%'");				
			}
		}
		//me trae la estructura de la base de datos
		ResultSet rs = agregar.executeQuery(sql.toString());
		List perfiles = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return perfiles;
	}
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<Perfil> perfiles = new ArrayList<Perfil>();
		while (rs.next()){
			//hago la conversion de lista en objetos
			perfiles.add(new Perfil(rs.getInt("per_id"), rs.getString("per_descripcion")));			
	}
	
		return perfiles;
}
}
