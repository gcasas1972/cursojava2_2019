package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.ItemDeTemario;
import ar.edu.inac.modelo.Provincia;
import ar.edu.inac.modelo.Temario;

/**
 * @author Belen
 * Fecha: 28/03/2019
 * Esta clase corresponde a la clase Provincia de DAO para el acceso a la base de datos.
 */
public class ItemDeTemarioDAO implements DAO{
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		ItemDeTemario item= (ItemDeTemario)obj;
		agregar.executeUpdate("insert into ItemsDeTemario (tem_id, item_descripcion) values(" +
								item.getTemario().getCodigo()+
								",'" +
								item.getDescripcion() + "')");
		ConnectionManager.desconectar();
	}
	public void eliminar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		ItemDeTemario item= (ItemDeTemario)obj;
		agregar.executeUpdate("delete from ItemsDeTemario where item_id =" +
								item.getCodigo());
		ConnectionManager.desconectar();
		
	}
	public void modificar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		ItemDeTemario item = (ItemDeTemario)obj;
		agregar.executeUpdate("update itemsDeTemario set " +
				"item_descripcion='" + item.getDescripcion() + "' "+
				" where item_id =" 	+ item.getCodigo() );
		ConnectionManager.desconectar();

		
	}
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		ItemDeTemario item= (ItemDeTemario)obj;
		
		StringBuffer sb = new StringBuffer("select t.tem_id, t.tem_descripcion");
			sb.append(", i.item_id, i.item_descripcion");
			sb.append(" from itemsDetemario i, temarios t");
			sb.append(" where t.tem_id=i.tem_id");
		if (item!=null && !item.isVacio()){
			if (item.getCodigo()>0){
				sb.append(" and item_id=");
				sb.append(item.getCodigo());
			}else if (item.getDescripcion()!=null && !item.getDescripcion().isEmpty()){
				sb.append(" and item_descripcion='");
				sb.append(item.getDescripcion());
				sb.append("'");				
			}
		}
		ResultSet rs = agregar.executeQuery(sb.toString());
		List ItemDeTemario = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return ItemDeTemario;
	}
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<ItemDeTemario> ItemDeTemariolist = new ArrayList<ItemDeTemario>();
		while (rs.next()){
			ItemDeTemariolist.add(new ItemDeTemario(rs.getInt("i.item_id"),
					rs.getString("i.item_descripcion"),
					new Temario(rs.getInt("t.tem_id"), rs.getString("t.tem_descripcion"))));			
	}
		return ItemDeTemariolist;
}
}
