package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Provincia;

/**
 * @author Lautaro
 * Fecha: 28/09/2018
 * Esta clase corresponde a la clase Provincia de DAO para el acceso a la base de datos.
 */
public class ProvinciaDAO implements DAO{
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Provincia prov = (Provincia)obj;
		
		//crea un solo objeto
		StringBuffer sql = new StringBuffer("insert into provincias (prov_nombre) values('");
		sql.append(prov.getDescripcion());
		sql.append("')");

		
		agregar.executeUpdate(sql.toString());
		ConnectionManager.desconectar();
	}
	public void eliminar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Provincia prov = (Provincia)obj;
		agregar.executeUpdate("delete from provincias where prov_id =" +
								prov.getCodigo());
		ConnectionManager.desconectar();
		
	}
	public void modificar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Provincia prov = (Provincia)obj;
		agregar.executeUpdate("update provincias set " +
				"prov_nombre='" + prov.getDescripcion() + "' "+
				" where prov_id =" 	+ prov.getCodigo() );
		ConnectionManager.desconectar();

		
	}
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Provincia prov = (Provincia)obj;
		
		StringBuffer sb = new StringBuffer("select prov_id, prov_nombre from provincias");
		if (prov!=null && !prov.isVacio()){
			if (prov.getCodigo()>0){
				sb.append(" where prov_id=");
				sb.append(prov.getCodigo());
			}else if (prov.getDescripcion()!=null && !prov.getDescripcion().isEmpty()){
				sb.append(" where prov_nombre='");
				sb.append(prov.getDescripcion());
				sb.append("'");				
			}
		}
		ResultSet rs = agregar.executeQuery(sb.toString());
		List provincias = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return provincias;
	}
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<Provincia> provincias = new ArrayList<Provincia>();
		while (rs.next()){
			provincias.add(new Provincia(rs.getInt("prov_id"), rs.getString("prov_nombre")));			
	}
		return provincias;
}
}
