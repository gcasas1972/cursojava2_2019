package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Usuario;

/**
 * @author Lautaro
 * Fecha: 28/09/2018
 * Esta clase corresponde a la clase Usuarios de DAO para el acceso a la base de datos.
 */
public class UsuariosDAO implements DAO{
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		
		Usuario usu = (Usuario)obj;
		StringBuffer sql = new StringBuffer ("insert into usuarios (usu_nombre, usu_password) values ('");
		sql.append (usu.getNombre());
		sql.append("','");
		sql.append(usu.getPassword());  
		sql.append("')");

		
		agregar.executeUpdate(sql.toString())	;
		
		ConnectionManager.desconectar();
	}
	public void eliminar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar = conexion.createStatement();
		
		Usuario usu = (Usuario)obj;
		
		StringBuffer sql = new StringBuffer("delete from usuarios where usu_id= ");
		sql.append(usu.getCodigo());
		
		eliminar.execute(sql.toString());
		eliminar.close();
		ConnectionManager.desconectar();
		
	}
	public void modificar(Object obj) throws ClassNotFoundException, 
				SQLException {
		
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar = conexion.createStatement();
		Usuario usu = (Usuario)obj;
		
		StringBuffer sql = new StringBuffer ("update usuarios set ");
		
		sql.append("usu_nombre='");
		sql.append(usu.getNombre());
		sql.append("',usu_password='");
		sql.append(usu.getPassword());
		sql.append("' where usu_id=");
		sql.append(usu.getCodigo());
	

		modificar.execute(sql.toString());
		modificar.close();
		ConnectionManager.desconectar();

		
	}
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Usuario usu = (Usuario)obj;
		
		StringBuffer sb = new StringBuffer("select usu_id, usu_nombre," +
				" usu_password from usuarios");
		if (usu!=null && !usu.isVacio()){
			if (usu.getCodigo()>0){
				sb.append(" where usu_id=");
				sb.append(usu.getCodigo());
			}else if (usu.getNombre()!=null && !usu.getNombre().isEmpty()){
				sb.append(" where usu_nombre like '%");
				sb.append(usu.getNombre());				
				sb.append("%'");				
			}
		}
		ResultSet rs = agregar.executeQuery(sb.toString());
		List<Usuario> Usuarios = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return Usuarios;
	}
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<Usuario> Usuarios = new ArrayList<Usuario>();
		while (rs.next()){
			Usuarios.add(new Usuario(rs.getInt("usu_id"), 
									 rs.getString("usu_nombre"), 
									 rs.getString("usu_password")));			
	}
		return Usuarios;
}
}
