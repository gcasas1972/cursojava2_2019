package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.dao.util.FechaUtil;
import ar.edu.inac.modelo.Alumno;
import ar.edu.inac.modelo.Comision;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Evaluacion;
import ar.edu.inac.modelo.Materia;
import ar.edu.inac.modelo.Modulo;
import ar.edu.inac.modelo.Temario;


public class ModuloDao implements DAO {

	@Override
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar=conexion.createStatement();
        Modulo mod=(Modulo)obj;

        StringBuffer sql = new StringBuffer("insert into modulos(mat_id, mod_descripcion) values (");
        sql.append(mod.getMateria().getCodigo());
        sql.append(",'");
        sql.append(mod.getDescripcion());
        sql.append("')");

        agregar.executeUpdate(sql.toString());		
		ConnectionManager.desconectar();	
	}

	@Override
	public void eliminar(Object obj)throws ClassNotFoundException, SQLException  {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar =conexion.createStatement();
        Modulo mod=(Modulo)obj;

        StringBuffer sql = new StringBuffer("delete from modulos where mod_id=");
        sql.append(mod.getCodigo());
        eliminar.executeUpdate(sql.toString());
	}
	@Override
	public void modificar(Object obj)throws ClassNotFoundException, SQLException  {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar =conexion.createStatement();
        Modulo mod=(Modulo)obj;
		StringBuffer sql = new StringBuffer("update modulos set mat_id=");
		sql.append(mod.getMateria().getCodigo());
		sql.append(",mod_descripcion='");
		sql.append(mod.getDescripcion());		
		sql.append("' where mod_id =");
		sql.append(mod.getCodigo());
		
		modificar.executeUpdate(sql.toString())	;
		ConnectionManager.desconectar();

	}

	@Override
	public List leer(Object obj)throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement leer = conexion.createStatement();
		Modulo mod = (Modulo)obj;
		StringBuffer sql = new StringBuffer("SELECT modu.mod_id, modu.mat_id, mat.mat_nombre,  modu.mod_descripcion");
		sql.append(" FROM modulos modu, materias mat");
		sql.append(" where modu.mat_id=mat.mat_id");
		
		if (mod!=null && !mod.isVacio()){
			if (mod.getCodigo()>0){
				sql.append(" and modu.mod_id=");
				sql.append(mod.getCodigo());				
			}else if (mod.getMateria()!=null && !mod.getMateria().isVacio()){
				sql.append(" and modu.mat_id =");
				sql.append(mod.getMateria().getCodigo());						
			}else if (mod.getDescripcion()!=null && !mod.getDescripcion().isEmpty()){
				sql.append(" and modu.mod_descripcion like '");
				sql.append(mod.getDescripcion());
				sql.append("%'");
			}
		}
		sql.append(" order by mat.mat_nombre, modu.mod_descripcion");
		ResultSet rs = leer.executeQuery(sql.toString());
		List provincias = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return provincias;
	}
		
		private List convertRsToObje(ResultSet rs) throws SQLException {
			List<Modulo> modulos = new ArrayList<Modulo>();
			Materia mat;
			while (rs.next()){
				//materia
				mat = new Materia(rs.getInt("modu.mat_id"), rs.getString("mat.mat_nombre"), null);
				modulos.add(new Modulo(rs.getInt("modu.mod_id"),rs.getString("modu.mod_descripcion"),mat,null));		
			}
			return modulos;
		}

		
	
}
