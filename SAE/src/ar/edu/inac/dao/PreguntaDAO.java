package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Alumno;
import ar.edu.inac.modelo.Evaluacion;
import ar.edu.inac.modelo.Materia;
import ar.edu.inac.modelo.Modulo;
import ar.edu.inac.modelo.Pregunta;
import ar.edu.inac.modelo.Respuesta;
import ar.edu.inac.modelo.Temario;

/**
 * 	@author Lautaro
 *	fechaderealizacion: 21/11/2018
 *	Esta clase corresponde al acceso a la base de datos 
 *	de objetos Pregunta.
 */
public class PreguntaDAO implements DAO{

	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Pregunta preg = (Pregunta)obj;
		//primero debo agregar las pregunta y luego las respuestas
		StringBuffer sbPreg = new StringBuffer("insert into preguntas");
		sbPreg.append("(preg_texto, preg_explicacion, preg_pathgrafico, mat_id, mod_id, eval_id)");
		sbPreg.append("values('");
		sbPreg.append(preg.getTexto());
		sbPreg.append("','");
		sbPreg.append(preg.getExplicacion());
		sbPreg.append("','");
		sbPreg.append(preg.getPathGrafico());
		sbPreg.append("',");
		sbPreg.append(preg.getMateria().getCodigo());
		sbPreg.append(",");
		sbPreg.append(preg.getModulo().getCodigo());
		sbPreg.append(",");
		sbPreg.append(preg.getEvaluacion().getCodigo());
		sbPreg.append(")");
		agregar.executeUpdate(sbPreg.toString());
		
		//agregar las respuestas
		List<Respuesta> lstRespuestas = preg.getRespuestas();
		for (Respuesta respuesta : lstRespuestas) {
			
			
		}
		agregar.close();
		ConnectionManager.desconectar();		
	}

	public void eliminar(Object obj) throws ClassNotFoundException,
			SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar = conexion.createStatement();
		Pregunta preg = (Pregunta)obj;
		StringBuffer sql = new StringBuffer("delete from preguntas where ");
		sql.append("preg_id=");
		sql.append(preg.getCodigo());
		eliminar.execute(sql.toString());
		eliminar.close();
		ConnectionManager.desconectar();
	}

	public void modificar(Object obj) throws ClassNotFoundException,
			SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar = conexion.createStatement();
		Pregunta preg = (Pregunta)obj;
		StringBuffer sql = new StringBuffer("update preguntas set ");
		sql.append("preg_texto='");
		sql.append(preg.getTexto());
		sql.append("', ");
		sql.append("preg_explicacion='");
		sql.append(preg.getExplicacion());
		sql.append("', ");
		sql.append("preg_pathgrafico='");
		sql.append(preg.getPathGrafico());
		sql.append("', ");
		sql.append("mat_id=");
		sql.append(preg.getMateria().getCodigo());
		sql.append(", ");
		sql.append("mod_id=");
		sql.append(preg.getModulo().getCodigo());
		sql.append(", ");
		sql.append("eval_id=");
		sql.append(preg.getEvaluacion().getCodigo());
		sql.append(" ");
		sql.append("where preg_id=");
		sql.append(preg.getCodigo());
		modificar.execute(sql.toString());
		modificar.close();
		ConnectionManager.desconectar();
	}

	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement leer = conexion.createStatement();
		Pregunta preg = (Pregunta)obj;
		//TODO PATRICIO cambiar este por el que esta en SelectPregunta.sql
		StringBuffer sql = new StringBuffer("select ");
		sql.append("e.eval_id, e.mat_id, e.alu_id, e.eval_estado, e.eval_fecharealizacion, ");
		sql.append("a.alu_id, a.part_id, a.prov_id, a.com_id, a.alu_nombre, a.alu_apellido, a.alu_direccion, a.alu_telefono, a.alu_dni, a.alu_email, ");
		sql.append("a.part_id, a.prov_id, ");
		sql.append("com.com_id, com.cur_id, com.mat_id, com.prof_id, com.tur_id, com.div_id, ");
		sql.append("p.prof_id, p.prof_nombre, p.prof_apellido, p.prof_direccion, p.prof_telefono, p.prof_dni, p.prof_email, p.prof_iosfa, ");
		sql.append("part.part_id, part.prov_id, part.part_nombre, ");
		sql.append("prov.prov_id, prov.prov_nombre, ");
		sql.append("modu.mod_id, modu.mat_id, modu.mod_descripcion, ");
		sql.append("mat.mat_id, mat.cur_id, mat.mat_nombre, mat.tem_id, ");
		sql.append("c.cur_id, c.cur_anio, c.cur_descripcion, ");
		sql.append("t.tem_id, t.tem_descripcion, ");
		sql.append("pr.preg_id, pr.preg_pathgrafico, pr.preg_explicacion, pr.preg_texto, ");
		sql.append("re.res_id,"); //gcasas se agrego el res_id que faltaba
		sql.append("re.res_texto, re.RES_ISCORRECTA, ");
		sql.append("tpre.tpreg_id  , tpre.tpreg_descripcion ");
		
		sql.append("from evaluaciones e, alumnos a, comisiones com, profesores p, partidos part, provincias prov, modulos modu, materias mat, cursos c, temarios t, preguntas pr, respuestas re, tipopregunta tpre ");
		sql.append("where e.alu_id=a.alu_id and ");	//Evaluacion y Alumno				ALUMNO y EVALUACION
		sql.append("e.mat_id=mat.mat_id and ");		//Evaluacion y Materia				MATERIA
		sql.append("a.com_id=com.com_id and ");		//Alumno y Comision					COMISION
		sql.append("com.prof_id=p.prof_id and ");	//Comision y Profesor				PROFESOR
		sql.append("a.part_id=part.part_id and ");	//Alumno y Partido				    PARTIDO
		sql.append("a.prov_id=prov.prov_id and ");	//Alumno y Provincia				PROVINCIA
		sql.append("mat.mat_id=modu.mat_id and ");	//Materia y Modulo					MODULO
		sql.append("mat.cur_id=c.cur_id and ");		//Materia y Curso					CURSO
		sql.append("mat.tem_id=t.tem_id and ");		//Materia y Temario					TEMARIO
		sql.append("pr.eval_id= e.eval_id and ");	//Pregunta y Evaluacion				PREGUNTA	
		sql.append("re.preg_id= pr.preg_id");		//Respuesta y Pregunta				RESPUESTA
												
		if(preg!=null && !preg.isVacio()){
			if(preg.getCodigo()>0){
				sql.append("and p.preg_id="+preg.getCodigo());
			}else if(preg.getTexto()!=null && !preg.getTexto().isEmpty()){
				sql.append(" and p.preg_texto like '%"+preg.getTexto()+"%'");
			}else if(preg.getPathGrafico()!=null && !preg.getPathGrafico().isEmpty()){
				sql.append(" and p.preg_pathgrafico like '%"+preg.getPathGrafico()+"%'");
			}
				
		}
		ResultSet rs = leer.executeQuery(sql.toString());
		List preguntas = convertRsToObj(rs);
		ConnectionManager.desconectar();
		return preguntas;
	}
	
	private List convertRsToObj(ResultSet rs) throws SQLException {
		List<Pregunta> preguntas = new ArrayList<Pregunta>();
		while(rs.next()){
			preguntas.add(new Pregunta(rs.getInt("preg_id"),
										null,
										rs.getString("preg_texto"),
										rs.getString("preg_pathgrafico"),
										rs.getString("preg_explicacion"),
										new Materia(rs.getInt("mat_id"),
													rs.getString("mat_nombre"), 
													new Temario(	rs.getInt("tem_id"),
																	rs.getString("tem_descripcion"))
													),
										new Modulo(	rs.getInt("mod_id"),
													rs.getString("mod_descripcion")),
										new Evaluacion(	rs.getInt("eval_id"),
														new Materia(rs.getInt("mat_id"),
																	rs.getString("mat_nombre"), 
																	new Temario(	rs.getInt("tem_id"),
																					rs.getString("tem_descripcion"))
														),
														new Alumno(rs.getInt("alu_id"),
																	rs.getString("alu_nombre"),
																	rs.getString("alu_apellido")),
														rs.getInt("eval_estado"),
														0, rs.getDate("eval_fecharealizacion"), 
														null, preguntas)
										)
							)
			;
		}
		return preguntas;
	}
}
