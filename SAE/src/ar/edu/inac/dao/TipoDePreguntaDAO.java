package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.TipoDeComponente;
import ar.edu.inac.modelo.TipoDePregunta;

public class TipoDePreguntaDAO implements DAO {

	@Override
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar =conexion.createStatement();
		
		TipoDePregunta tcomp= (TipoDePregunta) obj;
		StringBuffer sql = new StringBuffer ("insert into tipopregunta(TPREG_DESCRIPCION) values('");
		sql.append(tcomp.getDescripcion());
		sql.append("')");
		
		
		agregar.execute(sql.toString());
		
		
	}

	@Override
	public void eliminar(Object obj) throws ClassNotFoundException,
			SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar = conexion.createStatement();
		
		TipoDePregunta tcomp = (TipoDePregunta) obj;
		
		StringBuffer sql = new StringBuffer("delete from tipopregunta where");
		   sql.append(" TPREG_ID=");
	       sql.append(tcomp.getCodigo());
	       
	       eliminar.execute(sql.toString());
	       
	       eliminar.close();
	       ConnectionManager.desconectar();
	}

	@Override
	public void modificar(Object obj) throws ClassNotFoundException,
			SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar = conexion.createStatement();
		
		TipoDePregunta tcomp = (TipoDePregunta)obj;
		StringBuffer sql = new StringBuffer("update tipopregunta set TPREG_DESCRIPCION='");
		sql.append (tcomp.getDescripcion());
		sql.append("' where TPREG_ID=");
		sql.append(tcomp.getCodigo());
		modificar.execute(sql.toString());
		
		modificar.close();
		ConnectionManager.desconectar();
	}

	@Override
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement leer = conexion.createStatement();
		
		TipoDePregunta tcomp= (TipoDePregunta) obj;
		
		StringBuffer sql=
			new StringBuffer("select TPREG_ID,TPREG_DESCRIPCION from tipopregunta ");
		if (tcomp!=null && !tcomp.isVacio()){
			if (tcomp.getCodigo()>0){
					sql.append(" where TPREG_ID=");
					sql.append(tcomp.getCodigo());
			}else if (tcomp.getDescripcion()!=null || !tcomp.getDescripcion().isEmpty()){
				sql.append(" where TPREG_DESCRIPCION like '%");
				sql.append (tcomp.getDescripcion());
				sql.append("%'");
			}
		}
		sql.append(" order by TPREG_descripcion");
		ResultSet rs = leer.executeQuery(sql.toString());
		List tipopreguntas = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return tipopreguntas;
	}
	
	
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<TipoDePregunta> tipopreguntas = new ArrayList<TipoDePregunta>();
		while (rs.next()){
			tipopreguntas.add(new TipoDePregunta(rs.getInt("TPREG_ID"),
								rs.getString("TPREG_DESCRIPCION")));		
}
		return tipopreguntas;
	}
}
