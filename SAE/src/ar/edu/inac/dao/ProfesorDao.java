package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;



import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Partido;
import ar.edu.inac.modelo.Profesor;
import ar.edu.inac.modelo.Provincia;


public class ProfesorDao implements DAO {

	@Override
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		
		//tengo que castaear a curso
		Profesor prof = (Profesor)obj;
		
		StringBuffer sql = new StringBuffer("insert into profesores(");
		sql.append("part_id			, prov_id		, prof_nombre			, prof_apellido	, ");
		sql.append("prof_direccion	, prof_telefono	, prof_dni				, prof_email	, ");
		sql.append("prof_iosfa) values (");
		sql.append(prof.getPartido().getCodigo());
		sql.append(",");
		sql.append(prof.getProvincia().getCodigo());
		sql.append(",'");
		sql.append(prof.getNombre());
		sql.append("','");
		sql.append(prof.getApellido());
		
		sql.append("','");
		sql.append(prof.getDireccion());
		sql.append("','");
		sql.append(prof.getTelefono());
		sql.append("','");
		sql.append(prof.getDni());
		sql.append("','");
		sql.append(prof.getEmail());
		
		sql.append("','");
		sql.append(prof.getIosfa());
		sql.append("')");
		agregar.executeUpdate(sql.toString())	;
		
		ConnectionManager.desconectar();
		
	}
	@Override
	public void eliminar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar = conexion.createStatement();
		
		//tengo que castaear a curso
		Profesor prof = (Profesor)obj;
		
		StringBuffer sql = new StringBuffer("delete from profesores where prof_id=");
		sql.append(prof.getCodigo());
		
		eliminar.executeUpdate(sql.toString())	;
		ConnectionManager.desconectar();
	}

	@Override
	public void modificar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar = conexion.createStatement();
		
		//tengo que castaear a curso
		Profesor prof = (Profesor)obj;
		
		StringBuffer sql = new StringBuffer(	"update profesores set " 											+
												"part_id="			+prof.getPartido().getCodigo()					+", "+
												"prov_id="			+prof.getPartido().getProvincia().getCodigo()	+", "+
												"prof_nombre='"		+prof.getNombre()								+"', "+
												"prof_apellido='"	+prof.getApellido()								+"', "+
												"prof_direccion='"	+prof.getDireccion()							+"', "+
												"prof_telefono='"	+prof.getTelefono()								+"', "+
												"prof_dni='"		+prof.getDni()									+"', "+
												"prof_email='"		+prof.getEmail()								+"', "+
												"prof_iosfa='"		+prof.getIosfa()								+"' "+
												"where prof_id="	+prof.getCodigo()								);
		modificar.executeUpdate(sql.toString())	;
		ConnectionManager.desconectar();
	}

	@Override
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Profesor prof = (Profesor)obj;
		StringBuffer sb = new StringBuffer(	"select prof.prof_id, " 							+
											"prof.part_id, part.part_id, part.part_nombre, " 	+
											"prof.prov_id, prov.prov_id, prov.prov_nombre, "	+
											"prof.prof_nombre, " 								+
											"prof.prof_apellido, " 								+
											"prof.prof_direccion, " 							+
											"prof.prof_telefono, " 								+
											"prof.prof_dni, " 									+
											"prof.prof_email, " 								+
											"prof.prof_iosfa " 									+
											"from  " 											+
													"profesores prof, " 						+
													"partidos part, " 							+
													"provincias prov " 							+
											"where " 											+
													"prof.part_id=part.part_id " 				+
											"and " 												+
													"prof.prov_id=prov.prov_id"					);
		
		if (prof!=null && !prof.isVacio()){
			if (prof.getCodigo()>0){
				sb.append(" and prof.prof_id=");
				sb.append(prof.getCodigo());
			}else if (prof.getNombre()!=null && !prof.getNombre().isEmpty()){
				sb.append(" and prof_nombre like '%");
				sb.append(prof.getNombre());
				sb.append("%'");
			}else if (prof.getApellido()!=null && !prof.getApellido().isEmpty()){
				sb.append(" and prof_apellido like '%");
				sb.append(prof.getApellido());
				sb.append("%'");				
			}
		}
		ResultSet rs = agregar.executeQuery(sb.toString());
		List provincias = convertRsToObje(rs);
		ConnectionManager.desconectar();
		rs.close();
		return provincias;
	}
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<Profesor> profesores = new ArrayList<Profesor>();
		while (rs.next()){
			
			Provincia prov = new Provincia(	rs.getInt("prov_id"), 
											rs.getString("prov_nombre"));
			Partido   part = new Partido(	rs.getInt("part_id"), 
											rs.getString("part_nombre"), 
											prov);
			
			profesores.add(new Profesor(rs.getInt("prof_id"),
										rs.getString("prof_nombre"),
										rs.getString("prof_apellido"),
										rs.getString("prof_direccion"), 
										part,
										prov, 
										rs.getString("prof_telefono"),
										rs.getString("prof_dni"), 
										rs.getString("prof_email"), 
										rs.getString("prof_iosfa"), 
										null));			
		}
		return profesores;
	}
	

}
