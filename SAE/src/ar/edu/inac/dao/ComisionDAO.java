package ar.edu.inac.dao;

import java.sql.SQLException;
import java.util.List;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Alumno;
import ar.edu.inac.modelo.Comision;
import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Evaluacion;
import ar.edu.inac.modelo.Partido;
import ar.edu.inac.modelo.Provincia;

/**
 * @author Lautaro
 * Fecha: 19/10/2018
 * Esta clase corresponde a la clase Comisi�n aplicando DAO para el acceso a la base de datos.
 */

public class ComisionDAO implements DAO{

	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Comision coms = (Comision)obj;
		//TODO Preguntar si en la siguiente consulta
		//habria que agregar el atributo Profesor
		//en la clase Comision
		
		agregar.executeUpdate("insert into comisiones(com_division, com_turno, prof_id, cur_id, mat_id) values('" +
								coms.getDivision()				+"', '"+
								coms.getTurno() 				+ "', "+
								coms.getProfesor().getCodigo() 	+ ", " +
								coms.getCurso().getCodigo()		+ ", " +
								coms.getMateria().getCodigo()	+ ")"
								);
		ConnectionManager.desconectar();
	}

	public void eliminar(Object obj) throws ClassNotFoundException,
			SQLException {	
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Comision com = (Comision)obj;
		agregar.executeUpdate("delete from comisiones where com_id ="+
								com.getCodigo());
		ConnectionManager.desconectar();
	}

	public void modificar(Object obj) throws ClassNotFoundException,
			SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Comision com = (Comision)obj;
		agregar.executeUpdate("update comisiones set" 			+
				" com_division='" + com.getDivision() 			+ "' ,"+
				" com_turno	='"+ com.getTurno()					+ "' ,"+
				" prof_id	=" + com.getProfesor().getCodigo()  + ", " +
				" cur_id 	=" + com.getCurso().getCodigo()		+ ", " +
				" mat_id 	=" + com.getMateria().getCodigo() 	+
 				" where com_id =" 	+ com.getCodigo());
		ConnectionManager.desconectar();
	}

	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Comision com = (Comision)obj;
		
		StringBuffer sb = new StringBuffer("select comi.com_id, comi.prof_id,prof.prof_nombre, prof.prof_apellido,");
										sb.append("comi.cur_id, cur.cur_anio, cur.cur_descripcion,comi.mat_id, mat.mat_nombre,");
										sb.append("comi.com_turno, comi.com_division,alum.alu_id, alum.part_id, part.part_nombre,");
										sb.append("alum.prov_id, prov.prov_nombre, alum.com_id,  alum.alu_nombre, alum.alu_apellido,");
										sb.append("alum.alu_direccion, alum.alu_telefono, alum.alu_dni, alum.alu_email");
										sb.append(" from comisiones comi, alumnos alum, provincias prov, partidos part, profesores prof, cursos cur, materias mat");
										sb.append(" where comi.com_id=alum.com_id");
										sb.append(" and comi.cur_id  = cur.cur_id");
										sb.append(" and comi.mat_id  = mat.mat_id");
										sb.append(" and comi.prof_id = prof.prof_id");
										sb.append(" and alum.part_id = part.part_id");
										sb.append(" and alum.prov_id = prov.prov_id");
										
										
		if (com!=null && !com.isVacio()){
			if (com.getCodigo()>0){
				sb.append(" and comi.com_id = ");
				sb.append(com.getCodigo());
			}else if (	com.getDivision()!=null && !com.getDivision().isEmpty()){
				sb.append(" and comi.com_division='");
				sb.append(com.getDivision());
				sb.append("'");
			}else if ( com.getTurno()!=null && !com.getTurno().isEmpty()){
				sb.append(" and comi.com_turno='");
				sb.append(com.getTurno());
			}else if(com.getCurso()!=null && !com.getCurso().isVacio()){
				sb.append(" and comi.cur_id=");
				sb.append(com.getCurso().getCodigo());
			}
		}
		ResultSet rs = agregar.executeQuery(sb.toString());
		List comisiones = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return comisiones;
	}
	
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<Comision> comisiones = new ArrayList<Comision>();
		rs.next();
		Comision com = new Comision(rs.getInt("comi.com_id"), rs.getString("comi.com_turno"), rs.getString("comi.com_division"));
		com.addAlumno(new Alumno(	rs.getString("alum.alu_email"),
									rs.getString("alum.alu_dni"),
									rs.getString("alum.alu_telefono"),
									rs.getString("alum.alu_direccion"),
									rs.getString("alum.alu_apellido"),
									rs.getString("alum.alu_nombre"),
									new Comision(rs.getInt("comi.com_id"),rs.getString("comi.com_turno"),rs.getString("comi.com_division"))
									));
		comisiones.add(com);
		while (rs.next()){
			com.addAlumno(new Alumno(	rs.getString("alum.alu_email"),
										rs.getString("alum.alu_dni"),
										rs.getString("alum.alu_telefono"),
										rs.getString("alum.alu_direccion"),
										rs.getString("alum.alu_apellido"),
										rs.getString("alum.alu_nombre"),
										new Comision(rs.getInt("comi.com_id"),rs.getString("comi.com_turno"),rs.getString("comi.com_division"))
										));
			comisiones.add(com);
			}
		
	
		return comisiones;
	}
}
