package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Componente;
import ar.edu.inac.modelo.Pantalla;


/**
 * @author Diego Fecha: 28/03/2019 Esta clase corresponde a la clase Componente
 *         de DAO para el acceso a la base de datos.
 */
public class ComponenteDAO implements DAO{
	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Componente comp = (Componente)obj;
		
		
		agregar.executeUpdate("insert into componentes (pan_id, comp_descripcion) values(" +
								comp.getPantalla().getCodigo()+ ", '" + comp.getDescripcion() + "')");
		ConnectionManager.desconectar();
	}
	public void eliminar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Componente comp = (Componente)obj;
		
		
		agregar.executeUpdate(	"delete from componentes where comp_id = " + 
								comp.getCodigo());
		
		ConnectionManager.desconectar();
		
	}
	public void modificar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Componente comp = (Componente)obj;
		agregar.executeUpdate("update Componentes set " +
				"comp_descripcion='" + comp.getDescripcion() + "' "+
				", comp_visible="+ comp.isVisible() +
				", comp_editable="+ comp.isEditable()+
				" where comp_id =" 	+ comp.getCodigo() );
		ConnectionManager.desconectar();

		
	}
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Componente comp = (Componente)obj;
		
		
		StringBuffer sb = new StringBuffer("select p.pan_id, p.pan_descripcion,");
					sb.append(" c.comp_id, c.comp_descripcion, c.comp_visible, c.comp_editable");
					sb.append(" from componentes c , pantallas p");
					sb.append(" where c.pan_id = p.pan_id");
		
		
		
		if (comp!=null && !comp.isVacio()){
			if (comp.getCodigo()>0){
				sb.append(" and comp_id= ");
				sb.append(comp.getCodigo());
				
			}else if (comp.getDescripcion()!=null && !comp.getDescripcion().isEmpty()){
				sb.append(" and comp_descripcion= '");
				sb.append(comp.getDescripcion());
				sb.append("'");				
			}
		}
		ResultSet rs = agregar.executeQuery(sb.toString());
		List componentes = convertRsToObje(rs);
		ConnectionManager.desconectar();
		return componentes;
	}
	private List convertRsToObje(ResultSet rs) throws SQLException {
		List<Componente> componentes = new ArrayList<Componente>();
		while (rs.next()){
			componentes.add(new Componente(rs.getInt("c.comp_id"),rs.getString("c.comp_descripcion"),
										rs.getBoolean("c.comp_visible"), rs.getBoolean("c.comp_editable"),
										new Pantalla(rs.getInt("p.pan_id"), rs.getString("p.pan_descripcion"))));			
	}
		return componentes;
	}
}
