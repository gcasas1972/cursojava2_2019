package ar.edu.inac.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import ar.edu.inac.dao.util.ConnectionManager;
import ar.edu.inac.modelo.Materia;
import ar.edu.inac.modelo.Temario;

public class MateriaDAO implements DAO{

	public void agregar(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement agregar = conexion.createStatement();
		Materia mat = (Materia)obj;
		agregar.executeUpdate(	"insert into materias(cur_id, mat_nombre, tem_id)" 	+
								"values("	+
											mat.getCurso().getCodigo()				+ ", " +
								"'"+		mat.getNombre()							+ "', "
								+			mat.getTemario().getCodigo()			+ ")" );
		ConnectionManager.desconectar();
	}

	public void eliminar(Object obj) throws ClassNotFoundException,
			SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement eliminar = conexion.createStatement();
		Materia mat = (Materia)obj;
		eliminar.executeUpdate("delete from materias where mat_id="+mat.getCodigo());
		ConnectionManager.desconectar();
	}

	@Override
	public void modificar(Object obj) throws ClassNotFoundException,
			SQLException {
		ConnectionManager.conectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement modificar = conexion.createStatement();
		Materia mat = (Materia)obj;
		modificar.executeUpdate(	"update materias set " 											+
									"cur_id=" 		+mat.getCurso().getCodigo()						+", " +
									"mat_nombre='"	+mat.getNombre()								+"', " +
									"tem_id="		+mat.getTemario().getCodigo()					+" "+
									"where mat_id="	+mat.getCodigo());
		ConnectionManager.desconectar();
	}

	@Override
	public List leer(Object obj) throws ClassNotFoundException, SQLException {
		ConnectionManager.desconectar();
		Connection conexion = ConnectionManager.getConexion();
		Statement leer = conexion.createStatement();
		Materia mat = (Materia)obj;
		StringBuffer sb = new StringBuffer(	"select m.mat_id, m.cur_id, m.mat_nombre, m.tem_id, " +
											"t.tem_id, t.tem_descripcion, cur. " +
											"from materias m, temarios t, cursos cur " +
											//TODO Traer la tabla para despues crear el objeto curso
											"where m.tem_id=t.tem_id ");
		if(mat!=null && !mat.isVacio()){
			if(mat.getCodigo()>0){
				sb.append("and m.mat_id="+mat.getCodigo());
			}else if(mat.getNombre()!=null && !mat.getNombre().isEmpty()){
				sb.append(" and m.mat_nombre like '%"+mat.getNombre()+"%'");
			}
		}
		ResultSet rs = leer.executeQuery(sb.toString());
		List materias = convertRsToObj(rs);
		ConnectionManager.desconectar();
		rs.close();
		return materias;
	}

	private List convertRsToObj(ResultSet rs) throws SQLException {
		List<Materia> materias = new ArrayList<Materia>();
		while(rs.next()){
			//TODO D�nde guardo 'cur_id' obtenido en la consulta
		}
		return materias;
	}
}
