package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Curso;

public class ComponenteDescripcionNulaOVaciaValidator extends ComponenteValidator {

	@Override
	public boolean validar() {		
		return getComponente().getDescripcion()!=null && !getComponente().getDescripcion().isEmpty();
	}

	@Override
	public String getError() {		
		return "La descripcion no puede ser nula o vacia";
	}

}
