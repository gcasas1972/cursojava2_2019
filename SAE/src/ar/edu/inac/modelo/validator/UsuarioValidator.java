package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Usuario;

public abstract class UsuarioValidator extends Validator {
	//atributos
	
	protected static Usuario usuario;
	//getter 
	public Usuario getUsuario(){ return usuario;}
	@Override
	public void initialize() {	}

	@Override
	public boolean validarTodo() {
		boolean bln=true;
		initialize()	;
		StringBuffer sbError = new StringBuffer();
		for (Validator validacion : validaciones) {			
			if(!validacion.validar()){
				sbError.append(validacion.getError());
				sbError.append("\n");
				bln=false;
			}
		}
		errorAcumulado = sbError.toString();
		return bln;
	}
	
}
