package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Pantalla;

public class PantallaDescripcionEspacioDobleValidator extends PantallaValidator {

	@Override
	public boolean validar() {	
		return getPantalla().getDescripcion()==null?true:!getPantalla().getDescripcion().contains("  ");
	}

	@Override
	public String getError() {	
		return "La descripcion no puede tener dos espacios juntos";
	}


}
