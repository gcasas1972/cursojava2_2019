package ar.edu.inac.modelo.validator;

public class PerfilDescripcionTerminaConEspacioValidator extends PerfilValidator{

	public boolean validar() {
		return perfil.getDescripcion()==null || perfil.getDescripcion().isEmpty()?true:!(perfil.getDescripcion().charAt(getPerfil().getDescripcion().length()-1)==' ');
	}

	public String getError() {
		
		return "La descripci�n no puede finalizar con un espacio vac�o.";
	}
	
}
