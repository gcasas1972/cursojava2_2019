package ar.edu.inac.modelo.validator;


public class PerfilDescripcionEspacioDobleValidator extends PerfilValidator {

	@Override
	public boolean validar() {	
		return getPerfil().getDescripcion()==null?true:!getPerfil().getDescripcion().contains("  ");
	}

	@Override
	public String getError() {	
		return "La descripción no puede tener dos espacios juntos";
	}


}
