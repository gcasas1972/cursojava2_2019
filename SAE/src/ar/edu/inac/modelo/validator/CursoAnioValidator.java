package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Curso;

public class CursoAnioValidator extends CursoValidator {


	@Override
	public boolean validar() {		
		return getCurso().getAnio()>0 &&  getCurso().getAnio()<15;
	}

	@Override
	public String getError() {		
		return "los a�os deben ser mayor que 0 (cero) y menor que 15 (quince)";
	}



}
