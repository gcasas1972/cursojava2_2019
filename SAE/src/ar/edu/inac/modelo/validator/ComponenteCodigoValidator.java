package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Curso;

public class ComponenteCodigoValidator extends ComponenteValidator {


	@Override
	public boolean validar() {		
		return getComponente().getCodigo()>0 ;
	}

	@Override
	public String getError() {		
		return "El codigo debe ser mayor que 0 (cero)";
	}
	

}
