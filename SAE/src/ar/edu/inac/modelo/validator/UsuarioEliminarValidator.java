package ar.edu.inac.modelo.validator;

import java.util.ArrayList;


import ar.edu.inac.modelo.Usuario;

public class UsuarioEliminarValidator extends UsuarioValidator {
	private static UsuarioEliminarValidator validator;
	private UsuarioEliminarValidator() {
		super();

	}
	@Override
	public void initialize() {
		//aca se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		validaciones.add( new UsuarioCodigoValidator());
	}
	@Override
	public boolean validar() { 		return false;
	}

	@Override
	public String getError() {		return null;	}
	public static Validator getInstance(Usuario pUsuario){
		usuario = pUsuario;
		return validator==null?validator=new UsuarioEliminarValidator():validator;

	}


}
