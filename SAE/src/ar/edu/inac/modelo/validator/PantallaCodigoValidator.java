package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Pantalla;

public class PantallaCodigoValidator extends PantallaValidator {


	@Override
	public boolean validar() {		
		return getPantalla().getCodigo()>0 ;
	}

	@Override
	public String getError() {		
		return "El codigo debe ser mayor que 0 (cero)";
	}
	

}

