package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Pantalla;

public class PantallaDescripcionNulaOVaciaValidator extends PantallaValidator {

	@Override
	public boolean validar() {		
		return getPantalla().getDescripcion()!=null && !getPantalla().getDescripcion().isEmpty();
	}

	@Override
	public String getError() {		
		return "La descripcion no puede ser nula o vacia";
	}

}
