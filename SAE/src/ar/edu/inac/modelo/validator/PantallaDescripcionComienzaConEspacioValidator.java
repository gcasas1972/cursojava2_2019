package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Pantalla;

public class PantallaDescripcionComienzaConEspacioValidator extends PantallaValidator{

	public boolean validar() {
		return pantalla.getDescripcion()==null || pantalla.getDescripcion().isEmpty()?true:!(pantalla.getDescripcion().charAt(0)==' ');
	}

	public String getError() {
		return "La descripcion no puede comenzar con un espacio vac�o.";
	}

}

