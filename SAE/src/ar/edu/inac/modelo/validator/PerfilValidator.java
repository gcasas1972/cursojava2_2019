package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Perfil;

public abstract class PerfilValidator extends Validator {
	//Atributos
	
	protected static Perfil perfil;
	//Getter 
	public Perfil getPerfil(){ return perfil;}
	@Override
	public void initialize() {	}

	@Override
	public boolean validarTodo() {
		boolean bln=true;
		initialize()	;
		StringBuffer sbError = new StringBuffer();
		for (Validator validacion : validaciones) {			
			if(!validacion.validar()){
				sbError.append(validacion.getError());
				sbError.append("\n");
				bln=false;
			}
		}
		errorAcumulado = sbError.toString();
		return bln;
	}
	
}
