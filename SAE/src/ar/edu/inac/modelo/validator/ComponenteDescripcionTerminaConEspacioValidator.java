package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Curso;

public class ComponenteDescripcionTerminaConEspacioValidator extends ComponenteValidator{

	public boolean validar() {
		return componente.getDescripcion()==null || componente.getDescripcion().isEmpty()?true:!(componente.getDescripcion().charAt(getComponente().getDescripcion().length()-1)==' ');
	}

	public String getError() {
		
		return "La descripcion no puede finalizar con un espacio vac�o.";
	}
	
}
