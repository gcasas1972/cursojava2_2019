package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Modulo;

public class ModuloModificarValidator extends ModuloValidator {
	private static ModuloModificarValidator validator;
	private ModuloModificarValidator() {
		super();
	}
	@Override
	public void initialize() {
		//aca se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		validaciones.add(new ModuloCodigoValidator());
		validaciones.add(new ModuloDescripcionNulaOVaciaValidator());
		validaciones.add(new ModuloDescripcionEspacioDobleValidator());
		validaciones.add(new ModuloDescripcionComienzaConEspacioValidator());
		validaciones.add(new ModuloDescripcionTerminaConEspacioValidator());
	}
	@Override
	public boolean validar(){ 		return false;	}

	@Override
	public String getError() {		return null;	}
	
	public static Validator getInstance(Modulo pModulo){
		modulo = pModulo;
		return validator==null?validator=new ModuloModificarValidator():validator;

	}


}
