package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Componente;


public class ComponenteEliminarValidator extends ComponenteValidator {
	private static ComponenteEliminarValidator validator;
	private ComponenteEliminarValidator() {
		super();

	}
	@Override
	public void initialize() {
		//aca se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		validaciones.add( new ComponenteCodigoValidator());
		//validaciones.add( new ComponenteAnioValidator());
		validaciones.add( new ComponenteDescripcionNulaOVaciaValidator());
		validaciones.add( new ComponenteDescripcionEspacioDobleValidator());
	}
	@Override
	public boolean validar() { 		return false;
	}

	@Override
	public String getError() {		return null;	}
	public static Validator getInstance(Componente pComponente){
		componente = pComponente;
		return validator==null?validator=new ComponenteEliminarValidator():validator;

	}


}
