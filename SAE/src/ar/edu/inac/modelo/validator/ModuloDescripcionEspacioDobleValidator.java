package ar.edu.inac.modelo.validator;



public class ModuloDescripcionEspacioDobleValidator extends ModuloValidator {

	@Override
	public boolean validar() {	
		return getModulo().getDescripcion()==null?true:!getModulo().getDescripcion().contains("  ");
	}

	@Override
	public String getError() {	
		return "La descripcion no puede tener dos espacios juntos";
	}


}
