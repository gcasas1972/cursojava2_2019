package ar.edu.inac.modelo.validator;



public class ModuloDescripcionNulaOVaciaValidator extends ModuloValidator {

	@Override
	public boolean validar() {		
		return getModulo().getDescripcion()!=null && !getModulo().getDescripcion().isEmpty();
	}

	@Override
	public String getError() {		
		return "La descripcion no puede ser nula o vac�a";
	}

}
