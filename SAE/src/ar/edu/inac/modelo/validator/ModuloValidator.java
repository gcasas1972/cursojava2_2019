package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Modulo;

public abstract class ModuloValidator extends Validator {
	//atributos
	
	protected static Modulo modulo;
	//getter 
	public Modulo getModulo(){ return modulo;}
	@Override
	public void initialize() {	}

	@Override
	public boolean validarTodo() {
		boolean bln=true;
		initialize()	;
		StringBuffer sbError = new StringBuffer();
		for (Validator validacion : validaciones) {			
			if(!validacion.validar()){
				sbError.append(validacion.getError());
				sbError.append("\n");
				bln=false;
			}
		}
		errorAcumulado = sbError.toString();
		return bln;
	}
	
}
