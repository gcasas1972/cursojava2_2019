package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Curso;

public class CursoDescripcionTerminaConEspacioValidator extends CursoValidator{

	public boolean validar() {
		return curso.getDescripcion()==null || curso.getDescripcion().isEmpty()?true:!(curso.getDescripcion().charAt(getCurso().getDescripcion().length()-1)==' ');
	}

	public String getError() {
		
		return "La descripci�n no puede finalizar con un espacio vac�o.";
	}
	
}
