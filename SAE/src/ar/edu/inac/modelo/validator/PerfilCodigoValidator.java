package ar.edu.inac.modelo.validator;

public class PerfilCodigoValidator extends PerfilValidator {


	@Override
	public boolean validar() {		
		return getPerfil().getCodigo()>0 ;
	}

	@Override
	public String getError() {		
		return "El c�digo debe ser mayor que 0 (cero)";
	}
	

}
