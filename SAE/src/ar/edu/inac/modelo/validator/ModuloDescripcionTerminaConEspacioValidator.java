package ar.edu.inac.modelo.validator;



public class ModuloDescripcionTerminaConEspacioValidator extends ModuloValidator{

	public boolean validar() {
		return modulo.getDescripcion()==null || modulo.getDescripcion().isEmpty()?true:!(modulo.getDescripcion().charAt(getModulo().getDescripcion().length()-1)==' ');
	}

	public String getError() {
		
		return "La descripci�n no puede finalizar con un espacio vac�o.";
	}
	
}
