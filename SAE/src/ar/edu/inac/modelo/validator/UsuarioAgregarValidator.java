package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Usuario;
import ar.edu.inac.modelo.Curso;


public class UsuarioAgregarValidator extends UsuarioValidator {
	private static UsuarioAgregarValidator validator;
	
	public UsuarioAgregarValidator() {	super();
	}

	@Override
	public void initialize() {
		//aca se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		validaciones.add(new UsuarioDescripcionNulaOVaciaValidator());
		//validaciones.add(new UsuarioSinNombreValidator()); //esta es la nueva validacion
		//validaciones.add(new UsuarioSinPasswordValidator());
		//validaciones.add(new UsuarioDescripcionNulaOVaciaValidator());
	}
	
	@Override
	public boolean validar() {	return false;	}

	@Override
	public String getError() {	return null;	}
	
	public static Validator getInstance(Usuario pUsuario){
		usuario = pUsuario;
		return validator==null?validator=new UsuarioAgregarValidator():validator;

	}
}
