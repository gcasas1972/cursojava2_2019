package ar.edu.inac.modelo.validator;

public class CursoSinCodigoValidator extends CursoValidator {

	@Override
	public boolean validar() {
		return getCurso().getCodigo()==0;
	}

	@Override
	public String getError() {		
		return "El codigo debe estar vac�o";
	}

}
