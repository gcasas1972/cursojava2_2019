package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Curso;

public class ComponenteDescripcionEspacioDobleValidator extends ComponenteValidator {

	@Override
	public boolean validar() {	
		return getComponente().getDescripcion()==null?true:!getComponente().getDescripcion().contains("  ");
	}

	@Override
	public String getError() {	
		return "La descripcion no puede tener dos espacios juntos";
	}


}
