package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Componente;

public class ComponenteDescripcionComienzaConEspacioValidator extends ComponenteValidator{

	public boolean validar() {
		return componente.getDescripcion()==null || componente.getDescripcion().isEmpty()?true:!(componente.getDescripcion().charAt(0)==' ');
	}

	public String getError() {
		return "La descripcion no puede comenzar con un espacio vac�o.";
	}

}
