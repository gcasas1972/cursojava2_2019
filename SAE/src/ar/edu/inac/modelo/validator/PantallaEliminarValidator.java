package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Pantalla;


public class PantallaEliminarValidator extends PantallaValidator {
	private static PantallaEliminarValidator validator;
	private PantallaEliminarValidator() {
		super();

	}
	@Override
	public void initialize() {
		//aca se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		validaciones.add( new PantallaCodigoValidator());
		//validaciones.add( new PantallaAnioValidator());
		validaciones.add( new PantallaDescripcionNulaOVaciaValidator());
		validaciones.add( new PantallaDescripcionEspacioDobleValidator());
	}
	@Override
	public boolean validar() { 		return false;
	}

	@Override
	public String getError() {		return null;	}
	public static Validator getInstance(Pantalla pPantalla){
		pantalla = pPantalla;
		return validator==null?validator=new PantallaEliminarValidator():validator;

	}


}