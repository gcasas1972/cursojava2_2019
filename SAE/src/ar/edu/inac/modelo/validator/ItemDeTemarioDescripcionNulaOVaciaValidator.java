package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Curso;

public class ItemDeTemarioDescripcionNulaOVaciaValidator extends ItemDeTemarioValidator {

	@Override
	public boolean validar() {		
		return 	getItemDeTemario().getDescripcion()!=null 	&& 
				!getItemDeTemario().getDescripcion().isEmpty();
	}

	@Override
	public String getError() {		
		return "La descripcion no puede ser nula o vac�a";
	}

}
