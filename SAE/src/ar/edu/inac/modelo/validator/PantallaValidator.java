package ar.edu.inac.modelo.validator;

import java.util.ArrayList;
import ar.edu.inac.modelo.Pantalla;


public abstract class PantallaValidator extends Validator {
	//atributos
	
	protected static Pantalla pantalla;
	//getter 
	public Pantalla getPantalla(){ return pantalla;}
	@Override
	public void initialize() {	}

	@Override
	public boolean validarTodo() {
		boolean bln=true;
		initialize()	;
		StringBuffer sbError = new StringBuffer();
		for (Validator validacion : validaciones) {			
			if(!validacion.validar()){
				sbError.append(validacion.getError());
				sbError.append("\n");
				bln=false;
			}
		}
		errorAcumulado = sbError.toString();
		return bln;
	}
	
}
