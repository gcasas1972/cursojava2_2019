package ar.edu.inac.modelo.validator;


public class UsuarioCodigoValidator extends UsuarioValidator {


	@Override
	public boolean validar() {		
		return getUsuario().getCodigo()>0;
	}

	@Override
	public String getError() {		
		return "El c�digo debe ser mayor que 0 (cero)";
	}
	

}
