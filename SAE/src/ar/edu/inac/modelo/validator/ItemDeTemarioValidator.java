package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.ItemDeTemario;

public abstract class ItemDeTemarioValidator extends Validator {
	//atributos
	
	protected static ItemDeTemario itemDeTemario;
	//getter 
	public ItemDeTemario getItemDeTemario(){ return itemDeTemario;}
	@Override
	public void initialize() {	}

	@Override
	public boolean validarTodo() {
		boolean bln=true;
		initialize()	;
		StringBuffer sbError = new StringBuffer();
		for (Validator validacion : validaciones) {			
			if(!validacion.validar()){
				sbError.append(validacion.getError());
				sbError.append("\n");
				bln=false;
			}
		}
		errorAcumulado = sbError.toString();
		return bln;
	}
	
}
