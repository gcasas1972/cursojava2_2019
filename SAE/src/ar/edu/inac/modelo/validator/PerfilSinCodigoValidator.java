package ar.edu.inac.modelo.validator;

public class PerfilSinCodigoValidator extends PerfilValidator {

	@Override
	public boolean validar() {
		return getPerfil().getCodigo()==0;
	}

	@Override
	public String getError() {		
		return "El c�digo debe estar vac�o";
	}

}
