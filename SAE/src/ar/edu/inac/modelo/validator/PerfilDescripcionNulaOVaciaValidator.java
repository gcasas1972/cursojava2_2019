package ar.edu.inac.modelo.validator;

public class PerfilDescripcionNulaOVaciaValidator extends PerfilValidator {

	@Override
	public boolean validar() {		
		return getPerfil().getDescripcion()!=null && !getPerfil().getDescripcion().isEmpty();
	}

	@Override
	public String getError() {		
		return "La descripci�n no puede ser nula o vac�a";
	}

}
