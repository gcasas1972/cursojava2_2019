package ar.edu.inac.modelo.validator;

public class ModuloSinCodigoValidator extends ModuloValidator {

	@Override
	public boolean validar() {
		return getModulo().getCodigo()==0;
	}

	@Override
	public String getError() {		
		return "El codigo debe estar vacio";
	}

}
