package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Pantalla;


public class PantallaModificarValidator extends PantallaValidator {
	private static PantallaModificarValidator validator;
	private PantallaModificarValidator() {
		super();
	}
	@Override
	public void initialize() {
		//aca se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		validaciones.add( new PantallaCodigoValidator());
		//validaciones.add( new PantallaAnioValidator());
		validaciones.add( new PantallaDescripcionNulaOVaciaValidator());
		validaciones.add( new PantallaDescripcionEspacioDobleValidator());
		validaciones.add( new PantallaDescripcionComienzaConEspacioValidator());
		validaciones.add( new PantallaDescripcionTerminaConEspacioValidator());
	}
	@Override
	public boolean validar(){ 		return false;	}

	@Override
	public String getError() {		return null;	}
	
	public static Validator getInstance(Pantalla pPantalla){
		pantalla = pPantalla;
		return validator==null?validator=new PantallaModificarValidator():validator;

	}


}
