package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Perfil;

public class PerfilModificarValidator extends PerfilValidator {
	private static PerfilModificarValidator validator;
	private PerfilModificarValidator() {
		super();
	}
	@Override
	public void initialize() {
		//Se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		validaciones.add( new PerfilCodigoValidator());
		validaciones.add(new PerfilDescripcionNulaOVaciaValidator());
		validaciones.add(new PerfilDescripcionEspacioDobleValidator());
		validaciones.add(new PerfilDescripcionComienzaConEspacioValidator());
		validaciones.add(new PerfilDescripcionTerminaConEspacioValidator());
	}
	@Override
	public boolean validar(){ 		return false;	}

	@Override
	public String getError() {		return null;	}
	
	public static Validator getInstance(Perfil pPerfil){
		perfil = pPerfil;
		return validator==null?validator=new PerfilModificarValidator():validator;

	}


}
