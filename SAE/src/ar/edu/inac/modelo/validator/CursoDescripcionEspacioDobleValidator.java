package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Curso;

public class CursoDescripcionEspacioDobleValidator extends CursoValidator {

	@Override
	public boolean validar() {	
		return getCurso().getDescripcion()==null?true:!getCurso().getDescripcion().contains("  ");
	}

	@Override
	public String getError() {	
		return "La descripcion no puede tener dos espacios juntos";
	}


}
