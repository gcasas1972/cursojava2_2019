package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Pantalla;

public class PantallaDescripcionTerminaConEspacioValidator extends PantallaValidator{

	public boolean validar() {
		return pantalla.getDescripcion()==null || pantalla.getDescripcion().isEmpty()?true:!(pantalla.getDescripcion().charAt(getPantalla().getDescripcion().length()-1)==' ');
	}

	public String getError() {
		
		return "La descripcion no puede finalizar con un espacio vac�o.";
	}
	
}