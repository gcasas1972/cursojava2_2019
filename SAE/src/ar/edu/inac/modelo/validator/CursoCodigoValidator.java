package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Curso;

public class CursoCodigoValidator extends CursoValidator {


	@Override
	public boolean validar() {		
		return getCurso().getCodigo()>0 ;
	}

	@Override
	public String getError() {		
		return "El c�digo debe ser mayor que 0 (cero)";
	}
	

}
