package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.ItemDeTemario;

public class ItemDeTemarioAgregarValidator extends ItemDeTemarioValidator {
	private static ItemDeTemarioAgregarValidator validator;
	
	public ItemDeTemarioAgregarValidator() {	super();
	}

	@Override
	public void initialize() {
		//aca se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		validaciones.add(new ItemDeTemarioSinCodigoValidator()); //esta es la nueva validacion
		validaciones.add(new ItemDeTemarioDescripcionNulaOVaciaValidator());
	}
	
	@Override
	public boolean validar() {	return false;	}

	@Override
	public String getError() {	return null;	}
	
	public static Validator getInstance(ItemDeTemario pItem){
		itemDeTemario= pItem;
		return validator==null?validator=new ItemDeTemarioAgregarValidator():validator;

	}
}
