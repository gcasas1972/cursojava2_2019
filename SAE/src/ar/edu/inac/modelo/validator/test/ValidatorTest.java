package ar.edu.inac.modelo.validator.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ar.edu.inac.modelo.Curso;
import ar.edu.inac.modelo.Modulo;
import ar.edu.inac.modelo.validator.CursoAgregarValidator;
import ar.edu.inac.modelo.validator.CursoEliminarValidator;
import ar.edu.inac.modelo.validator.CursoValidator;
import ar.edu.inac.modelo.validator.CursoModificarValidator;
import ar.edu.inac.modelo.validator.ModuloAgregarValidator;

import ar.edu.inac.modelo.validator.Validator;

public class ValidatorTest {
	Validator cursoValidator;
	Curso curso;
	Modulo modulo;
	@Before
	public void setUp() throws Exception {
		
	}

	@After
	public void tearDown() throws Exception {
	}
	@Test
	public void testValidarModuloAgregarSinCodigo(){
		modulo = new Modulo(1,"descripcion");
		ModuloAgregarValidator.getInstance(modulo).validarTodo();
		assertEquals("El codigo debe estar vacio\n", ModuloAgregarValidator.getErrorAcumulado());
	}

	@Test
	public void testValidarCursoAnioNegativo() {
		curso = new Curso(0, -1, "descripcion");
		
		CursoAgregarValidator.getInstance(curso).validarTodo();
		assertEquals("los a�os deben ser mayor que 0 (cero) y menor que 15 (quince)\n", CursoAgregarValidator.getErrorAcumulado());
	}
	@Test
	public void testValidarCursoDescripcionNula() {
		curso = new Curso(0, 5, null);
		CursoAgregarValidator.getInstance(curso).validarTodo();		
		assertEquals("La descripcion no puede ser nula o vac�a\n", CursoAgregarValidator.getErrorAcumulado());
	}
	@Test
	public void testValidarCursoDescripcionVacia() {
		curso = new Curso(0, 5, "");
		
		CursoAgregarValidator.getInstance(curso).validarTodo();		
		assertEquals("La descripcion no puede ser nula o vac�a\n", CursoAgregarValidator.getErrorAcumulado());
	}
	@Test
	public void testValidarCursoDescripcionConDobleEspacio() {
		curso = new Curso(0, 5, "Gabriel  Casas");
		CursoAgregarValidator.getInstance(curso).validarTodo();		
		assertEquals("La descripcion no puede tener dos espacios juntos\n", CursoAgregarValidator.getErrorAcumulado());
	}
	@Test
	public void testValidarCursoCodigoModificacion() {
		curso = new Curso(0, 5, "Gabriel Casas");
		CursoModificarValidator.getInstance(curso).validarTodo();		
		assertEquals("El c�digo debe ser mayor que 0 (cero)\n", CursoModificarValidator.getErrorAcumulado());
	}
	@Test
	public void testValidarCodigoEliminacion() {
		curso = new Curso(0, 5, "Gabriel Casas");
		CursoEliminarValidator.getInstance(curso).validarTodo();		
		assertEquals("El c�digo debe ser mayor que 0 (cero)\n", CursoEliminarValidator.getErrorAcumulado());
	}
	@Test
	public void testValidarComienzaConEspacio() {
		curso = new Curso(0, 5, " Gabriel Casas");
		CursoAgregarValidator.getInstance(curso).validarTodo();		
		assertEquals("La descripci�n no puede comenzar con un espacio vac�o.\n", CursoAgregarValidator.getErrorAcumulado());
	}
	@Test
	public void testValidarFinalizaConEspacio() {
		curso = new Curso(0, 5, "Gabriel Casas ");
		CursoAgregarValidator.getInstance(curso).validarTodo();		
		assertEquals("La descripci�n no puede finalizar con un espacio vac�o.\n", CursoAgregarValidator.getErrorAcumulado());
	}	

	@Test
	public void testValidarTodoFalse() {
		curso = new Curso(0, -1, "Gabriel  Casas");
		assertFalse(CursoAgregarValidator.getInstance(curso).validarTodo());
	}
	@Test
	public void testValidarTodoTrue() {
		curso = new Curso(0, 6, "Gabriel Casas");		
		assertTrue(CursoAgregarValidator.getInstance(curso).validarTodo());
	}
	@Test
	public void testValidarSinCodigo() {
		curso = new Curso(10, 6, "Gabriel Casas");	
		CursoAgregarValidator.getInstance(curso).validarTodo();
		assertEquals("El codigo debe estar vac�o\n", CursoAgregarValidator.getErrorAcumulado());
	}
}
