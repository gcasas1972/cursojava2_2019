package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Curso;

public class CursoEliminarValidator extends CursoValidator {
	private static CursoEliminarValidator validator;
	private CursoEliminarValidator() {
		super();

	}
	@Override
	public void initialize() {
		//aca se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		validaciones.add( new CursoCodigoValidator());
	}
	@Override
	public boolean validar() { 		return false;
	}

	@Override
	public String getError() {		return null;	}
	public static Validator getInstance(Curso pCurso){
		curso = pCurso;
		return validator==null?validator=new CursoEliminarValidator():validator;

	}


}
