package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Curso;

public abstract class CursoValidator extends Validator {
	//atributos
	
	protected static Curso curso;
	//getter 
	public Curso getCurso(){ return curso;}
	@Override
	public void initialize() {	}

	@Override
	public boolean validarTodo() {
		boolean bln=true;
		initialize()	;
		StringBuffer sbError = new StringBuffer();
		for (Validator validacion : validaciones) {			
			if(!validacion.validar()){
				sbError.append(validacion.getError());
				sbError.append("\n");
				bln=false;
			}
		}
		errorAcumulado = sbError.toString();
		return bln;
	}
	
}
