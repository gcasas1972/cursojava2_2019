package ar.edu.inac.modelo.validator;



public class ModuloDescripcionComienzaConEspacioValidator extends ModuloValidator{

	public boolean validar() {
		return modulo.getDescripcion()==null || modulo.getDescripcion().isEmpty()?true:!(modulo.getDescripcion().charAt(0)==' ');
	}

	public String getError() {
		return "La descripci�n no puede comenzar con un espacio vac�o.";
	}

}
