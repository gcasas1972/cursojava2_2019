package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Curso;

public class CursoDescripcionNulaOVaciaValidator extends CursoValidator {

	@Override
	public boolean validar() {		
		return getCurso().getDescripcion()!=null && !getCurso().getDescripcion().isEmpty();
	}

	@Override
	public String getError() {		
		return "La descripcion no puede ser nula o vac�a";
	}

}
