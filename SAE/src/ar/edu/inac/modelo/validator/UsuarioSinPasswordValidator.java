package ar.edu.inac.modelo.validator;

public class UsuarioSinPasswordValidator extends UsuarioValidator {

	@Override
	public boolean validar() {
		
		return getUsuario().getPassword()=="";
	}

	@Override
	public String getError() {		
		return "Debe contener una password";
	}

}
