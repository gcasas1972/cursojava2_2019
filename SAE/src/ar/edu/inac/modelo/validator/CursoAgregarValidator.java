package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Curso;

public class CursoAgregarValidator extends CursoValidator {
	private static CursoAgregarValidator validator;
	
	public CursoAgregarValidator() {	super();
	}

	@Override
	public void initialize() {
		//aca se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		validaciones.add(new CursoSinCodigoValidator()); //esta es la nueva validacion
		validaciones.add(new CursoAnioValidator());
		validaciones.add(new CursoDescripcionNulaOVaciaValidator());
		validaciones.add(new CursoDescripcionEspacioDobleValidator());
		validaciones.add(new CursoDescripcionComienzaConEspacioValidator());
		validaciones.add(new CursoDescripcionTerminaConEspacioValidator());
	}
	
	@Override
	public boolean validar() {	return false;	}

	@Override
	public String getError() {	return null;	}
	
	public static Validator getInstance(Curso pCurso){
		curso = pCurso;
		return validator==null?validator=new CursoAgregarValidator():validator;

	}
}
