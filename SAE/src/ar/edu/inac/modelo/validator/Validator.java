package ar.edu.inac.modelo.validator;

import java.util.List;

public abstract class Validator {
	protected static String errorAcumulado;
	protected List<Validator> validaciones;
	
	public abstract boolean validar();
	
	public static String getErrorAcumulado(){return errorAcumulado;}
	
	//metodos abstractos
	public abstract void initialize()	;
	public abstract boolean validarTodo()	;
	public abstract String getError()	;
	
}
