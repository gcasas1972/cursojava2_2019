package ar.edu.inac.modelo.validator;

public class PerfilDescripcionComienzaConEspacioValidator extends PerfilValidator{

	public boolean validar() {
		return perfil.getDescripcion()==null || perfil.getDescripcion().isEmpty()?true:!(perfil.getDescripcion().charAt(0)==' ');
	}

	public String getError() {
		return "La descripci�n no puede comenzar con un espacio vac�o.";
	}

}
