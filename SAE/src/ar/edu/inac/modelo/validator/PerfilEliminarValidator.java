package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Perfil;

public class PerfilEliminarValidator extends PerfilValidator {
	private static PerfilEliminarValidator validator;
	private PerfilEliminarValidator() {
		super();

	}
	@Override
	public void initialize() {
		//Se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		validaciones.add( new PerfilCodigoValidator());
	}
	@Override
	public boolean validar() { 		return false;
	}

	@Override
	public String getError() {		return null;	}
	public static Validator getInstance(Perfil pPerfil){
		perfil = pPerfil;
		return validator==null?validator=new PerfilEliminarValidator():validator;

	}


}
