package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Componente;

public class ComponenteAgregarValidator extends ComponenteValidator {
	private static ComponenteAgregarValidator validator;
	
	public ComponenteAgregarValidator() {	super();
	}

	@Override
	public void initialize() {
		//aca se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		//validaciones.add(new CmponenteoAnioValidator());
		validaciones.add(new ComponenteDescripcionNulaOVaciaValidator());
		validaciones.add(new ComponenteDescripcionEspacioDobleValidator());
		validaciones.add(new ComponenteDescripcionComienzaConEspacioValidator());
		validaciones.add(new ComponenteDescripcionTerminaConEspacioValidator());
	}
	
	@Override
	public boolean validar() {	return false;	}

	@Override
	public String getError() {	return null;	}
	
	public static Validator getInstance(Componente pComponente){
		componente = pComponente;
		return validator==null?validator=new ComponenteAgregarValidator():validator;

	}
}
