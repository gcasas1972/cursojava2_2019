package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Usuario;

public class UsuarioModificarValidator extends UsuarioValidator {
	private static UsuarioModificarValidator validator;
	private UsuarioModificarValidator() {
		super();
	}
	@Override
	public void initialize() {
		//aca se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		//validaciones.add( new CursoCodigoValidator());
		//validaciones.add(new CursoAnioValidator());
		//validaciones.add(new CursoDescripcionNulaOVaciaValidator());
		//validaciones.add(new CursoDescripcionEspacioDobleValidator());
		//validaciones.add(new CursoDescripcionComienzaConEspacioValidator());
		//validaciones.add(new CursoDescripcionTerminaConEspacioValidator());
	}
	@Override
	public boolean validar(){ 		return false;	}

	@Override
	public String getError() {		return null;	}
	
	public static Validator getInstance(Usuario pUsuario){
		usuario = pUsuario;
		return validator==null?validator=new UsuarioModificarValidator():validator;

	}


}
