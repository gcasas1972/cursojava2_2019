package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Curso;

public class UsuarioDescripcionNulaOVaciaValidator extends UsuarioValidator {

	@Override
	public boolean validar() {		
		//return getCurso().getDescripcion()!=null && !getCurso().getDescripcion().isEmpty();
		return getUsuario().getPassword()!=null && !getUsuario().getPassword().isEmpty() && getUsuario().getNombre()!=null && !getUsuario().getNombre().isEmpty(); 
		
	}

	@Override
	public String getError() {		
		return "Debe contener un usuario y contrasenia";
	}

}
