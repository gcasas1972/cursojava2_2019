package ar.edu.inac.modelo.validator;

public class UsuarioSinNombreValidator extends UsuarioValidator {

	@Override
	public boolean validar() {
	
		//return getUsuario()!=null?true:getUsuario().getNombre()!=null;
		return getUsuario().getNombre()=="";
	}

	@Override
	public String getError() {		
		return "Debe contener un nombre";
	}

}
