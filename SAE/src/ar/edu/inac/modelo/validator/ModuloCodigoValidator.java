package ar.edu.inac.modelo.validator;



public class ModuloCodigoValidator extends ModuloValidator {


	@Override
	public boolean validar() {		
		return getModulo().getCodigo()>0 ;
	}

	@Override
	public String getError() {		
		return "El c�digo debe ser mayor que 0 (cero)";
	}
	

}
