package ar.edu.inac.modelo.validator;

public class ItemDeTemarioSinCodigoValidator extends ItemDeTemarioValidator {

	@Override
	public boolean validar() {
		return getItemDeTemario().getCodigo()==0;
	}

	@Override
	public String getError() {		
		return "El codigo debe estar vac�o";
	}

}
