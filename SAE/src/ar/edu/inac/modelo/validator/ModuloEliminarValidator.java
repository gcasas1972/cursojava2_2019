package ar.edu.inac.modelo.validator;

import java.util.ArrayList;

import ar.edu.inac.modelo.Modulo;

public class ModuloEliminarValidator extends ModuloValidator {
	private static ModuloEliminarValidator validator;
	private ModuloEliminarValidator() {
		super();

	}
	@Override
	public void initialize() {
		//aca se deben crear todas las validaciones
		validaciones = new ArrayList<Validator>();
		validaciones.add( new ModuloCodigoValidator());
	}
	@Override
	public boolean validar() { 		return false;
	}

	@Override
	public String getError() {		return null;	}
	public static Validator getInstance(Modulo pModulo){
		modulo = pModulo;
		return validator==null?validator=new ModuloEliminarValidator():validator;

	}


}
