package ar.edu.inac.modelo.validator;

import ar.edu.inac.modelo.Curso;

public class CursoDescripcionComienzaConEspacioValidator extends CursoValidator{

	public boolean validar() {
		return curso.getDescripcion()==null || curso.getDescripcion().isEmpty()?true:!(curso.getDescripcion().charAt(0)==' ');
	}

	public String getError() {
		return "La descripci�n no puede comenzar con un espacio vac�o.";
	}

}
