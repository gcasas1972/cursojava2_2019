package ar.edu.inac.modelo;
import java.util.ArrayList;
import java.util.List;



/**
 * @author Gabriel
 *Esta clase se relaciona con las comisiones y agrega el numero de iosfa a los
 *atributos de la persona
 */
public class Profesor extends Persona {
	//atributos
	private String 			iosfa		;
	private List<Comision> 	comisiones	;
	//constructores
	public Profesor() {}
	public Profesor(String pNom, String pApe, String pDir,
			String pTel, String pIosfa) {
		super(pNom, pApe, pDir, pTel);
		iosfa=pIosfa;
	}
	public Profesor(String pNom, String pApe) {
		super(pNom, pApe, null, null);
	}
	public Profesor(int pId, String pNom, String pApe) {
		super(pId, pNom, pApe);
	}
	public Profesor(int pCod			,String pNom			,String pApe	,String pDir, 
			   Partido pPart	,Provincia pProv		,String pTel	,String pDni, 
			   String pEmail	,String pIosfa			,List<Comision> pComisiones) {
		super(pCod, pNom, pApe, pDir, pPart, pProv, pTel, pDni, pEmail);
		iosfa = pIosfa;
		comisiones= pComisiones;
	}
	
	
	public String getIosfa() {				return iosfa	;	}
	public void setIosfa(String pIosfa) {	iosfa = pIosfa	;	}
	
	public List<Comision> getComisiones() {	return comisiones;	}
	
	public void addComision(Comision pCom){
		if(comisiones==null)
			comisiones=new ArrayList<Comision>();
		comisiones.add(pCom);
	}
	
	public void vaciar() {
		super.setApellido(null)		;
		super.setNombre(null)		;
		super.setDireccion(null)	;
		super.setDni(null)			;
		super.setEmail(null)		;
		super.setTelefono(null)		;
		super.setPartido(null)		;
		super.setProvincia(null)    ;
		super.setCodigo(0)			;
		comisiones 	= null			;
		iosfa 		= null			;
	}
	
	public boolean isVacio() {
		return  super.isVacio() && (super.getCodigo() == 0 && (super.getNombre()== null || super.getNombre().isEmpty()) && (super.getApellido() == null || 
				super.getApellido().isEmpty()) && (super.getDireccion() == null || super.getDireccion().isEmpty()) && (super.getTelefono() == null || 
				super.getTelefono().isEmpty()) && (super.getDni() == null || super.getDni().isEmpty()) && (super.getEmail() == null || 
				super.getEmail().isEmpty()) && super.getPartido() == null && super.getProvincia() == null ) && (comisiones == null && (iosfa == null || 
				iosfa.isEmpty())																															);}
	
	public String toString() {
		StringBuffer sb = new StringBuffer("codigo=")			   ;
		sb.append(super.getCodigo())							   ;
		sb.append(" nombre=")									   ;
		sb.append(super.getNombre())							   ;
		sb.append(" apellido=")									   ;
		sb.append(super.getApellido())							   ;
		sb.append(" direccion=")								   ;
		sb.append(super.getDireccion())							   ;
		sb.append(" telefono=")									   ;
		sb.append(super.getTelefono())							   ;
		sb.append(" dni=")									   	   ;
		sb.append(super.getDni())								   ;
		sb.append(" email=")									   ;
		sb.append(super.getEmail())								   ;
		sb.append(" partido=")									   ;
		sb.append(super.getPartido())							   ;
		sb.append(" provincia=")								   ;
		sb.append(super.getProvincia())							   ;
		sb.append(" comisiones=")								   ;
		sb.append(comisiones)									   ;
		sb.append(" iosfa=")								   	   ;
		sb.append(iosfa)									       ;
		
		
												
		return sb.toString()									   ;}

	public int hashCode() { return super.getCodigo()			   ;}

	public boolean equals(Object obj) {
		boolean bln=false										   ;
		Persona per=null										   ;
		if(obj instanceof Persona)
			per = (Persona)obj									   ;
			bln = super.getCodigo() == (per.getCodigo())		   ;
		return bln												   ;}


}
