package ar.edu.inac.modelo;

public class Division implements Vaciable, Model {
	//TODO CLAUDIO agrega los aributos codigo:int y descripcion:String
	//getter y setter equals, hascode y toString
	
	//atributos 
	private int   		codigo      ;
	
	private String 		descripcion ;
	
	//comtructores
	public Division (){ }
	public Division (int pCod, String pDescripcion){
		                       codigo      =       pCod               ;
		                       descripcion =       pDescripcion       ;
	}
	
	
	
	//getter y setter
	
	public int getCodigo() {  return codigo;}
    public void setCodigo(int codigo) {this.codigo = codigo;}

	public String getDescripcion() {return descripcion;}
    public void setDescripcion(String descripcion) {this.descripcion = descripcion;
	}

	
	@Override
	public void vaciar() {
		// TODO CLAUDIO vaciar todos los atributos, los enteros a 0 y los objetos a null
		        codigo      =  0       ;
		        descripcion =  null    ;
		        
		}


	@Override
	public boolean isVacio() {
		// TODO CLAUDIO verificar si estan todos los atributos vacios
		     
		return 
		      (codigo == 0 && 
		      (descripcion == null || descripcion.isEmpty()));
		
		
		
	}
	//HASCODE
	public int hashCode(){ return codigo ;      }
	
	public boolean equals(Object obj) {
		boolean bln=false										   ;
		Division div=null											   ;
		if(obj instanceof Division)
			div = (Division)obj									   ;
			bln = codigo == (div.getCodigo())					   ;
		return bln												   ;}
	
	
	
	
	// TOSTRING
	public String toString() {
		StringBuffer sb = new StringBuffer("codigo=")			   	;
		sb.append(codigo)										   	;
		sb.append(", descripcion=") 								;
		sb.append(descripcion)										;
												
		return sb.toString()									   ;}


	
	
	
	
	

}
