package ar.edu.inac.modelo;

/**
 * @author Esteban
 *
 */
public class ItemDeEvaluacion implements Model {
//atributos
private int 	codigo	;
private String 	texto	;

//constructores
//TODO Esteban,agregar constructor vacio y con structor con parametros

//getter y setter
public int getCodigo() {			return codigo;	}
public void setCodigo(int pCodigo) {codigo=pCodigo; }

public String getTexto(){			return "string";}
public void setTexto(String pTexto) {	texto=pTexto;}

//negocio
public void vaciar() {
	codigo = 0;
	texto = null														;}


public boolean isVacio(){
return 	codigo<=0 						&& 
		(texto==null || texto.isEmpty())								;}

public boolean equals(Object obj){
	boolean bln=false													;
	ItemDeEvaluacion eval=null											;
	if(obj instanceof ItemDeEvaluacion)
		eval = (ItemDeEvaluacion)obj									;
		bln = codigo==eval.getCodigo();									;
	return bln															;}
public String toString(){
	StringBuffer sb = new StringBuffer("codigo=")						;
	sb.append(codigo)													;
	sb.append(" texto=")												;
	sb.append(texto)													;
   return sb.toString()													;}
public int hashCode(){
	return codigo														;}

}
