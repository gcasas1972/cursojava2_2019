package ar.edu.inac.modelo;

import com.sun.crypto.provider.DESCipher;

/**
 * @author Gabriel
 * Esta clase especifica el tipo de componente a utilizar en la patalla 
 * entre los que se encuetran el label, combox, ect
 */
public class TipoDeComponente implements Model, Vaciable{
	private int codigo;
	private String descripcion;
	//constructores
	public TipoDeComponente() {	}
	public TipoDeComponente(int pCodigo, String pDescripcion) {
		super();
		this.codigo = pCodigo;
		this.descripcion = pDescripcion;
	}
	
	public int getCodigo() {		return codigo;	}
	public void setCodigo(int codigo) {	this.codigo = codigo;	}
	
	public String getDescripcion() {		return descripcion;	}
	public void setDescripcion(String descripcion) {		this.descripcion = descripcion;	}
	@Override
	public void vaciar() {
		codigo 		= 0;
		descripcion = null;
		
	}
	@Override
	public boolean isVacio() {
		return codigo ==0 &&
			   (descripcion==null || descripcion.isEmpty() );
	}
	
	//TODO Gabrielito completar equals, hashcode y toSTring
	
	
		

}
