package ar.edu.inac.modelo;

public class Respuesta implements Vaciable, Model{
	private int 	codigo			; //gcasas agregado
	private String  texto			;
	private boolean isCorrecta		;
	public Respuesta() {}
	
	public Respuesta(String texto, boolean isCorrecta) {
		super();
		this.texto = texto;
		this.isCorrecta = isCorrecta;
		
	}

	public Respuesta(int pCodigo, String pTexto, boolean pIscorrecta) {
		codigo 		= pCodigo		;
		texto 		= pTexto		;
		isCorrecta 	= pIscorrecta	;
		
	}

	public String getTexto() {						return texto;						}
	public void setTexto(String texto) {	this.texto = texto;										}
	
	public void setCorrecta(boolean isCorrecta) {	this.isCorrecta = isCorrecta;					}
	public boolean isCorrecta() {					return isCorrecta;					}
	
	
	public int getCodigo() {						return codigo;	}
	public void setCodigo(int pCod) {				this.codigo = pCod;	}

	@Override
	public void vaciar() {
		codigo			= 0		;
		texto 			= null 	;
		isCorrecta		= false	;		
	}
	@Override
	public boolean isVacio() {
		
		return codigo == 0 && 
		(texto == null || texto.isEmpty()) &&
		this.isCorrecta== false;
	}
	
	
	
	
}
