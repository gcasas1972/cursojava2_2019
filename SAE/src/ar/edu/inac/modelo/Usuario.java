package ar.edu.inac.modelo;


/**
 * @author Matias Quinteros
 * Fecha : 21/03/2019
 * Esta clase corresponde a usuarios que est� utilizando el programa.
 * mod by gcasas agregado de atributo descripcin
 */

public class Usuario implements Vaciable, Model{
	
	private int 			codigo		;
	private String 			nombre		;
	private String 			password	;
	public Usuario(){}
	public Usuario(int p_usu, String p_usu_d){
			codigo = p_usu;
			nombre = p_usu_d;
	}
	
	public Usuario(String p_nom ) { 
		nombre = p_nom;
		
	}
	public Usuario(int p_usu_id) {
		codigo = p_usu_id;
	}
	
	public Usuario(int Ucodigo, String Unombre, String Upassword) {
		super();
		this.codigo = Ucodigo			;
		this.nombre = Unombre			;
		this.password = Upassword		;

	}
	

	public Usuario(String Unombre, String Upassword) {
		nombre = Unombre;
		password = Upassword;
		
		// TODO Auto-generated constructor stub
	}
	public int getCodigo(){ return codigo						   ;}
	public void setCodigo (int codigo ){this.codigo = codigo       ;}
	
	public String getNombre() {			return nombre;	}
	public void setNombre(String nombre) {this.nombre = nombre;	}
	
	
	public String getPassword() {		return password;	}
	public void setPassword(String password) {		this.password = password;	}
	
	public void vaciar(){	
		codigo	= 0;
 	}

	public boolean isVacio(){	
	return 	  (codigo ==0 						&& 
			  (nombre==null || nombre.isEmpty()) &&
			  (password==null || password.isEmpty())) 		       ;}

					
		
	public int hashCode(){ return codigo						   ;}
	

	public boolean equals(Object obj) {
		boolean bln=false										   ;
		Usuario usr=null											   ;
		if(obj instanceof Usuario)
			usr = (Usuario)obj									   ;
			bln = codigo == (usr.getCodigo())					   ;
		return bln												   ;}

	
	public String toString() {
		StringBuffer sb = new StringBuffer("codigo=")			   	;
		sb.append(codigo)										   	;
		sb.append(", descripcion=") 								;
		sb.append(nombre)										;
												
		return sb.toString()									   ;}
	
	

	
}

