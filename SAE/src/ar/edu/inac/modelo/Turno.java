package ar.edu.inac.modelo;

public class Turno implements Vaciable, Model{
	private int 			codigo		;
	private String 			descripcion	;
	
	
	
	public Turno(int pCodigo, String pDescripcion) {
		codigo		=	pCodigo		;
		descripcion = 	pDescripcion;
	}
	//getter y setter equals, hashcode y toString
	public int getCodigo(){ return codigo						   ;}
	public void setCodigo (int codigo ){this.codigo = codigo       ;}
	
	public String getDescripcion() { return descripcion            ;}
	public void setDescripcion(String pDesc) {descripcion = pDesc  ;}
	
	public boolean equals(Object obj) {
		boolean bln=false										   ;
		Turno tur=null											   ;
		if(obj instanceof Turno)
			tur = (Turno)obj									   ;
			bln = codigo == (tur.getCodigo())					   ;
		return bln												   ;}
	
	public int hashCode(){ return codigo						   ;}
	
	public String toString() {
		StringBuffer sb = new StringBuffer("codigo=")			   	;
		sb.append(codigo)										   	;
		sb.append(",descripcion=") 								    ;
		sb.append(descripcion)										;
	
	return sb.toString()									       ;}
	
	
	// TODO GERARDO vaciar todos los atributos, los enteros a 0 y los objetos a null
	public void vaciar() { codigo	    = 0;
 	                       descripcion	= null;
				
	}

	public boolean isVacio() {
		return 	  (	codigo 	   ==0 && 
				  (descripcion ==null || descripcion.isEmpty()))	;}


		// TODO GERARDO verificar si estan todos los atributos vacios
		

		
	}


