package ar.edu.inac.modelo;

import java.util.ArrayList;
import java.util.List;
/**
 * @author Lautaro
 * Fecha : 31/08/2018
 * Esta clase corresponde a la pregunta que se va a realizar al alumno.
 */
public class Pregunta implements Vaciable,Model{
	//Atributos
	private int codigo;
	private List<Respuesta> respuestas;
	private String pathGrafico;
	private String texto;
	private String explicacion;
	private Materia materia;
	private Modulo modulo;
	private Evaluacion evaluacion;
	//TODO PATRICIO agregar el atributo tipoRespuesta:TipoDespuesta
	private TipoDePregunta  tipoDePregunta;
	
	//getter/ setter , toString
	
	//M�todos
	public void vaciar() {
		respuestas = null;
		pathGrafico = null;
		explicacion = null;
		materia = null;
		modulo = null;
	}
	
	public void addRespuestas(Respuesta pResp){
		if(respuestas==null)
			respuestas = new ArrayList<Respuesta>()	;
		respuestas.add(pResp)						;
	}
	
	public boolean isVacio(){
		boolean bln=false;
		if(respuestas==null || respuestas.isEmpty()){
			if(pathGrafico==null || pathGrafico.isEmpty()){
				if(materia==null  || materia.isVacio()){
					if(modulo==null || modulo.isVacio()){
						if(explicacion==null || explicacion.isEmpty())
						{
							bln=true;
						}
					}
				}
			}
		}
		return bln;}
	
	public List<Respuesta> getRespuestas(){ 				return respuestas;		}
	
	public String getPathGrafico() {return pathGrafico;}
	public void setPathGrafico(String pathGrafico) {this.pathGrafico = pathGrafico;}
	
	public String getExplicacion() {return explicacion;}
	public void setExplicacion(String explicacion) {this.explicacion = explicacion;}
	
	public Materia getMateria() {return materia;}
	public void setMateria(Materia materia) {this.materia = materia;}
	
	public Modulo getModulo() {return modulo;}
	public void setModulo(Modulo modulo) {this.modulo = modulo;}

	public int getCodigo(){return codigo;}
	public void setCodigo(int pCod){codigo = pCod;}
	
	public String getTexto(){return texto;}
	public void setTexto(String pTexto){texto = pTexto;}
	
	public Evaluacion getEvaluacion(){return evaluacion;}
	public void setEvaluacion(Evaluacion pEval){evaluacion = pEval;}
	
	public TipoDePregunta getTipoPregunta(){return tipoDePregunta;}
	public void setTipoDePregunta(TipoDePregunta pTipoDePregunta){tipoDePregunta = pTipoDePregunta;}
	
	public String toString(){
		StringBuffer sb = new StringBuffer("respuestas: ");
		sb.append(respuestas.toString());
		sb.append(", pathGrafico: ");
		sb.append(pathGrafico);
		sb.append(", explicacion: ");
		sb.append(explicacion);
		sb.append(", materia: ");
		sb.append(materia.toString());
		sb.append(", modulo: ");
		sb.append(modulo.toString());
		sb.append(", TipoDePregunta: ");
		sb.append(tipoDePregunta.toString());
		return sb.toString();
	}
	
	public int hashCode(){
		return pathGrafico.hashCode();
	}
	
	public boolean equals(Object obj){
		boolean bln = false;
		Pregunta pgta = null;
		if(obj instanceof Pregunta)
			pgta = (Pregunta)obj;
			bln = pathGrafico.equals(pgta.getPathGrafico());
		return bln;
	}
	
	public Pregunta(List<Respuesta> pResp, String pTexto, String pPathG, String pExp, Materia pMat, Modulo pMod, Evaluacion pEval, 
		TipoDePregunta pTipoDePregunta){
		respuestas = pResp;
		texto = pTexto;
		pathGrafico = pPathG;
		explicacion = pExp;
		materia = pMat;
		modulo = pMod;
		evaluacion = pEval;
		tipoDePregunta = pTipoDePregunta;
	}
	
	public Pregunta(int pCod, List<Respuesta> pResp, String pTexto, String pPathG, String pExp, Materia pMat, Modulo pMod, Evaluacion pEval){
		codigo = pCod;
		respuestas = pResp;
		texto = pTexto;
		pathGrafico = pPathG;
		explicacion = pExp;
		materia = pMat;
		modulo = pMod;
		evaluacion = pEval;
	}
	
	public Pregunta(){}
}