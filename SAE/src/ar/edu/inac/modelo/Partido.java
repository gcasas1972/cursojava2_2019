package ar.edu.inac.modelo;

/**
 * @author Lautaro
 * Fecha : 31/08/2018
 * Esta clase corresponde al partido donde se est� utilizando el programa.
 */
public class Partido implements Vaciable, Model{
	//atributos
	private int 	codigo		;
	private String 	descripcion	;	
	private Provincia provincia;
	
	//constructores
	public Partido(){}
	public Partido(String pNom){descripcion = pNom;}
	public Partido(String pNom, Provincia prov){descripcion=pNom; provincia=prov;}
	public Partido(int pCod, String pNom, Provincia prov){codigo=pCod; descripcion=pNom; provincia=prov;}
	public Partido(int partCod, String partNom, int proCod, String proNom){
		codigo = partCod;
		descripcion = partNom;
		Provincia provincia = new Provincia(proCod,proNom);
	}
	
	//getter y setter
	public int getCodigo() 				{return codigo;			}
	public void setCodigo(int codigo) 	{this.codigo = codigo;	}
	
	public String getDescripcion() 					{return descripcion;			}
	public void setDescripcion(String descripcion) 	{this.descripcion = descripcion;}
	
	public Provincia getProvincia() {return provincia;}
	public void setProvincia(Provincia provincia) {this.provincia = provincia;}

	//de negocio
	
	public int hashCode() {return descripcion.hashCode()					;}
	
	public boolean equals(Object obj){
		boolean bln=false													;
		Partido ptd=null													;
		if(obj instanceof Partido)
			ptd = (Partido)obj												;
			bln = descripcion.equals(ptd.getDescripcion())					;
		return bln															;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer("codigo=")					;
		sb.append(codigo)												;
		sb.append(" descripcion=")										;
		sb.append(descripcion)											;
		sb.append(" provincia=")										;
		sb.append(provincia)											;
		return sb.toString()											;}
	
	//De Interfaz
	public void vaciar() {
		codigo = 0;
		descripcion = null;
		provincia = null;}
	
	public boolean isVacio() {
		return ((descripcion==null || descripcion.isEmpty()) 	&& 
								codigo==0  						&& 
				(provincia==null || provincia.isVacio()));	
	}
}