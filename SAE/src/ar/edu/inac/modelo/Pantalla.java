package ar.edu.inac.modelo;
import java.util.List;

public class Pantalla implements Vaciable, Model{

	
	//declaro atributos
	private int codigo;
	private String descripcion;
	private List<Componente> componentes;
	private String nombre;
	//TODO JONATHAN FINALIZADO
	// agregar tambiel el atributo nombre:string, getter/setter y toString

	//constructores
	
	public Pantalla(){}
	public Pantalla(int pCod, String pDes){
		
		codigo = pCod;
		descripcion = pDes;
	}
	
	public Pantalla(String pDes, String pNom) {
		descripcion = pDes ;
		nombre		= pNom ;
	}
	public Pantalla(int pCod) {
		codigo = pCod ;
	}
	
	
	//getters & setters
	
	
	public Pantalla(int pCod, String pNom, String pDesc) {
		this.codigo = pCod;
		this.nombre = pNom;
		this.descripcion = pDesc;
	}
	public Pantalla(String pDesc) {
		descripcion=pDesc;
	}
	public int getCodigo() {return codigo;}
	public void setCodigo(int codigo) {this.codigo = codigo;}
	
	public String getDescripcion() {return descripcion;}
	public void setDescripcion(String descripcion) {this.descripcion = descripcion;}
	
	public List<Componente> getComponentes() {return componentes;}
	public void setComponentes(List<Componente> componentes) {this.componentes = componentes;}
	

	public String getNombre(){return nombre;}
	public void setNombre(String nombre) {this.nombre = nombre;}
	
	public void vaciar(){
		codigo 		= 	 0	;
		descripcion = null 	;
		componentes = null 	;
		nombre 		= null	;
		}
	//TODO JONATHAN VACIA TODO LO QUE FALTA
	
	public boolean isVacio(){	
	//TODO JONATHN AGREGA LOS ATRIBUTOS NUEVOS Y verificar que funcione
	return 	(codigo ==0 								&&
			(nombre==null)		|| nombre.isEmpty() )	&&
			(descripcion==null || descripcion.isEmpty());
	
	}
	//hashcode
	
	public int hashCode(){ return codigo;}
	
	//equals
	
	public boolean equals(Object obj) {
		boolean bln=false;
		Pantalla pan=null;
		if(obj instanceof Pantalla)
			pan = (Pantalla)obj;
			bln = codigo == (pan.getCodigo());
			return bln;
	}
	
	//toString
	
	public String toString() {
		StringBuffer sb = new StringBuffer("codigo=");
		sb.append(codigo);
		sb.append(",descripcion=");
		sb.append(descripcion);
		sb.append("nombre=");
		sb.append(nombre);
												
		return sb.toString();
		
	}
	
}
