package ar.edu.inac.modelo;

import java.util.ArrayList;
import java.util.List;
/**
 * @author Lautaro
 * Fecha : 31/08/2018
 * Esta clase corresponde a la provincia que est� utilizando el programa.
 */
public class Provincia implements Vaciable, Model{
	//Atributos
	private int 				codigo													;
	private String 				descripcion												;
	private List<Partido> 		partidos												;
	
	//M�todos
	
	//Constructores
	public Provincia(){	}
	public Provincia(String nombre){descripcion = nombre								;}
	
	public Provincia(int pCod, String pNom) {
		codigo=pCod;
		descripcion=pNom;
	}
	
	public Provincia(int pCod) {
		codigo = pCod																	;
	}
	
	//Getters y Setters
	public int getCodigo() {return codigo												;}
	public void setCodigo(int codigo) {this.codigo = codigo								;}
	
	public String getDescripcion() {return descripcion									;}
	public void setDescripcion(String descripcion) {this.descripcion = descripcion		;}
	
	public List<Partido> getPartidos() {return partidos									;}
	public void setPartidos(List<Partido> partidos) {this.partidos = partidos			;}
	
	/**
	 * Agrega un partido a la lista  de partidos
	 * @param pPartido, corresponde al partido a ser agregado
	 */
	public void addPartido(Partido pPartido){
		if(partidos==null)
			partidos = new ArrayList<Partido>();
		partidos.add(pPartido)						;
	}
	
	//M�todos polimorfos
	public boolean equals(Object obj){
		boolean bln=false																;
		Provincia pcia = null															;
		if(obj instanceof Provincia)
			pcia = (Provincia)obj														;
			bln = descripcion.equals(pcia.getDescripcion())								;
		return bln																		;}
	
	public int hashCode(){
		return descripcion.hashCode()													;}
	
	public String toString(){
		StringBuffer sb = new StringBuffer("codigo=")									;
		sb.append(codigo)																;
		sb.append(" descripcion=")														;
		sb.append(descripcion)															;
		sb.append(" partidos=")															;
		sb.append(partidos)																;
		return sb.toString()															;}
	
	//De Interfaz
	public void vaciar() {
		codigo = 0;
		descripcion = null;
		partidos = null;
	}
	public boolean isVacio() {
		return codigo<=0 && (descripcion==null || descripcion.isEmpty());												
		}
}