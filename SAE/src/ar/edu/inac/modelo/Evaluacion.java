package ar.edu.inac.modelo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;;

/**
 * @author Esteban
 *
 */

public class Evaluacion implements Vaciable, Model{
	//atributos
	private int codigo				;
	//mod by gcasas, se agregaron los atributos necesaeios INICIO ************ 
	private Materia materia			;
	private Alumno  alumno			;
	//mod by gcasas, se agregaron los atributos necesaeios FIN ***************
	private int estado				;	
	private Date fechaDeRealizacion	;
	private List preguntas			;
	//TODO PATRICIO, agregar los atribuso nota:float, fechaDeCreacion
	private float nota				;
	private Date fechaDeCreacion	;
	// constructores   
	public Evaluacion(){}
		
	public Evaluacion(int pCodigo, Materia pMateria, Alumno pAlumno, int pEstado, float pNota, Date pFechaDeRealizacion, 
			Date pFechaDeCreacion, List pPreguntas) {
		super();
		codigo 				= pCodigo				;		
		materia			 	= pMateria				;
		alumno 				= pAlumno				;
		estado 				= pEstado				;
		nota				= pNota					;
		fechaDeRealizacion 	= pFechaDeRealizacion	;
		fechaDeCreacion		= pFechaDeCreacion		;
		preguntas 			= pPreguntas			;
	}


	public Evaluacion(Materia materia2, Alumno alumno2, int i, Date asDate,
			List pPreguntas) {
		materia = materia2;
		alumno = alumno2;
		estado = i;
		fechaDeRealizacion = asDate;
		preguntas = pPreguntas;
	}

	//getter y setter
	public int getCodigo() {				return codigo;		}
	public void setCodigo(int pcodigo) {	codigo=pcodigo;		}

	
	public Materia getMateria() {		return materia;				}
	public void setMateria(Materia pMateria) {materia = pMateria;	}
	
	public Alumno getAlumno() {			return alumno;				}
	public void setAlumno(Alumno pAlumno) {		alumno = pAlumno;	}

	public void setPreguntas(List pPreg) {		preguntas = pPreg;	}

	public int getEstado() {				return estado;			}
	public void setEstado(int pestado) {		estado=pestado;}
	
	public float getNota() {				return nota;			}
	public void setNota(float pNota)	{			nota = pNota;	}
 
	public Date getFechaDeRealizacion()	{	return fechaDeRealizacion;			}
	public void setFechaDeRealizacion(Date pfech) {fechaDeRealizacion=pfech;	}
	
	public Date getFechaDeCreacion()	{	return fechaDeCreacion;		}
	public void setFechaDeCreacion(Date pfech) {fechaDeCreacion=pfech;	}

	public List getPreguntas(){		return preguntas;	}
	
	//metodos de negocio
	public void vaciar() {
		//TODO PATRICIO, agregar los nuevos atributos
		codigo 				= 0	; 
		estado				= 0	; 
		nota				= 0 ;
		fechaDeRealizacion 	= null; 
		fechaDeCreacion 	= null;
		preguntas 			= null;
		}

	public boolean isVacio(){
		//TODO PATRICIO, agregar los nuevos atributos
	return 	codigo<=0 					&& 
			estado==0 					&&
			nota==0						&&
			fechaDeRealizacion==null 	&& 
			fechaDeCreacion==null 		&&
			preguntas==null;	
	}

	public void addPregunta(Pregunta ppregunta) {
	if(preguntas==null)
		preguntas=new ArrayList<Pregunta>();
	preguntas.add(ppregunta);
	}
	
	public boolean equals(Object obj){
		boolean bln=false													;
		Evaluacion eval=null												;
		if(obj instanceof Evaluacion)
			eval = (Evaluacion)obj											;
			bln = codigo==eval.getCodigo();									;
		return bln;
	}
	public String toString(){
		StringBuffer sb = new StringBuffer("codigo=")				;
		sb.append(codigo)											;																				;
		sb.append(";estado=")										;
		sb.append(estado)											;
		sb.append(";nota=")                                         ;
		sb.append(nota)												;
		sb.append(";FechaDeRealizacion=")							;
		sb.append(fechaDeRealizacion)								;
		sb.append(";FechaDeCreacion=")								;
		sb.append(fechaDeCreacion)									;
		sb.append(";preguntas=")									;
		sb.append(preguntas)										;
		return sb.toString()										;
	}
	public int hashCode(){
	return codigo;
	}
	}
