select e.eval_id      , e.mat_id       , e.alu_id         , e.eval_estado         , e.eval_fecharealizacion ,
       a.alu_id       , a.part_id      , a.prov_id        , a.com_id              , a.alu_nombre            ,
       a.alu_apellido , a.alu_direccion, a.alu_telefono   , a.alu_dni,
       a.alu_email	  , a.part_id      , a.prov_id,
	   com.com_id     , com.cur_id     , com.mat_id           , com.prof_id,
       com.tur_id     , com.div_id,     
	   p.prof_id      , 
       p.prof_nombre  , p.prof_apellido, p.prof_direccion  , p.prof_telefono, p.prof_dni,
       p.prof_email   , p.prof_iosfa,    
       part.part_id   , part.prov_id, part.part_nombre,
       prov.prov_id   , prov.prov_nombre,
       modu.mod_id    , modu.mat_id, modu.mod_descripcion,
       mat.mat_id     , mat.cur_id, mat.mat_nombre, mat.tem_id,
       c.cur_id		  , c.cur_anio,
       c.cur_descripcion, 
       t.tem_id	      , t.tem_descripcion,
       pr.preg_id     , pr.preg_pathgrafico, pr.preg_explicacion, pr.preg_texto,
       re.res_texto   , re.RES_ISCORRECTA
       
       


from evaluaciones e, alumnos a,comisiones com,profesores p, 
	 partidos part, provincias prov,
	 modulos modu,
	 materias mat,
	 cursos c, 
	 temarios t, 
	 preguntas pr, 
	 respuestas re


    
    

where e.alu_id=a.alu_id   	 	and
	  a.com_id=com.com_id	 	and
	  com.prof_id=p.prof_id  	and
      a.part_id=part.part_id 	and
      a.prov_id=prov.prov_id 	and
      com.mat_id = mat.mat_id   and
      mat.mat_id=modu.mat_id 	and
      e.mat_id=mat.mat_id  		and
      mat.cur_id=c.cur_id 		and
      mat.tem_id=t.tem_id 		and 
      pr.eval_id= e.eval_id 	and
      re.preg_id= pr.preg_id
