-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.7.13-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema sae
--

CREATE DATABASE IF NOT EXISTS sae;
USE sae;

--
-- Definition of table `alumnos`
--

DROP TABLE IF EXISTS `alumnos`;
CREATE TABLE `alumnos` (
  `ALU_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PART_ID` int(10) unsigned DEFAULT NULL,
  `PROV_ID` int(10) unsigned DEFAULT NULL,
  `COM_ID` int(10) unsigned DEFAULT NULL,
  `ALU_NOMBRE` varchar(45) NOT NULL,
  `ALU_APELLIDO` varchar(45) NOT NULL,
  `ALU_DIRECCION` varchar(45) DEFAULT NULL,
  `ALU_TELEFONO` varchar(45) DEFAULT NULL,
  `ALU_DNI` varchar(45) DEFAULT NULL,
  `ALU_EMAIL` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`ALU_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `alumnos`
--

/*!40000 ALTER TABLE `alumnos` DISABLE KEYS */;
INSERT INTO `alumnos` (`ALU_ID`,`PART_ID`,`PROV_ID`,`COM_ID`,`ALU_NOMBRE`,`ALU_APELLIDO`,`ALU_DIRECCION`,`ALU_TELEFONO`,`ALU_DNI`,`ALU_EMAIL`) VALUES 
 (1,90,1,5,'Esteban','Saavedra',NULL,NULL,NULL,NULL),
 (2,90,1,5,'Ezequiel','Barbagallo',NULL,NULL,NULL,NULL),
 (3,90,1,5,'Ivan','Abaca',NULL,NULL,NULL,NULL),
 (4,90,1,5,'Lucas','Alaimo',NULL,NULL,NULL,NULL),
 (5,90,1,5,'Lautaro','Jaimes',NULL,NULL,NULL,NULL),
 (6,90,1,5,'Karen','Maciel',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `alumnos` ENABLE KEYS */;


--
-- Definition of table `comisiones`
--

DROP TABLE IF EXISTS `comisiones`;
CREATE TABLE `comisiones` (
  `COM_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PROF_ID` int(10) unsigned NOT NULL,
  `CUR_ID` int(10) unsigned NOT NULL,
  `MAT_ID` int(10) unsigned NOT NULL,
  `COM_TURNO` varchar(45) NOT NULL,
  `COM_DIVISION` varchar(45) NOT NULL,
  PRIMARY KEY (`COM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comisiones`
--

/*!40000 ALTER TABLE `comisiones` DISABLE KEYS */;
INSERT INTO `comisiones` (`COM_ID`,`PROF_ID`,`CUR_ID`,`MAT_ID`,`COM_TURNO`,`COM_DIVISION`) VALUES 
 (5,20,4,1,'Tarde','B'),
 (6,30,4,1,'Tarde','A'),
 (7,20,5,2,'Tarde','A'),
 (8,20,5,2,'Tarde','B'),
 (9,20,5,3,'Tarde','A');
/*!40000 ALTER TABLE `comisiones` ENABLE KEYS */;


--
-- Definition of table `cursos`
--

DROP TABLE IF EXISTS `cursos`;
CREATE TABLE `cursos` (
  `CUR_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CUR_ANIO` int(10) unsigned NOT NULL,
  `CUR_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`CUR_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cursos`
--

/*!40000 ALTER TABLE `cursos` DISABLE KEYS */;
INSERT INTO `cursos` (`CUR_ID`,`CUR_ANIO`,`CUR_DESCRIPCION`) VALUES 
 (1,1,'Primer año Aviónica'),
 (2,2,'Segundo año Aviónica'),
 (3,3,'Terecer año Aviónica'),
 (4,4,'Cuardo año Aviónica'),
 (6,6,'Sexto año Aviónica'),
 (7,7,'Septimo año Aviónica'),
 (8,8,'Aeronavegantes'),
 (67,3,'Terecer año Aviónica'),
 (68,1,'Primer año Mecanica'),
 (69,2,'Segundo año mec'),
 (70,1,'Monica'),
 (72,10,'riBer');
/*!40000 ALTER TABLE `cursos` ENABLE KEYS */;


--
-- Definition of table `evaluaciones`
--

DROP TABLE IF EXISTS `evaluaciones`;
CREATE TABLE `evaluaciones` (
  `EVAL_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MAT_ID` int(10) unsigned NOT NULL,
  `ALU_ID` int(10) unsigned NOT NULL,
  `EVAL_ESTADO` int(10) unsigned NOT NULL,
  `EVAL_FECHAREALIZACION` date DEFAULT NULL,
  PRIMARY KEY (`EVAL_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evaluaciones`
--

/*!40000 ALTER TABLE `evaluaciones` DISABLE KEYS */;
INSERT INTO `evaluaciones` (`EVAL_ID`,`MAT_ID`,`ALU_ID`,`EVAL_ESTADO`,`EVAL_FECHAREALIZACION`) VALUES 
 (1,2,1,1,'2018-12-10'),
 (2,2,4,1,'2018-12-11'),
 (3,2,3,1,'2018-12-12'),
 (4,2,6,1,'2018-12-13'),
 (5,2,5,1,'2018-12-14'),
 (6,2,2,1,'2018-12-15'),
 (34,16,38,1,'1996-09-19');
/*!40000 ALTER TABLE `evaluaciones` ENABLE KEYS */;


--
-- Definition of table `itemsdeevaluacion`
--

DROP TABLE IF EXISTS `itemsdeevaluacion`;
CREATE TABLE `itemsdeevaluacion` (
  `ITEMEVA_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ITEMEVA_TEXTO` varchar(45) NOT NULL,
  `EVA_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`ITEMEVA_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itemsdeevaluacion`
--

/*!40000 ALTER TABLE `itemsdeevaluacion` DISABLE KEYS */;
/*!40000 ALTER TABLE `itemsdeevaluacion` ENABLE KEYS */;


--
-- Definition of table `itemsdetemario`
--

DROP TABLE IF EXISTS `itemsdetemario`;
CREATE TABLE `itemsdetemario` (
  `ITEM_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TEM_ID` int(10) unsigned NOT NULL,
  `ITEM_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`ITEM_ID`),
  KEY `FK_itemsdetemario_temario` (`TEM_ID`),
  CONSTRAINT `FK_itemsdetemario_temario` FOREIGN KEY (`TEM_ID`) REFERENCES `temarios` (`TEM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `itemsdetemario`
--

/*!40000 ALTER TABLE `itemsdetemario` DISABLE KEYS */;
INSERT INTO `itemsdetemario` (`ITEM_ID`,`TEM_ID`,`ITEM_DESCRIPCION`) VALUES 
 (1,1,'Ley de Coulomb'),
 (2,1,'Capacitores'),
 (3,1,'Teorema de Thevening'),
 (4,1,'Teorema de Norton'),
 (5,1,'Teoría del galvanómetro'),
 (6,1,'Cálculo de voltimetro Rm - mucho'),
 (7,1,'Cálculo de Amperímetro Rsh - shiquita'),
 (8,1,'Mallas y nodos'),
 (9,1,'Superposición'),
 (10,2,'repaso de Karnaugh'),
 (11,2,'Decodificadores y multiplexores'),
 (12,2,'Práctica con utilización de Arduino'),
 (13,2,'Flip Flop RS, JK, D y T'),
 (14,2,'Práctica'),
 (15,2,'Contadores asincrónicos'),
 (16,2,'Contadores sincrónicos'),
 (17,2,'Registro de desplazamientos'),
 (18,3,'Repaso de Emisor común'),
 (19,3,'Diseño de emisor común'),
 (20,3,'Práctica y armado de circuito'),
 (21,3,'Par dárlington'),
 (22,3,'Análisis dinámico parámetros h'),
 (23,3,'Cálculo de ri, ria, ro y ros');
/*!40000 ALTER TABLE `itemsdetemario` ENABLE KEYS */;


--
-- Definition of table `materias`
--

DROP TABLE IF EXISTS `materias`;
CREATE TABLE `materias` (
  `MAT_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `CUR_ID` int(10) unsigned NOT NULL,
  `MAT_NOMBRE` varchar(45) NOT NULL,
  `TEM_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`MAT_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `materias`
--

/*!40000 ALTER TABLE `materias` DISABLE KEYS */;
INSERT INTO `materias` (`MAT_ID`,`CUR_ID`,`MAT_NOMBRE`,`TEM_ID`) VALUES 
 (1,4,'Teoría de circuitos',1),
 (2,5,'Técnicas Digitales',2),
 (3,5,'Electrónica 5to',3);
/*!40000 ALTER TABLE `materias` ENABLE KEYS */;


--
-- Definition of table `modulos`
--

DROP TABLE IF EXISTS `modulos`;
CREATE TABLE `modulos` (
  `MOD_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `MAT_ID` int(10) unsigned NOT NULL,
  `MOD_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`MOD_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modulos`
--

/*!40000 ALTER TABLE `modulos` DISABLE KEYS */;
INSERT INTO `modulos` (`MOD_ID`,`MAT_ID`,`MOD_DESCRIPCION`) VALUES 
 (1,1,'Primer trimestre'),
 (2,1,'Segundo trimestre'),
 (3,1,'Tercer trimestre'),
 (4,1,'Integradora'),
 (5,1,'Diciembre'),
 (6,1,'Marzo'),
 (7,2,'Primer trimestre'),
 (8,2,'Segundo trimestre'),
 (9,2,'Tercer trimestre'),
 (10,2,'Integradora'),
 (11,2,'Diciembre'),
 (12,2,'Marzo'),
 (13,3,'Primer trimestre'),
 (14,3,'Segundo trimestre'),
 (15,3,'Tercer trimestre'),
 (16,3,'Diciembre'),
 (17,3,'Marzo');
/*!40000 ALTER TABLE `modulos` ENABLE KEYS */;


--
-- Definition of table `pantallas`
--

DROP TABLE IF EXISTS `pantallas`;
CREATE TABLE `pantallas` (
  `PAN_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PAN_DESCRIPCION` varchar(50) NOT NULL,
  PRIMARY KEY (`PAN_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pantallas`
--

/*!40000 ALTER TABLE `pantallas` DISABLE KEYS */;
/*!40000 ALTER TABLE `pantallas` ENABLE KEYS */;


--
-- Definition of table `partidos`
--

DROP TABLE IF EXISTS `partidos`;
CREATE TABLE `partidos` (
  `PART_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PROV_ID` int(10) unsigned NOT NULL DEFAULT '1',
  `PART_NOMBRE` varchar(45) NOT NULL,
  PRIMARY KEY (`PART_ID`) USING BTREE,
  KEY `FK_partidos_provincia` (`PROV_ID`),
  CONSTRAINT `FK_partidos_provincia` FOREIGN KEY (`PROV_ID`) REFERENCES `provincias` (`PROV_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=139 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partidos`
--

/*!40000 ALTER TABLE `partidos` DISABLE KEYS */;
INSERT INTO `partidos` (`PART_ID`,`PROV_ID`,`PART_NOMBRE`) VALUES 
 (5,1,'Adolfo Alsina'),
 (6,1,'Adolfo Gonzales Chaves'),
 (7,1,'	Alberti	 '),
 (8,1,'	Almirante Brown	 '),
 (9,1,'	Arrecifes	 '),
 (10,1,'	Avellaneda	 '),
 (11,1,'	Ayacucho	 '),
 (12,1,'	Azul	 '),
 (13,1,'	Bahía Blanca	 '),
 (14,1,'	Balcarce	 '),
 (15,1,'	Baradero	 '),
 (16,1,'	Benito Juárez	 '),
 (17,1,'	Berazategui	 '),
 (18,1,'	Berisso	 '),
 (19,1,'	Bolívar	 '),
 (20,1,'	Bragado	 '),
 (21,1,'	Brandsen	 '),
 (22,1,'	Campana	 '),
 (23,1,'	Cañuelas	 '),
 (24,1,'	Capitán Sarmiento Carlos	 '),
 (25,1,'	Carlos Casares	 '),
 (26,1,'	Carlos Tejedor	 '),
 (27,1,'	Carmen de Areco	 '),
 (28,1,'	Castelli	 '),
 (29,1,'	Chacabuco	 '),
 (30,1,'	Chascomús	 '),
 (31,1,'	Chivilcoy	 '),
 (32,1,'	Colón	 '),
 (33,1,'	Coronel de Marina Leonardo Rosales	 '),
 (34,1,'	Coronel Dorrego	 '),
 (35,1,'	Coronel Pringles	 '),
 (36,1,'	Coronel Suárez	 '),
 (37,1,'	Daireaux	 '),
 (38,1,'	Dolores	 '),
 (39,1,'	Ensenada	 '),
 (40,1,'	Escobar	 '),
 (41,1,'	Esteban Echeverría	 '),
 (42,1,'	Exaltación de la Cruz	 '),
 (43,1,'	Ezeiza	 '),
 (44,1,'	Florencio Varela	 '),
 (45,1,'	Florentino Ameghino	 '),
 (46,1,'	General Alvarado	 '),
 (47,1,'	General Alvear	 '),
 (48,1,'	General Arenales	 '),
 (49,1,'	General Belgrano	 '),
 (50,1,'	General Guido	 '),
 (51,1,'	General Juan Madariaga	 '),
 (52,1,'	General La Madrid	 '),
 (53,1,'	General Las Heras	 '),
 (54,1,'	General Lavalle	 '),
 (55,1,'	General Paz	 '),
 (56,1,'	General Pinto	 '),
 (57,1,'	General Pueyrredón	 '),
 (58,1,'	General Rodríguez	 '),
 (59,1,'	General San Martín	 '),
 (60,1,'	General Viamonte	 '),
 (61,1,'	General Villegas	 '),
 (62,1,'	Guaminí	 '),
 (63,1,'	Hipólito Yrigoyen	 '),
 (64,1,'	Hurlingham	 '),
 (65,1,'	Ituzaingó	 '),
 (66,1,'	José C. Paz	 '),
 (67,1,'	Junín	 '),
 (68,1,'	La Costa	 '),
 (69,1,'	La Matanza	 '),
 (70,1,'	Lanús	 '),
 (71,1,'	La Plata	 '),
 (72,1,'	Laprida	 '),
 (73,1,'	Las Flores	 '),
 (74,1,'	Leandro N. Alem	 '),
 (75,1,'	Lincoln	 '),
 (76,1,'	Lobería	 '),
 (77,1,'	Lobos	 '),
 (78,1,'	Lomas de Zamora	 '),
 (79,1,'	Luján	 '),
 (80,1,'	Magdalena	 '),
 (81,1,'	Maipú	 '),
 (82,1,'	Malvinas Argentinas	 '),
 (83,1,'	Mar Chiquita	 '),
 (84,1,'	Marcos Paz	 '),
 (85,1,'	Mercedes	 '),
 (86,1,'	Merlo	 '),
 (87,1,'	Monte	 '),
 (88,1,'	Monte Hermoso	 '),
 (89,1,'	Moreno	 '),
 (90,1,'	Morón	 '),
 (91,1,'	Navarro	 '),
 (92,1,'	Necochea	 '),
 (93,1,'	Nueve de Julio (9 de Julio)	 '),
 (94,1,'	Olavarría	 '),
 (95,1,'	Patagones	 '),
 (96,1,'	Pehuajó	 '),
 (97,1,'	Pellegrini	 '),
 (98,1,'	Pergamino	 '),
 (99,1,'	Pila	 '),
 (100,1,'	Pilar	 '),
 (101,1,'	Pinamar	 '),
 (102,1,'	Presidente Perón	 '),
 (103,1,'	Puan	 '),
 (104,1,'	Punta Indio	 '),
 (105,1,'	Quilmes	 '),
 (106,1,'	Ramallo	 '),
 (107,1,'	Rauch	 '),
 (108,1,'	Rivadavia	 '),
 (109,1,'	Rojas	 '),
 (110,1,'	Roque Pérez	 '),
 (111,1,'	Saavedra	 '),
 (112,1,'	Saladillo	 '),
 (113,1,'	Salliqueló	 '),
 (114,1,'	Salto	 '),
 (115,1,'	San Andrés de Giles	 '),
 (116,1,'	San Antonio de Areco	 '),
 (117,1,'	San Cayetano	 '),
 (118,1,'	San Fernando	 '),
 (119,1,'	San Isidro	 '),
 (120,1,'	San Miguel	 '),
 (121,1,'	San Nicolás	 '),
 (122,1,'	San Pedro	 '),
 (123,1,'	San Vicente	 '),
 (124,1,'	Suipacha	 '),
 (125,1,'	Tandil	 '),
 (126,1,'	Tapalqué	 '),
 (127,1,'	Tigre	 '),
 (128,1,'	Tordillo	 '),
 (129,1,'	Tornquist	 '),
 (130,1,'	Trenque Lauquen	 '),
 (131,1,'	Tres Arroyos	 '),
 (132,1,'	Tres de Febrero	 '),
 (133,1,'	Tres Lomas	 '),
 (134,1,'	Veinticinco de Mayo (25 de Mayo)	 '),
 (135,1,'	Vicente López	 '),
 (136,1,'	Villa Gesell	 '),
 (137,1,'	Villarino	 '),
 (138,1,'	Zárate	 ');
/*!40000 ALTER TABLE `partidos` ENABLE KEYS */;


--
-- Definition of table `perfiles`
--

DROP TABLE IF EXISTS `perfiles`;
CREATE TABLE `perfiles` (
  `PER_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PER_DESCRIPCION` varchar(50) NOT NULL,
  PRIMARY KEY (`PER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perfiles`
--

/*!40000 ALTER TABLE `perfiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `perfiles` ENABLE KEYS */;


--
-- Definition of table `perfiles_pantallas`
--

DROP TABLE IF EXISTS `perfiles_pantallas`;
CREATE TABLE `perfiles_pantallas` (
  `PER_ID` int(10) unsigned NOT NULL,
  `PAN_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`PER_ID`,`PAN_ID`),
  KEY `FK_PERFILES_PANTALLAS_PAN` (`PAN_ID`),
  CONSTRAINT `FK_PERFILES_PANTALLAS_PAN` FOREIGN KEY (`PAN_ID`) REFERENCES `pantallas` (`PAN_ID`),
  CONSTRAINT `FK_PERFILES_PANTALLAS_PER` FOREIGN KEY (`PER_ID`) REFERENCES `perfiles` (`PER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perfiles_pantallas`
--

/*!40000 ALTER TABLE `perfiles_pantallas` DISABLE KEYS */;
/*!40000 ALTER TABLE `perfiles_pantallas` ENABLE KEYS */;


--
-- Definition of table `personas`
--

DROP TABLE IF EXISTS `personas`;
CREATE TABLE `personas` (
  `PER_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PART_ID` int(10) unsigned DEFAULT NULL,
  `PROV_ID` int(10) unsigned DEFAULT NULL,
  `COM_ID` int(10) unsigned DEFAULT NULL,
  `PER_NOMBRE` varchar(45) NOT NULL,
  `PER_APELLIDO` varchar(45) NOT NULL,
  `PER_DIRECCION` varchar(45) DEFAULT NULL,
  `PER_TELEFONO` varchar(45) DEFAULT NULL,
  `PER_DNI` varchar(45) DEFAULT NULL,
  `PER_EMAIL` varchar(45) DEFAULT NULL,
  `PER_TIPOPERSONA` varchar(45) NOT NULL,
  `PER_IOSFA` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`PER_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `personas`
--

/*!40000 ALTER TABLE `personas` DISABLE KEYS */;
/*!40000 ALTER TABLE `personas` ENABLE KEYS */;


--
-- Definition of table `preguntas`
--

DROP TABLE IF EXISTS `preguntas`;
CREATE TABLE `preguntas` (
  `PREG_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `EVAL_ID` int(10) unsigned NOT NULL,
  `MOD_ID` int(10) unsigned NOT NULL,
  `MAT_ID` int(10) unsigned NOT NULL,
  `PREG_PATHGRAFICO` varchar(45) NOT NULL,
  `PREG_EXPLICACION` varchar(45) NOT NULL,
  `PREG_TEXTO` varchar(45) NOT NULL,
  PRIMARY KEY (`PREG_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `preguntas`
--

/*!40000 ALTER TABLE `preguntas` DISABLE KEYS */;
/*!40000 ALTER TABLE `preguntas` ENABLE KEYS */;


--
-- Definition of table `profesores`
--

DROP TABLE IF EXISTS `profesores`;
CREATE TABLE `profesores` (
  `PROF_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PART_ID` int(10) unsigned DEFAULT NULL,
  `PROV_ID` int(10) unsigned DEFAULT NULL,
  `PROF_NOMBRE` varchar(45) NOT NULL,
  `PROF_APELLIDO` varchar(45) NOT NULL,
  `PROF_DIRECCION` varchar(45) DEFAULT NULL,
  `PROF_TELEFONO` varchar(45) DEFAULT NULL,
  `PROF_DNI` varchar(45) DEFAULT NULL,
  `PROF_EMAIL` varchar(45) DEFAULT NULL,
  `PROF_IOSFA` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`PROF_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profesores`
--

/*!40000 ALTER TABLE `profesores` DISABLE KEYS */;
INSERT INTO `profesores` (`PROF_ID`,`PART_ID`,`PROV_ID`,`PROF_NOMBRE`,`PROF_APELLIDO`,`PROF_DIRECCION`,`PROF_TELEFONO`,`PROF_DNI`,`PROF_EMAIL`,`PROF_IOSFA`) VALUES 
 (20,5,1,'Gabriel','Casas','Solari 3866','15-6005-7247','67965345','gcasas@gmail.com','58321/00'),
 (21,45,1,'Juan','Perez','Sordeaux 3866','4697-1010','34965345','jperez@gmail.com','58322/00'),
 (22,55,1,'Pedro','Luna','Santo Domingo 3866','4697-1011','22964545','pluna@gmail.com','58323/00'),
 (23,65,1,'Roberto','Lippa','Cucha chucha 3866','4697-1012','18965345','rlippa@gmail.com','58324/00'),
 (24,75,1,'Maria','Carniglia','Pierrestegui 3866','4697-1013','45965345','mcarniglia@gmail.com','58325/00'),
 (25,85,1,'Jose','Lopez','San Martin 3866','4697-1014','22335345','jlopez@gmail.com','58326/00'),
 (26,87,1,'Carlos','Ramirez','Belgrano 3866','4697-1015','24965345','cramirez@gmail.com','58327/00'),
 (27,90,1,'Lorena','Miranda','Azul 3866','4697-1016','25965345','lmiranda@gmail.com','58328/00'),
 (28,95,1,'Sabrina','Circovich','Verde 3866','4697-1017','27965345','ssircovich@gmail.com','58329/00'),
 (29,97,1,'Joel','Antonov','Medrano 3866','4697-1018','23965345','gcasas@gmail.com','58333/00'),
 (30,90,1,'Federico ','Weber','Medrano 3866','4697-1018','23965345','gcasas@gmail.com','58333/00');
/*!40000 ALTER TABLE `profesores` ENABLE KEYS */;


--
-- Definition of table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
CREATE TABLE `provincias` (
  `PROV_ID` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PROV_NOMBRE` varchar(100) NOT NULL,
  PRIMARY KEY (`PROV_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provincias`
--

/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
INSERT INTO `provincias` (`PROV_ID`,`PROV_NOMBRE`) VALUES 
 (1,'Buenos Aires'),
 (40,'Catamarca'),
 (41,'Chaco'),
 (42,'Chubut'),
 (43,'Córdoba'),
 (44,'Corrientes'),
 (45,'Entre Ríos'),
 (46,'Formosa'),
 (47,'Jujuy'),
 (48,'La Pampa'),
 (49,'La Rioja'),
 (50,'Mendoza'),
 (51,'Misiones'),
 (52,'Neuquén'),
 (53,'Río Negro'),
 (54,'Salta	SA'),
 (55,'San Juan'),
 (56,'San Luis'),
 (57,'Santa Cruz'),
 (58,'Santa Fe'),
 (59,'Santiago del Estero'),
 (60,'Tierra del Fuego, Antártida e Islas del Atlántico Sur'),
 (61,'Tucumán');
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;


--
-- Definition of table `respuestas`
--

DROP TABLE IF EXISTS `respuestas`;
CREATE TABLE `respuestas` (
  `RES_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PREG_ID` int(10) unsigned NOT NULL,
  `RES_TEXTO` varchar(45) NOT NULL,
  `RES_ISCORRECTA` tinyint(1) NOT NULL,
  `RES_ISSELECCIONADA` tinyint(1) NOT NULL,
  PRIMARY KEY (`RES_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `respuestas`
--

/*!40000 ALTER TABLE `respuestas` DISABLE KEYS */;
/*!40000 ALTER TABLE `respuestas` ENABLE KEYS */;


--
-- Definition of table `temarios`
--

DROP TABLE IF EXISTS `temarios`;
CREATE TABLE `temarios` (
  `TEM_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `TEM_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`TEM_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temarios`
--

/*!40000 ALTER TABLE `temarios` DISABLE KEYS */;
INSERT INTO `temarios` (`TEM_ID`,`TEM_DESCRIPCION`) VALUES 
 (1,'Teoría de circuitos 4 año'),
 (2,'Técnicas Digitales 5 año'),
 (3,'Electrónica 5 año'),
 (5,'Computadora de Aeronaves 7 año');
/*!40000 ALTER TABLE `temarios` ENABLE KEYS */;


--
-- Definition of table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `USU_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `USU_DESCRIPCION` varchar(45) NOT NULL,
  PRIMARY KEY (`USU_ID`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuarios`
--

/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;


--
-- Definition of table `usuarios_perfiles`
--

DROP TABLE IF EXISTS `usuarios_perfiles`;
CREATE TABLE `usuarios_perfiles` (
  `USU_ID` int(10) unsigned NOT NULL,
  `PER_ID` int(10) unsigned NOT NULL,
  PRIMARY KEY (`USU_ID`,`PER_ID`),
  KEY `FK_usuarios_perfiles_per` (`PER_ID`),
  CONSTRAINT `FK_usuarios_perfiles_per` FOREIGN KEY (`PER_ID`) REFERENCES `perfiles` (`PER_ID`),
  CONSTRAINT `FK_usuarios_perfiles_usu` FOREIGN KEY (`USU_ID`) REFERENCES `usuarios` (`USU_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `usuarios_perfiles`
--

/*!40000 ALTER TABLE `usuarios_perfiles` DISABLE KEYS */;
/*!40000 ALTER TABLE `usuarios_perfiles` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
