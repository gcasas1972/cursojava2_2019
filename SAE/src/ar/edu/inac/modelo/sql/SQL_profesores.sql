-- select que trae todos datos de provincias y partidos
select  prof.prof_id    , prof.part_id    , part.part_nombre  , prof.prov_id        ,
        prov.prov_nombre, prof.prof_nombre, prof.prof_apellido, prof.prof_direccion ,
        prof.prof_telefono, prof.prof_dni , prof.prof_email	  , prof.prof_iosfa 

from  profesores prof, partidos part, provincias prov
where prof.part_id = part. part_id and
      prof.prov_id = prov.prov_id and
      prof.prof_id = 30
      
-- update
update profesores set
	part_id			=  	15,
	prov_id    		=   1 ,
	prof_nombre		=	'Gabriel modificado _test',
	prof_apellido	=	'Casas Modificado',
	prof_direccion	= 	'Solari Modificado',
    prof_telefono	=	'4697-1533 modificado' ,
    prof_dni	  	=  	'22965726 modificado' ,
    prof_email	  	=  	'gcasas1972@gmail modificado' ,
    prof_iosfa   	= 	'45267/01 modificado'
where prof_id		= 	30