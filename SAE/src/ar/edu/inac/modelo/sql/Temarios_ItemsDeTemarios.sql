-- debo insertar los temarios

-- insertamos los temarios
insert into temarios (tem_descripcion) values ('Teor�a de circuitos 4 a�o');
insert into temarios (tem_descripcion) values ('T�cnicas Digitales 5 a�o');
insert into temarios (tem_descripcion) values ('Electr�nica 5 a�o');
insert into temarios (tem_descripcion) values ('Electr�nica 6 a�o');
insert into temarios (tem_descripcion) values ('Computadora de Aeronaves 7 a�o');
-- items de Teoria de circuitos
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Teor�a de circuitos 4 a�o'),'Ley de Coulomb');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Teor�a de circuitos 4 a�o'),'Capacitores');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Teor�a de circuitos 4 a�o'),'Teorema de Thevening');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Teor�a de circuitos 4 a�o'),'Teorema de Norton');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Teor�a de circuitos 4 a�o'),'Teor�a del galvan�metro');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Teor�a de circuitos 4 a�o'),'C�lculo de voltimetro Rm - mucho');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Teor�a de circuitos 4 a�o'),'C�lculo de Amper�metro Rsh - shiquita');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Teor�a de circuitos 4 a�o'),'Mallas y nodos');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Teor�a de circuitos 4 a�o'),'Superposici�n');

-- items de T�cnicas digitales
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='T�cnicas Digitales 5 a�o'),'repaso de Karnaugh');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='T�cnicas Digitales 5 a�o'),'Decodificadores y multiplexores');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='T�cnicas Digitales 5 a�o'),'Pr�ctica con utilizaci�n de Arduino');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='T�cnicas Digitales 5 a�o'),'Flip Flop RS, JK, D y T');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='T�cnicas Digitales 5 a�o'),'Pr�ctica');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='T�cnicas Digitales 5 a�o'),'Contadores asincr�nicos');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='T�cnicas Digitales 5 a�o'),'Contadores sincr�nicos');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='T�cnicas Digitales 5 a�o'),'Registro de desplazamientos');

-- electr�nica de 5to a�o
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Electr�nica 5 a�o'),'Repaso de Emisor com�n');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Electr�nica 5 a�o'),'Dise�o de emisor com�n');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Electr�nica 5 a�o'),'Pr�ctica y armado de circuito');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Electr�nica 5 a�o'),'Realizaci�n de recta de carga est�tica y din�mica');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Electr�nica 5 a�o'),'Par d�rlington');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Electr�nica 5 a�o'),'Uso de circuitos al corte  y saturaci�n pr�cticos');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Electr�nica 5 a�o'),'An�lisis din�mico par�metros h');
insert into itemsdetemario(tem_id, item_descripcion) values((SELECT tem_id FROM temarios where tem_descripcion ='Electr�nica 5 a�o'),'C�lculo de ri, ria, ro y ros');
-