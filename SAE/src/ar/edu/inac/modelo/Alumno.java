package ar.edu.inac.modelo;

import java.util.ArrayList;
import java.util.List;



/**
 * @author Gabriel
 *	Esta clase asocia al alumnos a una comision y a todas sus evaluaciones
 */
public class Alumno extends Persona{
	//
	Comision comision;
	List<Evaluacion> evaluaciones;
	
	public Alumno(	String pNombre	, String pApellido	, String pDireccion,String pTelefono, 
					String pDni		, String pEmail		, Comision pComision				) {
		
		super(pNombre,pApellido,pDireccion, pTelefono);
				
		comision = pComision;
	}
	
	public Alumno(	String pNombre	, String pApellido	, String pDireccion,String pTelefono, 
			String pDni		, String pEmail		, Comision pComision	, Partido pPar, Provincia pProv			) {

		super(pNombre, pApellido, pDireccion, pTelefono, pDni, pEmail, pPar, pProv);		
		comision = pComision;
}
	
	public Alumno() {
	}

	public Alumno(int pCodigo, String pNombre, String pApellido) {
		super(pCodigo, pNombre, pApellido);
	}

	/**
	 * Este m�todo permite agregar evaluaciones a la lista de evaluaciones exisntes
	 * @param pEval, es el  parametro por el cual se agrega una evaluacion
	 */
	public void addEvaluaciones(Evaluacion pEval){
		if(evaluaciones==null)
			evaluaciones = new ArrayList<Evaluacion>()				;
		evaluaciones.add(pEval)										;	}
	
	public Comision getComision() {					return comision;	}
	public void setComision(Comision pCom) {		comision = pCom;	}
	
	public void vaciar() {
		super.setApellido(null)		;
		super.setNombre(null)		;
		super.setDireccion(null)	;
		super.setDni(null)			;
		super.setEmail(null)		;
		super.setTelefono(null)		;
		super.setPartido(null)		;
		super.setProvincia(null)    ;
		super.setCodigo(0)			;
		comision 		= null		;
		evaluaciones 	= null		;
	}
	
	public boolean isVacio() {
		return  super.isVacio() && (super.getCodigo() == 0 && (super.getNombre()== null || super.getNombre().isEmpty()) && (super.getApellido() == null || 
				super.getApellido().isEmpty()) && (super.getDireccion() == null || super.getDireccion().isEmpty()) && (super.getTelefono() == null || 
				super.getTelefono().isEmpty()) && (super.getDni() == null || super.getDni().isEmpty()) && (super.getEmail() == null || 
				super.getEmail().isEmpty()) && super.getPartido() == null && super.getProvincia() == null ) && (comision == null || 
				(comision.isVacio()) && (evaluaciones == null || evaluaciones.isEmpty())																);}
	
	public String toString() {
		StringBuffer sb = new StringBuffer("codigo=")			   ;
		sb.append(super.getCodigo())							   ;
		sb.append(" nombre=")									   ;
		sb.append(super.getNombre())							   ;
		sb.append(" apellido=")									   ;
		sb.append(super.getApellido())							   ;
		sb.append(" direccion=")								   ;
		sb.append(super.getDireccion())							   ;
		sb.append(" telefono=")									   ;
		sb.append(super.getTelefono())							   ;
		sb.append(" dni=")									   	   ;
		sb.append(super.getDni())								   ;
		sb.append(" email=")									   ;
		sb.append(super.getEmail())								   ;
		sb.append(" partido=")									   ;
		sb.append(super.getPartido())							   ;
		sb.append(" provincia=")								   ;
		sb.append(super.getProvincia())							   ;
		sb.append(" comision=")									   ;
		sb.append(comision)										   ;
		sb.append(" evaluaciones=")								   ;
		sb.append(evaluaciones)									   ;
		
		
												
		return sb.toString()									   ;}

	public int hashCode() { return super.getCodigo()			   ;}

	public boolean equals(Object obj) {
		boolean bln=false										   ;
		Persona per=null										   ;
		if(obj instanceof Persona)
			per = (Persona)obj									   ;
			bln = super.getCodigo() == (per.getCodigo())		   ;
		return bln												   ;}


}
