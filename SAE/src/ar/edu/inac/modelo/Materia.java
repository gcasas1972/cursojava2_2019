package ar.edu.inac.modelo;

import java.util.List;

public class Materia implements Vaciable, Model {

	//Atributos
	private int codigo ;
	private String nombre ;
	private Temario temario ;
	private List <Evaluacion> evaluaciones ; 
	private List <Modulo> modulos ;
	private Curso curso;
	
	//Constructores
	public Materia(){ }
	public Materia(int mCod, String mNom, Temario mTem ){
			codigo  = mCod																		   ;
			nombre  = mNom                                                                         ;
			temario = mTem                                                                         ;}
	public Materia(int pCod, String pNomMateria, Curso pCurso, Temario pTemario) {
		codigo = pCod		;
		nombre = pNomMateria;
		curso  = pCurso		;
		temario= pTemario	;
		
	}
	//getter y setter
	public int getCodigo(){ return codigo;                     										}
	public void setCodigo(int codigo){ this.codigo = codigo;              							}
	
	public String getNombre(){ return nombre;                     									}
	public void setNombre(String nombre){ this.nombre = nombre;            						    }
	
	public Temario getTemario(){ return temario;                    								}
	public void setTemario(Temario temario){ this.temario = temario;   					            }
	
	public List<Evaluacion> getEvaluaciones(){ return evaluaciones;               					}
	public void addEvaluacion(Evaluacion evalua){ evaluaciones.add(evalua);          				}
	
	public Curso getCurso(){return curso															;}
	public void setCurso(Curso pCur){curso = pCur													;}
	
    public void vaciar(){
    		 codigo = 0																			   ;
    		 nombre = null																	       ;
    		 temario = null																	       ;}
	public boolean isVacio(){ return (codigo ==0 && nombre == "" && temario == null )			   ;}
	
	public List<Modulo> getModulos(){ return modulos											   ;}
	public void addModulo(Modulo mod){ modulos.add(mod)											   ;}
	
	public int hashCode() {return nombre.hashCode()												   ;}
	
	public boolean equals(Object obj) {
		boolean bln=false										   								   ;
		Materia mat=null											  							   ;
		if(obj instanceof Materia)
			mat = (Materia)obj									  								   ;
			bln = nombre.equals (mat.getNombre())					  							   ;
		return bln												   								   ;}

	
	public String toString() {
		StringBuffer sb = new StringBuffer("codigo=")			  								   ;
		sb.append(codigo)										    							   ;
		sb.append(" materia=")									   								   ;
		sb.append(nombre)											   							   ;
		sb.append(" temas=")									   								   ;
		sb.append(temario)											   							   ;
												
		return sb.toString()																	   ;}
	
	
}
