package ar.edu.inac.modelo;

public class ItemDeTemario implements Vaciable, Model {
	/**
	 * @author Belen
	 * Fecha : 28/03/2019
	 * Esta clase corresponde al item del temario que est� utilizando el programa.
	 */
	//atributos
	private int codigo			;
	private String descripcion	;
	private Temario temario		;
	
	//constructores
	public ItemDeTemario(){}
	public ItemDeTemario(int pCod){		codigo = pCod;	}
	public ItemDeTemario(int pCod,String pDes){
		codigo = pCod;
		descripcion = pDes;
	}
	public ItemDeTemario(String pNom){descripcion = pNom;}
	public ItemDeTemario(String pNom, Temario tem){descripcion=pNom; temario=tem;}
	public ItemDeTemario(int pCod, String pNom, Temario tem){codigo=pCod; descripcion=pNom; temario=tem;}
	public ItemDeTemario(int itemCod, String itemNom, int temCod, String temNom){
		codigo = itemCod;
		descripcion = itemNom;
		Temario temario = new Temario(temCod,temNom);
	}
	
	//getters y setters

	public int 		getCodigo		()					{return codigo						;} 
	public void 	setCodigo		(int codigo)		{this.codigo 		= codigo		;}
	public String 	getDescripcion	()					{return descripcion					;} 
	public void 	setDescripcion	(String descripcion){this.descripcion 	= descripcion	;}
	public Temario 	getTemario		() 					{return temario						;}
	public void 	setTemario		(Temario temario) 	{this.temario 		= temario		;}
	
	//de negocio
	
	public int hashCode() {return descripcion.hashCode()	;}
	
	public boolean equals(Object obj){
		//TODO Belen 
		boolean bln=false									;
		ItemDeTemario item=null								;
		if(obj instanceof Partido)
			item = (ItemDeTemario)obj						;
			bln = descripcion.equals(item.getDescripcion())	;
		return bln											;
	}
	
	public String toString(){
		StringBuffer sb = new StringBuffer("codigo=")		;
		sb.append(codigo)									;
		sb.append(" descripcion=")							;
		sb.append(descripcion)								;
		sb.append(" temario=")								;
		sb.append(temario)									;
		return sb.toString()								;}
	
	//metodos de la interfaz
	
	public void vaciar() {
		codigo = 0			;
		descripcion = null	;
		temario = null		;}
	
	public boolean isVacio() {
		return ((descripcion==null || descripcion.isEmpty()) 
				&& codigo==0 && temario==null);	
	}		
}
