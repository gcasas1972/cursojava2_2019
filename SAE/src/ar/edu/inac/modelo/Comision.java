package ar.edu.inac.modelo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lautaro
 * Fecha: 31/08/2018
 * Descripcion: clase que corresponde a la comisi�n.
 */

public class Comision implements Vaciable, Model{
	//Atributos
	private int codigo;
	private Curso curso;
	//mod by gcasas
	private Materia materia;
	private String turno;
	private String division;
	private List<Alumno> alumnos;
	private Profesor profesor;
	
	//M�todos
	
	//From Vaciable
	public void vaciar() {	
		codigo = 0;
		curso = null;
		turno = null;
		division = null;
		alumnos = null;
	}
	
	public boolean isVacio() {
		return		codigo>0 && curso!=null && !turno.isEmpty() && 
					!division.isEmpty() && alumnos!=null &&turno!=null
					&& division!=null 													;}
	
	//Getter y setters
	public int getCodigo() {return codigo												;}
	public void setCodigo(int codigo) {this.codigo = codigo								;}

	public Curso getCurso() {return curso												;}
	public void setCurso(Curso curso) {this.curso = curso								;}

	//mod by gcasas
	
	public String getTurno() {return turno												;}
	
	public Materia getMateria() {		return materia									;}
	public void setMateria(Materia pMateria) {materia = pMateria						;}

	public void setTurno(String turno) {this.turno = turno								;}
	
	public String getDivision() {return division										;}
	public void setDivision(String division) {this.division = division					;}

	public List<Alumno> getAlumnos() {return alumnos									;}
	public void addAlumno(Alumno alu){
		if(alumnos==null)
			alumnos=new ArrayList<Alumno>();
		alumnos.add(alu)									
		;}
	
	public Profesor getProfesor(){return profesor;}
	public void setProfesor(Profesor pProf){profesor = pProf;}
	
	public boolean equals(Object obj){
		boolean bln = false;
		if(obj instanceof Comision){
			Comision co = (Comision)obj;
			bln = 	super.equals(obj) && 
					codigo==co.getCodigo();
		}
		return bln;}
	
	public int hashCode(){return super.hashCode() + codigo;}
	
	public String toString(){
		StringBuffer sb = new StringBuffer("codigo=");
		sb.append(codigo);
		sb.append(", curso=");
		sb.append(curso.getCodigo());
		sb.append(", turno=");
		sb.append(turno);
		sb.append(", division=");
		sb.append(division);
		sb.append(alumnos.toString());
		return sb.toString();}
	
	//Constructores
	public Comision(){}
	public Comision(int pCod, Curso pCur, String pTur, String pDiv, List<Alumno> pAlu, Profesor pProf){
		codigo = pCod;
		curso = pCur;
		turno = pTur;
		division = pDiv;
		alumnos = pAlu;
		profesor = pProf;
	}

	public Comision(int pCod, String pTur, String pDiv) {
		codigo 	= pCod;
		turno 	= pTur;
		division= pDiv;
	}
	
	//Constructor creado para agilizar testeo
	public Comision(String pDiv){
		division = pDiv;
	}
	
	//Constructor creado para agilizar testeo
	public Comision(int pCod){
		codigo = pCod;
	}

	public Comision(String pTurno, String pDivision, Profesor pProf) {
		turno	= pTurno	;
		division= pDivision	;
		profesor= pProf		;
	}

	public Comision(int pCodigo, Curso pCurso, Materia pMateria, String pTurno,
			String pDivision, List<Alumno> pAlumnos, Profesor pProfesor) {
		super();
		codigo 	= pCodigo	;
		curso 	= pCurso	;
		materia = pMateria	;	
		turno 	= pTurno	;
		division= pDivision	;
		alumnos = pAlumnos	;
		profesor= pProfesor	;
	}
	
}
