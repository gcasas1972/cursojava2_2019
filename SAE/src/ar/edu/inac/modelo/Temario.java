package ar.edu.inac.modelo;

import java.util.ArrayList;
import java.util.List;
/**
 * @author Ivan
 * Fecha : 31/08/2018
 * Esta clase corresponde al temario que est� utilizando el programa.
 */
public class Temario implements Vaciable, Model{
	//atributos

	private int codigo				;
	private String descripcion		;
	private List<ItemDeTemario>items;
	
	//costructores
	
	public Temario(){}
	public Temario(String nombre)			{descripcion 	= nombre;}
	public Temario(int pCod, String pNom)	{codigo			=	pCod;
											 descripcion	=	pNom;}
	public Temario(int pCod) 				{codigo 		= pCod	;}
	
	
	//getters y setters

	public int getCodigo		()					{return codigo					;} 
	public void setCodigo		(int codigo)		{this.codigo = codigo			;}
	public String getDescripcion()					{return descripcion				;} 
	public void setDescripcion	(String descripcion){this.descripcion = descripcion	;}
	public List<ItemDeTemario> 	getItems() 			{return items					;}
	

	public void addItem	(ItemDeTemario pItem){
		/**
		 * Este es el modelo para agregar un item, primero verifico que si la lista esta en null
		 * si es asi creo un array, caso contrario al arrayList que existe le agrego un item
		 * @param pItem, corresponde al item a ser agregado
		 */
		if(items==null)
			items = new ArrayList<ItemDeTemario>();
		items.add(pItem);
		
	}
	
	//M�todos polimorfos
	
	public boolean equals(Object obj){
		boolean bln=false									;
		Temario tem = null									;
		if(obj instanceof Temario)
			tem = (Temario)obj								;
			bln = descripcion.equals(tem.getDescripcion())	;
		return bln											;}
	
	public int hashCode(){
		return descripcion.hashCode()						;}
	
	public String toString(){
		StringBuffer sb = new StringBuffer("temario=")		;
		sb.append(codigo)									;
		sb.append(" descripcion=")							;
		sb.append(descripcion)								;
		sb.append(" items=")								;
		sb.append(items)									;
		return sb.toString()								;}
	
	//metodos de la interfaz
	
	public void 	vaciar(){
		codigo = 0			; 
		descripcion = null	; 
		items = null		;
	}
	
	public boolean isVacio(){
		return codigo<=0 && (descripcion==null || descripcion.isEmpty());
	}	
}
