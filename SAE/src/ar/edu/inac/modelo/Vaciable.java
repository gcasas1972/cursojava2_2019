package ar.edu.inac.modelo;

/**
 * @author Gabriel
 * fecha : 24/08/2018
 * descripcion:
 * 			permite que las clases implementen un metodo para vaciar 
 * su contenido y otro para determinar si el objeto est� vac�o
 *
 */
public interface Vaciable {
	public void vaciar();
	public boolean isVacio();

}
