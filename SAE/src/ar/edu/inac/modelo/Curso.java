package ar.edu.inac.modelo;

import java.util.List;

/**
 * @author Karen
 * Fecha : 31/08/2018
 * Esta clase corresponde al curso que est� utilizando el programa.
 * mod by gcasas agregado de atributo descripcin
 * se le agrego la interface model
 */

public class Curso implements Model{ 
	//atributos 
	private int 			codigo		;
	private int 			anio		;
	//mod by gcsaas
	private String 			descripcion	;
	private List<Materia> 	materias	;
	
	//Constructores
	public Curso(){ }
	public Curso(int cCod, int cAnio, String pDescripcion){
										codigo		= cCod				   	;
										anio		= cAnio				   	;
										descripcion	= pDescripcion			;	
	}
	
	public Curso(int pAnio, String pDescripcion) {
		anio 		= pAnio			;
		descripcion = pDescripcion	;
		
	}
	//getter y setter
	public int getCodigo(){ return codigo						   ;}
	public void setCodigo (int codigo ){this.codigo = codigo       ;}
	
	public int getAnio(){ return anio							   ;}
	public void setAnio(int anio ){	this.anio = anio 			   ;}

	public String getDescripcion() {			return descripcion;	}
	public void setDescripcion(String pDesc) {descripcion = pDesc;	}

	public List getMaterias(){	return materias					   ;}
	public void addMateria( Materia pMateria ){						
		//TODO Karen, fitate como se agrega una materia a la listas
	}
	
	//metodos de negocio
	public void vaciar(){	codigo	= 0;
						 	anio	= 0;
	}

	
	public boolean isVacio(){	
		return 	  (	codigo 	==0 && 
					anio 	==0 && 
				(descripcion==null || descripcion.isEmpty()))			 		       ;}

	
	

	
	public int hashCode(){ return codigo						   ;}

	public boolean equals(Object obj) {
		boolean bln=false										   ;
		Curso cur=null											   ;
		if(obj instanceof Curso)
			cur = (Curso)obj									   ;
			bln = codigo == (cur.getCodigo())					   ;
		return bln												   ;}

	
	public String toString() {
		StringBuffer sb = new StringBuffer("codigo=")			   	;
		sb.append(codigo)										   	;
		sb.append(",anio=")										   	;		
		sb.append(anio)											   	;
		sb.append(", descripcion=") 								;
		sb.append(descripcion)										;
												
		return sb.toString()									   ;}
	
}


