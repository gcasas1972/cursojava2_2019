package ar.edu.inac.modelo;

import java.util.List;
/**
 * @author Ezequiel
 * Fecha : 31/08/2018
 * Esta clase corresponde a la modulo que est� utilizando el programa.
 */
public class Modulo implements Vaciable, Model{
	//Atributos
	private int 				codigo		;
	private String 				descripcion	;
	//mod by gcass  12/12/2018
	private Materia				materia 	;
	private List<ItemDeTemario> items		;
	
	//M�todos
	
	//Constructores
	public Modulo(){}
	
	public Modulo(int pCod, String pNom, Materia pMat, List<ItemDeTemario> pItems) {
		codigo		= pCod	;
		descripcion	= pNom	;
		materia 	= pMat	;
		items 		= pItems;
	}
	
	public Modulo(int pCod, String pDesc) {
		codigo		= pCod	;
		descripcion	= pDesc	;
		
	}
	//Getters y Setters
	public int getCodigo() {return codigo												;}
	public void setCodigo(int codigo) {this.codigo = codigo								;}
	
	public String getDescripcion() {return descripcion									;}
	public void setDescripcion(String descripcion) {this.descripcion = descripcion		;}
		
	public Materia getMateria() {				return materia							;}
	public void setMateria(Materia pMat) {		materia = pMat							;}
	
	public List<ItemDeTemario> getItems() {return getItems()							;}
	public void setItems(List<ItemDeTemario> pItems) {items = pItems			;}
	
	//M�todos polimorfos
	public boolean equals(Object obj){
		boolean bln=false																;															;
		Modulo mod = null                                                               ;
		if(obj instanceof Modulo)
			mod = (Modulo)obj														    ;
			bln = descripcion.equals(mod.getDescripcion())								;
		return bln																		;}
	
	public int hashCode(){
		return descripcion.hashCode()													;}
	
	public String toString(){
		StringBuffer sb = new StringBuffer("codigo=")									;
		sb.append(codigo)																;
		sb.append(" descripcion=")														;
		sb.append(descripcion)															;
		sb.append(" modulo=")															;
		sb.append("modulo=")														    ;
		return sb.toString()															;}
	
	//De Interfaz
	public void vaciar() {
		codigo = 0;
		descripcion = null;
		items = null;}
	public boolean isVacio() {		
		return ( (descripcion==null || descripcion.isEmpty()) 	&& 
					codigo==0 									&& 
				(items==null || items.isEmpty()));											
		}
}
