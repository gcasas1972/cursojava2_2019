package ar.edu.inac.modelo;


public abstract class Persona implements Vaciable, Model{
	
	//atributos
	
	private int codigo							;
	private String nombre						; 
	private String apellido						;
	private String direccion					;
	
	private String telefono						;
	private String dni							;
	
	private String email						;
	private Partido partido						;
	private Provincia provincia					;

	
	//Constructores
	
	public Persona() {}
	
	public Persona(String pNombre, String pApellido, String pDireccion,String pTelefono, String pDni, String pEmail, Partido pPar, Provincia pProv){
		nombre = pNombre										;
		apellido = pApellido									;
		direccion = pDireccion									;
		telefono = pTelefono									;
		dni = pDni												;
		email = pEmail											;
		partido = pPar											;
		provincia = pProv										;
	}
	

	public Persona(int pCod			,String pNom			,String pApe	,String pDir, 
				   Partido pPart	,Provincia pProv		,String pTel	,String pDni, 
				   String pEmail) {
		super();
		codigo 		= pCod	;
		nombre 		= pNom	;
		apellido	= pApe	;
		direccion	= pDir	;
		partido		= pPart	;
		provincia	= pProv	;
		telefono	= pTel	;
		dni			= pDni	;
		email 		= pEmail;
	}
	public Persona(String pNom, String pApe, String pDir, String pTel) {
		super();
		this.nombre 	= pNom;
		this.apellido 	= pApe;
		this.direccion	= pDir;
		this.telefono	= pTel;		
	}
	
	//getter y setter


	public Persona(int pId, String pNom, String pApe) {
		codigo 	= pId	;
		nombre 	= pNom	;	
		apellido= pApe	;
	}

	public String getNombre() {			  			return nombre;						}
	public void setNombre(String nombre) {			this.nombre = nombre;				}
	
	public String getApellido() {					return apellido;				    }
	public void setApellido(String apellido) {		this.apellido = apellido;			}
	
	public String getDireccion() {					return direccion;					}
	public void setDireccion(String direccion) {	this.direccion = direccion;	 		}

	public String getTelefono() {				    return telefono;					}
	public void setTelefono(String telefono) { 		this.telefono = telefono;			}
	
	public String getDni() {						return dni;							}
	public void setDni(String dni) {				this.dni = dni;						}
	
	public String getEmail() {						return email;			    		}
	public void setEmail(String email) {			this.email = email;					}
	
	public int getCodigo() {						return codigo;						}
	public void setCodigo(int codigo) {				this.codigo = codigo;				}

	public Partido getPartido() {return partido;}
	public void setPartido(Partido partido) {this.partido = partido;}

	public Provincia getProvincia() {return provincia;}
	public void setProvincia(Provincia provincia) {this.provincia = provincia;}

	public void vaciar() {
							codigo 		= 0;    
							nombre 		= null; 
							apellido 	= null;											
							direccion	= null;
							telefono 	= null;						
							dni			= null;						
							email		= null;						
							partido		= null;					
							provincia 	= null;		
							
	}

	public boolean isVacio() {
		
		return (codigo == 0 && (nombre == null || nombre.isEmpty()) && (apellido == null || apellido.isEmpty()) && 
				(direccion == null || direccion.isEmpty()) && (telefono == null || telefono.isEmpty()) && (dni == null || dni.isEmpty()) && 
				(email == null || email.isEmpty()) && partido == null && provincia == null );}
	
	public String toString() {
		StringBuffer sb = new StringBuffer("codigo=")			   ;
		sb.append(codigo)										   ;
		sb.append(" nombre=")									   ;
		sb.append(nombre)										   ;
		sb.append(" apellido=")									   ;
		sb.append(apellido)										   ;
		sb.append(" direccion=")								   ;
		sb.append(direccion)									   ;
		sb.append(" telefono=")									   ;
		sb.append(telefono)										   ;
		sb.append(" dni=")									   	   ;
		sb.append(dni)											   ;
		sb.append(" email=")									   ;
		sb.append(email)										   ;
		sb.append(" partido=")									   ;
		sb.append(partido)										   ;
		sb.append(" provincia=")								   ;
		sb.append(provincia)									   ;
												
		return sb.toString()									   ;}

	public int hashCode() { return codigo						   ;}

	public boolean equals(Object obj) {
		boolean bln=false										   ;
		Persona per=null										   ;
		if(obj instanceof Persona)
			per = (Persona)obj									   ;
			bln = codigo == (per.getCodigo())					   ;
		return bln												   ;}
		
}




	
	
