package ar.edu.inac.modelo;

public class TipoDePregunta implements Vaciable, Model {
	private int codigo;
	private String descripcion;
	
	// constructores 
	public TipoDePregunta(){}
	public TipoDePregunta(int pCod,String pDescripcion){
									codigo=				pCod				;
									descripcion=		pDescripcion		;
						
	}
	
	//getter y setter
	public int getCodigo() {return codigo;
	}
	public void setCodigo(int codigo) {this.codigo = codigo;
	}
	public String getDescripcion() {return descripcion;
	}
	public void setDescripcion(String descripcion) {this.descripcion = descripcion;
	}
	// TODO GUSTAVO vaciar todos los atributos, los enteros a 0 y los objetos a null
	@Override
	public void vaciar() {  
							codigo		=	0			;
							descripcion	=	null		;
	}
	// TODO GUSTAVO verificar si estan todos los atributos vacios

	@Override
	public boolean isVacio() {
		return 
		(codigo == 0 && 
			      (descripcion == null || descripcion.isEmpty()));
	}
// hash code
public int hashCode(){ return codigo ;      }
	
	public boolean equals(Object obj) {
		boolean bln=false										   ;
		TipoDePregunta tpd=null											   ;
		if(obj instanceof TipoDePregunta)
			tpd = (TipoDePregunta)obj									   ;
			bln = codigo == (tpd.getCodigo())					   ;
		return bln												   ;}
	
	
	
	
	// TOSTRING
	public String toString() {
		StringBuffer sb = new StringBuffer("codigo=")			   	;
		sb.append(codigo)										   	;
		sb.append(", descripcion=") 								;
		sb.append(descripcion)										;
												
		return sb.toString()									   ;}
}
