package ar.edu.inac.modelo;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Nazareno
 * Fecha : 21/03/2019
 */

public class Perfil implements Vaciable, Model {
	
	private int codigo;
	private String descripcion;
	private List<Pantalla> pantallas;
	
	public Perfil(){ }
	public Perfil(int pCod, String pDes){
		codigo = pCod;
		descripcion	= pDes;}
	public Perfil (String pDes2){
		descripcion	= pDes2;
	}
	public Perfil (int pCod){
		codigo = pCod;
	}
	
	public int getCodigo(){
		return codigo;}
	public void setCodigo(int pCod ){
		codigo = pCod;}

	public String getDescripcion() {			
		return descripcion;}
	public void setDescripcion(String pDes){
		descripcion = pDes;}

	public List<Pantalla> getPantalla(){	
		return pantallas;}
	public void setPantallas(List<Pantalla> pPan) {
		pantallas = pPan;
	}
	
	public void addPantalla (Pantalla pPantallas){
		if(pantallas==null){
		pantallas = new ArrayList<Pantalla>();}
		pantallas.add (pPantallas);
	}
	
	public int hashCode(){
		return codigo;}
	
	public boolean equals(Object obj) {
		boolean bln=false;
		if(obj instanceof Perfil){
			Perfil per = (Perfil)obj;
			bln = codigo == (per.getCodigo());}	
		return bln;}
	
	public String toString(){
		StringBuffer sb = new StringBuffer ("Codigo=");
		sb.append(codigo);
		sb.append(", Descripcion=");
		sb.append(descripcion);
		return sb.toString();}
	@Override
	public void vaciar() {
		codigo = 0;
		descripcion = null;
		pantallas = null;
	}	
	@Override
	public boolean isVacio() {
		return codigo<=0 && (descripcion==null || descripcion.isEmpty());
	}
}
