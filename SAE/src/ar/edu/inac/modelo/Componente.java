package ar.edu.inac.modelo;

/**
 * Autor: Diego Torres fecha: 28/03/2019 esta clase corresponde al Componente
 * que esta utilizando el programa.
 * 
 */
public class Componente implements Vaciable, Model {
	// atributos.
	private int codigo;
	private String descripcion;
	private boolean visible;
	private boolean editable;
	private Pantalla pantalla;
	private String nombre;
	 //SE AGREGA NOMBRE
	
	//TODO VANINA, agregar en la tabla comp_nombre y agregar el atributo
	// nombre:String getter / setter y el metodo toString
	

	// metodos.

	

	// constructores.
	public Componente(int codigo, String descripcion,
			boolean visible, boolean editable, Pantalla pantalla) {
		
		super();
		this.codigo = codigo;
		this.descripcion = descripcion;
		this.visible = visible;
		this.editable = editable;
		this.pantalla = pantalla;
	}
	public Componente() {
	
	
	}
	
	public Componente(String pDes){
		descripcion = pDes;
	}
	public Componente(int cCod){
		codigo = cCod;
	}
	public Componente(int cCod, String cDes){
		codigo = cCod;
		descripcion = cDes;
	}	
	
	public Componente(Pantalla pan, String pDescripcion) {
		pantalla 	= pan;
		descripcion = pDescripcion;
	}
    
	//SE CREA EL CONSTRUCTOR
	public Componente(int pCodigo, String pDescripcion, boolean pVisible,
			boolean pEditable, Pantalla pPantalla, String pNombre) {
		super();
		this.codigo = pCodigo;
		this.descripcion = pDescripcion;
		this.visible = pVisible;
		this.editable = pEditable;
		this.pantalla = pPantalla;
		this.nombre = pNombre;
	}
	// gettes y setter.
	
	
	public Pantalla getPantalla() {						return pantalla					;	}

	public void setPantalla(Pantalla pantalla) {		this.pantalla = pantalla		;	}
	
	public int getCodigo() {							return codigo					;	}

	public void setCodigo(int codigo) {					this.codigo = codigo			;	}

	public String getDescripcion() {					return descripcion				;	}

	public void setDescripcion(String descripcion) {	this.descripcion = descripcion	;	}

	public boolean isVisible() {						return visible					;	}

	public void setVisible(boolean visible) {			this.visible = visible			;	}

	public boolean isEditable() {						return editable					;	}

	public void setEditable(boolean editable) {			this.editable = editable		;	}

	@Override
	public int hashCode() {								return codigo					;	}
	@Override
	
	//SE COMPLETA EL VACIAR
	public void vaciar() {
		//TODO VANINA tu socio anterior fue un vago
		this.codigo = 0	;
		this.descripcion = null	;
		this.visible = false 	;
		this.editable = false	;
		this.pantalla = null	;
		this.nombre= null		;
	
	}
	@Override
	public boolean isVacio() {
		
		//return  super.isVacio() && (super.getCodigo() == 0 && (super.getNombre()== null || super.getNombre().isEmpty()) && (super.getApellido() == null || 
			//	super.getApellido().isEmpty()) && (super.getDireccion() == null || super.getDireccion().isEmpty()) && (super.getTelefono() == null || 
				//super.getTelefono().isEmpty()) && (super.getDni() == null || super.getDni().isEmpty()) && (super.getEmail() == null || 
			//	super.getEmail().isEmpty()) && super.getPartido() == null && super.getProvincia() == null ) && (comision == null || 
				//(comision.isVacio()) && (evaluaciones == null || evaluaciones.isEmpty())
				
	//}
		

		//TODO VANINA completa lo que haga falta
		return false					;	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Componente other = (Componente) obj;
		if (codigo != other.codigo)
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("codigo = ");
		sb.append(codigo);
		sb.append(", descripcion = ");
		sb.append(descripcion);

		return sb.toString();
	}

	

}