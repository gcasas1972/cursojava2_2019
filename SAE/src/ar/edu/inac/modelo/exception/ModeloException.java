package ar.edu.inac.modelo.exception;

public class ModeloException extends Exception{
	private String solucion;
	public ModeloException(String pError, String pSolucion) {
		super(pError);
		solucion = pSolucion;
		
	}
	
	public String getSolucion(){		return solucion;	}

}
